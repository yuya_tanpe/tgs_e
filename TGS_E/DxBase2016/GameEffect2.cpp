#include "stdafx.h"
#include "Project.h"

namespace basedx11{
	//--------------------------------------------------------------------------------------
	//class CoinEffect : public MultiParticle;
	//用途: コインを取った時のエフェクト
	//--------------------------------------------------------------------------------------
	//構築と破棄
	CoinEffect::CoinEffect(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	CoinEffect::~CoinEffect(){}
	//TODO : ちょっとみずらいかもしれない
	void CoinEffect::InsertCoinEffect(const Vector3& Pos){
		auto ParticlePtr = InsertParticle(1);
		ParticlePtr->SetEmitterPos(Pos);
		ParticlePtr->SetTextureResource(L"SPARK_TX");
		SetDrawLayer(1);
		ParticlePtr->SetDrawOption(Particle::DrawOption::Normal);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
			//パーティクルのサイズ
			rParticleSprite.m_LocalScale = Vector2(1.0f, 0.4f);
			rParticleSprite.m_LocalPos.x = 0.0f;
			rParticleSprite.m_LocalPos.y = 0.0f;
			rParticleSprite.m_LocalPos.z = 0.0f;

			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 0.0f,
				rParticleSprite.m_LocalPos.y * 0.0f,
				rParticleSprite.m_LocalPos.z * 0.0f
				);
			//色の指定
			rParticleSprite.m_Color = Color4(1.0f, 1.0f, 0.0f, 1.0f);
		}
	}

	//変化
	void CoinEffect::OnUpdate(){
		float fTimeSpan = App::GetApp()->GetElapsedTime();

		for (auto ParticlePtr : GetParticleVec())
		{
			ParticlePtr->AddTotalTime(fTimeSpan);
			for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec())
			{
				if (rParticleSprite.m_Active)
				{
					rParticleSprite.m_LocalQt.AddRotation(Vector3(1.0f, 2.0f, 0.0f));
					if (ParticlePtr->GetTotalTime() > 0.2f) {
						rParticleSprite.m_Active = false;
					}
				}
			}
		}
	}

	//--------------------------------------------------------------------------------------
	//class NonMove_HitEffect : public MultiParticle;
	//用途: 複数の煙クラス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	NonMove_HitEffect::NonMove_HitEffect(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	NonMove_HitEffect::~NonMove_HitEffect(){}

	void NonMove_HitEffect::InsertNonMoveEffect(const Vector3& Pos){
		auto ParticlePtr = InsertParticle(30);
		ParticlePtr->SetEmitterPos(Pos);
		ParticlePtr->SetTextureResource(L"SPARK_TX");
		ParticlePtr->SetMaxTime(0.5f);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
			//パーティクルのサイズ
			rParticleSprite.m_LocalScale = Vector2(0.1f, 0.1f);
			rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 0.1f - 0.05f;
			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 0.1f - 0.05f;
			rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 0.1f - 0.05f;

			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 20.0f,
				rParticleSprite.m_LocalPos.y * 20.0f,
				rParticleSprite.m_LocalPos.z * 20.0f
				);
			//色の指定
			rParticleSprite.m_Color = Color4(1.0f, 0.0f, 0.0f, 1.0f);
		}
	}

	//変化
	void NonMove_HitEffect::OnUpdate(){
		float fTimeSpan = App::GetApp()->GetElapsedTime();

		for (auto ParticlePtr : GetParticleVec())
		{
			ParticlePtr->AddTotalTime(fTimeSpan);
			for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec())
			{
				if (rParticleSprite.m_Active)
				{
					rParticleSprite.m_LocalPos += rParticleSprite.m_Velocity  * fTimeSpan;
					rParticleSprite.m_Color += Color4(0.00f, 0.009f, 0.00f, -0.03f);
					
					if (ParticlePtr->GetTotalTime() > 0.5f) {
						rParticleSprite.m_Active = false;
					}
				}
			}
		}
	}



}
//endof  basedx11