#pragma once
#include "stdafx.h"

namespace basedx11{
	/**
	* @class Coin_S
	* @brief 取った時に演出ありのオブジェクト
	* @author　sike yuya
	* @date	2016/06/08
	*/
	class Coin_S : public GameObject{
		shared_ptr< StateMachine<Coin_S> >  m_StateMachine;	//ステートマシーン
		//当たった後の経過時間
		float m_HitTimer;
		//コインマネージャーに1枚追加する
		void Coin_1_Plus();

	public:
		Coin_S(const shared_ptr<Stage>& StagePtr);
		virtual ~Coin_S(){}

		virtual void OnCreate() override;
		virtual void OnUpdate() override;
		virtual void OnEvent(const shared_ptr<Event>& event)override;
		//リフレッシュ関数
		void Refresh();
		//アクセサ
		shared_ptr< StateMachine<Coin_S> > GetStateMachine() const{
			return m_StateMachine;
		}
		
		

		//ドキュメントから呼ばれる関数
		//プレイヤーとの衝突したときに呼ぶ
		void GoHitMotion();
		//このオブジェクトがリフレッシュステートかの取得
		bool IsRefreshAbled()const;
		
		//当たった時のアクション
		void HitAction();
		//-----------------------------------------
		//---------NoHitStateで使用Motion----------
		//-----------------------------------------
		//最初のステートでの動き
		void ExecFirstMotion();
		//-----------------------------------------
		//---------HitStateで使用Motion----------
		//-----------------------------------------
		void StartHitMotion();
		bool ExecHitMotion();
		void EndHitMotion();




	};

	//--------------------------------------------------------------------------------------
	//	class NoHitState : public ObjState<Coin_S>;
	//	用途: 通常移動
	//--------------------------------------------------------------------------------------
	class NoHitState : public ObjState<Coin_S>
	{
		NoHitState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<NoHitState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Coin_S>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Coin_S>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Coin_S>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class HitState : public ObjState<Coin_S>;
	//	用途: 何かに当たった時
	//--------------------------------------------------------------------------------------
	class HitState : public ObjState<Coin_S>
	{
		HitState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<HitState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Coin_S>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Coin_S>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Coin_S>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class RefreshState : public ObjState<Coin_S>;
	//	用途: リフレッシュ待ちステート
	//--------------------------------------------------------------------------------------
	class RefreshState : public ObjState<Coin_S>
	{
		RefreshState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<RefreshState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Coin_S>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Coin_S>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Coin_S>& Obj)override;
	};

	/**
	* @class Fly_Item
	* @brief 取るとボーナススコアが上がるアイテム 浮いてるバージョン
	* @author　sike yuya
	* @date	2016/06/08
	*/
	class Fly_Item : public GameObject {
		//オブジェクトを消してエフェクトを出し、音を出す
		void EndObject();
	public:
		//構築と破棄
		Fly_Item(const shared_ptr<Stage>& StagePtr);
		virtual ~Fly_Item(){}
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate() override;
		virtual void OnEvent(const shared_ptr<Event>& event)override;
	};
}
//endof  basedx11
