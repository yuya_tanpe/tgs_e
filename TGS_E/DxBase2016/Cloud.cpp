#include "stdafx.h"
#include "Project.h"

namespace basedx11{
	/**
	* @class Cloud
	* @brief 雲のオブジェクトの基底クラス
	* @author　sike yuya
	* @date	New 4/30
	*/

	Cloud::Cloud(const shared_ptr<Stage>& Stageptr, const CloudName& Name) :
		GameObject(Stageptr),
		m_Tagname(Name)
	{}

	void Cloud::OnCreate(){
		//自分は描画しない
		SetDrawActive(false);

		//Transformの設定
		auto PtrTrans = AddComponent<Transform>();
		PtrTrans->SetPosition(0, 0, 0);
		PtrTrans->SetRotation(0.0f, 0.0f, 0.0f);
		PtrTrans->SetScale(2.0f, 1.4f, 2.0f);

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(0.1f, 0.2f, 0.1f),
			Vector3(0.0f, XM_PI, 0.0f),
			Vector3(0.0f, 0.0f, 0.0f)
			);

		//描画コンポーネントの設定	雲専用のShader使用
		auto PtrDraw = AddComponent<PNTStaticModelCloudDraw>();
		//描画するメッシュを設定
		PtrDraw->SetMeshResource(Select_Meshresorce(m_Tagname));
		PtrDraw->SetMeshToTransformMatrix(SpanMat);
		PtrDraw->SetOwnShadowActive(false);

		//透過処理
		SetAlphaActive(true);

		////アクションの登録
		//auto PtrAction = AddComponent<Action>();
		//PtrAction->AddScaleTo(2.0f, Vector3(1.84f, 1.25f, 1.84f));
		//PtrAction->AddScaleTo(2.0f, Vector3(2.3f, 1.5f, 2.3f));
		////ループする
		//PtrAction->SetLooped(true);
		////アクション開始
		//PtrAction->Run();
	}

	void Cloud::OnEvent(const shared_ptr<Event>& event)
	{
		if (event->m_MsgStr == L"OBJ_ACTIVE")
		{
		}

		if (event->m_MsgStr == L"OBJ_STOP")
		{
		}
	}

	//Tagnameによってメッシュを変える
	wstring Cloud::Select_Meshresorce(const CloudName& name){
		switch (name)
		{
		case CloudName::None:
			return L"WHITECLOUD_MESH";
			break;
		case CloudName::White:
			return L"WHITECLOUD_MESH";
			break;
		case CloudName::Thunder:
			return L"KAMINARI_2_MESH";
			break;
		default:
			assert(!"不正な値の検出 Cloud.cppの確認してください");
			return L"";
			break;
		}
	}

	/**
	* @class Tutorial_cloud
	* @brief Cloud継承のオブジェクト　白い雲
	* @author　sike yuya
	* @date	New 5/26
	*/

	Tutorial_cloud::Tutorial_cloud(const shared_ptr<Stage>& Stageptr, const Vector3& i_pos, bool TF) :
		GameObject(Stageptr),
		m_Pos(i_pos),
		m_draw(TF)
	{}

	void Tutorial_cloud::OnCreate(){
		//Transformの設定
		auto PtrTrans = AddComponent<Transform>();
		PtrTrans->SetPosition(m_Pos);
		PtrTrans->SetRotation(0.0f, 0.0f, 0.0f);
		PtrTrans->SetScale(1.9f, 1.4f, 1.9f);

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(0.1f, 0.2f, 0.1f),
			Vector3(0.0f, XM_PI, 0.0f),
			Vector3(0.0f, -0.4f, 0.0f)
			);

		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<PNTStaticModelCloudDraw>();
		//描画するメッシュを設定
		PtrDraw->SetMeshResource(L"WHITECLOUD_MESH");
		PtrDraw->SetMeshToTransformMatrix(SpanMat);
		PtrDraw->SetOwnShadowActive(false);
		PtrDraw->SetDrawActive(m_draw);

		//透過処理
		SetAlphaActive(true);
		SetDrawLayer(0);

		//アクションの登録
		//TODO : Actionの登録は、全部Sky_Fallコンポーネントに任せる形にする
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddScaleTo(2.0f, Vector3(1.8f, 1.25f, 1.8f));
		PtrAction->AddScaleTo(2.0f, Vector3(2.4f, 1.5f, 2.4f));
		//ループする
		PtrAction->SetLooped(true);
		//アクション開始
		PtrAction->Run();
	}

	//変化
	void Tutorial_cloud::OnUpdate(){
	}
}
//endof  basedx11