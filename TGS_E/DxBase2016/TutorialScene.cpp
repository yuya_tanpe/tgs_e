#include "stdafx.h"
#include "Project.h"

namespace basedx11
{
	//構築と破棄
	TutorialScene::TutorialScene() :Stage(),
		m_Rowsize(6),			//ステージ縦の大きさ
		m_Colsize(5),			//ステージ横の大きさ
		m_IsPushButton(false),
		m_PlayNum(0)
	{
		//配列の要素数確定
		vector<CloudName> TempSub(m_Colsize, CloudName::None);
		m_CloudPatternVec.assign(m_Rowsize, TempSub);

		vector<weak_ptr<GameObject>> TempWeakSub(m_Colsize);
		m_CloudObjectVec.assign(m_Rowsize, TempWeakSub);

		//チュートリアルパターンの作成
		m_TutorialPatternVec = {
			{ CloudName::None, CloudName::White, CloudName::White, CloudName::White, CloudName::None },
			{ CloudName::None, CloudName::White, CloudName::White, CloudName::White, CloudName::None },
			{ CloudName::White, CloudName::White, CloudName::White, CloudName::White, CloudName::White },
			{ CloudName::White, CloudName::White, CloudName::White, CloudName::White, CloudName::White },
			{ CloudName::White, CloudName::White, CloudName::White, CloudName::White, CloudName::White },
			{ CloudName::None, CloudName::White, CloudName::White, CloudName::White, CloudName::None }
		};
	}

	//Easyパターンで作成
	void TutorialScene::PatternToVec(vector<vector<CloudName>>& DifficultyVec){
		for (size_t Row = 0; Row < m_Rowsize; Row++){
			SetCloudVec(Row, DifficultyVec, Row);
		}
	}

	void TutorialScene::CreateResourses()
	{
		//仮背景のテクスチャ
		wstring strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStageUI\\watasi1.png";
		App::GetApp()->RegisterTexture(L"WATER_TX", strTexture);
		//仮背景のテクスチャ
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"TutorialUI\\Controller.png";
		App::GetApp()->RegisterTexture(L"CONTROLLER_TX", strTexture);
		//仮背景のテクスチャ
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"TutorialUI\\Frame.png";
		App::GetApp()->RegisterTexture(L"FRAME_TX", strTexture);
		//仮背景のテクスチャ
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"TutorialUI\\HowToPlay.png";
		App::GetApp()->RegisterTexture(L"HOWTOPLAY_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"TutorialUI\\Controller_2.png";
		App::GetApp()->RegisterTexture(L"L_STICK_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"TutorialUI\\Controller_3.png";
		App::GetApp()->RegisterTexture(L"R_BUTTON_TX", strTexture);
		// ボタンのテクスチャ
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"TutorialUI\\Word_1.png";
		App::GetApp()->RegisterTexture(L"WORD_1_TX", strTexture);
		// ボタンのテクスチャ
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"TutorialUI\\Word_2.png";
		App::GetApp()->RegisterTexture(L"WORD_2_TX", strTexture);

		wstring MoveWav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\MoveRL.wav";
		App::GetApp()->RegisterWav(L"MOVERL", MoveWav);
		/*wstring BGM = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\TutorialBGM.wav";
		App::GetApp()->RegisterWav(L"TUTORIALBGM", BGM);*/
		

		App::GetApp()->RegisterStaticModelMesh(L"WHITECLOUD_MESH", App::GetApp()->m_wstrRelativeDataPath, L"kumo_ver2.bmf");
		App::GetApp()->RegisterStaticModelMesh(L"KUMO_MESH", App::GetApp()->m_wstrRelativeDataPath, L"hituji.bmf");

		

		// ボタンのテクスチャ
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"TutorialUI\\AButtonSelect.png";
		App::GetApp()->RegisterTexture(L"ABUTTON_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"TutorialUI\\start.png";
		App::GetApp()->RegisterTexture(L"STARTBUTTON_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"TutorialUI\\BButton.png";
		App::GetApp()->RegisterTexture(L"BBUTTON_TUTORIAL_TX", strTexture);
	}

	//ビューの作成
	void TutorialScene::CreateViews()
	{
		//最初にデフォルトのレンダリングターゲット類を作成する
		//パラメータの2048.0fはシャドウマップのサイズ。大きいほど影が細かくなる（4096.0fなど）
		//影を細かくするとメモリを大量に消費するので注意！
		CreateDefaultRenderTargets(2048.0f);

		//影のビューサイズの設定。この値を小さくすると影が表示される範囲が小さくなる。
		//値が小さいほうが影は細かくなる
		//スタティック変数なので一度設定したらその値はステージを超えて保持される。
		Shadowmap::SetViewSize(32.0f);

		//マルチビューコンポーネントの取得
		auto PtrMultiView = GetComponent<MultiView>();

		//マルチビューにビューの追加
		auto PtrView = PtrMultiView->AddView();

		//ビューの矩形を設定（ゲームサイズ全体）
		Rect2D<float> rect(0, 0, (float)App::GetApp()->GetGameWidth(), (float)App::GetApp()->GetGameHeight());

		//背景色(RGB)
		Color4 ViewBkColor(0.25f, 0.4f, 0.8f, 1.0f);

		//最初のビューにパラメータの設定
		PtrView->ResetParamaters<Camera, MultiLight>(rect, ViewBkColor, 1, 0.0f, 1.0f);

		//最初のビューのビューのライトの設定
		auto PtrLight = PtrView->GetMultiLight()->GetLight(0);
		PtrLight->SetPositionToDirectional(-0.25f, 1.0f, -0.25f);

		//ビューのカメラの設定
		auto PtrCamera = PtrView->GetCamera();
		//カメラ位置	SetEye	カメラの位置		SetAt	カメラの注視位置
		PtrCamera->SetEye(Vector3(4.3f, 8.0f, -3.0f));
		PtrCamera->SetAt(Vector3(4.3f, 0.0f, 3.0f));
	}

	//雲作成メソッド
	void TutorialScene::CreateWhiteCloud(size_t row, size_t col){
		//座標数値の事前準備
		auto Xpos = static_cast<float>(col);
		auto Zpos = static_cast<float>(row);
		auto Ptr = AddGameObject<Tutorial_cloud>(Vector3(Xpos, 0, Zpos), true);
		SetCloudObject(row, col, Ptr);
	}
	void TutorialScene::CreateNonCloud(size_t row, size_t col){
		//座標数値の事前準備
		auto Xpos = static_cast<float>(col);
		auto Zpos = static_cast<float>(row);
		auto Ptr = AddGameObject<Tutorial_cloud>(Vector3(Xpos, 0, Zpos), false);
		SetCloudObject(row, col, Ptr);
	}

	//雲の作成
	void TutorialScene::CreateCloudVec(){
		//サイズ分for文で回す
		for (size_t row = 0; row < m_Rowsize; row++){
			for (size_t col = 0; col < m_Colsize; col++){
				CloudName CloudNameObj = GetCloudNameVec(row, col);
				switch (CloudNameObj)
				{
				case CloudName::None:
					CreateNonCloud(row, col);
					break;
				case CloudName::White:
					CreateWhiteCloud(row, col);
					break;
				case CloudName::Thunder:
					break;
				default:
					break;
				}
			}
		}
	}

	//ユーザー入力で操作するバーの作成
	void TutorialScene::CreateInput_bar(){
		auto PtrBar = AddGameObject<Tutorial_bar>();
		SetSharedGameObject(L"TUTORIAL_BAR", PtrBar);
	}

	// ボタン類の作成
	void TutorialScene::CreateButton()
	{
		auto AButton = AddGameObject<Tutorial_AButton>(Vector3(840.0f, -450.0f, 0.0f));
		SetSharedGameObject(L"ABUTTON", AButton);
		auto StartButton = AddGameObject<Tutorial_StartButton>(Vector3(670.0f, -398.0f, 0.0f));
		SetSharedGameObject(L"STARTBUTTON", StartButton);
		auto BButton = AddGameObject<Tutorial_BButton>(Vector3(285.0f, -465.0f, 0.0f));
		SetSharedGameObject(L"BBUTTON", BButton);
	}

	//UIのフレームの作成
	void TutorialScene::CreateUI(){
		//フレーム
		AddGameObject<Tutorial_Frame>(Vector3(0, 0, 0), true);
		//HowToPlayの文字
		AddGameObject<Tutorial_Word>(Vector3(0.0f, 470.0f, 0));
		//コントローラーの画像
		auto PtrController = AddGameObject<Tutorial_Controller>(Vector3(560.0f, -250.0f, 0.0f));
		SetSharedGameObject(L"TUTORIAL_CONTROLLER", PtrController);
		//説明
		auto PtrWord = AddGameObject<Tutorial_Setumei>(Vector3(560.0f, 130.0f, 0.0f));
		SetSharedGameObject(L"TUTORIAL_SETUMEI", PtrWord);
	}

	//背景の作成
	void TutorialScene::CreateBackGround(){
		//ステージへのゲームオブジェクトの追加
		auto Ptr = AddGameObject<GameObject>();
		auto PtrTransMatrix = Ptr->GetComponent<TransformMatrix>();
		Quaternion Qt;
		Qt.RotationRollPitchYawFromVector(Vector3(XM_PIDIV2, 0, 0));
		Matrix4X4 WorldMat;
		WorldMat.DefTransformation(
			Vector3(40.0f, 40.0f, 1.0f),
			Qt,
			Vector3(0.0f, -1.0f, 0.0f)
			);
		PtrTransMatrix->SetWorldMatrix(WorldMat);

		//描画コンポーネントの追加
		auto DrawComp = Ptr->AddComponent<PNTStaticDraw>();
		DrawComp->SetDiffuse(Color4(0.0f, 0.0f, 0.0f, 0.3f));
		//描画コンポーネントに形状（メッシュ）を設定
		DrawComp->SetMeshResource(L"DEFAULT_SQUARE");
		//描画コンポーネントテクスチャの設定
		DrawComp->SetTextureResource(L"WATER_TX");
		//自分に影が映りこむようにする
		DrawComp->SetOwnShadowActive(true);
	}

	void TutorialScene::CreatePlayer(){
		auto PtrPlayer = AddGameObject<Tutorial_Character>();
		SetSharedGameObject(L"TUTORIAL_PLAYER", PtrPlayer);
		AddGameObject<Fade>(true, false);
	}

	void TutorialScene::OnUpdate()
	{
		//ステートマシンのUpdateを行う
		//この中でステートの更新が行われる(Execute()関数が呼ばれる)
		m_StateMachine->Update();
	}

	// 初期化
	void TutorialScene::OnCreate()
	{
		try{
			// SEの設定
			auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
			pMultiSoundEffect->AddAudioResource(L"TUTORIAL_BUTTON_SE");
			pMultiSoundEffect->AddAudioResource(L"TUTORIAL_BUTTON1_SE");
			pMultiSoundEffect->AddAudioResource(L"GAMESTART"); 
			pMultiSoundEffect->AddAudioResource(L"TUTORIALBGM"); 

			//BGMの再生
			pMultiSoundEffect->Start(L"TUTORIALBGM", 1, 0.3f);

			// リソースの作成
			CreateResourses();
			//ビュー類を作成する
			CreateViews();
			//Easyで雲作成
			PatternToVec(m_TutorialPatternVec);
			//雲の作成
			CreateCloudVec();
			//入力バーの作成
			CreateInput_bar();
			//背景
			CreateBackGround();
			//枠組みの作成
			CreateUI();
			//プレイヤーの作成
			CreatePlayer();
			// ボタン類の作成
			CreateButton();

			//ステートマシンの構築
			m_StateMachine = make_shared< StateMachine<TutorialScene> >(GetThis<TutorialScene>());
			//最初のステートをDefaultStateに設定
			m_StateMachine->ChangeState(FarstTutorialState::Instance());
		}
		catch (...){
			throw;
		}
	}

	//-----------------------------
	//FarstTutorialStateで使用するMotion
	//-----------------------------
	//Bボタンが押されたか
	bool TutorialScene::Is_AButtonPush(){
		// コントローラーの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		// Aボタンが押されたらGameStageへ移動
		if (CntlVec[0].bConnected){
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A){
				return true;
			}
		}
		return false;
	}

	//Aボタンが押されたか
	bool TutorialScene::Is_BButtonPush(){
		// コントローラーの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		// Aボタンが押されたらGameStageへ移動
		if (CntlVec[0].bConnected){
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_B){
				return true;
			}
		}
		return false;
	}

	//Aボタンを押してゲームをスタートする
	bool TutorialScene::Is_APushGameStart(){
		// コントローラーの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		// Aボタンが押されたらGameStageへ移動
		if (CntlVec[0].bConnected){
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A && !m_IsPushButton){
				m_IsPushButton = true;
				return true;
			}
		}
		return false;
	}

	//入力バーの移動アクションのスタート
	void TutorialScene::StartAction_InputBar(){
		auto Pbar = GetSharedGameObject<Tutorial_bar>(L"TUTORIAL_BAR");
		Pbar->StarAction();
	}

	//時間を置いてからの画像切り替え
	//TODO : 時間を少し置く処理がまだできてない
	void TutorialScene::ChangeController_Texture(){
		auto pController = GetSharedGameObject<Tutorial_Controller>(L"TUTORIAL_CONTROLLER");
		pController->StartAction_ChangeTexture();
		auto pWord = GetSharedGameObject<Tutorial_Setumei>(L"TUTORIAL_SETUMEI");
		pWord->ChangeTexture(L"WORD_1_TX");
	}

	//何秒間か待つ
	bool TutorialScene::WaitTime(){
		m_waittime += 0.035f;
		if (m_waittime > 10.0f){
			m_waittime = 0.0f;
			return true;
		}
		return false;
	}
	//オブジェクトの位置等を初期化する
	void TutorialScene::ResetObject(){
		auto pController = GetSharedGameObject<Tutorial_Controller>(L"TUTORIAL_CONTROLLER");
		auto Pbar = GetSharedGameObject<Tutorial_bar>(L"TUTORIAL_BAR");
		pController->Reset();
		Pbar->Reset();
	}
	//SEの再生
	void TutorialScene::SoundPlay(const wstring& SEname, const float& volume){
		auto pSound = GetComponent<MultiSoundEffect>();
		pSound->Start(SEname, 0, volume);
	}

	//SEの再生
	void TutorialScene::SoundStop(const wstring& SEname){
		auto pSound = GetComponent<MultiSoundEffect>();
		pSound->Stop(SEname);
	}
	//-----------------------------
	//SecondTutorialStateで使用するMotion
	//-----------------------------
	//シーンを移動する
	void TutorialScene::SceneMove(){
		AddGameObject<Fade>(false, true);
		PostEvent(0.5f, GetThis<TutorialScene>(), App::GetApp()->GetSceneBase(), L"ToGame");
	}
	//入力バーの非表示
	void TutorialScene::Drawfalse_Inputbar(bool TF){
		auto Pbar = GetSharedGameObject<Tutorial_bar>(L"TUTORIAL_BAR");
		Pbar->SetDrawUpdate(TF);
		auto PtrPlayer = GetSharedGameObject<Tutorial_Character>(L"TUTORIAL_PLAYER");
		PtrPlayer->SetUpdateActive(!TF);
	}

	// ボタンの非表示
	void TutorialScene::ButtonUI(bool a)
	{
		auto AButton = GetSharedGameObject<Tutorial_AButton>(L"ABUTTON");
		AButton->SetDrawUpdate(a);
		auto StartButton = GetSharedGameObject<Tutorial_StartButton>(L"STARTBUTTON");
		StartButton->SetDrawUpdate(!a);
		auto BButton = GetSharedGameObject<Tutorial_BButton>(L"BBUTTON");
		BButton->SetDrawActive(!a);
	}

	//キャラクターが移動する
	void TutorialScene::StartAction_Player(){
		auto PtrPlayer = GetSharedGameObject<Tutorial_Character>(L"TUTORIAL_PLAYER");
		PtrPlayer->Jump();
	}
	//説明の画像切り替え
	void TutorialScene::ChangeController_RButtonTexture(){
		auto pController = GetSharedGameObject<Tutorial_Controller>(L"TUTORIAL_CONTROLLER");
		pController->StartAction_ButtonTexture();
		auto pWord = GetSharedGameObject<Tutorial_Setumei>(L"TUTORIAL_SETUMEI");
		pWord->ChangeTexture(L"WORD_2_TX");
	}

	//再生している回数を図ってステートのリセットをする
	void TutorialScene::PlayCount(){
		if (m_PlayNum == 2){
			//カウントのリセット
			m_PlayNum = 0;
			GetStateMachine()->ChangeState(SecondTutorialState::Instance());
		}
		else{
			m_PlayNum++;
		}
	}

	//リセットしたときにキャラの位置を初期値に移動
	void TutorialScene::PlayerReset(){
		auto PtrPlayer = GetSharedGameObject<Tutorial_Character>(L"TUTORIAL_PLAYER");
		PtrPlayer->ResetPos();
	}

	//--------------------------------------------------------------------------------------
	//	class FarstTutorialState : public ObjState<TutorialScene>;
	//	用途: 雲の移動を教えるステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<FarstTutorialState> FarstTutorialState::Instance(){
		static shared_ptr<FarstTutorialState> instance;
		if (!instance){
			instance = shared_ptr<FarstTutorialState>(new FarstTutorialState);
		}
		return instance;
	}
	void FarstTutorialState::Enter(const shared_ptr<TutorialScene>& Obj){
		//インプットバーの移動アクション
		Obj->StartAction_InputBar();
		//時間を置いてからの画像切り替え
		Obj->ChangeController_Texture();
		//入力バーの非表示
		Obj->Drawfalse_Inputbar(true);
		// Aボタンの非表示
		Obj->ButtonUI(true);
	}
	void FarstTutorialState::Execute(const shared_ptr<TutorialScene>& Obj){
		//Bボタンを押したら、次のチュートリアルに移動
		if (Obj->Is_AButtonPush()){
			Obj->SoundPlay(L"TUTORIAL_BUTTON_SE", 0.5f);
			Obj->GetStateMachine()->ChangeState(SecondTutorialState::Instance());
		}
		//待ち時間が経過したら、ステートの再度呼ぶ
		if (Obj->WaitTime()){
			Obj->GetStateMachine()->ChangeState(FarstTutorialState::Instance());
		}
	}
	void FarstTutorialState::Exit(const shared_ptr<TutorialScene>& Obj){
		//画像、位置の初期化
		Obj->ResetObject();
	}

	//--------------------------------------------------------------------------------------
	//	class SecondTutorialState : public ObjState<TutorialScene>;
	//	用途: プレイヤーの移動を教えるステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<SecondTutorialState> SecondTutorialState::Instance(){
		static shared_ptr<SecondTutorialState> instance;
		if (!instance){
			instance = shared_ptr<SecondTutorialState>(new SecondTutorialState);
		}
		return instance;
	}
	void SecondTutorialState::Enter(const shared_ptr<TutorialScene>& Obj){
		//入力バーの非表示
		Obj->Drawfalse_Inputbar(false);
		// Aボタンの非表示
		Obj->ButtonUI(false);
		//テクスチャの変更
		Obj->ChangeController_RButtonTexture();
	}
	void SecondTutorialState::Execute(const shared_ptr<TutorialScene>& Obj){
		//Aボタンを押されたら、ゲームシーンに移動
		if (Obj->Is_APushGameStart()){
			Obj->SoundPlay(L"GAMESTART", 0.5f);
			Obj->SceneMove();
		}
		//Bボタンを押したら、前の説明に戻る
		if (Obj->Is_BButtonPush()){
			Obj->SoundPlay(L"TUTORIAL_BUTTON1_SE", 0.5f);
			Obj->GetStateMachine()->ChangeState(FarstTutorialState::Instance());
		}
		//待ち時間が経過したら、ステートの再度呼ぶ
		if (Obj->WaitTime()){
			//チュートリアルの再生回数を見てステートのリセットをかける
			Obj->PlayCount();
			//キャラクターが移動を開始する
			Obj->StartAction_Player();
		}
	}
	void SecondTutorialState::Exit(const shared_ptr<TutorialScene>& Obj){
		//プレイヤーの位置をリセットする
		Obj->PlayerReset();
	}
}
//endof  basedx11