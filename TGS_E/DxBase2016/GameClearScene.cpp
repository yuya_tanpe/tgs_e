#include "stdafx.h"
#include "Project.h"

namespace basedx11
{
	// リソースの作成
	void GameClearScene::CreateResourses()
	{
		// テクスチャ
		wstring strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameClear.png";
		App::GetApp()->RegisterTexture(L"CLEAR_TX", strTexture);
	}

	// ビューの作成
	void GameClearScene::CreateViews()
	{
		//最初にデフォルトのレンダリングターゲット類を作成する
		//パラメータの2048.0fはシャドウマップのサイズ。大きいほど影が細かくなる（4096.0fなど）
		//影を細かくするとメモリを大量に消費するので注意！
		CreateDefaultRenderTargets(2048.0f);

		//影のビューサイズの設定。この値を小さくすると影が表示される範囲が小さくなる。
		//値が小さいほうが影は細かくなる
		//スタティック変数なので一度設定したらその値はステージを超えて保持される。
		Shadowmap::SetViewSize(32.0f);

		//マルチビューコンポーネントの取得
		auto PtrMultiView = GetComponent<MultiView>();

		//マルチビューにビューの追加
		auto PtrView = PtrMultiView->AddView();

		//ビューの矩形を設定（ゲームサイズ全体）
		Rect2D<float> rect(0, 0, (float)App::GetApp()->GetGameWidth(), (float)App::GetApp()->GetGameHeight());

		//背景色(RGB)
		Color4 ViewBkColor(0.0f, 0.125f, 0.3f, 1.0f);

		//最初のビューにパラメータの設定
		PtrView->ResetParamaters<Camera, MultiLight>(rect, ViewBkColor, 1, 0.0f, 1.0f);

		//最初のビューのビューのライトの設定
		auto PtrLight = PtrView->GetMultiLight()->GetLight(0);
		PtrLight->SetPositionToDirectional(-0.25f, 1.0f, -0.25f);

		//ビューのカメラの設定
		auto PtrCamera = PtrView->GetCamera();
		PtrCamera->SetEye(Vector3(0.0f, 0.0f, -10.0f));
		PtrCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
	}

	// ゲームクリアスプライトの作成
	void GameClearScene::CreateGameClearSprite()
	{
		auto Ptr = AddGameObject<GameObject>();
		auto PtrTransMatrix = Ptr->GetComponent<TransformMatrix>();
		Quaternion Qt;
		Qt.RotationRollPitchYawFromVector(Vector3(0, 0, 0));
		Matrix4X4 WorldMat;
		WorldMat.DefTransformation(
			Vector3(5.0f, 5.0f, 5.0f),
			Qt,
			Vector3(0.0f, 0.0f, 0.0f)
			);
		PtrTransMatrix->SetWorldMatrix(WorldMat);

		//描画コンポーネントの追加
		auto DrawComp = Ptr->AddComponent<PNTStaticDraw>();
		//描画コンポーネントに形状（メッシュ）を設定
		DrawComp->SetMeshResource(L"DEFAULT_SQUARE");
		//描画コンポーネントテクスチャの設定
		DrawComp->SetTextureResource(L"CLEAR_TX");
		//自分に影が映りこむようにする
		DrawComp->SetOwnShadowActive(false);
	}

	//サウンドの再生
	/*void GameClearScene::SoundPlay(wstring soundname, float volume){
		auto pMultiSoundEffect = GetComponent<MultiSoundEffect>();
		pMultiSoundEffect->Start(soundname, 0, volume);
		}*/

	// 初期化
	void GameClearScene::OnCreate()
	{
		/*auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Cursor");*/

		try
		{
			// リソースの作成
			CreateResourses();

			// ビューの作成
			CreateViews();

			// ゲームクリアスプライトの作成
			CreateGameClearSprite();

			//音の再生
			/*void SoundPlay(wstring soundname, float volume);*/
		}
		catch (...)
		{
			throw;
		}
	}

	// 更新
	void GameClearScene::OnUpdate()
	{
		// コントローラーの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();

		// Aボタンが押されたらタイトルシーンへ移動
		if (CntlVec[0].bConnected)
		{
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A)
			{
				PostEvent(0.0f, GetThis<GameClearScene>(), App::GetApp()->GetSceneBase(), L"ToTitle");
			}

			//name	:	youitirou
			//Aボタンを押されたときの音を鳴らす
			/*SoundPlay(L"Cursor", 0.6f);*/
		}
	}
}
//endof  basedx11