#include "stdafx.h"
#include "Project.h"

namespace basedx11{
	//--------------------------------------------------------------------------------------
	//	class Player : public GameObject;
	//	用途: プレイヤー
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Player::Player(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr),
		m_LeftStick(false),
		m_horizontal_line(0)
	{}

	//初期化
	//テスト駆動で作成	確認が取れたら、クラス作成して実装する
	void Player::OnCreate(){
		//Transformの設定
		auto PtrTrans = AddComponent<Transform>();
		PtrTrans->SetPosition(Vector3(2.0f, 0.6f, static_cast<float>(m_horizontal_line)));
		PtrTrans->SetRotation(0.0f, 0.0f, 0.0f);
		PtrTrans->SetScale(7.0f, 0.3f, 1.0f);

		//独自コンポーネント　下に落ちる処理
		auto PtrFall = AddComponent<Sky_Fall>();

		//サウンドを登録.
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"MOVERL");

		//描画コンポーネント
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetDiffuse(Color4(0.1f, 0.1f, 0.15f, 0.5f));
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetTextureResource(L"PURPLE_TX");
		//透過処理
		SetAlphaActive(true);
		//文字列をつける
		auto PtrString = AddComponent<StringSprite>();
		PtrString->SetText(L"");
		PtrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));
	}

	void Player::OnLastUpdate(){
		wstring Name{ L"スティック関連のデバッグ" };
		Name += L"\n";
		Name += L"\n";

		auto fps = App::GetApp()->GetStepTimer().GetFramesPerSecond();
		wstring FPS(L"FPS: ");
		FPS += Util::UintToWStr(fps);
		FPS += L"\n";

		wstring Line(L"UserLine: ");
		Line += Util::UintToWStr(m_horizontal_line);
		Line += L"\n";

		wstring DEV_INPUT(L"右スティックの入力:  ");
		if (m_LeftStick){
			DEV_INPUT += L"傾きました";
		}
		else
		{
			DEV_INPUT += L"入力が来ていません";
		}

		DEV_INPUT += L"\n";

		wstring str = Name + FPS + DEV_INPUT + Line;
		//文字列をつける
		auto PtrString = GetComponent<StringSprite>();
		PtrString->SetStartPosition(Point2D<float>(200.0f, 10.0f));
		PtrString->SetFontColor(Color4(0.0f, 0.0f, 0.0f, 1.0f));
		PtrString->SetText(str);
	}

	void Player::OnUpdate(){
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		auto PtrGameStage = dynamic_pointer_cast<GameStage>(GetStage());
		auto Sh_Obj = PtrGameStage->GetSharedGameObject<Player_Character>(L"Player_Character");
		auto charaLine = Sh_Obj->GetRow();
		//スティック入力　デッドライン設定
		bool Is_inputX = CntlVec[0].fThumbLX < 0.2f && CntlVec[0].fThumbLX > -0.2f;
		bool Is_inputY = CntlVec[0].fThumbLY < 0.2f && CntlVec[0].fThumbLY > -0.2f;

		bool line = m_horizontal_line == charaLine;
		if (CntlVec[0].bConnected){
			//スティック入力がないとき
			if (Is_inputX && Is_inputY){
				m_LeftStick = false;
			}
			else if (!m_LeftStick){
				auto pMultiSoundEffect = GetComponent<MultiSoundEffect>();
				if (CntlVec[0].fThumbLX > 0.85f && !line){
					m_LeftStick = true;
					pMultiSoundEffect->Start(L"MOVERL", 0, 0.4f);
					FirstLine_Right();
					SecondLine_Right();
					ThirdLine_Right();
					FourthLine_Right();
					FifthLine_Right();
					PtrGameStage->MoveRight_vec(m_horizontal_line);
				}
				else if (CntlVec[0].fThumbLX < -0.85f && !line){
					m_LeftStick = true;
					pMultiSoundEffect->Start(L"MOVERL", 0, 0.4f);
					FirstLine_Left();
					SecondLine_Left();
					ThirdLine_Left();
					FourthLine_Left();
					FifthLine_Left();
					PtrGameStage->MoveLeft_vec(m_horizontal_line);
				}
				else if (CntlVec[0].fThumbLY > 0.85f){
					m_LeftStick = true;
					if (m_horizontal_line == 9)	{
						m_horizontal_line = 0;
					}
					else{
						m_horizontal_line++;
						auto Pos = GetComponent<Transform>()->GetPosition();
						Pos.z += 1;

						GetComponent<Transform>()->SetPosition(Pos);
					}
				}
				else if (CntlVec[0].fThumbLY < -0.85f){
					m_LeftStick = true;
					if (m_horizontal_line == -1)	{
						m_horizontal_line = 9;
					}
					else{
						m_horizontal_line--;
						auto Pos = GetComponent<Transform>()->GetPosition();
						Pos.z -= 1;
						GetComponent<Transform>()->SetPosition(Pos);
					}
				}
			}
		}
	}

	//1番目の列の移動
	void Player::FirstLine_Right(){
		auto PtrGameStage = dynamic_pointer_cast<GameStage>(GetStage());
		//配列からGameObjectを取得
		auto Obj = PtrGameStage->GetCloudObject(m_horizontal_line, 0);
		//weak_ptr型からshared_ptr型に変換し、取得
		auto pCloudObject = dynamic_pointer_cast<Cloud>(Obj.lock());
		if (pCloudObject){
			//雲の位置取得
			auto CloudPos = pCloudObject->GetComponent<Transform>()->GetPosition();
			CloudPos.x++;
			if (CloudPos.x > 4.0f){
				CloudPos.x = 0.0f;
			}

			pCloudObject->GetComponent<Transform>()->SetPosition(CloudPos);
		}
	}
	//2番目の列の移動
	void Player::SecondLine_Right(){
		auto PtrGameStage = dynamic_pointer_cast<GameStage>(GetStage());
		//配列からGameObjectを取得
		auto Obj = PtrGameStage->GetCloudObject(m_horizontal_line, 1);
		//weak_ptr型からshared_ptr型に変換し、取得
		auto pCloudObject = dynamic_pointer_cast<Cloud>(Obj.lock());
		if (pCloudObject){
			//雲の位置取得
			auto CloudPos = pCloudObject->GetComponent<Transform>()->GetPosition();
			CloudPos.x++;
			if (CloudPos.x > 4.0f){
				CloudPos.x = 0.0f;
			}

			pCloudObject->GetComponent<Transform>()->SetPosition(CloudPos);
		}
	}
	//3番目の列の移動
	void Player::ThirdLine_Right(){
		auto PtrGameStage = dynamic_pointer_cast<GameStage>(GetStage());
		//配列からGameObjectを取得
		auto Obj = PtrGameStage->GetCloudObject(m_horizontal_line, 2);
		//weak_ptr型からshared_ptr型に変換し、取得
		auto pCloudObject = dynamic_pointer_cast<Cloud>(Obj.lock());
		if (pCloudObject){
			//雲の位置取得
			auto CloudPos = pCloudObject->GetComponent<Transform>()->GetPosition();
			CloudPos.x++;
			if (CloudPos.x > 4.0f){
				CloudPos.x = 0.0f;
			}

			pCloudObject->GetComponent<Transform>()->SetPosition(CloudPos);
		}
	}
	//4番目の列の移動
	void Player::FourthLine_Right(){
		auto PtrGameStage = dynamic_pointer_cast<GameStage>(GetStage());
		//配列からGameObjectを取得
		auto Obj = PtrGameStage->GetCloudObject(m_horizontal_line, 3);
		//weak_ptr型からshared_ptr型に変換し、取得
		auto pCloudObject = dynamic_pointer_cast<Cloud>(Obj.lock());
		if (pCloudObject){
			//雲の位置取得
			auto CloudPos = pCloudObject->GetComponent<Transform>()->GetPosition();
			CloudPos.x++;
			if (CloudPos.x > 4.0f){
				CloudPos.x = 0.0f;
			}

			pCloudObject->GetComponent<Transform>()->SetPosition(CloudPos);
		}
	}
	//5番目の列の移動
	void Player::FifthLine_Right(){
		auto PtrGameStage = dynamic_pointer_cast<GameStage>(GetStage());
		//配列からGameObjectを取得
		auto Obj = PtrGameStage->GetCloudObject(m_horizontal_line, 4);
		//weak_ptr型からshared_ptr型に変換し、取得
		auto pCloudObject = dynamic_pointer_cast<Cloud>(Obj.lock());
		if (pCloudObject){
			//雲の位置取得
			auto CloudPos = pCloudObject->GetComponent<Transform>()->GetPosition();
			CloudPos.x++;
			if (CloudPos.x > 4.0f){
				CloudPos.x = 0.0f;
			}

			pCloudObject->GetComponent<Transform>()->SetPosition(CloudPos);
		}
	}

	void Player::FirstLine_Left(){
		auto PtrGameStage = dynamic_pointer_cast<GameStage>(GetStage());
		//配列からGameObjectを取得
		auto Obj = PtrGameStage->GetCloudObject(m_horizontal_line, 0);
		//weak_ptr型からshared_ptr型に変換し、取得
		auto pCloudObject = dynamic_pointer_cast<Cloud>(Obj.lock());
		if (pCloudObject){
			//雲の位置取得
			auto CloudPos = pCloudObject->GetComponent<Transform>()->GetPosition();
			CloudPos.x--;
			if (CloudPos.x < 0.0f){
				CloudPos.x = 4.0f;
			}

			pCloudObject->GetComponent<Transform>()->SetPosition(CloudPos);
		}
	}
	void Player::SecondLine_Left(){
		auto PtrGameStage = dynamic_pointer_cast<GameStage>(GetStage());
		//配列からGameObjectを取得
		auto Obj = PtrGameStage->GetCloudObject(m_horizontal_line, 1);
		//weak_ptr型からshared_ptr型に変換し、取得
		auto pCloudObject = dynamic_pointer_cast<Cloud>(Obj.lock());
		if (pCloudObject){
			//雲の位置取得
			auto CloudPos = pCloudObject->GetComponent<Transform>()->GetPosition();
			CloudPos.x--;
			if (CloudPos.x < 0.0f){
				CloudPos.x = 4.0f;
			}

			pCloudObject->GetComponent<Transform>()->SetPosition(CloudPos);
		}
	}
	void Player::ThirdLine_Left(){
		auto PtrGameStage = dynamic_pointer_cast<GameStage>(GetStage());
		//配列からGameObjectを取得
		auto Obj = PtrGameStage->GetCloudObject(m_horizontal_line, 2);
		//weak_ptr型からshared_ptr型に変換し、取得
		auto pCloudObject = dynamic_pointer_cast<Cloud>(Obj.lock());
		if (pCloudObject){
			//雲の位置取得
			auto CloudPos = pCloudObject->GetComponent<Transform>()->GetPosition();
			CloudPos.x--;
			if (CloudPos.x < 0.0f){
				CloudPos.x = 4.0f;
			}

			pCloudObject->GetComponent<Transform>()->SetPosition(CloudPos);
		}
	}
	void Player::FourthLine_Left(){
		auto PtrGameStage = dynamic_pointer_cast<GameStage>(GetStage());
		//配列からGameObjectを取得
		auto Obj = PtrGameStage->GetCloudObject(m_horizontal_line, 3);
		//weak_ptr型からshared_ptr型に変換し、取得
		auto pCloudObject = dynamic_pointer_cast<Cloud>(Obj.lock());
		if (pCloudObject){
			//雲の位置取得
			auto CloudPos = pCloudObject->GetComponent<Transform>()->GetPosition();
			CloudPos.x--;
			if (CloudPos.x < 0.0f){
				CloudPos.x = 4.0f;
			}

			pCloudObject->GetComponent<Transform>()->SetPosition(CloudPos);
		}
	}
	void Player::FifthLine_Left(){
		auto PtrGameStage = dynamic_pointer_cast<GameStage>(GetStage());
		//配列からGameObjectを取得
		auto Obj = PtrGameStage->GetCloudObject(m_horizontal_line, 4);
		//weak_ptr型からshared_ptr型に変換し、取得
		auto pCloudObject = dynamic_pointer_cast<Cloud>(Obj.lock());
		if (pCloudObject){
			//雲の位置取得
			auto CloudPos = pCloudObject->GetComponent<Transform>()->GetPosition();
			CloudPos.x--;
			if (CloudPos.x < 0.0f){
				CloudPos.x = 4.0f;
			}

			pCloudObject->GetComponent<Transform>()->SetPosition(CloudPos);
		}
	}
}

//endof  basedx11