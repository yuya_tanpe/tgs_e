#include "stdafx.h"
#include "Project.h"

namespace basedx11
{
	void StageSelectScene::CreateResourses()
	{
		// テクスチャ
		wstring strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Start.png";
		App::GetApp()->RegisterTexture(L"SELECT_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"AButtonSelect.png";
		App::GetApp()->RegisterTexture(L"ABUTTON_TX", strTexture);
		// サウンド
		wstring strSound = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\TitleSE.wav";
		App::GetApp()->RegisterWav(L"SELECT_SE", strSound);
	}

	//ビューの作成
	void StageSelectScene::CreateViews()
	{
		//最初にデフォルトのレンダリングターゲット類を作成する
		//パラメータの2048.0fはシャドウマップのサイズ。大きいほど影が細かくなる（4096.0fなど）
		//影を細かくするとメモリを大量に消費するので注意！
		CreateDefaultRenderTargets(2048.0f);

		//影のビューサイズの設定。この値を小さくすると影が表示される範囲が小さくなる。
		//値が小さいほうが影は細かくなる
		//スタティック変数なので一度設定したらその値はステージを超えて保持される。
		Shadowmap::SetViewSize(32.0f);

		//マルチビューコンポーネントの取得
		auto PtrMultiView = GetComponent<MultiView>();

		//マルチビューにビューの追加
		auto PtrView = PtrMultiView->AddView();

		//ビューの矩形を設定（ゲームサイズ全体）
		Rect2D<float> rect(0, 0, (float)App::GetApp()->GetGameWidth(), (float)App::GetApp()->GetGameHeight());

		//背景色(RGB)
		Color4 ViewBkColor(0.0f, 0.125f, 0.3f, 1.0f);

		//最初のビューにパラメータの設定
		PtrView->ResetParamaters<Camera, MultiLight>(rect, ViewBkColor, 1, 0.0f, 1.0f);

		//最初のビューのビューのライトの設定
		auto PtrLight = PtrView->GetMultiLight()->GetLight(0);
		PtrLight->SetPositionToDirectional(-0.25f, 1.0f, -0.25f);

		//ビューのカメラの設定
		auto PtrCamera = PtrView->GetCamera();
		PtrCamera->SetEye(Vector3(0.0f, 0.0f, -8.0f));
		PtrCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
	}

	void StageSelectScene::CreateSelectSprite()
	{
		auto Ptr = AddGameObject<GameObject>();
		auto PtrTransMatrix = Ptr->GetComponent<TransformMatrix>();
		Quaternion Qt;
		Qt.RotationRollPitchYawFromVector(Vector3(0, 0, 0));
		Matrix4X4 WorldMat;
		WorldMat.DefTransformation(
			Vector3(5.0f, 3.0f, 1.0f),
			Qt,
			Vector3(0.0f, 1.0f, 0.0f)
			);
		PtrTransMatrix->SetWorldMatrix(WorldMat);

		auto DrawComp = Ptr->AddComponent<PNTStaticDraw>();

		DrawComp->SetMeshResource(L"DEFAULT_SQUARE");

		DrawComp->SetTextureResource(L"SELECT_TX");

		Ptr->SetAlphaActive(true);

		DrawComp->SetOwnShadowActive(false);
	}

	void StageSelectScene::CreateGameSelectUI()
	{
		AddGameObject<StageSelectUI>(Vector3(900.0f, -450.0f, 0.0f));
	}

	void StageSelectScene::OnUpdate()
	{
		// コントローラーの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		// Aボタンが押されたらGameStageへ移動
		if (CntlVec[0].bConnected){
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A){
				PostEvent(0.0f, GetThis<StageSelectScene>(), App::GetApp()->GetSceneBase(), L"ToGame");

				// SEの再生
				auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
				pMultiSoundEffect->AddAudioResource(L"SELECT_SE");
				pMultiSoundEffect->Start(L"SELECT_SE", 0, 0.1f);
			}
		}
	}

	// 初期化
	void StageSelectScene::OnCreate()
	{
		try{
			// リソースの作成
			CreateResourses();

			//ビュー類を作成する
			CreateViews();

			//ボタンの実装
			//CreateButton();

			//タイトル
			CreateSelectSprite();

			CreateGameSelectUI();
		}
		catch (...){
			throw;
		}
	}
}
//endof  basedx11