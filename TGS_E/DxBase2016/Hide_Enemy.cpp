#include "stdafx.h"
#include "Project.h"

namespace basedx11{
	/**
	* @class Hide_Enemy
	* @brief 先を見えにくくする敵
	* @author　sike yuya
	* @date	2016/06/05
	*/

	//Hide_Enemy::Hide_Enemy(const shared_ptr<Stage>& StagePtr, const Vector3& i_start_pos, Enemy::Cell& i_cell) :
	//	GameObject(StagePtr),
	//	m_Pos(i_start_pos),
	//	m_Cell(i_cell)
	//{}
	////初期化
	//void Hide_Enemy::OnCreate(){
	//	//Transformの設定
	//	auto PtrTrans = AddComponent<Transform>();
	//	PtrTrans->SetPosition(m_Pos);
	//	PtrTrans->SetRotation(45.0f, 0.0f, 45.0f);
	//	PtrTrans->SetScale(0.4f, 0.4f, 0.4f);

	//	//描画コンポーネントの設定
	//	auto PtrDraw = AddComponent<PNTStaticDraw>();
	//	//描画するメッシュを設定
	//	//TODO : モデル未作成
	//	PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
	//	//描画するテクスチャを設定
	//	PtrDraw->SetTextureResource(L"BLUE_TX");

	//	//サウンドを登録.
	//	auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
	//	pMultiSoundEffect->AddAudioResource(L"Enemy");

	//	//敵の名前の登録
	//	SetEnemyName(CharacterName::HideEnemy);
	//	//キャラクター配列に登録
	//	SetName_CharacterVec();

	//	//ステートマシンの構築
	//	m_StateMachine = make_shared< StateMachine<Hide_Enemy> >(GetThis<Hide_Enemy>());
	//	//最初のステートをHideStateに設定
	//	m_StateMachine->ChangeState(HideState::Instance());

	//}

	////変化
	//void Hide_Enemy::OnUpdate(){
	//	m_StateMachine->Update();
	//}

	////自分の名前の登録
	//void Hide_Enemy::SetEnemyName(const CharacterName& name){
	//	m_EnemyName = name;
	//}

	////自分の作成された場所を配列に登録
	//void Hide_Enemy::SetName_CharacterVec(){
	//	auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
	//	assert(GameStagePtr && "ゲームステージの取得に失敗しました。");
	//	//キャラクター配列に自身を設定する		引数1 ~ 2 セル座標,　3 自分の名前
	//	GameStagePtr->SetCharacterName(m_Cell.m_Row, m_Cell.m_Col, m_EnemyName);
	//}

	////自身が消えたら配列から削除
	//void Hide_Enemy::Erase_CharacterVec(){
	//	auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
	//	assert(GameStagePtr && "ゲームステージの取得に失敗しました。");
	//	//キャラクター配列に自身を設定する		引数1 ~ 2 セル座標,　3 自分の名前
	//	GameStagePtr->SetCharacterName(m_Cell.m_Row, m_Cell.m_Col, CharacterName::None);
	//}

	////--------------------------------------------------------------------------------------
	////	class HideState : public ObjState<Hide_Enemy>;
	////	用途: 隠れてるステート
	////--------------------------------------------------------------------------------------
	//shared_ptr<HideState> HideState::Instance(){
	//	static shared_ptr<HideState> instance;
	//	if (!instance){
	//		instance = shared_ptr<HideState>(new HideState);
	//	}
	//	return instance;
	//}
	//void HideState::Enter(const shared_ptr<Hide_Enemy>& Obj){
	//}
	//void HideState::Execute(const shared_ptr<Hide_Enemy>& Obj){
	//}
	//void HideState::Exit(const shared_ptr<Hide_Enemy>& Obj){
	//}
	////--------------------------------------------------------------------------------------
	////	class ActionState : public ObjState<Hide_Enemy>;
	////	用途: 画面に画像を出して邪魔をするステート
	////--------------------------------------------------------------------------------------
	//shared_ptr<ActionState> ActionState::Instance(){
	//	static shared_ptr<ActionState> instance;
	//	if (!instance){
	//		instance = shared_ptr<ActionState>(new ActionState);
	//	}
	//	return instance;
	//}
	//void ActionState::Enter(const shared_ptr<Hide_Enemy>& Obj){
	//}
	//void ActionState::Execute(const shared_ptr<Hide_Enemy>& Obj){
	//}
	//void ActionState::Exit(const shared_ptr<Hide_Enemy>& Obj){
	//}
}
//endof  basedx11