#pragma once
#include "stdafx.h"

namespace basedx11{
	//--------------------------------------------------------------------------------------
	//	Drawコンポーネント : 雲のモデル用描画処理
	//--------------------------------------------------------------------------------------
	DECLARE_DX11_PIXEL_SHADER(PSPNTStaticCloud)

	//--------------------------------------------------------------------------------------
	//	class PNTStaticModelCloudDraw : public DrawComponent;
	//	用途: PNTStaticModelDraw描画コンポーネント
	//--------------------------------------------------------------------------------------
	class PNTStaticModelCloudDraw : public DrawComponent {
		void DrawNotShadow();
	public:
		explicit PNTStaticModelCloudDraw(const shared_ptr<GameObject>& GameObjectPtr);
		virtual ~PNTStaticModelCloudDraw();
		shared_ptr<MeshResource> GetMeshResource() const;
		void SetMeshResource(const shared_ptr<MeshResource>& MeshRes);
		void SetMeshResource(const wstring& MeshKey);
		bool GetOwnShadowActive() const;
		bool IsOwnShadowActive() const;
		void SetOwnShadowActive(bool b);
		//操作
		virtual void OnCreate()override;
		virtual void OnUpdate()override {}
		virtual void OnDraw()override;
	private:
		// pImplイディオム
		struct Impl;
		unique_ptr<Impl> pImpl;
	};

	//--------------------------------------------------------------------------------------
	//	Drawコンポーネント : モデル用ライティングを影響しない描画処理
	//--------------------------------------------------------------------------------------
	DECLARE_DX11_PIXEL_SHADER(PSPNTStaticNoLight)


	//--------------------------------------------------------------------------------------
	//	class PNTStaticModelNoLightDraw : public DrawComponent;
	//	用途: PNTStaticModelNoLightDraw描画コンポーネント
	//--------------------------------------------------------------------------------------
	class PNTStaticModelNoLightDraw : public DrawComponent {
		void DrawNotShadow();
	public:
		explicit PNTStaticModelNoLightDraw(const shared_ptr<GameObject>& GameObjectPtr);
		virtual ~PNTStaticModelNoLightDraw();
		shared_ptr<MeshResource> GetMeshResource() const;
		void SetMeshResource(const shared_ptr<MeshResource>& MeshRes);
		void SetMeshResource(const wstring& MeshKey);
		bool GetOwnShadowActive() const;
		bool IsOwnShadowActive() const;
		void SetOwnShadowActive(bool b);
		//操作
		virtual void OnCreate()override;
		virtual void OnUpdate()override {}
		virtual void OnDraw()override;
	private:
		// pImplイディオム
		struct Impl;
		unique_ptr<Impl> pImpl;
	};


}
