#pragma once

#include "stdafx.h"

namespace basedx11{
	/**
	* @class input_bar
	* @brief ユーザーが入力で上下に動かすオブジェクト
	* @author　sike yuya
	* @date	New 5/2
	*/
	class input_bar : public GameObject {
		// ---------- member ----------.
		//入力されたかを保持
		bool m_InputStick;
		size_t m_NowRow;
		// ---------- method ----------.

		//左スティック入力受付
		size_t GetControllerStick();

	public:
		input_bar(const shared_ptr<Stage>& StagePtr);
		virtual ~input_bar(){}
		//初期化
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate()override;

		//現在の行を取得する
		const size_t GetNowRow() const{
			return m_NowRow;
		}

		//Rowを一つ下げる
		void BarDown(){
			m_NowRow--;
		}

		//最終更新
		virtual void OnLastUpdate()override;
		virtual void OnEvent(const shared_ptr<Event>& event)override;
		//サウンドの再生
		void SoundPlay(wstring soundname, float volume);
	};

	/**
	* @class Tutorial_bar
	* @brief ユーザーが入力で上下に動かすオブジェクト
	* @author　sike yuya
	* @date	New 5/26
	*/
	class Tutorial_bar : public GameObject {
		// ---------- member ----------.
		//今どのラインにいるか保持
		int m_horizontal_line;
	public:
		Tutorial_bar(const shared_ptr<Stage>& StagePtr);
		virtual ~Tutorial_bar(){}
		//初期化
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate()override;

		//チュートリアルでの動きをアクションを代用する
		void StarAction();

		//位置の初期化
		void Reset();
		//非表示と更新を止める
		void SetDrawUpdate(const bool& TF);
	};
}
//endof  basedx11
