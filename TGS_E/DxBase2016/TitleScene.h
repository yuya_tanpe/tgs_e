#pragma once
#include "stdafx.h"

namespace basedx11
{
	// -----------------------------
	// 音源提供の表示に使用
	// -----------------------------
	class License : public Stage
	{
		// リソースの作成
		void CreateResourse();

		// ビューの作成
		void CreateView();

		// 背景（画像）の作成
		void CreateBG();

		// Fadeの作成
		void CreateFade();

	public:
		// 構築と破棄
		License() : Stage(){}

		// 初期化
		virtual void OnCreate() override;

		// 更新
		virtual void OnUpdate() override;
	};


	class TitleScene : public Stage
	{
		bool IsPushButton;

		// リソースの作成
		void CreateResourses();

		//ビューの作成
		void CreateViews();

		// タイトルの作成
		void CreateTitle();

		// AボタンUIの作成
		void CreateAbuttonUI();

		// 背景の作成
		void CreateBG();

		// 羊のスプライト作成
		void CreateSheepSprite();


	public:
		//構築と破棄
		TitleScene() :Stage(), IsPushButton(false){}
		virtual ~TitleScene(){
			SoundStop(L"BGM");
			SoundStop(L"START");
		}

		//初期化
		virtual void OnCreate()override;

		//更新
		virtual void OnUpdate()override;
		//音のストップ
		void SoundStop(wstring soundname);
	};
}
//end basedx11