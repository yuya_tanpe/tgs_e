#include "stdafx.h"
#include "Project.h"

namespace basedx11
{
	StageSelectUI::StageSelectUI(shared_ptr<Stage>& StagePtr, const Vector3& Pos) :
		GameObject(StagePtr), m_Pos(Pos)
	{}

	StageSelectUI::~StageSelectUI(){}

	void StageSelectUI::OnCreate()
	{
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_Pos);
		PtrTransform->SetScale(7.5f, 2.5f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);

		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"ABUTTON_TX");

		//センター原点
		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_CenterZeroPlusUpY);

		//両面描画
		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);

		//拡大・縮小
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddScaleTo(1.0f, Vector3(6.0f, 2.0f, 1.0f));
		PtrAction->AddScaleTo(1.0f, Vector3(7.5f, 2.5f, 1.0f));
		PtrAction->SetLooped(true);
		PtrAction->Run();
	}

	void StageSelectUI::OnUpdate()
	{
	}
}
//endof  basedx11