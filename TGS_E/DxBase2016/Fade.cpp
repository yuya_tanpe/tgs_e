#include "stdafx.h"
#include "Project.h"

namespace basedx11
{
	//--------------------------------------------------------------------------------------
	//	class Fade : public GameObject;
	//	用途: フェード用
	//--------------------------------------------------------------------------------------

	//構築と破棄
	Fade::Fade(const shared_ptr<Stage>& StagePtr, bool FadeIn, bool FadeOut) :
		GameObject(StagePtr),
		FadeInActive(FadeIn),
		FadeOutActive(FadeOut)
	{}

	void Fade::CreateResourses()
	{
		//画像
		wstring strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Fade\\Black.png";
		App::GetApp()->RegisterTexture(L"BLACK_TX", strTexture);
	}

	//初期化
	void Fade::OnCreate()
	{
		// リソースの作成
		CreateResourses();

		//初期位置などの設定
		auto Ptr = AddComponent<Transform>();
		Ptr->SetScale(100.0f, 70.0f, 0.0f);

		// スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(0.0f, 0.0f, 0.0f, 0.0));
		PtrSprite->SetTextureResource(L"BLACK_TX");

		//透明処理
		SetAlphaActive(true);
	}

	void Fade::OnUpdate()
	{
		// 経過時間を取得
		auto ElapsedTime = App::GetApp()->GetElapsedTime();

		// FadeIn が true のとき、アルファ値を下げて 不透明に
		if (FadeInActive == true)
		{
			GetComponent<PCTSpriteDraw>()->SetEmissive(Color4(1.0f, 1.0f, 1.0f, FadeInAlpha -= ElapsedTime / 2));
		}

		// FadeOut が true のとき、アルファ値を上げて 透明に
		if (FadeOutActive == true)
		{
			GetComponent<PCTSpriteDraw>()->SetEmissive(Color4(1.0f, 1.0f, 1.0f, FadeOutAlpha += ElapsedTime));
		}
	}
}
//endof  basedx11