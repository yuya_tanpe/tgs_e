#pragma once
#include "stdafx.h"
namespace basedx11{


	//敵基底クラス
	class NonMove_Enemy : public GameObject{
		shared_ptr< StateMachine<NonMove_Enemy> >  m_StateMachine;	//ステートマシーン

	public :
		NonMove_Enemy(const shared_ptr<Stage>& StagePtr);
		virtual ~NonMove_Enemy(){}

		virtual void OnCreate() override;
		virtual void OnUpdate() override;
		
		//当たった時のアクション
		virtual void EnemyAction() = 0;
		//リフレッシュ関数
		virtual void Refresh() = 0;

		//ステートの初期化
		void ResetState();

		//アクセサ
		shared_ptr< StateMachine<NonMove_Enemy> > GetStateMachine() const{
			return m_StateMachine;
		}

		//ドキュメントから呼ばれる関数
		//プレイヤーとの衝突したときに呼ぶ
		void GoPlayerKillMotion();
		//このオブジェクトがリフレッシュステートかの取得
		bool IsRefreshAbled();

		//-----------------------------------------
		//---------Player_NoHitStateで使用Motion----------
		//-----------------------------------------
		//最初のステートでの動き
		void ExecFirstMotion();

		//-----------------------------------------
		//---------PlayerKillStateで使用Motion----------
		//-----------------------------------------
		void StartHitMotion();
		//bool ExecHitMotion();
		void EndHitMotion();

		
	};

	//--------------------------------------------------------------------------------------
	//	class Player_NoHitState : public ObjState<Enemy>;
	//	用途: プレイヤーと当たってない時
	//--------------------------------------------------------------------------------------
	class Player_NoHitState : public ObjState<NonMove_Enemy>
	{
		Player_NoHitState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<Player_NoHitState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<NonMove_Enemy>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<NonMove_Enemy>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<NonMove_Enemy>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class PlayerKillState : public ObjState<Enemy>;
	//	用途: プレイヤーと当たった時
	//--------------------------------------------------------------------------------------
	class PlayerKillState : public ObjState<NonMove_Enemy>
	{
		PlayerKillState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<PlayerKillState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<NonMove_Enemy>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<NonMove_Enemy>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<NonMove_Enemy>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class Refresh_NonEnemyState : public ObjState<Enemy>;
	//	用途: リフレッシュ待ちステート
	//--------------------------------------------------------------------------------------
	class Refresh_NonEnemyState : public ObjState<NonMove_Enemy>
	{
		Refresh_NonEnemyState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<Refresh_NonEnemyState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<NonMove_Enemy>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<NonMove_Enemy>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<NonMove_Enemy>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class KillEnemyState : public ObjState<Enemy>;
	//	用途: プレイヤーに倒されたとき
	//--------------------------------------------------------------------------------------
	class KillEnemyState : public ObjState<NonMove_Enemy>
	{
		KillEnemyState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<KillEnemyState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<NonMove_Enemy>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<NonMove_Enemy>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<NonMove_Enemy>& Obj)override;
	};

	/**
	* @class Needle_Enemy
	* @brief とげとげの敵		Enemyクラスを継承
	* @author　sike yuya
	* @date	2016/04/18
	*/
	class Needle_Enemy : public NonMove_Enemy
	{

	public:
		Needle_Enemy(const shared_ptr<Stage>& StagePtr);
		virtual ~Needle_Enemy(){}
		//作成
		virtual void OnCreate() override;

		//敵とプレイヤーが当たった時　　純粋仮想関数
		virtual void EnemyAction() override;
		//リフレッシュ関数	純粋仮想関数
		virtual void Refresh() override;
	};


	/*
	* @class Lance_Enemy
	* @brief 直進してくる敵
	* @author　sike yuya
	* @date	2016/06/21
	*/
	class Lance_Enemy : public NonMove_Enemy{
	public:
		//構築と破棄
		Lance_Enemy(const shared_ptr<Stage>& StagePtr);
		virtual ~Lance_Enemy(){}
		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		//敵とプレイヤーが当たった時　　純粋仮想関数
		virtual void EnemyAction() override;
		//リフレッシュ関数	純粋仮想関数
		virtual void Refresh() override;
	};
	

	/**
	* @class Bomb_Enemy
	* @brief 爆弾の敵
	* @author　sike yuya
	* @date	2016/06/21
	*/
	class Bomb_Enemy : public NonMove_Enemy {
		
	public:
		//構築と破棄
		Bomb_Enemy(const shared_ptr<Stage>& StagePtr);
		virtual ~Bomb_Enemy(){}
		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		//敵とプレイヤーが当たった時　　純粋仮想関数
		virtual void EnemyAction() override;
		//リフレッシュ関数	純粋仮想関数
		virtual void Refresh() override;
	};
}
//endof  basedx11
