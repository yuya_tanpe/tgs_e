#pragma once

#include "stdafx.h"

namespace basedx11{
	//--------------------------------------------------------------------------------------
	//	class GameStage : public Stage;
	//	用途: ゲームステージクラス
	//--------------------------------------------------------------------------------------
	class GameStage : public Stage{
		//ステートマシーン
		shared_ptr< StateMachine<GameStage> >  m_StateMachine;
		//経過時間
		float m_totaltime;
		//最初の画像経過を数える
		int readycount;
		//最後のスプライトを表示したら処理しないようにする
		bool IsStart;

		//リソースの作成と登録
		void CreateResourses();
		//ビューの作成
		void CreateViews();
		//自身の分身になるキャラクターの作成
		void CreatePlayer();
		//雲ステージの作成
		void CreateWhiteCloud();
		//ユーザーが操作するバーの作成
		void CreateInput_bar();
		//エフェクト作成
		void CreateEffect();
		//背景の作成
		void CreateBackGround();
		//デバッグ表示
		void DebugOutput();
		//文字列コンポーネントの追加
		void AddCompoString();
		//スコア関連の作成
		void CreateScoreSprite();
		// スコアテクスチャの作成
		void CreateScoreTexture();
		//ボーナススコアの作成
		void CreateCoinSprite();
		// コインスプライトの作成
		void CreateCoinUI();
		//スプライトの作成
		void CreateReadySprite();
		//雲の作成
		void CreateCloud();
		//アイテムの作成
		void CreateItem();
		//ドキュメントとビューの作成
		void CreateGameDocumentView();
		//看板の作成
		void CreateKanban();
		//羊の作成
		void CreateSheep();
		//オブジェクトグループの作成
		void CreateGroupVec();

	public:
		//構築と破棄
		GameStage();
		virtual ~GameStage(){}
		//初期化
		virtual void OnCreate()override;
		//変化
		virtual void OnUpdate()override;
		//ランダムな値を返す	( 第1引数 最小値  第2引数 最大値 )
		int RandToint(int min, int max);
		//落ちたエフェクト用　小さいブロック
		void CreateEffectBox(Vector3 Pos);
		//ゲームオーバースプライトの作成
		void CreateGameOverSprite();
		//花火を出す
		void OneLeftFireWorks();
		//右側に花火を出す
		void OneRightFireWorks();

		// ---------- Motion -----------.
		//待ち時間
		bool WaitTime();
		//プレイヤーのステートを切り替え　CloudSearchStateに移行
		void Player_ChangeState();

		// ---------- Accessor ----------.

		//ゲームスタートしたかを取得
		bool IsStartTF(){
			return IsStart;
		}

		//ステートマシンの取得
		shared_ptr< StateMachine<GameStage> > GetStateMachine() const{
			return m_StateMachine;
		}

		//---------------CreateReadySpriteで使用するMotion-----------------

		//時間測定
		bool waittime();
		//時間が経過したら画像を変更する命令を送る
		void TextureChange();
		//画像を消す
		void fade();
		//ドキュメントへの命令
		void Is_DocumentUpdate(bool tf);
	};

	//--------------------------------------------------------------------------------------
	//	class GameReadyState : public ObjState<GameStage>;
	//	用途: ゲームスタート待ちステート
	//--------------------------------------------------------------------------------------
	class GameReadyState : public ObjState<GameStage>
	{
		GameReadyState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<GameReadyState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<GameStage>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<GameStage>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<GameStage>& Obj)override;
	};
	//--------------------------------------------------------------------------------------
	//	class PlayGameState : public ObjState<GameStage>;
	//	用途: ゲーム中ステート
	//--------------------------------------------------------------------------------------
	class PlayGameState : public ObjState<GameStage>
	{
		PlayGameState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<PlayGameState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<GameStage>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<GameStage>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<GameStage>& Obj)override;
	};
}
//endof  basedx11
