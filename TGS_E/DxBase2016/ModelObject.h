#pragma once

#include "stdafx.h"

namespace basedx11
{
	// モデル：カモメ
	class KamomeModel : public GameObject
	{
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;

	public:
		KamomeModel(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
			);

		void OnCreate();
	};

	// モデル：魚
	class FishModel : public GameObject
	{
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;

	public:
		FishModel(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
			);

		void OnCreate();
	};

	// モデル：アイテム
	class ItemModel : public GameObject
	{
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;

	public:
		ItemModel(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
			);

		void OnCreate();
	};

	// モデル：クジラ�@
	class KujiraModel_1 : public GameObject
	{
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;

	public:
		KujiraModel_1(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
			);

		void OnCreate();
	};

	// モデル：クジラ�A
	class KujiraModel_2 : public GameObject
	{
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;

	public:
		KujiraModel_2(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
			);

		void OnCreate();
	};

	// モデル：クジラの子
	class KujiraChildModel : public GameObject
	{
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;

	public:
		KujiraChildModel(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
			);

		void OnCreate();
	};

	// モデル：ピラミッド
	class PiramidModel : public GameObject
	{
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;

	public:
		PiramidModel(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
			);

		void OnCreate();
	};

	// モデル：土
	class TutiModel : public GameObject
	{
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;

	public:
		TutiModel(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
			);

		void OnCreate();
	};

	// モデル：木
	class WoodModel : public GameObject
	{
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;

	public:
		WoodModel(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
			);

		void OnCreate();
	};

	// モデル：ヨット
	class YottoModel : public GameObject
	{
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;

	public:
		YottoModel(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
			);

		void OnCreate();
	};
}
//endof  basedx11
