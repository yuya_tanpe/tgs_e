#pragma once

#include "stdafx.h"

namespace basedx11
{
	class ResultSprite : public GameObject {
		Vector3 m_StartPos;
		wstring m_Texname;
		//頂点の配列の配列（2次元配列）
		vector< vector<VertexPositionColorTexture> > m_NumberVertexVec;
	public:
		//構築と破棄
		ResultSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos, const wstring& TexName);
		virtual ~ResultSprite();

		//初期化
		virtual void OnCreate() override;

		//スコアの描画
		void ScoreDraw(int score);
	};

	//リザルトのスコアマネージャー
	class ResultScoreManager : public GameObject
	{
	private:
		shared_ptr<ResultSprite> m_KURAI1;
		shared_ptr<ResultSprite> m_KURAI10;
		shared_ptr<ResultSprite> m_KURAI100;
		shared_ptr<ResultSprite> m_KURAI1000;

		//桁の管理
		int place;

		//桁の保存配列
		int place_array[4];

		void scoreUpdate(const int Score_1, const int Score_10, const int Score_100);

	public:
		ResultScoreManager(shared_ptr<Stage>& StagePtr, shared_ptr<ResultSprite> KURAI_1, shared_ptr<ResultSprite> KURAI_10,
			shared_ptr<ResultSprite> KURAI_100, shared_ptr<ResultSprite> KURAI_1000);
		~ResultScoreManager(){}

		virtual void OnCreate() override;
		
		//スコアの描画
		void ScoreDraw(int num);
		void ScoreUpdate(const int score_1, const int score_10, const int score_100, const int score_1000);
	};
}
//endof  basedx11