#pragma once

#include "stdafx.h"

namespace basedx11
{
	class StageSelectScene : public Stage
	{
		void CreateSelectSprite();

		//仮実装
		//void CreateButton();

		void CreateResourses();

		//ビューの作成
		void CreateViews();

		void CreateGameSelectUI();

	public:
		//構築と破棄
		StageSelectScene() :Stage(){}
		virtual ~StageSelectScene(){}

		//初期化
		virtual void OnCreate()override;

		//更新
		virtual void OnUpdate()override;
	};
}
//endof  basedx11