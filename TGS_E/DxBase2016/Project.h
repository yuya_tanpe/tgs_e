#pragma once

enum class CharacterName{
	None,
	Player,
	BonusItem,
	BlowEnemy,
	HideEnemy,
};

enum class CloudName{
	None,
	White,
	Thunder,
	BonusItem,
	FlyItem,
	Enemy_NonMove,
	Enemy_Straight,
	Enemy_Blow,
};

enum class Direction{
	Front,
	Back,
	Right,
	Left,
};

#include "resource.h"
#include "Common.h"
#include <random>
//-----シーン関連--------
#include "Scene.h"
#include "GameClearScene.h"
#include "StageSelectScene.h"
#include "TitleScene.h"
#include "GameOverScene.h"
#include "GameStage.h"
#include "CustomDraw.h"
//------コンポーネント関連
#include "original_component.h"
//-----UI.エフェクト関連
#include "TitleUI.h"
#include "SceneEffect.h"
#include "SpriteObject.h"
#include "StageSelectUI.h"
#include "GameStageUI.h"
#include "GameStageObject.h"
#include "GameOverUI.h"
#include "GameEffect.h"
#include "GameEffect2.h"
//------敵関連-------------
#include "Enemy_Character.h"
#include "Blow_Enemy.h"
#include "Hide_Enemy.h"
//----オブジェクト関連----
#include "Cloud.h"
#include "input_bar.h"
#include "Character.h"
#include "Sheep.h"
#include "Kanban.h"
//-----スコア、コイン関連----
#include "ScoreSprite.h"
#include "CoinManager.h"
#include "GameSystem.h"
#include "CustomCamera.h"
#include "TutorialScene.h"
#include "TutorialUI.h"
#include "ModelObject.h"
#include "Bonus_Item.h"
//-----システム関連--------
#include "GameDocument_Sub.h"
#include "GameDocument.h"
#include "GameView.h"
