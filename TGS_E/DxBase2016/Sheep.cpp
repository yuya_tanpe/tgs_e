#include "stdafx.h"
#include "Project.h"

namespace basedx11{
	/**
	* @class Sheep
	* @brief 羊
	* @author　sike yuya
	* @date	2016/06/16
	*/
	Sheep::Sheep(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr),
		m_CntlLock(false),
		m_waittime(0.0f),
		m_AnimeDirection(Direction::Front),
		m_AnimeTime(0),
		m_FrontAnimeTime(0),
		Is_DocumentUpdate(false),
		Is_FrontEnemy(false),
		SheepLife(3),
		Is_Thunder(false)
	{
		m_LocalAnimeMatrix.Identity();
	}

	Sheep::~Sheep(){
		GetComponent<MultiSoundEffect>()->Stop(L"GAMEBGM");
	}

	void Sheep::OnCreate(){
		//文字列をつける
		auto PtrString = AddComponent<StringSprite>();
		PtrString->SetText(L"");
		PtrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));

		//自分は描画しない
		SetDrawActive(false);
		GetStage()->SetSharedGameObject(L"Sheep", GetThis<Sheep>());
		//行列操作するのでTransformMatrixを使う
		Matrix4X4 WorldMat;
		WorldMat.DefTransformation(
			Vector3(0.6f, 0.3f, 0.6f),
			Vector3(0.0f, 0.0f, 0.0f),
			Vector3(0.0f, 0.0f, 0.0f)
			);
		GetComponent<TransformMatrix>()->SetWorldMatrix(WorldMat);

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(1.0f, 1.0f, 1.0f),
			Vector3(0.0f, XM_PI, 0.0f),
			Vector3(0.0f, -0.5f, 0.0f)
			);

		//影をつける（シャドウマップを描画する）
		auto ShadowPtr = AddComponent<Shadowmap>();
		//影の形（メッシュ）を設定
		ShadowPtr->SetMeshResource(L"KUMO_MESH");
		ShadowPtr->SetMeshToTransformMatrix(SpanMat);

		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<PNTStaticModelDraw>();
		//描画するメッシュを設定
		PtrDraw->SetMeshResource(L"KUMO_MESH");
		PtrDraw->SetMeshToTransformMatrix(SpanMat);
		//透明処理
		SetAlphaActive(true);

		//CustomCameraCameraを使用し、プレイヤーを注視するようにする
		auto PtrCamera = dynamic_pointer_cast<CustomCamera>(GetStage()->GetCamera(0));
		if (PtrCamera){
			//MyCameraに注目するオブジェクト（羊）に設定
			PtrCamera->SetTargetObject(GetThis<GameObject>());
		}

		//SEの登録
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Walk");
		pMultiSoundEffect->AddAudioResource(L"WATER");
		pMultiSoundEffect->AddAudioResource(L"THENDER");
		pMultiSoundEffect->AddAudioResource(L"GAMEBGM");
		pMultiSoundEffect->AddAudioResource(L"JUMP");
		pMultiSoundEffect->AddAudioResource(L"NOTMOVE2"); 
		pMultiSoundEffect->AddAudioResource(L"FALLBGM"); 

		pMultiSoundEffect->Start(L"GAMEBGM", 1, 0.3f);


		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<Sheep> >(GetThis<Sheep>());
		//最初のステートをSheep_WaitTimeStateに設定
		m_StateMachine->ChangeState(Sheep_WaitTimeState::Instance());
	}

	void Sheep::OnUpdate(){
		//ステートマシンのUpdateを行う
		m_StateMachine->Update();
	}
	//デバッグ表示
	void Sheep::OnLastUpdate(){
		//ドキュメント取得
		auto DocumentPtr = GetStage()->GetSharedGameObject<GameDocument>(L"GameDocument");

		wstring WstrDir{ L"プレイヤーの進行向き" };
		switch (m_AnimeDirection)
		{
		case Direction::Front:
			WstrDir += L"前向き";
			break;
		case Direction::Right:
			WstrDir += L"右向き";
			break;
		case Direction::Left:
			WstrDir += L"左向き";
			break;
		default:
			break;
		}
		WstrDir += L"\n";

		wstring dir{ L"コントローラー : " };
		if (m_CntlLock){
			dir += L"入力できません";
		}
		else{
			dir += L"入力可能";
		}
		dir += L"\n";

		wstring Statename(L"Statename : ");
		if (m_StateMachine->GetCurrentState() == Sheep_WaitTimeState::Instance()){
			Statename = L"Sheep_WaitTimeState";
		}
		else if (m_StateMachine->GetCurrentState() == Sheep_FrontMoveState::Instance()){
			Statename = L"Sheep_FrontMoveState";
		}
		else if (m_StateMachine->GetCurrentState() == Sheep_SideMoveState::Instance()){
			Statename = L"Sheep_SideMoveState";
		}
		else if (m_StateMachine->GetCurrentState() == Sheep_GameOverState::Instance()){
			Statename = L"Sheep_GameOverState";
		}
		Statename += L"\n";
		
		wstring WstrDocumentTime(L"Document__TotalTime : ");
		WstrDocumentTime += Util::FloatToWStr(DocumentPtr->GetTotalTime());
		WstrDocumentTime += L"\n";

		wstring WstrPlayerCell{ L"プレイヤーのセル位置 : " };
		WstrPlayerCell += L"\n";

		WstrPlayerCell += L"Xの値" + Util::IntToWStr(DocumentPtr->GetPlayerX());
		WstrPlayerCell += L"\n";
		WstrPlayerCell += L"Yの値" + Util::IntToWStr(DocumentPtr->GetPlayerY());
		WstrPlayerCell += L"\n";

		wstring CloudNumber{ L"ライフ :" };
		CloudNumber += Util::IntToWStr(SheepLife);
		CloudNumber += L"\n";

		wstring WstrSheepTime{ L"羊の合計時間 : " };
		WstrSheepTime += Util::FloatToWStr(m_waittime);
		WstrSheepTime += L"\n";


		//全部の文字列を合計する
		wstring str = WstrDir + dir + Statename + WstrDocumentTime + WstrPlayerCell + WstrSheepTime + CloudNumber;

		auto PtrString = GetComponent<StringSprite>();
		PtrString->SetStartPosition(Point2D<float>(10.0f, 120.0f));
		PtrString->SetFontColor(Color4(1.0f, 0.0f, 0.0f, 1.0f));
		//PtrString->SetText(str);
	}

	//-----------------------------
	//WaitTimeStateで使用するMotion
	//-----------------------------
	//何秒間か待つ
	bool Sheep::WaitTime(){
		auto DocumentPtr = GetStage()->GetSharedGameObject<GameDocument>(L"GameDocument");
		if (DocumentPtr->GetTotalTime() >= 1.0f){
				Is_DocumentUpdate = true;
				return true;
			}
			return false;
	}
	//右スティックによる向き変更
	size_t Sheep::GetControllerStick(){
		//コントローラーの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		//スティックがリセットされているか
		if (m_CntlLock){
			if (CntlVec[0].bConnected){
				if (abs(CntlVec[0].fThumbRY) >= 0.7f){
					//Y方向優先
					//上下は少し甘くする
					if (CntlVec[0].fThumbRY > 0.0f){
						m_CntlLock = false;
						return 3;
					}
					else{
						m_CntlLock = false;
						return 4;
					}
				}
				else if (abs(CntlVec[0].fThumbRX) >= 0.6f){
					//X方向
					if (CntlVec[0].fThumbRX < 0.0f){
						m_CntlLock = false;
						//右
						return 1;
					}
					else{
						m_CntlLock = false;
						//左
						return 2;
					}
				}
			}
		}
		else{
			//リセットされてない
			if (CntlVec[0].bConnected){
				if (CntlVec[0].fThumbRX == 0.0f && CntlVec[0].fThumbRY == 0.0f){
					m_CntlLock = true;
				}
			}
		}
		return 0;
	}
	//スティック入力による向き変え
	void Sheep::Change_SheepDirection(const size_t& num){
		switch (num)
		{
		case 1:
			//左入力
			NextLeftMove();
			break;
		case 2:
			//右入力
			NextRightMove();
			break;
		case 3:
			//上入力
			NextFrontMove();
			break;
		case 4:
			//下入力
			break;
		default:
			//入力なし
			break;
		}
	}
	//次に行く道を決定する（準備段階）
	void Sheep::NextFrontMove(){
		//進行方向を前に
		m_AnimeDirection = Direction::Front;
	}
	void Sheep::NextRightMove(){
		auto DocumentPtr = GetStage()->GetSharedGameObject<GameDocument>(L"GameDocument");
		bool flg = DocumentPtr->PlayerMapRightCheck();
		if (flg){
			//進行方向を右に
			m_AnimeDirection = Direction::Right;
		}
		else{
		}
	}
	void Sheep::NextLeftMove(){
		auto DocumentPtr = GetStage()->GetSharedGameObject<GameDocument>(L"GameDocument");
		bool flg = DocumentPtr->PlayerMapLeftCheck();
		if (flg){
			//進行方向を左に
			m_AnimeDirection = Direction::Left;
		}
		else{
			//進行方向を変更することができない
			GetComponent<MultiSoundEffect>()->Start(L"NOTMOVE2", 0, 0.3f);
		}
	}

	//ジャンプする方向
	bool Sheep::IsFrontEnemy(){
		auto DocumentPtr = GetStage()->GetSharedGameObject<GameDocument>(L"GameDocument");
		//敵のマップ参照
		auto& EnemyMap = DocumentPtr->GetEnemyPtrMap();
		auto NextZ = DocumentPtr->GetPlayerY() + 1;
		auto CellX = DocumentPtr->GetPlayerX();
		auto sh = EnemyMap[NextZ][CellX];
		if (sh == nullptr) return false;
		//敵がいたら
		if (sh->GetStateMachine()->GetCurrentState() == Player_NoHitState::Instance()){
			return true;
		}
		return false;
	}
	//プレイヤーがマップ上を動いたかどうか
	bool Sheep::IsPlayerMoveMotion(){
		auto DocumentPtr = GetStage()->GetSharedGameObject<GameDocument>(L"GameDocument");
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected){
			if (!m_CntlLock){
				//右スティック操作でスティック入力を受け取る
				if (CntlVec[0].fThumbRX >= 0.7f){
					//右
					m_CntlLock = true;
					bool flg = DocumentPtr->PlayerMapRightCheck();
					if (flg){
						m_AnimeDirection = Direction::Right;
					}
					return flg;
				}
				else if (CntlVec[0].fThumbRX <= -0.7f){
					//左
					m_CntlLock = true;
					bool flg = DocumentPtr->PlayerMapLeftCheck();
					if (flg){
						m_AnimeDirection = Direction::Left;
					}
					return flg;
				}
			}
			else{
				if (CntlVec[0].fThumbRX == 0.0f){
					m_CntlLock = false;
				}
			}
		}
		return false;
	}
	//下の雲から通れない雲ならゲームオーバー演出		
	bool Sheep::UnderCloud_SelectState(){
		auto DocumentPtr = GetStage()->GetSharedGameObject<GameDocument>(L"GameDocument");
		auto CloudNum = DocumentPtr->GetCloudMap()[DocumentPtr->GetPlayerY()][DocumentPtr->GetPlayerX()];
		switch (CloudNum)
		{
		case 1:
			return false;
		case 2:
			if (!Is_Thunder){
				GetComponent<MultiSoundEffect>()->Start(L"THENDER", 0, 0.4f);
				Is_Thunder = true;
			}
			return false;
		default:
			return true;
		}

	}
	//左右に敵がいたか
	void Sheep::IsEnemyRL(){
		auto DocumentPtr = GetStage()->GetSharedGameObject<GameDocument>(L"GameDocument");
		//敵のマップ参照
		auto& EnemyMap = DocumentPtr->GetEnemyPtrMap();
		size_t NextX, NextZ;
		//右
		if (m_AnimeDirection == Direction::Right){
			NextZ = DocumentPtr->GetPlayerY();
			NextX = DocumentPtr->GetPlayerX() + 1;
		}
		else{
			//左
			NextZ = DocumentPtr->GetPlayerY();
			NextX = DocumentPtr->GetPlayerX() - 1;
		}

		auto sh = EnemyMap[NextZ][NextX];
		if (sh == nullptr) return;
		//敵がいたら
		if (sh->GetStateMachine()->GetCurrentState() == Player_NoHitState::Instance()){
			SheepLife--;
		}
	}
	//変数のリセット
	void Sheep::Reset(){
		Is_Thunder = false;
	}

	//-----------------------------
	//GameOverStateで使用するMotion
	//-----------------------------
	//ドキュメント側に流れを止める命令
	void Sheep::NotUpdate_Document(){
		auto DocumentPtr = GetStage()->GetSharedGameObject<GameDocument>(L"GameDocument");
		//羊のライフが０だったら最後のラインを消す
		if (SheepLife == 0){
			DocumentPtr->LastLine_CloudDelete();
		}
		DocumentPtr->SetMapMove(false);
	}

	//---------------------------------------------------------------------------
	//----------------これ今は使えないから、改良しないといけない-------------------
	//プレイヤーの向きを変更する
	Vector3 Sheep::Return_Direction(Direction dir){
		switch (dir)
		{
		case Direction::Front:
			return Vector3(0.0f, 0.0f / 180.0f * XM_PI, 0.0f);
			break;
		case Direction::Right:
			return Vector3(0.0f, 90.0f / 180.0f * XM_PI, 0.0f);
			break;
		case Direction::Left:
			return Vector3(0.0f, 270.0f / 180.0f * XM_PI, 0.0f);
			break;
		default:
			assert(!"不正な値が入りました。引数のDirectionの値を確認してください。");
			return Vector3(0.0f, 0.0f, 0.0f);
			break;
		}
	}
	//羊の向きの変更
	void Sheep::Change_Direction(Direction dir){
		auto PtrRotate = GetComponent<RotateTo>();
		PtrRotate->SetParams(0.35f, Return_Direction(dir));
		PtrRotate->Run();
	}

	//-----------------------------------------------------------
	//---------------Sheep_FrontMoveStateで使用------------------
	//-----------------------------------------------------------
	//プレイヤーのアニメーション開始
	void Sheep::FrontAnimeStartMotion(){
		auto DocumentPtr = GetStage()->GetSharedGameObject<GameDocument>(L"GameDocument");
		m_FrontAnimeTime = 0;
		//初速度（プラス方向）
		m_FrontGravVelocity = 4.0f;
		m_Grav = -8.0f;
		m_LocalAnimeMatrix.Identity();
		DocumentPtr->SetMapMove(false);

		//ジャンプする時の音
		GetComponent<MultiSoundEffect>()->Start(L"JUMP", 0, 0.3f);
	}
	//プレイヤーのアニメ更新と、アニメーション中かどうか
	bool Sheep::IsFrontAnimeMotion(){
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		m_FrontAnimeTime += 0.04f;

		Vector3 Pos = m_LocalAnimeMatrix.PosInMatrixSt();
		m_FrontGravVelocity += m_Grav * 0.04f;
		Pos.x = 0.0f;
		Pos.y += m_FrontGravVelocity * 0.04f;
		Pos.z = m_FrontAnimeTime;

		if (Pos.y <= 0){
			Pos.y = 0;
		}

		m_LocalAnimeMatrix.DefTransformation(
			Vector3(1.0f, 1.0f, 1.0f),
			Vector3(0, 0, 0),
			Pos
			);

		if (m_FrontAnimeTime <= 1.0f){
			return false;
		}
		return true;
	}
	//アニメーションの後処理
	void Sheep::FrontAnimeEndMotion(){
		auto DocumentPtr = GetStage()->GetSharedGameObject<GameDocument>(L"GameDocument");
		m_AnimeTime = 0;
		m_GravVelocity = 0.0f;
		m_LocalAnimeMatrix.Identity();
		DocumentPtr->SetMapMove(true);
		GetStage()->GetSharedGameObject<ScoreManager>(L"ScoreManager")->ScorePlus();
	}

	//------------------------------------------------------------
	//---------------Sheep_SideMoveStateで使用-------------------
	//------------------------------------------------------------
	//ジャンプするアニメーションのローカル計算
	void Sheep::PlayerAnimeStartMotion(){
		auto DocumentPtr = GetStage()->GetSharedGameObject<GameDocument>(L"GameDocument");
		m_AnimeTime = 0;
		//初速度（プラス方向）
		m_GravVelocity = 3.0f;
		m_Grav = -5.0f;
		m_LocalAnimeMatrix.Identity();
		DocumentPtr->SetMapMove(false);
		//横に飛ぶ音
		GetComponent<MultiSoundEffect>()->Start(L"Walk", 0, 0.3f);
	}
	bool Sheep::IsPlayerAnimeMotion(){
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		m_AnimeTime += 0.04f;

		Vector3 Pos = m_LocalAnimeMatrix.PosInMatrixSt();
		m_GravVelocity += m_Grav * 0.04f;
		Pos.y += m_GravVelocity * 0.04f;
		Pos.z = 0;

		switch (m_AnimeDirection){
		case Direction::Left:
			Pos.x = -m_AnimeTime;
			break;
		case Direction::Right:
			Pos.x = m_AnimeTime;
			break;
		}

		if (Pos.y <= 0){
			Pos.y = 0;
		}

		m_LocalAnimeMatrix.DefTransformation(
			Vector3(1.0f, 1.0f, 1.0f),
			Vector3(0, 0, 0),
			Pos
			);

		if (m_AnimeTime >= 1.2f){
			return false;
		}
		return true;
	}
	void Sheep::PlayerAnimeEndMotion(){
		auto DocumentPtr = GetStage()->GetSharedGameObject<GameDocument>(L"GameDocument");
		m_AnimeTime = 0.0f;
		m_GravVelocity = 0.0f;
		m_LocalAnimeMatrix.Identity();
		switch (m_AnimeDirection){
		case Direction::Left:
			DocumentPtr->SetPlayerMapLeft();
			break;
		case Direction::Right:
			DocumentPtr->SetPlayerMapRight();
			break;
		}
		DocumentPtr->SetMapMove(true);
	}

	//------------------------------------------------------------
	//---------------GameOverStateで使用-------------------
	//------------------------------------------------------------
	//落ちる時のアニメーションの関数
	void Sheep::FallAnimeStartMotion(){
		m_AnimeTime = 0;
		//初速度（プラス方向）
		m_GravVelocity = 0.0f;
		m_Grav = -2.0f;
		m_LocalAnimeMatrix.Identity();
		//落ちた時のBGM
		GetComponent<MultiSoundEffect>()->Start(L"FALLBGM", 0, 0.3f);
	}
	//落ちるアニメーションの更新
	bool Sheep::IsFallAnimeMotion(){
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		m_AnimeTime += 0.02f;

		Vector3 Pos = m_LocalAnimeMatrix.PosInMatrixSt();
		m_GravVelocity += m_Grav * 0.02f;
		Pos.x = -0.15f;
		Pos.y += m_GravVelocity * 0.02f;
		Pos.z = 0.0f;

		if (Pos.y <= -2.0f){
			Pos.y = -2.0f;
		}

		float Rot = 0.0f;
		Rot += XM_PI * ElapsedTime;

		m_LocalAnimeMatrix.DefTransformation(
			Vector3(1.0f, 1.0f, 1.0f),
			Vector3(0, Rot, 0),
			Pos
			);

		if (m_AnimeTime <= 1.0f){
			return false;
		}

		return true;
	}
	//落ちるアニメーションの終了
	void Sheep::FallAnimeEndMotion(){
		m_AnimeTime = 0;
		m_GravVelocity = 0.0f;
		m_LocalAnimeMatrix.Identity();
	}
	//ゲームオーバーに移行
	void Sheep::MoveGameOverScene(){
		//BGMのストップ
		GetComponent<MultiSoundEffect>()->Stop(L"GAMEBGM");
		PostEvent(4.5f, GetThis<Sheep>(), App::GetApp()->GetSceneBase(), L"ToGameOver");
	}
	//ゲームオーバーの演出
	void Sheep::AddObj_NotDrawBar(){
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		GameStagePtr->CreateGameOverSprite();
		GameStagePtr->CreateEffectBox(GetComponent<TransformMatrix>()->GetWorldMatrix().PosInMatrix());
	}

	//何秒間か待つ
	bool Sheep::ThuderWaitTime(){
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		m_AnimeTime += ElapsedTime;
		if (m_AnimeTime >= 0.8f){
			return true;
		}
		return false;
	}

	//------------------------------------------------------------
	//---------------Sheep_DamegeStateで使用-------------------
	//------------------------------------------------------------
	//ダメージを受けた時のアニメーションの関数
	void Sheep::DamegeAnimeStartMotion(){
		auto DocumentPtr = GetStage()->GetSharedGameObject<GameDocument>(L"GameDocument");
		m_FrontAnimeTime = 0;
		//初速度（プラス方向）
		m_FrontGravVelocity = 4.0f;
		m_Grav = -8.0f;
		m_LocalAnimeMatrix.Identity();
		//羊のライフを１減らす
		SheepLife--;
		//流れを止める
		DocumentPtr->SetMapMove(false);
		//横に飛ぶ音
		GetComponent<MultiSoundEffect>()->Start(L"Walk", 0, 0.3f);
	}

	//ダメージを受けた時のアニメーションの更新
	bool Sheep::IsDamegeAnimeMotion(){
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		m_FrontAnimeTime += 0.04f;
		Vector3 Pos = m_LocalAnimeMatrix.PosInMatrixSt();

		if (m_FrontAnimeTime >= 0.5f ){
			m_FrontGravVelocity += m_Grav * 0.04f;
			Pos.x = 0.0f;
			Pos.y += m_FrontGravVelocity * 0.04f;
			Pos.z -= 0.04f;
		}
		else{
			m_FrontGravVelocity += m_Grav * 0.04f;
			Pos.x = 0.0f;
			Pos.y += m_FrontGravVelocity * 0.04f;
			Pos.z = m_FrontAnimeTime;
		}

		if (Pos.y <= 0){
			Pos.y = 0;
		}

		m_LocalAnimeMatrix.DefTransformation(
			Vector3(1.0f, 1.0f, 1.0f),
			Vector3(0, 0, 0),
			Pos
			);

		if (m_FrontAnimeTime >= 1.0f){
			return true;
		}
		return false;
	}
	//ダメージを受けた時のアニメーションの終了
	void Sheep::DamegeAnimeEndMotion(){
		auto DocumentPtr = GetStage()->GetSharedGameObject<GameDocument>(L"GameDocument");
		m_AnimeTime = 0;
		m_GravVelocity = 0.0f;
		m_LocalAnimeMatrix.Identity();
		DocumentPtr->SetMapMove(true);

	}

	//羊のライフが０だったらTrueを返却
	bool Sheep::IsSheeoLife_zero(){
		if (SheepLife == 0){ 
			return true; 
		}
		
		return false;
	}

	//--------------------------------------------------------------------------------------
	//	class  Sheep_WaitTimeState : public ObjState<Sheep>;
	//	用途: 待っているステート
	//--------------------------------------------------------------------------------------
	shared_ptr<Sheep_WaitTimeState> Sheep_WaitTimeState::Instance(){
		static shared_ptr<Sheep_WaitTimeState> instance;
		if (!instance){
			instance = shared_ptr<Sheep_WaitTimeState>(new Sheep_WaitTimeState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void Sheep_WaitTimeState::Enter(const shared_ptr<Sheep>& Obj){}
	//ステート実行中に毎ターン呼ばれる関数
	void Sheep_WaitTimeState::Execute(const shared_ptr<Sheep>& Obj){
		//移動しおわった最初に雲の判定をとってステートを判断
		if(Obj->UnderCloud_SelectState()){
			Obj->GetStateMachine()->ChangeState(Sheep_GameOverState::Instance());
			return;
		}

		//入力ない場合、まっすぐ進む
		if (Obj->WaitTime()){
			if (Obj->IsFrontEnemy()){
				Obj->GetStateMachine()->ChangeState(Sheep_DamegeState::Instance());
			}
			else{
				Obj->GetStateMachine()->ChangeState(Sheep_FrontMoveState::Instance());
			}
		}

		//左右の入力したとき
		if (Obj->IsPlayerMoveMotion() && !Obj->IsThunder()){	
			Obj->IsEnemyRL();
			Obj->GetStateMachine()->ChangeState(Sheep_SideMoveState::Instance());
		}
	}
	//ステートにから抜けるときに呼ばれる関数
	void Sheep_WaitTimeState::Exit(const shared_ptr<Sheep>& Obj){
		//変数初期化
		Obj->Reset();
	}

	//--------------------------------------------------------------------------------------
	//	class Sheep_FrontMoveState : public ObjState<Sheep>;
	//	用途: 正面移動（ジャンプしている）ステート
	//--------------------------------------------------------------------------------------
	shared_ptr<Sheep_FrontMoveState> Sheep_FrontMoveState::Instance(){
		static shared_ptr<Sheep_FrontMoveState> instance;
		if (!instance){
			instance = shared_ptr<Sheep_FrontMoveState>(new Sheep_FrontMoveState);
		}
		return instance;
	}
	void Sheep_FrontMoveState::Enter(const shared_ptr<Sheep>& Obj){
		Obj->FrontAnimeStartMotion();
	}
	void Sheep_FrontMoveState::Execute(const shared_ptr<Sheep>& Obj){
		if (Obj->IsFrontAnimeMotion()){
			//アニメーションが終了した
			Obj->GetStateMachine()->ChangeState(Sheep_WaitTimeState::Instance());
		}
	}
	void Sheep_FrontMoveState::Exit(const shared_ptr<Sheep>& Obj){
		Obj->FrontAnimeEndMotion();
	}
	//--------------------------------------------------------------------------------------
	//	class Sheep_SideMoveState : public ObjState<Sheep>;
	//	用途: 移動（ジャンプしている）ステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<Sheep_SideMoveState> Sheep_SideMoveState::Instance(){
		static shared_ptr<Sheep_SideMoveState> instance;
		if (!instance){
			instance = shared_ptr<Sheep_SideMoveState>(new Sheep_SideMoveState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void Sheep_SideMoveState::Enter(const shared_ptr<Sheep>& Obj){
		Obj->PlayerAnimeStartMotion();
	}
	//ステート実行中に毎ターン呼ばれる関数
	void Sheep_SideMoveState::Execute(const shared_ptr<Sheep>& Obj){
		if (!Obj->IsPlayerAnimeMotion()){
			//ステート変更
			Obj->GetStateMachine()->ChangeState(Sheep_WaitTimeState::Instance());
		}
	}
	//ステートにから抜けるときに呼ばれる関数
	void Sheep_SideMoveState::Exit(const shared_ptr<Sheep>& Obj){
		Obj->PlayerAnimeEndMotion();
		//前を進行するようにする
		Obj->NextFrontMove();
	}

	//--------------------------------------------------------------------------------------
	//	class Sheep_GameOverState : public ObjState<Sheep>;
	//	用途: 前に雲がないときに進んでしまったときのState
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<Sheep_GameOverState> Sheep_GameOverState::Instance(){
		static shared_ptr<Sheep_GameOverState> instance;
		if (!instance){
			instance = shared_ptr<Sheep_GameOverState>(new Sheep_GameOverState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void Sheep_GameOverState::Enter(const shared_ptr<Sheep>& Obj){
		//ドキュメント側に流れを止めるようにいう
		Obj->NotUpdate_Document();
		//落ちるアニメーションの準備
		Obj->FallAnimeStartMotion();
		//演出の作成
		Obj->AddObj_NotDrawBar();
	}
	//ステート実行中に毎ターン呼ばれる関数
	void Sheep_GameOverState::Execute(const shared_ptr<Sheep>& Obj){
		if (!Obj->IsFallAnimeMotion()){
			Obj->MoveGameOverScene();
		}
	}
	//ステートにから抜けるときに呼ばれる関数
	void Sheep_GameOverState::Exit(const shared_ptr<Sheep>& Obj){
		//値の初期化
		Obj->FallAnimeEndMotion();
	}

	//--------------------------------------------------------------------------------------
	//	class Sheep_DamegeState : public ObjState<Sheep>;
	//	用途: ダメージを受けたとき
	//--------------------------------------------------------------------------------------
	shared_ptr<Sheep_DamegeState> Sheep_DamegeState::Instance(){
		static shared_ptr<Sheep_DamegeState> instance;
		if (!instance){
			instance = shared_ptr<Sheep_DamegeState>(new Sheep_DamegeState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void Sheep_DamegeState::Enter(const shared_ptr<Sheep>& Obj){
		Obj->DamegeAnimeStartMotion();
	}
	//ステート実行中に毎ターン呼ばれる関数
	void Sheep_DamegeState::Execute(const shared_ptr<Sheep>& Obj){
		if (Obj->IsDamegeAnimeMotion()){
			if (Obj->IsSheeoLife_zero()){
				//羊のライフが0だったらゲームオーバー
				Obj->GetStateMachine()->ChangeState(Sheep_GameOverState::Instance());
				return;
			}
			Obj->GetStateMachine()->ChangeState(Sheep_WaitTimeState::Instance());
		}
	}
	//ステートにから抜けるときに呼ばれる関数
	void Sheep_DamegeState::Exit(const shared_ptr<Sheep>& Obj){
		Obj->DamegeAnimeEndMotion();
	}

	//--------------------------------------------------------------------------------------
	//	class  Sheep_ThunderState : public ObjState<Sheep>;
	//	用途: 痺れているステート
	//--------------------------------------------------------------------------------------
	shared_ptr<Sheep_ThunderState> Sheep_ThunderState::Instance(){
		static shared_ptr<Sheep_ThunderState> instance;
		if (!instance){
			instance = shared_ptr<Sheep_ThunderState>(new Sheep_ThunderState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void Sheep_ThunderState::Enter(const shared_ptr<Sheep>& Obj){}
	//ステート実行中に毎ターン呼ばれる関数
	void Sheep_ThunderState::Execute(const shared_ptr<Sheep>& Obj){
		if (Obj->ThuderWaitTime()){
			Obj->GetStateMachine()->ChangeState(Sheep_FrontMoveState::Instance());
		}
	}
	//ステートにから抜けるときに呼ばれる関数
	void Sheep_ThunderState::Exit(const shared_ptr<Sheep>& Obj){
	}
}
//endof  basedx11