#pragma once

#include "stdafx.h"

namespace basedx11{
	//--------------------------------------------------------------------------------------
	//	class GameDocument : public GameObject;
	//	用途: ドキュメントクラス
	//--------------------------------------------------------------------------------------
	class GameDocument : public GameObject{
		//雲の配列マップ		パターン込み
		vector<vector<UINT>> m_CloudMap;
		vector<vector<UINT>> m_CloudMap_A;
		vector<vector<UINT>> m_CloudMap_B;
		vector<vector<UINT>> m_CloudMap_C;
		vector<vector<UINT>> m_CloudMap_D;

		vector<vector<UINT>> m_CloudMap_E;
		vector<vector<UINT>> m_CloudMap_F;
		vector<vector<UINT>> m_CloudMap_G;
		vector<vector<UINT>> m_CloudMap_H;
		vector<vector<UINT>> m_CloudMap_I;
		vector<vector<UINT>> m_CloudMap_J;
		vector<vector<UINT>> m_CloudMap_K;
		vector<vector<UINT>> m_CloudMap_L;
		vector<vector<UINT>> m_CloudMap_M;
		vector<vector<UINT>> m_CloudMap_N;
		vector<vector<UINT>> m_CloudMap_O;

		vector<vector<UINT>> m_CloudMap_P;
		vector<vector<UINT>> m_CloudMap_Q;
		vector<vector<UINT>> m_CloudMap_R;
		vector<vector<UINT>> m_CloudMap_S;
		vector<vector<UINT>> m_CloudMap_T;
		vector<vector<UINT>> m_CloudMap_U;
		vector<vector<UINT>> m_CloudMap_V;
		vector<vector<UINT>> m_CloudMap_W;
		vector<vector<UINT>> m_CloudMap_X;
		vector<vector<UINT>> m_CloudMap_Y;
		vector<vector<UINT>> m_CloudMap_Z;


		//敵の配列マップ
		vector<vector<UINT>> m_EnemyMap_A;
		vector<vector<UINT>> m_EnemyMap_B;
		vector<vector<UINT>> m_EnemyMap_C;
		vector<vector<UINT>> m_EnemyMap_D;

		vector<vector<UINT>> m_EnemyMap_E;
		vector<vector<UINT>> m_EnemyMap_F;
		vector<vector<UINT>> m_EnemyMap_G;
		vector<vector<UINT>> m_EnemyMap_H;
		vector<vector<UINT>> m_EnemyMap_I;
		vector<vector<UINT>> m_EnemyMap_J;
		vector<vector<UINT>> m_EnemyMap_K;
		vector<vector<UINT>> m_EnemyMap_L;
		vector<vector<UINT>> m_EnemyMap_M;
		vector<vector<UINT>> m_EnemyMap_N;
		vector<vector<UINT>> m_EnemyMap_O;

		vector<vector<UINT>> m_EnemyMap_P;
		vector<vector<UINT>> m_EnemyMap_Q;
		vector<vector<UINT>> m_EnemyMap_R;
		vector<vector<UINT>> m_EnemyMap_S;
		vector<vector<UINT>> m_EnemyMap_T;
		vector<vector<UINT>> m_EnemyMap_U;
		vector<vector<UINT>> m_EnemyMap_V;
		vector<vector<UINT>> m_EnemyMap_W;
		vector<vector<UINT>> m_EnemyMap_X;
		vector<vector<UINT>> m_EnemyMap_Y;
		vector<vector<UINT>> m_EnemyMap_Z;

		//アイテムの配列マップ
		vector<vector<UINT>>  m_ItemMap_A;
		vector<vector<UINT>>  m_ItemMap_B;
		vector<vector<UINT>>  m_ItemMap_C;
		vector<vector<UINT>>  m_ItemMap_D;

		vector<vector<UINT>>  m_ItemMap_E;
		vector<vector<UINT>>  m_ItemMap_F;
		vector<vector<UINT>>  m_ItemMap_G;
		vector<vector<UINT>>  m_ItemMap_H;
		vector<vector<UINT>>  m_ItemMap_I;
		vector<vector<UINT>>  m_ItemMap_J;
		vector<vector<UINT>>  m_ItemMap_K;
		vector<vector<UINT>>  m_ItemMap_L;
		vector<vector<UINT>>  m_ItemMap_M;
		vector<vector<UINT>>  m_ItemMap_N;
		vector<vector<UINT>>  m_ItemMap_O;

		vector<vector<UINT>>  m_ItemMap_P;
		vector<vector<UINT>>  m_ItemMap_Q;
		vector<vector<UINT>>  m_ItemMap_R;
		vector<vector<UINT>>  m_ItemMap_S;
		vector<vector<UINT>>  m_ItemMap_T;
		vector<vector<UINT>>  m_ItemMap_U;
		vector<vector<UINT>>  m_ItemMap_V;
		vector<vector<UINT>>  m_ItemMap_W;
		vector<vector<UINT>>  m_ItemMap_X;
		vector<vector<UINT>>  m_ItemMap_Y;
		vector<vector<UINT>>  m_ItemMap_Z;

		//旗などの目印オブジェクトのマップ
		vector<vector<UINT>>  m_MarkMap;


		//新規で作ったコインマップ
		//TODO : コインから汎用できるように修正
		vector < vector< shared_ptr<Coin_S> >> m_CoinPtrMap;
		//新規で作った動かない敵用のマップ
		vector < vector< shared_ptr<NonMove_Enemy> >>m_EnemyPtrMap;

		//プレイヤーの配列マップ
		vector<vector<UINT>> m_PlayerMap;
		//入力バーの配列マップ
		vector<vector<UINT>> m_BarMap;

		//合計時間
		float m_TotalTime;

		//雲の左右移動時に使用するタイマー
		float m_RLMoveTime;

		//左右入力したか
		bool Is_StickRLMove;
		//どこの行を変更したか
		size_t ChangeRow;
		//右か左か
		bool Is_Right;

		//---------------ゲーム本体の設定---------------------
		//ゲームのスピード
		float m_GameSpeed;
		//マップを動かすか否か
		bool m_IsMapMove;

		//------------------プレイヤー関連-------------
		//プレイヤーのいる位置
		size_t m_PlayerX;
		size_t m_PlayerY;

		//-----------------生成パターン関連----------------------------
		//何行生成したか
		int LineNum;
		//現在のパターン数
		int PatternNum;
		//簡単なパターン
		int EasyList[24];

		//雲のマップ読み込むに使う
		map<int, vector<vector<UINT>> > m_CloudMapList;

		//敵のマップ読み込みに使う
		map<int, vector<vector<UINT>> > m_EnemyMapList;
		//アイテムの読み込みに使う
		map<int, vector<vector<UINT>> > m_ItemMapList;

		//ラップアラウンドしてくれる
		void wrap_around_LineNum();

		//生成パターンの作成
		void Create_PatternList();
		//次生成するパターンの１行を取得する
		vector<vector<UINT>> GetNextRow(map<int, vector<vector<UINT>> >& CreateList);

		//-------ドキュメントでの処理の統合-------------
		//ドキュメントの更新
		void AllDocumentUpdate();

		//コインが当たったか
		void IsHitCoin();
		//動かない敵と当たったか
		void IsNonEnemy();



	public:
		//構築と破棄
		GameDocument(const shared_ptr<Stage>& StagePtr);
		virtual ~GameDocument();

		//ゲームのスピードを取得する
		float GetGameSpeed()const{
			return m_GameSpeed;
		}

		//現在マップが動いているか取得
		const bool GetMapMove() const{
			return m_IsMapMove;
		}

		//流れを止めるか
		void SetMapMove(bool b){
			m_IsMapMove = b;
		}

		//-------------現在動かしてるマップ関連を取得する-------------
		//雲のマップ取得
		const vector<vector<UINT>>& GetCloudMap() const{
			return m_CloudMap;
		}

		//入力バーのマップ取得
		const vector<vector<UINT>>& GetBarMap() const{
			return m_BarMap;
		}

		//プレイヤーマップの取得
		const vector<vector<UINT>>& GetPlayerMap() const{
			return m_PlayerMap;
		}

		//アイテムマップの取得
		const vector<vector<UINT>>& GetMarkMap() const{
			return m_MarkMap;
		}

		//コインオブジェクトのポインター配列
		vector< vector< shared_ptr<Coin_S> > >& GetCoinPtrMap(){
			return m_CoinPtrMap;
		}

		//敵オブジェクトのポインター配列の取得
		vector< vector< shared_ptr<NonMove_Enemy> >>& GetEnemyPtrMap(){
			return m_EnemyPtrMap;
		}

		//入力バーの１行を設定する
		void SetBarMap_Row(size_t row, const vector<UINT>& rowVec){
			m_BarMap[row] = rowVec;
		}

		//１行の取得
		vector<UINT>& GetLineVec(size_t Row){
			return m_CloudMap[Row];
		}

		//トータルタイムを得る
		float GetTotalTime()const{
			return m_TotalTime;
		}

		//トータルタイムをリセットする
		void ResetTimer(){
			m_TotalTime = 0.0f;
		}

		//現在の時間取得
		float GetNowTime() const{
			return GetTotalTime() - App::GetApp()->GetElapsedTime();
		}

		//左右に動くための時間取得
		float GetRLMoveTime()const{
			return m_RLMoveTime;
		}

		//左右に動くための現在の時間を取得
		float GetRLMoveNowTime()const{
			return m_RLMoveTime - App::GetApp()->GetElapsedTime();
		}

		//コントローラーが左右入力したか取得
		const bool GetStickRLMove() const{
			return Is_StickRLMove;
		}

		//左右のスティック操作が行われた
		//TODO : 操作詰め込みすぎた
		void Active_StickRLMove(const size_t& row, const bool right){
			Is_StickRLMove = true;
			ChangeRow = row;
			Is_Right = right;
		}

		//変更した行数を取得
		const size_t GetChangeRow() const{
			return ChangeRow;
		}

		//Trueなら右入力
		const bool GetRLStick(){
			return Is_Right;
		}

		//プレイヤーのXの位置を取得
		const size_t GetPlayerX() const{
			return m_PlayerX;
		}

		//プレイヤーのYの位置を取得
		const size_t GetPlayerY() const{
			return m_PlayerY;
		}

		//コインのポインターを取得する
		shared_ptr<Coin_S> GetCoin();

		//動かない敵のポインターの取得
		shared_ptr<NonMove_Enemy> Get_NeedleEnemy();
		shared_ptr<NonMove_Enemy> Get_LanceEnemy();
		shared_ptr<NonMove_Enemy> Get_BombEnemy();

		//Itemマップを一つ進める
		void CoinMapNext();

		//敵マップを一つ進める
		void EnemyMapNext();

		//雲のマップを一つ進める
		void CloudNext();

		//雲配列１行を左右に移動させる
		void CloudMap_RightMove(const size_t& nowRow);
		//雲配列１行を左右に移動させる
		void CloudMap_LeftMove(const size_t& nowRow);

		//左右に動かすときのタイマースタート
		void SideMove_StartTimer(bool tf);

		//入力バーのマップを一つ進める
		void BarMapNext();

		//入力バーマップを上に更新
		void BarMap_Up(const size_t& nowRow);
		//入力バーマップを下に更新
		void BarMap_Down(const size_t& nowRow);

		//アイテムのマップをひとつ進める
		void NextMarkMap();

		//敵のマップをひとつ進める
		void NextEnemyMap();

		//Playerマップをプレイヤー位置に従って更新する
		void PlayerMapRefresh();
		//Playerマップを１つ動かすことができるかどうかをチェックする
		//動かせるならtrue
		bool PlayerMapLeftCheck()const;
		bool PlayerMapRightCheck()const;

		//Playerマップを１つ動かす
		void SetPlayerMapLeft();
		void SetPlayerMapRight();
		void SetPlayerMapFront();
		void SetPlayerMapBack();

		//雲マップの最後の１列だけ消去して落ちるようにする
		void LastLine_CloudDelete();

		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate() override;
		//最終更新
		virtual void OnLastUpdate()override;
	};
}
//endof  basedx11