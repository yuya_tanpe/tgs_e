#include "stdafx.h"
#include "Project.h"

namespace basedx11{
	/**
	* @class input_bar
	* @brief ユーザーが入力で上下に動かすオブジェクト
	* @author　sike yuya
	* @date	New 5/2
	*/
	//TODO : プレイヤーと同じ列の時、動かせないようにする
	//構築
	input_bar::input_bar(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr),
		m_InputStick(true),
		m_NowRow(1)
	{}

	//初期化
	void input_bar::OnCreate(){
		//描画処理は行わない
		SetDrawActive(false);
		GetStage()->SetSharedGameObject(L"input_bar", GetThis<input_bar>());

		//固定サイズ
		static const Vector3 barSize{ 7.0f, 0.2f, 0.8f };
		//Transform追加
		auto PtrTrans = AddComponent<Transform>();
		PtrTrans->SetPosition(Vector3(0.0f, 0.0f, 0.0f));
		PtrTrans->SetRotation(0, 0, 0);
		PtrTrans->SetScale(barSize);

		//描画コンポーネント
		//TODO : 入力バーのモデル作成
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetDiffuse(Color4(0.9f, 0.2f, 0.8f, 0.6f));
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		//透過処理
		SetAlphaActive(true);

		//バック表示の文字列をつける
		auto PtrString = AddComponent<StringSprite>();
		PtrString->SetText(L"");
		PtrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));

		// SEの再生
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"MOVERL");
		pMultiSoundEffect->AddAudioResource(L"NOTMOVE");
	}

	//変化
	void input_bar::OnUpdate(){
		//コントローラーの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		auto PtrGameStage = dynamic_pointer_cast<GameStage>(GetStage());
		auto PtrDocument = PtrGameStage->GetSharedGameObject<GameDocument>(L"GameDocument");
		assert(PtrDocument && "本来ありえないがドキュメントの取得に失敗した");
		//コントローラーの入力から命令を送る
		switch (GetControllerStick())
		{
		case 1:
			//左入力
			PtrDocument->CloudMap_LeftMove(m_NowRow);
			//ドキュメントに左右操作が行われたことを伝える
			PtrDocument->Active_StickRLMove(m_NowRow, false);
			//サウンドの再生
			GetComponent<MultiSoundEffect>()->Start(L"MOVERL", 0, 0.5f);
			break;
		case 2:
			//右入力
			PtrDocument->CloudMap_RightMove(m_NowRow);
			//ドキュメントに左右操作が行われたことを伝える
			PtrDocument->Active_StickRLMove(m_NowRow, true);
			//サウンドの再生
			GetComponent<MultiSoundEffect>()->Start(L"MOVERL", 0, 0.5f);

			break;
		case 3:
			//上入力
			if (m_NowRow <= 8){
				PtrDocument->BarMap_Up(m_NowRow);
				m_NowRow += 1;
				//上下移動の音変更
				GetComponent<MultiSoundEffect>()->Start(L"MOVERL", 0, 0.5f);
			}
			else{
				GetComponent<MultiSoundEffect>()->Start(L"NOTMOVE", 0, 0.5f);
			}
			break;
		case 4:
			//下入力
			if (m_NowRow != 0){
				PtrDocument->BarMap_Down(m_NowRow);
				m_NowRow -= 1;
				//上下移動の音変更
				GetComponent<MultiSoundEffect>()->Start(L"MOVERL", 0, 0.5f);
			}
			else{
				GetComponent<MultiSoundEffect>()->Start(L"NOTMOVE", 0, 0.5f);
			}
			break;
		default:
			//入力なし
			break;
		}
	}

	//最終更新
	void input_bar::OnLastUpdate(){
		wstring wstrRow{ L"Row : " };
		wstrRow += L"\n";
		wstrRow += L" " + Util::IntToWStr(m_NowRow) + L"\n";

		wstring str = wstrRow;

		//文字列をつける
		auto PtrString = GetComponent<StringSprite>();
		PtrString->SetStartPosition(Point2D<float>(10.0f, 10.0f));
		PtrString->SetFontColor(Color4(0.0f, 0.0f, 0.0f, 1.0f));
		//PtrString->SetText(str);
	}

	void input_bar::OnEvent(const shared_ptr<Event>& event)
	{
		if (event->m_MsgStr == L"OBJ_ACTIVE")
		{
			//SetUpdateActive(true);
		}

		if (event->m_MsgStr == L"OBJ_STOP")
		{
			//SetUpdateActive(false);
		}
	}

	//左スティック入力受付
	size_t input_bar::GetControllerStick(){
		auto PtrDocument = GetStage()->GetSharedGameObject<GameDocument>(L"GameDocument");
		//コントローラーの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		//スティックがリセットされているか
		if (m_InputStick){
			if (CntlVec[0].bConnected){
				if (abs(CntlVec[0].fThumbLY) >= 0.5f){
					//Y方向優先
					//上下は少し甘くする
					if (CntlVec[0].fThumbLY > 0.0f){
						m_InputStick = false;
						return 3;
					}
					else{
						m_InputStick = false;
						return 4;
					}
				}
				
				else if (abs(CntlVec[0].fThumbLX) >= 0.7f && PtrDocument->GetPlayerY() != m_NowRow){
					//X方向
					if (CntlVec[0].fThumbLX < 0.0f && PtrDocument->GetPlayerY() != m_NowRow){
						m_InputStick = false;
						return 1;
					}
					else{
						m_InputStick = false;
						return 2;
					}
				}
			}
		}
		else{
			//リセットされてない
			if (CntlVec[0].bConnected){
				if (CntlVec[0].fThumbLX == 0.0f && CntlVec[0].fThumbLY == 0.0f){
					m_InputStick = true;
				}
			}
		}
		//TODO : 移動できない音の実装
		return 0;
	}

	//サウンドの再生
	void input_bar::SoundPlay(wstring soundname, float volume){
		auto pMultiSoundEffect = GetComponent<MultiSoundEffect>();
		pMultiSoundEffect->Start(soundname, 0, volume);
	}

	/**
	* @class Tutorial_bar
	* @brief ユーザーが入力で上下に動かすオブジェクト
	* @author　sike yuya
	* @date	New 5/26
	*/
	Tutorial_bar::Tutorial_bar(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr), m_horizontal_line(0)
	{}

	//初期化
	void Tutorial_bar::OnCreate(){
		//固定サイズ
		static const Vector3 barSize{ 6.1f, 0.4f, 1.1f };
		float firstZPos = static_cast<float>(m_horizontal_line);
		//Transform追加
		auto PtrTrans = AddComponent<Transform>();
		PtrTrans->SetPosition(Vector3(2.0f, 0.0f, firstZPos));
		PtrTrans->SetRotation(0, 0, 0);
		PtrTrans->SetScale(barSize);

		//アクションを使って操作する
		AddComponent<Action>();

		//描画コンポーネント
		//TODO : 入力バーのモデル作成
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetDiffuse(Color4(0.4f, 0.3f, 1.0f, 0.8f));
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"WHITE_TX");
		//透過処理
		SetAlphaActive(true);
		//表示レイヤー
		SetDrawLayer(1);

		//でバック表示の文字列をつける
		auto PtrString = AddComponent<StringSprite>();
		PtrString->SetText(L"");
		PtrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));

		// SEの再生
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"MOVERL");
	}

	//変化
	void Tutorial_bar::OnUpdate(){
	}

	//チュートリアルでの動きをアクションを代用する
	void Tutorial_bar::StarAction(){
		auto pAction = GetComponent<Action>();
		auto pPos = GetComponent<Transform>()->GetPosition();
		pAction->AddMoveInterval(0.5f);
		pAction->AddMoveTo(0.8f, Vector3(pPos.x, pPos.y, pPos.z + 1.0f));
		pAction->AddMoveInterval(1.0f);
		pAction->AddMoveTo(0.8f, Vector3(pPos.x, pPos.y, 2.0f));
		pAction->SetLooped(false);
		pAction->Run();
	}

	void Tutorial_bar::Reset(){
		auto PtrTrans = GetComponent<Transform>();
		auto pAction = GetComponent<Action>();
		PtrTrans->SetPosition(Vector3(2.0f, 0.0f, 0.0f));
		pAction->AllActionClear();
	}
	//非表示と更新を止める
	void Tutorial_bar::SetDrawUpdate(const bool& TF){
		SetDrawActive(TF);
		SetUpdateActive(TF);
	}
}
//endof  basedx11