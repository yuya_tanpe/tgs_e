#pragma once

#include "stdafx.h"

namespace basedx11{
	/**
	* @class Sheep
	* @brief 羊
	* @author　sike yuya
	* @date	2016/06/16
	*/

	class Sheep : public GameObject{
		//ステートマシン
		shared_ptr< StateMachine<Sheep> >  m_StateMachine;	//ステートマシーン
		//スティック操作によるなんか
		bool m_CntlLock;
		//アニメーション時の移動方向
		Direction m_AnimeDirection;
		//待ち時間に使用
		float m_waittime;
		//ドキュメントのアップデートができるか
		bool Is_DocumentUpdate;
		//目の前に敵がいる場合
		bool Is_FrontEnemy;
		//羊のライフ
		size_t SheepLife;
		//雷の上にいるとき
		bool Is_Thunder;
		//-------羊を左右にジャンプさせるために必要な変数---------
		//アニメーションのためのローカル行列
		Matrix4X4 m_LocalAnimeMatrix;
		//アニメーションタイム
		float m_AnimeTime;
		//現在の速度
		float m_GravVelocity;
		//加速度
		float m_Grav;

		//------羊を前にジャンプさせるために必要な変数-------
		//アニメーションタイム
		float m_FrontAnimeTime;
		//現在の速度
		float m_FrontGravVelocity;

		//向きを変更する値の取得
		Vector3 Return_Direction(Direction dir);
		//羊の向きの変更
		void Change_Direction(Direction dir);

	public:
		//構築と破棄
		Sheep(const shared_ptr<Stage>& StagePtr);
		virtual ~Sheep();
		//アクセサ
		shared_ptr< StateMachine<Sheep> > GetStateMachine() const{
			return m_StateMachine;
		}
	
		//アニメーション用のローカルマトリックスの取得
		const Matrix4X4& GetLocalAnimeMatrix() const{
			return m_LocalAnimeMatrix;
		}

		//羊が更新する準備がおわったか
		const bool GetUpdateSheep() const{
			return Is_DocumentUpdate;
		}

		//初期値に
		void ResetBool(){
			Is_DocumentUpdate = false;
		}

		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate() override;
		//デバッグ表示
		virtual void OnLastUpdate() override;
		//-----------------------------
		//WaitTimeStateで使用するMotion
		//-----------------------------
		//待ち時間の計測
		bool WaitTime();
		//進行方向によってステートを切り替える
		void SelectState_Dir();
		//ジャンプする方向
		bool IsFrontEnemy();
		//下の雲から通れない雲ならゲームオーバー演出
		bool UnderCloud_SelectState();

		//右スティックによる向き変更
		size_t GetControllerStick();
		//羊の進行する向きを変更する
		void Change_SheepDirection(const size_t& num);
		//次に行く道を決定する（準備段階）
		void NextFrontMove();
		void NextRightMove();
		void NextLeftMove();
		//-----------------------------
		//Sheep_SideMoveStateで使用するMotion
		//-----------------------------
		//プレイヤーがマップ上を動いたかどうか
		bool IsPlayerMoveMotion();
		//プレイヤーのアニメーション開始
		void PlayerAnimeStartMotion();
		//プレイヤーのアニメ更新と、アニメーション中かどうか
		bool IsPlayerAnimeMotion();
		//プレイヤーのアニメーション終了
		void PlayerAnimeEndMotion();
		//左右に敵がいたか
		void IsEnemyRL();
		//雷か
		const bool IsThunder(){
			return Is_Thunder;
		}
		void Reset();
		//-----------------------------
		//Sheep_FrontMoveStateで使用するMotion
		//-----------------------------
		//正面にジャンプする時の関数
		//プレイヤーのアニメーション開始
		void FrontAnimeStartMotion();
		//プレイヤーのアニメ更新と、アニメーション中かどうか
		bool IsFrontAnimeMotion();
		//プレイヤーのアニメーション終了
		void FrontAnimeEndMotion();

	
		//-----------------------------
		//GameOverStateで使用するMotion
		//-----------------------------
		//ドキュメント側に流れを止める命令
		void NotUpdate_Document();
		//シーン移動する
		void MoveGameOverScene();
		//落ちる時のアニメーションの関数
		void FallAnimeStartMotion();
		//落ちるアニメーションの更新
		bool IsFallAnimeMotion();
		//落ちるアニメーションの終了
		void FallAnimeEndMotion();
		//ゲームオーバーの演出
		void AddObj_NotDrawBar();

		//-----------------------------
		//Sheep_DamegeStateで使用するMotion
		//-----------------------------
		//ダメージを受けた時のアニメーションの関数
		void DamegeAnimeStartMotion();
		//ダメージを受けた時のアニメーションの更新
		bool IsDamegeAnimeMotion();
		//ダメージを受けた時のアニメーションの終了
		void DamegeAnimeEndMotion();
		//羊のライフが０だったらTrueを返却
		bool IsSheeoLife_zero();
		bool ThuderWaitTime();
	};

	//--------------------------------------------------------------------------------------
	//	class  Sheep_WaitTimeState : public ObjState<Sheep>;
	//	用途: 待っているステート
	//--------------------------------------------------------------------------------------
	class Sheep_WaitTimeState : public ObjState<Sheep>
	{
		Sheep_WaitTimeState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<Sheep_WaitTimeState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Sheep>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Sheep>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Sheep>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class Sheep_FrontMoveState : public ObjState<Sheep>;
	//	用途: 正面移動（ジャンプしている）ステート
	//--------------------------------------------------------------------------------------
	class Sheep_FrontMoveState : public ObjState<Sheep>
	{
		Sheep_FrontMoveState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<Sheep_FrontMoveState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Sheep>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Sheep>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Sheep>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class Sheep_SideMoveState : public ObjState<Sheep>;
	//	用途: 左右移動（ジャンプしている）ステート
	//--------------------------------------------------------------------------------------
	class Sheep_SideMoveState : public ObjState<Sheep>
	{
		Sheep_SideMoveState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<Sheep_SideMoveState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Sheep>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Sheep>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Sheep>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class Sheep_GameOverState : public ObjState<Sheep>;
	//	用途: 前に雲がないときに進んでしまったときのState
	//--------------------------------------------------------------------------------------
	class Sheep_GameOverState : public ObjState<Sheep>
	{
		Sheep_GameOverState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<Sheep_GameOverState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Sheep>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Sheep>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Sheep>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class Sheep_DamegeState : public ObjState<Sheep>;
	//	用途: ダメージを受けたとき
	//--------------------------------------------------------------------------------------
	class Sheep_DamegeState : public ObjState<Sheep>
	{
		Sheep_DamegeState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<Sheep_DamegeState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Sheep>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Sheep>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Sheep>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class  Sheep_ThunderState : public ObjState<Sheep>;
	//	用途: 痺れているステート
	//--------------------------------------------------------------------------------------
	class Sheep_ThunderState : public ObjState<Sheep>
	{
		Sheep_ThunderState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<Sheep_ThunderState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Sheep>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Sheep>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Sheep>& Obj)override;
	};

}
//endof  basedx11
