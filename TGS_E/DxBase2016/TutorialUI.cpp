#include "stdafx.h"
#include "Project.h"

namespace basedx11
{
	/**
	* @class Tutorial_Frame
	* @brief チュートリアル画面のフレーム
	* @author yuya
	* @date 2016/05/26
	*/
	//構築と破棄
	Tutorial_Frame::Tutorial_Frame(shared_ptr<Stage>& StagePtr, const Vector3& StartPos, const bool DrawTF) :
		GameObject(StagePtr), m_StartPos(StartPos), m_DrawActive(DrawTF){
	}
	Tutorial_Frame::~Tutorial_Frame(){}

	//初期化
	void Tutorial_Frame::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(15.0f, 8.5f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//アクションの追加
		AddComponent<Action>();
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"FRAME_TX");
		//透明処理
		SetAlphaActive(true);
		//描画するか
		SetDrawActive(m_DrawActive);
		//左上原点
		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_CenterZeroPlusUpY);
	}

	//描画を行う
	void Tutorial_Frame::DrawActive(){
		//描画するか
		SetDrawActive(true);
	}

	/**
	* @class Tutorial_Word
	* @brief チュートリアル画面のフレーム
	* @author yuya
	* @date 2016/05/26
	*/
	//構築と破棄
	Tutorial_Word::Tutorial_Word(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos)
	{}
	Tutorial_Word::~Tutorial_Word(){}

	//初期化
	void Tutorial_Word::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(5.0f, 1.8f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//アクションの追加
		AddComponent<Action>();
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"HOWTOPLAY_TX");
		//透明処理
		SetAlphaActive(true);
		//左上原点
		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_CenterZeroPlusUpY);
	}

	/**
	* @class Tutorial_Controller
	* @brief チュートリアル画面のフレーム
	* @author yuya
	* @date 2016/05/26
	*/
	Tutorial_Controller::Tutorial_Controller(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos)
	{}
	Tutorial_Controller::~Tutorial_Controller(){}

	//初期化
	void Tutorial_Controller::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(4.0f, 4.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//アクションの追加
		AddComponent<Action>();
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"CONTROLLER_TX");
		//透明処理
		SetAlphaActive(true);
		//左上原点
		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_CenterZeroPlusUpY);
	}

	//画像の切り替えをする		//TODO : 画像を創ったらすぐ適応
	void Tutorial_Controller::StartAction_ChangeTexture(){
		auto PtrDraw = GetComponent<PCTSpriteDraw>();
		PtrDraw->SetTextureResource(L"L_STICK_TX");
	}

	//画像の切り替えをする		//TODO : 画像を創ったらすぐ適応
	void Tutorial_Controller::StartAction_ButtonTexture(){
		auto PtrDraw = GetComponent<PCTSpriteDraw>();
		PtrDraw->SetTextureResource(L"R_BUTTON_TX");
	}
	//画像の初期化
	void Tutorial_Controller::Reset(){
		auto PtrDraw = GetComponent<PCTSpriteDraw>();
		PtrDraw->SetTextureResource(L"CONTROLLER_TX");
	}
	// AボタンのUI
	Tutorial_AButton::Tutorial_AButton(shared_ptr<Stage>& StagePtr, const Vector3& Pos) :
		GameObject(StagePtr), m_Pos(Pos)
	{}
	Tutorial_AButton::~Tutorial_AButton()
	{}

	//初期化
	void Tutorial_AButton::OnCreate()
	{
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_Pos);
		PtrTransform->SetScale(2.0f, 1.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);

		//アクションの追加
		AddComponent<Action>();

		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"ABUTTON_TX");

		//透明処理
		SetAlphaActive(true);

		//左上原点
		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_CenterZeroPlusUpY);
	}

	void Tutorial_AButton::SetDrawUpdate(const bool& a)
	{
		SetDrawActive(a);
		SetUpdateActive(a);
	}

	// STARTボタンのUI
	Tutorial_StartButton::Tutorial_StartButton(shared_ptr<Stage>& StagePtr, const Vector3& Pos) :
		GameObject(StagePtr), m_Pos(Pos)
	{}
	Tutorial_StartButton::~Tutorial_StartButton()
	{}

	//初期化
	void Tutorial_StartButton::OnCreate()
	{
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_Pos);
		PtrTransform->SetScale(4.0f, 2.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);

		//アクションの追加
		AddComponent<Action>();

		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"STARTBUTTON_TX");

		//透明処理
		SetAlphaActive(true);

		//左上原点
		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_CenterZeroPlusUpY);
	}

	void Tutorial_StartButton::SetDrawUpdate(const bool& a)
	{
		SetDrawActive(a);
		SetUpdateActive(a);
	}

	// BボタンのUI
	Tutorial_BButton::Tutorial_BButton(shared_ptr<Stage>& StagePtr, const Vector3& Pos) :
		GameObject(StagePtr), m_Pos(Pos)
	{}
	Tutorial_BButton::~Tutorial_BButton()
	{}

	//初期化
	void Tutorial_BButton::OnCreate()
	{
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_Pos);
		PtrTransform->SetScale(2.0f, 1.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);

		//アクションの追加
		AddComponent<Action>();

		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"BBUTTON_TUTORIAL_TX");

		//透明処理
		SetAlphaActive(true);

		//左上原点
		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_CenterZeroPlusUpY);
	}

	void Tutorial_BButton::SetDrawUpdate(const bool& a)
	{
		SetDrawActive(a);
		SetUpdateActive(a);
	}

	/**
	* @class Tutorial_Setumei
	* @brief チュートリアル画面のフレーム
	* @author yuya
	* @date 2016/05/26
	*/
	//構築と破棄
	Tutorial_Setumei::Tutorial_Setumei(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos), m_texture(L"WORD_1_TX")
	{}
	Tutorial_Setumei::~Tutorial_Setumei(){}

	//初期化
	void Tutorial_Setumei::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(4.5f, 4.5f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//アクションの追加
		AddComponent<Action>();
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(m_texture);
		//透明処理
		SetAlphaActive(true);
		//左上原点
		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_CenterZeroPlusUpY);
	}

	//画像切り替え
	void Tutorial_Setumei::ChangeTexture(wstring wstr){
		auto PtrDraw = GetComponent<PCTSpriteDraw>();
		PtrDraw->SetTextureResource(wstr);
	}
}
//endof  basedx11