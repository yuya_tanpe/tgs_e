#pragma once

#include "stdafx.h"

namespace basedx11{
	/**
	* @class GameSystem
	* @brief ゲームステージに常駐するobject　主に命令を送るだけ
	* @author　sike yuya
	* @date	2016/05/11
	*/
	class GameSystem : public GameObject {
		// ---------- member ----------.

		//スコアを見て、命令をおくる　
		shared_ptr<ScoreManager> m_ScoreManagerPtr;

	public:
		GameSystem(shared_ptr<Stage>& StagePtr, const shared_ptr<ScoreManager> ptr_scoremanager);
		~GameSystem(){}

		virtual void OnCreate() override;
		//変化
		virtual void OnUpdate() override;
		//イベント取得
		virtual void OnEvent(const shared_ptr<Event>& event) override;
		// ---------- Accessor ----------.
	};
}
//endof  basedx11