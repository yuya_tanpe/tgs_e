#include "stdafx.h"
#include "Project.h"

namespace basedx11{
	/**
	* @class Sky_Fall
	* @brief 常に下に流れるコンポーネント
	* @author　sike yuya
	* @date	 2016/-4/17
	*/
	//TODO : Actionを使った移動に処理を書き換える
	Sky_Fall::Sky_Fall(const shared_ptr<GameObject>& GameObjectPtr) :
		Component(GameObjectPtr),
		m_speed(0.005f)
	{}
	Sky_Fall::~Sky_Fall(){}
	void Sky_Fall::OnUpdate(){
		float Time = App::GetApp()->GetElapsedTime();
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetGameObject()->GetStage());
		//TODO : 横に動く時、止めているけど遅くするだけでもいいかも
		auto ObjectTrans = GetGameObject()->GetComponent<Transform>();
		Vector3 ObjectPos = ObjectTrans->GetPosition();
		ObjectPos.z -= m_speed;
		ObjectTrans->SetPosition(ObjectPos);
	}
}
//endof  basedx11