#pragma once

#include "stdafx.h"

namespace basedx11{
	/**
	* @class CoinSprite
	* @brief コイン数表記 0 ~ 9までの表示
	* @author yuya
	* @date 2016/06/09
	*/
	class CoinSprite : public GameObject {
		Vector3 m_StartPos;
		//頂点の配列の配列（2次元配列）
		vector< vector<VertexPositionColorTexture> > m_NumberVertexVec;
		bool m_DrawActive;
	public:
		//構築と破棄
		CoinSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos, const bool DrawTF);
		virtual ~CoinSprite();
		//初期化
		virtual void OnCreate() override;
		//描画を行う
		void DrawActive();
		//スコアの描画
		void ScoreDraw(int score);
	};

	/**
	* @class CoinManager
	* @brief コインスプライトの管理更新を行う
	* @author yuya
	* @date 2016/06/09
	*/
	class CoinManager : public GameObject
	{
	private:
		//コインスプライトの管理
		shared_ptr<CoinSprite> m_KURAI1;
		shared_ptr<CoinSprite> m_KURAI10;
		shared_ptr<CoinSprite> m_KURAI100;
		shared_ptr<CoinSprite> m_KURAI1000;

		//Newスコア
		int Coin;
		//桁の管理
		int place;
		//桁の保存配列
		int place_array[4];

		//スコアスプライトの最終更新
		void CoinUpdate(const int score_1, const int score_10, const int score_100, const int score_1000);

	public:
		CoinManager(shared_ptr<Stage>& StagePtr, shared_ptr<CoinSprite> KURAI_1, shared_ptr<CoinSprite> KURAI_10, shared_ptr<CoinSprite> KURAI_100, shared_ptr<CoinSprite> KURAI_1000);
		~CoinManager(){
			auto PtrScene = dynamic_pointer_cast<Scene>(App::GetApp()->GetSceneBase());
			PtrScene->SetCoin(Coin);

		}
		//コインを１枚追加
		void Coin_1_Plus();
		//コインを１0枚追加
		void Coin_10_Plus();

		//スコア合計の取得
		int GetCoin(){
			return Coin;
		}
	};
}
//endof  basedx11