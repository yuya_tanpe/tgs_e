#include "stdafx.h"
#include "Project.h"

namespace basedx11{
	/**
	* @class Blow_Enemy
	* @brief オブジェクトを吹き飛ばす敵　（プレイヤーや敵など）
	* @author　sike yuya
	* @date	2016/06/04
	*/

	//Blow_Enemy::Blow_Enemy(const shared_ptr<Stage>& StagePtr, const Vector3& i_start_pos, Enemy::Cell& i_cell) :
	//	GameObject(StagePtr),
	//	m_Pos(i_start_pos),
	//	m_Cell(i_cell)
	//{}

	////初期化
	//void Blow_Enemy::OnCreate(){
	//	//Transformの設定
	//	auto PtrTrans = AddComponent<Transform>();
	//	PtrTrans->SetPosition(m_Pos);
	//	PtrTrans->SetRotation(45.0f, 0.0f, 45.0f);
	//	PtrTrans->SetScale(0.4f, 0.4f, 0.4f);

	//	//描画コンポーネントの設定
	//	auto PtrDraw = AddComponent<PNTStaticDraw>();
	//	//描画するメッシュを設定
	//	//TODO : モデル未作成
	//	PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
	//	//描画するテクスチャを設定
	//	PtrDraw->SetTextureResource(L"BLUE_TX");

	//	//アクションの追加
	//	AddComponent<Action>();
	//	//サウンドを登録.
	//	auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
	//	pMultiSoundEffect->AddAudioResource(L"Enemy");
	//	//ステートマシンの構築
	//	m_StateMachine = make_shared< StateMachine<Blow_Enemy> >(GetThis<Blow_Enemy>());
	//	//最初のステートをSearchStateに設定
	//	m_StateMachine->ChangeState(SearchState::Instance());
	//}

	////変化
	//void Blow_Enemy::OnUpdate(){
	//	m_StateMachine->Update();
	//	//位置の取得
	//	auto Pos = GetComponent<Transform>()->GetPosition();
	//	//デッドライン
	//	//この使用変えようかな　なんか微妙なきがする
	//	if (Pos.z < -2.5f){
	//		SetDrawActive(false);
	//		SetUpdateActive(false);
	//	}
	//}

	////自分の名前の登録
	//void Blow_Enemy::SetEnemyName(const CharacterName& name){
	//	m_EnemyName = name;
	//}

	////自分の作成された場所を配列に登録
	//void Blow_Enemy::SetName_CharacterVec(){
	//	auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
	//	assert(GameStagePtr && "ゲームステージの取得に失敗しました。");
	//	//キャラクター配列に自身を設定する		引数1 ~ 2 セル座標,　3 自分の名前
	//	GameStagePtr->SetCharacterName(m_Cell.m_Row, m_Cell.m_Col, m_EnemyName);
	//}

	////自身が消えたら配列から削除
	//void Blow_Enemy::Erase_CharacterVec(const Enemy::Cell& cell){
	//	auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
	//	assert(GameStagePtr && "ゲームステージの取得に失敗しました。");
	//	//キャラクター配列に自身を設定する		引数1 ~ 2 セル座標,　3 自分の名前
	//	GameStagePtr->SetCharacterName(cell.m_Row, cell.m_Col, CharacterName::None);
	//}

	////-----------SearchStateで使用するMotion-----------
	////上下左右のキャラクター配列を検索する
	//void Blow_Enemy::Search(){
	//	auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
	//	assert(GameStagePtr && "ゲームステージの取得に失敗しました。");
	//	//左右優先
	//	//左の検索
	//	//if (m_Cell.m_Col != 0){
	//	//	auto CharaName = GameStagePtr->GetCharacterName(m_Cell.m_Row , m_Cell.m_Col - 1);
	//	//	//なんかしらいたらステート変更
	//	//	if (CharaName != CharacterName::None && CharaName != CharacterName::BlowEnemy){
	//	//		m_Direction = Direction::Left;
	//	//		GetStateMachine()->ChangeState(BlowState::Instance());
	//	//	}
	//	//}
	//	////右の検索
	//	//if (m_Cell.m_Col != 4){
	//	//	auto CharaName = GameStagePtr->GetCharacterName(m_Cell.m_Row, m_Cell.m_Col + 1);
	//	//	//なんかしらいたらステート変更
	//	//	if (CharaName != CharacterName::None && CharaName != CharacterName::BlowEnemy){
	//	//		m_Direction = Direction::Right;
	//	//		GetStateMachine()->ChangeState(BlowState::Instance());
	//	//	}
	//	//}
	//	//
	//	////下の検索
	//	////下端なら下を検索しない
	//	//if (m_Cell.m_Row == 19){
	//	//	auto CharaName = GameStagePtr->GetCharacterName(m_Cell.m_Row - 1, m_Cell.m_Col);
	//	//	//なんかしらいたらステート変更
	//	//	if (CharaName != CharacterName::None && CharaName != CharacterName::BlowEnemy){
	//	//		m_Direction = Direction::Back;
	//	//		GetStateMachine()->ChangeState(BlowState::Instance());
	//	//	}
	//	//}

	//	//プレイヤーの取得
	//	auto PtrPlayer = GameStagePtr->GetSharedGameObject<Player_Character>(L"Player_Character");
	//	assert(PtrPlayer && "プレイヤーが取得できませんでした。Blow_Enemy.cpp");
	//	auto Pos = PtrPlayer->GetComponent<Transform>()->GetPosition();
	//	auto len = Vector3EX::Length(Pos - GetComponent<Transform>()->GetPosition());
	//	if (len < 3.0f){
	//		//下に打つ
	//		m_Direction = Direction::Back;
	//		GetStateMachine()->ChangeState(BlowState::Instance());
	//	}

	//	//else if (PlayerCell.m_Col == m_Cell.m_Col){
	//	//	if (PlayerCell.m_Row > m_Cell.m_Row){
	//	//		//左に打つ
	//	//		m_Direction = Direction::Left;
	//	//		GetStateMachine()->ChangeState(BlowState::Instance());
	//	//	}
	//	//	else{
	//	//		//右に打つ
	//	//		m_Direction = Direction::Right;
	//	//		GetStateMachine()->ChangeState(BlowState::Instance());
	//	//	}
	//	//}
	//}

	////下の雲のセル座標を取得し、自身に設定する
	//void Blow_Enemy::GetCloudCell_SetMyCell(){
	//	auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
	//	auto wpCloud = GameStagePtr->GetCloudObject(m_Cell.m_Row, m_Cell.m_Col);
	//	auto ShCloud = dynamic_pointer_cast<Cloud>(wpCloud.lock());
	//	assert(ShCloud && "雲オブジェクトが取得できませんでした。BlowEnemy.cpp");
	//	////下の雲からセル座標を取得する
	//	//auto CloudCellPtr = ShCloud->GetCloudCell();
	//	////セルの更新
	//	//m_Cell.m_Col = CloudCellPtr.m_Col;
	//	//m_Cell.m_Row = CloudCellPtr.m_Row;
	//	////右にあった
	//	////右の検索
	//	//if (m_Cell.m_Col != 4){
	//	//	if (CharacterName::BlowEnemy == GameStagePtr->GetCharacterName(m_Cell.m_Row, m_Cell.m_Col + 1)){
	//	//		Enemy::Cell workcell;
	//	//		workcell = m_Cell;
	//	//		workcell.m_Col += 1;
	//	//		Erase_CharacterVec(workcell);
	//	//	}
	//	//}
	//	//else{
	//	//	if (CharacterName::BlowEnemy == GameStagePtr->GetCharacterName(m_Cell.m_Row, m_Cell.m_Col - 4)){
	//	//		Enemy::Cell workcell;
	//	//		workcell = m_Cell;
	//	//		workcell.m_Col -= 4;
	//	//		Erase_CharacterVec(workcell);
	//	//	}
	//	//}
	//	//if (m_Cell.m_Col != 0){
	//	//	if (CharacterName::BlowEnemy == GameStagePtr->GetCharacterName(m_Cell.m_Row, m_Cell.m_Col - 1)){
	//	//		Enemy::Cell workcell;
	//	//		workcell = m_Cell;
	//	//		workcell.m_Col -= 1;
	//	//		Erase_CharacterVec(workcell);
	//	//	}
	//	//}
	//	//else{
	//	//	if (CharacterName::BlowEnemy == GameStagePtr->GetCharacterName(m_Cell.m_Row, m_Cell.m_Col + 4)){
	//	//		Enemy::Cell workcell;
	//	//		workcell = m_Cell;
	//	//		workcell.m_Col += 4;
	//	//		Erase_CharacterVec(workcell);
	//	//	}
	//	//}
	//}
	////-----------BlowStateで使用するMotion-----------
	////Directionによって、弾の出現させる位置を決める
	//Vector3 Blow_Enemy::ReturnVec3_Direction(){
	//	auto PtrPos = GetComponent<Transform>()->GetPosition();
	//	switch (m_Direction)
	//	{
	//	case Direction::Back:
	//		return PtrPos - Vector3(0.0f, 0.0f, 0.4f);
	//		break;
	//	case Direction::Right:
	//		return PtrPos - Vector3(0.4f, 0.0f, 0.0f);
	//		break;
	//	case Direction::Left:
	//		return PtrPos + Vector3(0.4f, 0.0f, 0.0f);
	//		break;
	//	default:
	//		assert(!"不正な値を検出しました。m_Directionの値を確認してください。Blow_Enemy.cpp");
	//		return PtrPos + Vector3(0, 0, 0);
	//		break;
	//	}
	//}
	////弾を作成する	弾の大きさは0.3fで固定
	//void Blow_Enemy::CreateBlow_Ball(){
	//	//ゲームステージの取得
	//	auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
	//	//自身のポジションから
	//	GameStagePtr->AddGameObject<Blow_Ball>(ReturnVec3_Direction(), m_Direction);
	//}
	////自身をDirectionの反対側に飛ばす
	//void Blow_Enemy::StartBlowAction(){
	//	auto PtrAction = GetComponent<Action>();
	//	PtrAction->AddMoveBy(0.5f, ReturnBlowVec());
	//	PtrAction->SetLooped(false);
	//	PtrAction->Run();
	//}
	////Directionを元に反対に自分を飛ばすVector3を返す
	////自身が飛ぶ方向の調整
	//Vector3 Blow_Enemy::ReturnBlowVec(){
	//	auto PtrPos = GetComponent<Transform>()->GetPosition();
	//	switch (m_Direction)
	//	{
	//	case Direction::Back:
	//		return PtrPos - Vector3(0.0f, 0.0f, 3.0f);
	//		break;
	//	case Direction::Right:
	//		return PtrPos + Vector3(3.0f, 0.0f, 0.0f);
	//		break;
	//	case Direction::Left:
	//		return PtrPos - Vector3(3.0f, 0.0f, 0.0f);
	//		break;
	//	default:
	//		assert(!"不正な値を検出しました。m_Directionの値を確認してください。Blow_Enemy.cpp");
	//		return Vector3(0, 0, 0);
	//		break;
	//	}
	//}
	////--------------------------------------------------------------------------------------
	////	class SearchState : public ObjState<Blow_Enemy>;
	////	用途: 探索ステート
	////--------------------------------------------------------------------------------------
	//shared_ptr<SearchState> SearchState::Instance(){
	//	static shared_ptr<SearchState> instance;
	//	if (!instance){
	//		instance = shared_ptr<SearchState>(new SearchState);
	//	}
	//	return instance;
	//}
	//void SearchState::Enter(const shared_ptr<Blow_Enemy>& Obj){
	//}
	//void SearchState::Execute(const shared_ptr<Blow_Enemy>& Obj){
	//	//セルの更新
	//	Obj->GetCloudCell_SetMyCell();
	//	//キャラの検索
	//	Obj->Search();
	//}
	//void SearchState::Exit(const shared_ptr<Blow_Enemy>& Obj){
	//}

	////--------------------------------------------------------------------------------------
	////	class BlowState : public ObjState<Blow_Enemy>;
	////	用途: 吹き飛ばすステート
	////--------------------------------------------------------------------------------------
	//shared_ptr<BlowState> BlowState::Instance(){
	//	static shared_ptr<BlowState> instance;
	//	if (!instance){
	//		instance = shared_ptr<BlowState>(new BlowState);
	//	}
	//	return instance;
	//}
	//void BlowState::Enter(const shared_ptr<Blow_Enemy>& Obj){
	//	//弾を出す
	//	Obj->CreateBlow_Ball();
	//	//自身をどっかに飛ばす
	////	Obj->StartBlowAction();
	//}
	//void BlowState::Execute(const shared_ptr<Blow_Enemy>& Obj){
	//}
	//void BlowState::Exit(const shared_ptr<Blow_Enemy>& Obj){
	//}

	///**
	//* @class Blow_Ball
	//* @brief オブジェクトを吹き飛ばす弾　（プレイヤーや敵など）
	//* @author　sike yuya
	//* @date	2016/06/06
	//*/

	//Blow_Ball::Blow_Ball(const shared_ptr<Stage>& StagePtr, const Vector3& pos, const Direction& blowdirection) :
	//	GameObject(StagePtr),
	//	m_Direction(blowdirection),
	//	m_Pos(pos)
	//
	//{}

	////初期化
	//void Blow_Ball::OnCreate(){
	//	static const Vector3 ObjSize{ 0.3f, 0.3f, 0.3f };

	//	auto PtrTrans = AddComponent<Transform>();
	//	PtrTrans->SetPosition(m_Pos);
	//	PtrTrans->SetScale(ObjSize);
	//	PtrTrans->SetRotation(0,0,0);

	//	//描画コンポーネントの設定
	//	auto PtrDraw = AddComponent<PNTStaticDraw>();
	//	//描画するメッシュを設定
	//	PtrDraw->SetMeshResource(L"DEFAULT_SPHERE");
	//	//描画するテクスチャを設定
	//	PtrDraw->SetTextureResource(L"RED_TX");

	//	//当たり判定の設定
	//	AddComponent<CollisionSphere>();
	//	//TODO : 確認でコリジョンの可視化 あとで消す予定
	//	AddComponent<PNTCollisionDraw>();

	//}

	////変化
	//void Blow_Ball::OnUpdate(){
	//	auto Elpsed = App::GetApp()->GetElapsedTime();
	//	//生命時間を設定
	//	m_DestroyTime += Elpsed;
	//	//動く向きを引数にして、進む
	//	MoveDirection(m_Direction);
	//	if (m_DestroyTime > 3.0f){
	//		//描画と更新を止める
	//		SetDrawActive(false);
	//		SetUpdateActive(false);
	//	}
	//}

	////衝突判定
	//void Blow_Ball::OnCollision(const shared_ptr<GameObject>& other){
	//	//プレイヤーと接触
	//	auto PlayerHit = dynamic_pointer_cast<Player_Character>(other);
	//	//直進してくる敵が接触
	//	auto StraightHit = dynamic_pointer_cast<Straight_Enemy>(other);
	//	//TODO : 動く敵との接触

	//	if (PlayerHit){
	//		//プレイヤー側にノックバックする関数を命令する
	//		PlayerHit->SetBlowDirection(m_Direction);
	//		//TODO : エフェクトの追加と音の追加
	//		//オブジェクトの削除
	//		SetDrawActive(false);
	//		SetUpdateActive(false);
	//	}

	//	if (StraightHit){
	//		//TODO : エフェクトの追加と音の追加
	//		//オブジェクトの削除
	//		SetDrawActive(false);
	//		SetUpdateActive(false);
	//	}
	//}

	////進む向きを決める
	//void Blow_Ball::MoveDirection(const Direction& dir){
	//	auto PtrTrans = GetComponent<Transform>();
	//	auto PtrPos = PtrTrans->GetPosition();
	//
	//	auto Elpsed = App::GetApp()->GetElapsedTime();
	//	switch (dir)
	//	{
	//	case Direction::Back:
	//		PtrPos.z -= Elpsed;
	//		break;
	//	case Direction::Right:
	//		PtrPos.x += Elpsed;
	//		break;
	//	case Direction::Left:
	//		PtrPos.x -= Elpsed;
	//		break;
	//	default:
	//		assert(!"不正な値を検出しました。引数のDirectionを確認してください Blow_Ball.cpp");
	//		break;
	//	}

	//	PtrTrans->SetPosition(PtrPos);
	//}

	////エフェクトと音の再生
	//void Blow_Ball::StartEffect_Sound(){
	//}
}
//endof  basedx11