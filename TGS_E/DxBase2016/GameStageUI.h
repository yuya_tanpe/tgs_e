#pragma once

#include "stdafx.h"

namespace basedx11{
	//--------------------------------------------------------------------------------------
	//	class NumberSprite : public GameObject;
	//	用途: 配置オブジェクト
	//--------------------------------------------------------------------------------------
	class NumberSprite : public GameObject{
		Vector3 m_StartPos;
		//頂点の配列の配列（2次元配列）
		vector< vector<VertexPositionColorTexture> > m_NumberVertexVec;
		float m_TotalTime;
		bool DrawActive;
	public:
		//構築と破棄
		NumberSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos, const bool DrawTF);
		virtual ~NumberSprite();
		//初期化
		virtual void OnCreate() override;
		//変化
		virtual void OnUpdate() override;
		//スコアを１プラスする
		void ScorePlus();
	};

	// スコアテクスチャ
	class ScoreUI : public GameObject
	{
		Vector3 m_Pos;

	public:
		// 構築と破棄
		ScoreUI(shared_ptr<Stage>& StagePtr, const Vector3& Pos);
		virtual ~ScoreUI();

		// 初期化
		virtual void OnCreate() override;
	};

	/**
	* @class ScoreframeSprite
	* @brief スコアの枠
	* @author yuya
	* @date 2016/05/12
	*/
	class ScoreframeSprite : public GameObject {
		Vector3 m_StartPos;
		//頂点の配列の配列（2次元配列）
		vector< vector<VertexPositionColorTexture> > m_NumberVertexVec;
	public:
		//構築と破棄
		ScoreframeSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~ScoreframeSprite();

		//初期化
		virtual void OnCreate() override;
	};



	// コインスプライト
	class GameStage_CoinSprite : public GameObject
	{
		Vector3 m_Pos;

	public:
		// 構築と破棄
		GameStage_CoinSprite(shared_ptr<Stage>& StagePtr, const Vector3& Pos);
		virtual ~GameStage_CoinSprite();

		// 初期化
		virtual void OnCreate() override;
	};

	/**
	* @class GameOverSprite
	* @brief ゲームオーバーを知らせるスプライト
	* @author yuya
	* @date 2016/05/19
	*/
	class GameOverSprite : public GameObject {
		Vector3 m_StartPos;
		//頂点の配列の配列（2次元配列）
		vector< vector<VertexPositionColorTexture> > m_NumberVertexVec;
	public:
		//構築と破棄
		GameOverSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~GameOverSprite();

		//初期化
		virtual void OnCreate() override;
		//初期化
		virtual void OnUpdate() override;
	};

	/**
	* @class GameReadySprite
	* @brief ゲーム始まる前の数字スプライト
	* @author yuya
	* @date 2016/06/10
	*/
	class GameReadySprite : public GameObject{
		Vector3 m_StartPos;
		vector<wstring> m_TextureVec;
		int m_texturenum;
	public:
		//構築と破棄
		GameReadySprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~GameReadySprite();
		//初期化
		virtual void OnCreate() override;

		//次に表示するテクスチャを表示する
		void NextTexture();

		//フェードアウト
		void Fadeout();
	};
}
//endof  basedx11