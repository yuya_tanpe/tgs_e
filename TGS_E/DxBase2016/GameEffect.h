#pragma once

#include "stdafx.h"

namespace basedx11
{
	//--------------------------------------------------------------------------------------
	//class MultiCircle : public MultiParticle;
	//用途: 複数のスパーククラス
	//--------------------------------------------------------------------------------------
	class MultiCircle : public MultiParticle{
	public:
		//構築と破棄
		MultiCircle(shared_ptr<Stage>& StagePtr);
		virtual ~MultiCircle();
		//--------------------------------------------------------------------------------------
		//	void InsertCircle(
		//	const Vector3& Pos	//放出位置（エミッター位置）
		//	);
		//用途: スパークを放出する
		//戻り値: なし。
		//--------------------------------------------------------------------------------------
		void InsertCircle(const Vector3& Pos, const Color4 color);
		virtual void OnUpdate() override;
	};

	class MultiSpark : public MultiParticle{
	public:
		//構築と破棄
		MultiSpark(shared_ptr<Stage>& StagePtr);
		virtual ~MultiSpark();
		//--------------------------------------------------------------------------------------
		//	void InsertCircle(
		//	const Vector3& Pos	//放出位置（エミッター位置）
		//	);
		//用途: スパークを放出する
		//戻り値: なし。
		//--------------------------------------------------------------------------------------
		//Create
		void InsertSpark(const Vector3& Pos, const Color4 color);
		//変化
		//virtual void OnUpdate() override;
	};

	/*
	* @class Firework_Effect
	* @brief お祝いエフェクト
	* @author yuyu sike
	* @date 2016/05/12
	*/
	class Firework_Effect : public MultiParticle{
	public:
		//構築と破棄
		Firework_Effect(shared_ptr<Stage>& StagePtr);
		~Firework_Effect(){}

		//変化と作成
		virtual void OnUpdate() override;
		void InsertFirework_Effect(const Vector3& Pos);
	};

	/*
	* @class PlayerMove_Effect
	* @brief プレイヤーが動いた時の煙エフェクト
	* @author yuyu sike
	* @date 2016/05/13
	*/
	class PlayerMove_Effect : public MultiParticle{
	public:
		//構築と破棄
		PlayerMove_Effect(shared_ptr<Stage>& StagePtr);
		~PlayerMove_Effect(){}

		//変化と作成
		virtual void OnUpdate() override;
		void InsertPlayerMove_Effect(const Vector3& Pos);
	};

	//--------------------------------------------------------------------------------------
	//class Circle : public MultiParticle;
	//用途: 落ちたときの円の表示
	//--------------------------------------------------------------------------------------
	class Circle : public MultiParticle{
	public:
		//構築と破棄
		Circle(shared_ptr<Stage>& StagePtr);
		virtual ~Circle();

		void InsertCircle(const Vector3& Pos);

		virtual void OnUpdate()override;
	};

	//--------------------------------------------------------------------------------------
	//	class EffectBox : public GameObject;
	//	用途: エフェクト用のボックス
	//--------------------------------------------------------------------------------------
	class EffectBox : public GameObject{
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
		Vector3 m_JumpVec;
		int aaa;
		float m_max;
	public:
		//構築と破棄
		EffectBox(const shared_ptr<Stage>& StagePtr,
			const Vector3& Position, const Vector3& pos
			);
		virtual ~EffectBox();
		//初期化
		virtual void OnCreate() override;
		//操作
		virtual void OnUpdate() override;
		virtual void OnCollision(const shared_ptr<GameObject>& other) override;
	};

	//--------------------------------------------------------------------------------------
	//class FireWorks_Right : public MultiParticle;
	//用途: 花火の演出
	//--------------------------------------------------------------------------------------
	class FireWorks_Right : public MultiParticle{
	private:
		shared_ptr< StateMachine<FireWorks_Right> >  m_StateMachine;	//ステートマシーン
		shared_ptr<Particle> m_Particle;
		int poi;
		Vector3 m_EffectPos;
	public:
		//構築と破棄
		FireWorks_Right(shared_ptr<Stage>& StagePtr);
		virtual ~FireWorks_Right();

		void InsertFireWorks_Right(const Vector3& Pos);
		virtual void OnCreate()override;
		virtual void OnUpdate()override;

		//最初のパーティクルの設定
		void firstParticleMethod(ParticleSprite& particle);
		void SecondParticleMethod(ParticleSprite& particle);
		//最初の動き
		void FirstMoveParticle();

		//二番目の設定
		void FirstStepParticle();
		//二番目の動き
		void SecondMoveParticle();
		//待ってるパーティクル
		void WaitParticle();

		//アクセサ
		shared_ptr< StateMachine<FireWorks_Right> > GetStateMachine() const{
			return m_StateMachine;
		}
	};

	//--------------------------------------------------------------------------------------
	//	class FirstStep : public ObjState<FireWorks_Right>;
	//	用途: 上に打ちあがる
	//--------------------------------------------------------------------------------------
	class FirstStep : public ObjState<FireWorks_Right>
	{
		FirstStep(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<FirstStep> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<FireWorks_Right>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<FireWorks_Right>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<FireWorks_Right>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class SecondStep : public ObjState<FireWorks_Right>;
	//	用途: ひとつひとつが広がりながら離れる
	//--------------------------------------------------------------------------------------
	class SecondStep : public ObjState<FireWorks_Right>
	{
		SecondStep(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<SecondStep> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<FireWorks_Right>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<FireWorks_Right>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<FireWorks_Right>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class WaitStep : public ObjState<FireWorks_Right>;
	//	用途: テスト駆動　待ってる感じ
	//--------------------------------------------------------------------------------------
	class WaitStep : public ObjState<FireWorks_Right>
	{
		WaitStep(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<WaitStep> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<FireWorks_Right>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<FireWorks_Right>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<FireWorks_Right>& Obj)override;
	};


	//--------------------------------------------------------------------------------------
	//class FireWorks_Left : public MultiParticle;
	//用途: 花火の演出
	//--------------------------------------------------------------------------------------
	class FireWorks_Left : public MultiParticle{
	private:
		shared_ptr< StateMachine<FireWorks_Left> >  m_StateMachine;	//ステートマシーン
		shared_ptr<Particle> m_Particle;
		int poi;
		Vector3 m_EffectPos;
	public:
		//構築と破棄
		FireWorks_Left(shared_ptr<Stage>& StagePtr);
		virtual ~FireWorks_Left();

		void InsertFireWorks_Left(const Vector3& Pos);
		virtual void OnCreate()override;
		virtual void OnUpdate()override;

		//最初のパーティクルの設定
		void firstParticleMethod(ParticleSprite& particle);
		void SecondParticleMethod(ParticleSprite& particle);
		//最初の動き
		void FirstMoveParticle();

		//二番目の設定
		void FirstStepParticle();
		//二番目の動き
		void SecondMoveParticle();
		//待ってるパーティクル
		void WaitParticle();

		//アクセサ
		shared_ptr< StateMachine<FireWorks_Left> > GetStateMachine() const{
			return m_StateMachine;
		}
	};

	//--------------------------------------------------------------------------------------
	//	class FirstStep : public ObjState<FireWorks_Left>;
	//	用途: 上に打ちあがる
	//--------------------------------------------------------------------------------------
	class FirstStep_Left : public ObjState<FireWorks_Left>
	{
		FirstStep_Left(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<FirstStep_Left> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<FireWorks_Left>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<FireWorks_Left>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<FireWorks_Left>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class SecondStep_Left : public ObjState<FireWorks_Left>;
	//	用途: ひとつひとつが広がりながら離れる
	//--------------------------------------------------------------------------------------
	class SecondStep_Left : public ObjState<FireWorks_Left>
	{
		SecondStep_Left(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<SecondStep_Left> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<FireWorks_Left>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<FireWorks_Left>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<FireWorks_Left>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class WaitStep : public ObjState<FireWorks_Left>;
	//	用途: テスト駆動　待ってる感じ
	//--------------------------------------------------------------------------------------
	class WaitStep_Left : public ObjState<FireWorks_Left>
	{
		WaitStep_Left(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<WaitStep_Left> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<FireWorks_Left>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<FireWorks_Left>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<FireWorks_Left>& Obj)override;
	};
}
//endof  basedx11