#include "stdafx.h"
#include "Project.h"

namespace basedx11{
	//--------------------------------------------------------------------------------------
	//class MultiCircle : public MultiParticle;
	//用途: 複数のスパーククラス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MultiCircle::MultiCircle(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	MultiCircle::~MultiCircle(){}

	void MultiCircle::InsertCircle(const Vector3& Pos, const Color4 color){
		auto ParticlePtr = InsertParticle(20);
		ParticlePtr->SetEmitterPos(Pos);
		ParticlePtr->SetTextureResource(L"CIRCLE_TX");
		ParticlePtr->SetMaxTime(1.0f);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
			rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 0.1f - 0.05f;
			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 0.1f;
			rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 0.1f - 0.05f;
			rParticleSprite.m_LocalScale = Vector2(0.2f, 0.2f);
			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 15.0f,
				rParticleSprite.m_LocalPos.y * 15.0f,
				rParticleSprite.m_LocalPos.z * 15.0f
				);

			//ランダム
			rParticleSprite.m_Color = Color4(Util::RandZeroToOne() * 2.0f, Util::RandZeroToOne() * 2.0f, Util::RandZeroToOne() * 2.0f, 0.5f);
		}
	}

	MultiSpark::MultiSpark(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	MultiSpark::~MultiSpark(){}

	void MultiSpark::InsertSpark(const Vector3& Pos, const Color4 color){
		auto ParticlePtr = InsertParticle(20);
		ParticlePtr->SetEmitterPos(Pos);
		ParticlePtr->SetTextureResource(L"SPARK_TX");
		ParticlePtr->SetMaxTime(1.0f);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
			rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 0.1f - 0.05f;
			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 0.1f;
			rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 0.1f - 0.05f;
			rParticleSprite.m_LocalScale = Vector2(0.2f, 0.2f);
			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x *  20.0f,
				rParticleSprite.m_LocalPos.y * 20.0f,
				rParticleSprite.m_LocalPos.z *20.0f
				);
			//色の指定
			rParticleSprite.m_Color = color;
		}
	}

	void MultiCircle::OnUpdate(){
		float fTimeSpan = App::GetApp()->GetElapsedTime();
		for (auto ParticlePtr : GetParticleVec()){
			//トータル時間に加算
			ParticlePtr->AddTotalTime(fTimeSpan);
			for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
				//サイズを小さくする
				rParticleSprite.m_LocalScale -= Vector2(0.003f, 0.003f);
				rParticleSprite.m_LocalPos += rParticleSprite.m_Velocity * fTimeSpan;

				if (ParticlePtr->GetTotalTime() >= ParticlePtr->GetMaxTime()){
					//制限時間になったら
					rParticleSprite.m_Active = false;
				}
			}
		}
	}

	/*
	* @class Firework_Effect
	* @brief お祝いエフェクト
	* @author yuyu sike
	* @date 2016/05/12
	*/

	Firework_Effect::Firework_Effect(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}

	//エフェクトの生成
	void Firework_Effect::InsertFirework_Effect(const Vector3& Pos){
		//エフェクトの数
		auto ParticlePtr = InsertParticle(1);
		//エフェクトをどこから出すか
		ParticlePtr->SetEmitterPos(Pos);
		ParticlePtr->SetTextureResource(L"SPARK_TX");
		SetDrawLayer(0);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
			//パーティクルのサイズ
			rParticleSprite.m_LocalScale = Vector2(0.8f, 0.8f);
			//ランダム
			rParticleSprite.m_Color = Color4(1.0f, 0.0f, 0.0f, 0.5f);
			rParticleSprite.m_Active = true;
			//パーティクルの位置
			rParticleSprite.m_LocalPos.x = 0.0f;
			rParticleSprite.m_LocalPos.y = 0.0f;
			rParticleSprite.m_LocalPos.z = 0.0f;

			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 0.0f,
				rParticleSprite.m_LocalPos.y * 0.0f,
				rParticleSprite.m_LocalPos.z * 0.0f
				);
		}
	}

	//変化
	void Firework_Effect::OnUpdate(){
		float fTimeSpan = App::GetApp()->GetElapsedTime();

		for (auto ParticlePtr : GetParticleVec())
		{
			ParticlePtr->AddTotalTime(fTimeSpan);
			for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec())
			{
				if (rParticleSprite.m_Active)
				{
					rParticleSprite.m_LocalPos += rParticleSprite.m_Velocity * fTimeSpan;
					rParticleSprite.m_Color -= Color4(0.02f, 0.02f, 0.02f, 0.04f);
					rParticleSprite.m_LocalScale -= Vector2(0.001f, 0.001f);
					if (ParticlePtr->GetTotalTime() > 1.0f) {
						rParticleSprite.m_Active = false;
					}
				}
			}
		}
	}

	/*
	* @class PlaterMove_Effect
	* @brief プレイヤーが動いた時の煙エフェクト
	* @author yuyu sike
	* @date 2016/05/13
	*/
	//構築と破棄
	PlayerMove_Effect::PlayerMove_Effect(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}

	void PlayerMove_Effect::InsertPlayerMove_Effect(const Vector3& Pos){
		auto ParticlePtr = InsertParticle(15);
		ParticlePtr->SetEmitterPos(Pos);
		ParticlePtr->SetTextureResource(L"SPARK_TX");
		SetDrawLayer(1);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
			//パーティクルのサイズ
			rParticleSprite.m_LocalScale = Vector2(0.5f, 0.5f);
			rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 0.1f - 0.05f;
			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 0.1f;
			rParticleSprite.m_LocalPos.z = 0.0f;
			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 5.0f,
				rParticleSprite.m_LocalPos.y * 0.0f,
				rParticleSprite.m_LocalPos.z + -0.5f
				);
			//色の指定
			rParticleSprite.m_Color = Color4(0.9f, 0.9f, 0.9f, 0.3f);
		}
	}

	//変化
	void PlayerMove_Effect::OnUpdate(){
		float fTimeSpan = App::GetApp()->GetElapsedTime();

		for (auto ParticlePtr : GetParticleVec())
		{
			ParticlePtr->AddTotalTime(fTimeSpan);
			for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec())
			{
				if (rParticleSprite.m_Active)
				{
					rParticleSprite.m_LocalPos += rParticleSprite.m_Velocity * fTimeSpan;
					rParticleSprite.m_Color -= Color4(0.002f, 0.002f, 0.002f, 0.014f);
					rParticleSprite.m_LocalScale -= Vector2(0.001f, 0.001f);
					if (ParticlePtr->GetTotalTime() > 0.8f) {
						rParticleSprite.m_Active = false;
					}
				}
			}
		}
	}

	//--------------------------------------------------------------------------------------
	//class Circle : public MultiParticle;
	//用途: 落ちたときの円の表示
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Circle::Circle(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	Circle::~Circle(){}

	void Circle::InsertCircle(const Vector3& Pos){
		auto ParticlePtr = InsertParticle(1);
		ParticlePtr->SetEmitterPos(Pos);
		ParticlePtr->SetTextureResource(L"CIRCLE_TX");
		SetDrawLayer(1);
		ParticlePtr->SetDrawOption(Particle::DrawOption::Normal);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
			//パーティクルのサイズ
			rParticleSprite.m_LocalScale = Vector2(1.0f, 1.0f);
			rParticleSprite.m_LocalPos.x = 0.0f;
			rParticleSprite.m_LocalPos.y = 0.0f;
			rParticleSprite.m_LocalPos.z = 0.0f;

			rParticleSprite.m_LocalQt = Quaternion(90.0f / 180.0f * XM_PI, 0.0f, 0.0f);

			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 0.0f,
				rParticleSprite.m_LocalPos.y * 0.0f,
				rParticleSprite.m_LocalPos.z * 0.0f
				);
			//色の指定
			rParticleSprite.m_Color = Color4(1.0f, 1.0f, 1.0f, 1.0f);
		}
	}

	//変化
	void Circle::OnUpdate(){
		float fTimeSpan = App::GetApp()->GetElapsedTime();
		for (auto ParticlePtr : GetParticleVec())
		{
			ParticlePtr->AddTotalTime(fTimeSpan);
			for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec())
			{
				if (rParticleSprite.m_Active)
				{
					rParticleSprite.m_LocalScale += Vector2(0.02f, 0.02f);
					rParticleSprite.m_Color -= Color4(0.002f, 0.002f, 0.002f, 0.014f);
					if (ParticlePtr->GetTotalTime() > 1.0f) {
						rParticleSprite.m_Active = false;
					}
				}
			}
		}
	}

	//--------------------------------------------------------------------------------------
	//	class EffectBox : public GameObject;
	//	用途: エフェクト用のボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	EffectBox::EffectBox(const shared_ptr<Stage>& StagePtr,
		const Vector3& Jump, const Vector3& pos
		) :
		GameObject(StagePtr),
		m_Position(pos),
		m_Scale(Vector3(0.1f, 0.1f, 0.1f)),
		m_Rotation(Vector3(0.0f, 0.0f, 0.0f)),
		m_JumpVec(Jump),
		aaa(0),
		m_max(2.0f)
	{
	}
	EffectBox::~EffectBox(){}

	//初期化
	void EffectBox::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);
		auto GravityPtr = AddComponent<Gravity>();
		GravityPtr->SetBaseY(-0.7f);
		GravityPtr->StartJump(m_JumpVec);

		//反発を実装する場合はRigidbodyをつける
		auto PtrRegid = AddComponent<Rigidbody>();
		PtrRegid->SetReflection(10.0f);
		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetTextureResource(L"BLUE_TX");
		PtrDraw->SetOwnShadowActive(true);
	}

	void EffectBox::OnUpdate(){
		auto PtrGravity = GetComponent<Gravity>();
		if (PtrGravity->GetGravityVelocity().Length() <= 0 && aaa < 3){
			auto Scale = GetComponent<Transform>()->GetScale();
			GetComponent<Transform>()->SetScale(Vector3(Scale.x - 0.02f, Scale.y - 0.02f, Scale.z - 0.02f));
			PtrGravity->StartJump(Vector3(0.0f, 2.0f, 0.0f));
			PtrGravity->SetBaseY(PtrGravity->GetBaseY() - 0.04f);
			aaa++;
		}
		if (aaa == 3){
			SetDrawActive(false);
			SetUpdateActive(false);
		}

		if (aaa < 3){
			auto vec = GetComponent<Transform>()->GetRotation();
			float rx = (Util::RandZeroToOne() - 0.5f) / 2.0f;
			float ry = (Util::RandZeroToOne() - 0.5f) / 2.0f;
			float rz = (Util::RandZeroToOne() - 0.5f) / 2.0f;
			Vector3 Work{ vec.x + rx, vec.y + ry, vec.z + rz };
			GetComponent<Transform>()->SetRotation(Work);
		}
	}

	void EffectBox::OnCollision(const shared_ptr<GameObject>& other){
	}

	//--------------------------------------------------------------------------------------
	//class FireWorks_Right : public MultiParticle;
	//用途: 花火の演出
	//--------------------------------------------------------------------------------------
	//構築と破棄
	FireWorks_Right::FireWorks_Right(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr), poi(0)
	{}
	FireWorks_Right::~FireWorks_Right(){}

	void FireWorks_Right::InsertFireWorks_Right(const Vector3& Pos){
		auto ParticlePtr = InsertParticle(50);
		m_Particle = ParticlePtr;
		ParticlePtr->SetEmitterPos(Pos);
		m_EffectPos = Pos;
		//TODO : テクスチャ作ったから置き換えと修正
		ParticlePtr->SetTextureResource(L"FIREWORK_2");
		SetDrawLayer(2);
		//ParticlePtr->SetMaxTime(0.5f);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
			//初期設定
			firstParticleMethod(rParticleSprite);
		}
		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<FireWorks_Right> >(GetThis<FireWorks_Right>());
		//最初のステートをFirstStepに設定
		m_StateMachine->ChangeState(FirstStep::Instance());
	}

	//作成
	void FireWorks_Right::OnCreate(){
		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<FireWorks_Right> >(GetThis<FireWorks_Right>());
		//最初のステートをFirstStepに設定
		m_StateMachine->ChangeState(FirstStep::Instance());
	}
	//変化
	void FireWorks_Right::OnUpdate(){
		//ステートマシンのUpdateを行う
		//この中でステートの更新が行われる(Execute()関数が呼ばれる)
		m_StateMachine->Update();
	}

	//最初のパーティクルの設定
	void FireWorks_Right::firstParticleMethod(ParticleSprite& particle){
		//パーティクルのサイズ
		particle.m_LocalScale = Vector2(0.1f, 0.1f);
		particle.m_LocalPos.x = 0.0f;
		particle.m_LocalPos.y = 0.1f;
		particle.m_LocalPos.z = 0.0f;

		//各パーティクルの移動速度を指定
		particle.m_Velocity = Vector3(
			particle.m_LocalPos.x * 0.0f,
			particle.m_LocalPos.y * 15.0f,
			particle.m_LocalPos.z * 0.0f
			);
		//色の指定
		//TODO : めんどいからランダムに設定
		particle.m_Color = Color4(Util::RandZeroToOne() * 1.0f, Util::RandZeroToOne() * 1.0f, Util::RandZeroToOne() * 1.0f, 0.5f);
	}

	//二番目の設定
	void FireWorks_Right::FirstStepParticle(){
		m_Particle->SetEmitterPos(m_EffectPos + Vector3(0.0f, 2.0f, 0.0f));
		m_Particle->SetDrawOption(Particle::DrawOption::Normal);
		//vector<ParticleSprite>& pSpriteVec = m_Particle->GetParticleSpriteVec();
		for (auto& rParticleSprite : m_Particle->GetParticleSpriteVec()){
			//初期設定
			SecondParticleMethod(rParticleSprite);
		}
	}

	void FireWorks_Right::SecondParticleMethod(ParticleSprite& particle){
		//パーティクルのサイズ
		auto scale = Util::RandZeroToOne() * 0.3f;
		particle.m_LocalScale = Vector2(scale, scale);
		particle.m_LocalPos.x = Util::RandZeroToOne() * 0.1f - 0.05f;
		particle.m_LocalPos.y = Util::RandZeroToOne() * 0.1f - 0.05f;
		particle.m_LocalPos.z = Util::RandZeroToOne() * 0.1f - 0.05f;
		//各パーティクルの移動速度を指定
		particle.m_Velocity = Vector3(
			particle.m_LocalPos.x * 150.0f,
			particle.m_LocalPos.y * 150.0f,
			particle.m_LocalPos.z * 150.0f
			);
		//色の指定
		particle.m_Color = Color4(Util::RandZeroToOne() * 1.0f, Util::RandZeroToOne() * 1.0f, Util::RandZeroToOne() * 1.0f, 0.5f);
	}

	//最初の動き
	void FireWorks_Right::FirstMoveParticle(){
		float fTimeSpan = App::GetApp()->GetElapsedTime();
		for (auto ParticlePtr : GetParticleVec())
		{
			ParticlePtr->AddTotalTime(fTimeSpan);
			for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec())
			{
				if (rParticleSprite.m_Active){
					rParticleSprite.m_LocalPos += rParticleSprite.m_Velocity * fTimeSpan;
					if (ParticlePtr->GetTotalTime() > 2.0f) {
						GetStateMachine()->ChangeState(SecondStep::Instance());
						ParticlePtr->SetTotalTime(0.0f);
					}
				}
			}
		}
	}
	//二番目の動き
	void FireWorks_Right::SecondMoveParticle(){
		float fTimeSpan = App::GetApp()->GetElapsedTime();
		for (auto ParticlePtr : GetParticleVec())
		{
			ParticlePtr->AddTotalTime(fTimeSpan);
			for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec())
			{
				if (rParticleSprite.m_Active){
					poi++;
					if (poi == 3){
						rParticleSprite.m_Color = Color4(Util::RandZeroToOne() * 1.0f, Util::RandZeroToOne() * 1.0f, Util::RandZeroToOne() * 1.0f, 0.5f);
						poi = 0;
					}
					if (ParticlePtr->GetTotalTime() < 0.2f) {
						rParticleSprite.m_Color = Color4(0.8f, Util::RandZeroToOne() * 1.0f / 2.0f, Util::RandZeroToOne() * 1.0f / 2.0f, 0.5f);
						rParticleSprite.m_LocalScale += Vector2(0.003f, 0.003f);
						rParticleSprite.m_LocalPos += rParticleSprite.m_Velocity * fTimeSpan;
						rParticleSprite.m_LocalQt.AddRotation(Vector3(0.0f, 0.0f, 0.3f));
					}
					else{
						rParticleSprite.m_LocalPos += rParticleSprite.m_Velocity * 0.0003f;
						rParticleSprite.m_LocalQt.AddRotation(Vector3(0.0f, 0.0f, 0.13f));
					}

					if (ParticlePtr->GetTotalTime() > 1.0f) {
						rParticleSprite.m_LocalScale -= Vector2(0.003f, 0.003f);
						rParticleSprite.m_LocalQt.AddRotation(Vector3(0.0f, 0.0f, 0.1f));
					}
					if (ParticlePtr->GetTotalTime() > 2.0f) {
						rParticleSprite.m_Active = false;
					}
				}
			}
		}
	}

	//待ってるパーティクル
	void FireWorks_Right::WaitParticle(){
		m_Particle->SetEmitterPos(Vector3(0.0f, 0.0f, 0.0f));
		//vector<ParticleSprite>& pSpriteVec = m_Particle->GetParticleSpriteVec();
		for (auto& rParticleSprite : m_Particle->GetParticleSpriteVec()){
			//パーティクルのサイズ
			rParticleSprite.m_LocalScale = Vector2(0.1f, 0.1f);
			rParticleSprite.m_LocalPos.x = 0.0f;
			rParticleSprite.m_LocalPos.y = 0.1f;
			rParticleSprite.m_LocalPos.z = 0.0f;

			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 0.0f,
				rParticleSprite.m_LocalPos.y * 15.0f,
				rParticleSprite.m_LocalPos.z * 0.0f
				);
			//色の指定
			//TODO : めんどいからランダムに設定
			rParticleSprite.m_Color = Color4(Util::RandZeroToOne() * 1.0f, Util::RandZeroToOne() * 1.0f, Util::RandZeroToOne() * 1.0f, 0.5f);
		}
	}
	//--------------------------------------------------------------------------------------
	//	class FirstStep : public ObjState<FireWorks_Right>;
	//	用途: 上に打ちあがる
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<FirstStep> FirstStep::Instance(){
		static shared_ptr<FirstStep> instance;
		if (!instance){
			instance = shared_ptr<FirstStep>(new FirstStep);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void FirstStep::Enter(const shared_ptr<FireWorks_Right>& Obj){
	}
	//ステート実行中に毎ターン呼ばれる関数
	void FirstStep::Execute(const shared_ptr<FireWorks_Right>& Obj){
		Obj->FirstMoveParticle();
	}
	//ステートにから抜けるときに呼ばれる関数
	void FirstStep::Exit(const shared_ptr<FireWorks_Right>& Obj){
	}

	//--------------------------------------------------------------------------------------
	//	class SecondStep : public ObjState<FireWorks_Right>;
	//	用途: ひとつひとつが広がりながら離れる
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<SecondStep> SecondStep::Instance(){
		static shared_ptr<SecondStep> instance;
		if (!instance){
			instance = shared_ptr<SecondStep>(new SecondStep);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void SecondStep::Enter(const shared_ptr<FireWorks_Right>& Obj){
		Obj->FirstStepParticle();
	}
	//ステート実行中に毎ターン呼ばれる関数
	void SecondStep::Execute(const shared_ptr<FireWorks_Right>& Obj){
		Obj->SecondMoveParticle();
	}
	//ステートにから抜けるときに呼ばれる関数
	void SecondStep::Exit(const shared_ptr<FireWorks_Right>& Obj){
	}

	//--------------------------------------------------------------------------------------
	//	class WaitStep : public ObjState<FireWorks_Right>;
	//	用途: テスト駆動　待ってる感じ
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<WaitStep> WaitStep::Instance(){
		static shared_ptr<WaitStep> instance;
		if (!instance){
			instance = shared_ptr<WaitStep>(new WaitStep);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void WaitStep::Enter(const shared_ptr<FireWorks_Right>& Obj){
		Obj->WaitParticle();
	}
	//ステート実行中に毎ターン呼ばれる関数
	void WaitStep::Execute(const shared_ptr<FireWorks_Right>& Obj){
	}
	//ステートにから抜けるときに呼ばれる関数
	void WaitStep::Exit(const shared_ptr<FireWorks_Right>& Obj){
	}



	//--------------------------------------------------------------------------------------
	//class FireWorks_Left : public MultiParticle;
	//用途: 左の花火の演出
	//--------------------------------------------------------------------------------------
	//構築と破棄
	FireWorks_Left::FireWorks_Left(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr), poi(0)
	{}
	FireWorks_Left::~FireWorks_Left(){}

	void FireWorks_Left::InsertFireWorks_Left(const Vector3& Pos){
		auto ParticlePtr = InsertParticle(50);
		m_Particle = ParticlePtr;
		ParticlePtr->SetEmitterPos(Pos);
		m_EffectPos = Pos;
		ParticlePtr->SetTextureResource(L"FIREWORK_2");
		SetDrawLayer(2);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
			//初期設定
			firstParticleMethod(rParticleSprite);
		}
		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<FireWorks_Left> >(GetThis<FireWorks_Left>());
		//最初のステートをFirstStepに設定
		m_StateMachine->ChangeState(FirstStep_Left::Instance());
	}

	//作成
	void FireWorks_Left::OnCreate(){
		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<FireWorks_Left> >(GetThis<FireWorks_Left>());
		//最初のステートをFirstStepに設定
		m_StateMachine->ChangeState(FirstStep_Left::Instance());
	}
	//変化
	void FireWorks_Left::OnUpdate(){
		//ステートマシンのUpdateを行う
		//この中でステートの更新が行われる(Execute()関数が呼ばれる)
		m_StateMachine->Update();
	}

	//最初のパーティクルの設定
	void FireWorks_Left::firstParticleMethod(ParticleSprite& particle){
		//パーティクルのサイズ
		particle.m_LocalScale = Vector2(0.1f, 0.1f);
		particle.m_LocalPos.x = 0.0f;
		particle.m_LocalPos.y = 0.1f;
		particle.m_LocalPos.z = 0.0f;

		//各パーティクルの移動速度を指定
		particle.m_Velocity = Vector3(
			particle.m_LocalPos.x * 0.0f,
			particle.m_LocalPos.y * 15.0f,
			particle.m_LocalPos.z * 0.0f
			);
		//色の指定
		//TODO : めんどいからランダムに設定
		particle.m_Color = Color4(Util::RandZeroToOne() * 1.0f, Util::RandZeroToOne() * 1.0f, Util::RandZeroToOne() * 1.0f, 0.5f);
	}

	//二番目の設定
	void FireWorks_Left::FirstStepParticle(){
		m_Particle->SetEmitterPos(m_EffectPos + Vector3(0.0f, 3.0f, 0.0f));
		m_Particle->SetDrawOption(Particle::DrawOption::Normal);
		for (auto& rParticleSprite : m_Particle->GetParticleSpriteVec()){
			//初期設定
			SecondParticleMethod(rParticleSprite);
		}
	}

	void FireWorks_Left::SecondParticleMethod(ParticleSprite& particle){
		//パーティクルのサイズ
		auto scale = Util::RandZeroToOne() * 0.3f;
		particle.m_LocalScale = Vector2(scale, scale);
		particle.m_LocalPos.x = Util::RandZeroToOne() * 0.1f - 0.05f;
		particle.m_LocalPos.y = Util::RandZeroToOne() * 0.1f - 0.05f;
		particle.m_LocalPos.z = Util::RandZeroToOne() * 0.1f - 0.05f;
		//各パーティクルの移動速度を指定
		particle.m_Velocity = Vector3(
			particle.m_LocalPos.x * 150.0f,
			particle.m_LocalPos.y * 150.0f,
			particle.m_LocalPos.z * 150.0f
			);
		//色の指定
		particle.m_Color = Color4(Util::RandZeroToOne() * 1.0f, Util::RandZeroToOne() * 1.0f, Util::RandZeroToOne() * 1.0f, 0.5f);
	}

	//最初の動き
	void FireWorks_Left::FirstMoveParticle(){
		float fTimeSpan = App::GetApp()->GetElapsedTime();
		for (auto ParticlePtr : GetParticleVec())
		{
			ParticlePtr->AddTotalTime(fTimeSpan);
			for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec())
			{
				if (rParticleSprite.m_Active){
					rParticleSprite.m_LocalPos += rParticleSprite.m_Velocity * fTimeSpan;
					if (ParticlePtr->GetTotalTime() > 2.0f) {
						GetStateMachine()->ChangeState(SecondStep_Left::Instance());
						ParticlePtr->SetTotalTime(0.0f);
					}
				}
			}
		}
	}
	//二番目の動き
	void FireWorks_Left::SecondMoveParticle(){
		float fTimeSpan = App::GetApp()->GetElapsedTime();
		for (auto ParticlePtr : GetParticleVec())
		{
			ParticlePtr->AddTotalTime(fTimeSpan);
			for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec())
			{
				if (rParticleSprite.m_Active){
					poi++;
					if (poi == 3){
						rParticleSprite.m_Color = Color4(Util::RandZeroToOne() * 1.0f, Util::RandZeroToOne() * 1.0f, Util::RandZeroToOne() * 1.0f, 0.5f);
						poi = 0;
					}
					if (ParticlePtr->GetTotalTime() < 0.2f) {
						rParticleSprite.m_Color = Color4(0.8f, Util::RandZeroToOne() * 1.0f / 2.0f, Util::RandZeroToOne() * 1.0f / 2.0f, 0.5f);
						rParticleSprite.m_LocalScale += Vector2(0.003f, 0.003f);
						rParticleSprite.m_LocalPos += rParticleSprite.m_Velocity * fTimeSpan;
						rParticleSprite.m_LocalQt.AddRotation(Vector3(0.0f, 0.0f, 0.3f));
					}
					else{
						rParticleSprite.m_LocalPos += rParticleSprite.m_Velocity * 0.0003f;
						rParticleSprite.m_LocalQt.AddRotation(Vector3(0.0f, 0.0f, 0.13f));
					}

					if (ParticlePtr->GetTotalTime() > 1.0f) {
						rParticleSprite.m_LocalScale -= Vector2(0.003f, 0.003f);
						rParticleSprite.m_LocalQt.AddRotation(Vector3(0.0f, 0.0f, 0.1f));
					}
					if (ParticlePtr->GetTotalTime() > 2.0f) {
						rParticleSprite.m_Active = false;
					}
				}
			}
		}
	}

	//待ってるパーティクル
	void FireWorks_Left::WaitParticle(){
		m_Particle->SetEmitterPos(Vector3(0.0f, 0.0f, 0.0f));
		for (auto& rParticleSprite : m_Particle->GetParticleSpriteVec()){
			//パーティクルのサイズ
			rParticleSprite.m_LocalScale = Vector2(0.1f, 0.1f);
			rParticleSprite.m_LocalPos.x = 0.0f;
			rParticleSprite.m_LocalPos.y = 0.1f;
			rParticleSprite.m_LocalPos.z = 0.0f;

			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 0.0f,
				rParticleSprite.m_LocalPos.y * 15.0f,
				rParticleSprite.m_LocalPos.z * 0.0f
				);
			//色の指定
			//TODO : めんどいからランダムに設定
			rParticleSprite.m_Color = Color4(Util::RandZeroToOne() * 1.0f, Util::RandZeroToOne() * 1.0f, Util::RandZeroToOne() * 1.0f, 0.5f);
		}
	}
	//--------------------------------------------------------------------------------------
	//	class FirstStep_Left : public ObjState<FireWorks_Left>;
	//	用途: 上に打ちあがる
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<FirstStep_Left> FirstStep_Left::Instance(){
		static shared_ptr<FirstStep_Left> instance;
		if (!instance){
			instance = shared_ptr<FirstStep_Left>(new FirstStep_Left);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void FirstStep_Left::Enter(const shared_ptr<FireWorks_Left>& Obj){
	}
	//ステート実行中に毎ターン呼ばれる関数
	void FirstStep_Left::Execute(const shared_ptr<FireWorks_Left>& Obj){
		Obj->FirstMoveParticle();
	}
	//ステートにから抜けるときに呼ばれる関数
	void FirstStep_Left::Exit(const shared_ptr<FireWorks_Left>& Obj){
	}

	//--------------------------------------------------------------------------------------
	//	class SecondStep_Left : public ObjState<FireWorks_Left>;
	//	用途: ひとつひとつが広がりながら離れる
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<SecondStep_Left> SecondStep_Left::Instance(){
		static shared_ptr<SecondStep_Left> instance;
		if (!instance){
			instance = shared_ptr<SecondStep_Left>(new SecondStep_Left);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void SecondStep_Left::Enter(const shared_ptr<FireWorks_Left>& Obj){
		Obj->FirstStepParticle();
	}
	//ステート実行中に毎ターン呼ばれる関数
	void SecondStep_Left::Execute(const shared_ptr<FireWorks_Left>& Obj){
		Obj->SecondMoveParticle();
	}
	//ステートにから抜けるときに呼ばれる関数
	void SecondStep_Left::Exit(const shared_ptr<FireWorks_Left>& Obj){
	}

	//--------------------------------------------------------------------------------------
	//	class WaitStep_Left : public ObjState<FireWorks_Left>;
	//	用途: テスト駆動　待ってる感じ
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<WaitStep_Left> WaitStep_Left::Instance(){
		static shared_ptr<WaitStep_Left> instance;
		if (!instance){
			instance = shared_ptr<WaitStep_Left>(new WaitStep_Left);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void WaitStep_Left::Enter(const shared_ptr<FireWorks_Left>& Obj){
		Obj->WaitParticle();
	}
	//ステート実行中に毎ターン呼ばれる関数
	void WaitStep_Left::Execute(const shared_ptr<FireWorks_Left>& Obj){
	}
	//ステートにから抜けるときに呼ばれる関数
	void WaitStep_Left::Exit(const shared_ptr<FireWorks_Left>& Obj){
	}


}
//endof  basedx11