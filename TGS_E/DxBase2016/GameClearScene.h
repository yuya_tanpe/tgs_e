#pragma once

#include "stdafx.h"

namespace basedx11
{
	class GameClearScene : public Stage
	{
		// リソースの作成
		void CreateResourses();

		// ビューの作成
		void CreateViews();

		// ゲームクリアスプライトの作成
		void CreateGameClearSprite();

		////音のストップ
		//void SoundStop(wstring soundname);

		////音の再生
		//void SoundPlay(wstring soundname, float volume);

	public:
		// 構築と破棄
		GameClearScene() : Stage(){}
		virtual ~GameClearScene(){}

		// 初期化
		virtual void OnCreate() override;

		// 更新
		virtual void OnUpdate() override;
	};
}
//endof  basedx11