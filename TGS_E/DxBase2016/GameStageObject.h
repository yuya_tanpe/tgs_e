#pragma once

#include "stdafx.h"

namespace basedx11
{
	//--------------------------------------------------------------------------------------
	//	class SideWall : public GameObject;
	//	用途: ゲーム画面横の壁
	//--------------------------------------------------------------------------------------
	class SideWall : public GameObject{
	private:
		Vector3 m_Pos;

	public:
		SideWall(const shared_ptr<Stage>& StagePtr, const Vector3& i_start_pos);
		virtual ~SideWall(){}

		//初期化
		virtual void OnCreate() override;
	};
	//--------------------------------------------------------------------------------------
	//	class RollingTorus : public GameObject;
	//	用途: 回転するトーラス
	//--------------------------------------------------------------------------------------
	class RollingTorus : public GameObject
	{
		shared_ptr< StateMachine<RollingTorus> >  m_StateMachine;	//ステートマシーン
		Vector3 m_StartPos;
		float m_YRot;
		const float m_MaxRotationSpeed;
		float m_RotationSpeed;

	public:
		//構築と破棄
		RollingTorus(const shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~RollingTorus();

		//アクセサ
		shared_ptr< StateMachine<RollingTorus> > GetStateMachine() const
		{
			return m_StateMachine;
		}

		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		//衝突時
		virtual void OnCollision(const shared_ptr<GameObject>& other) override;
	};

	//--------------------------------------------------------------------------------------
	//	class TorusDefaultState : public ObjState<RollingTorus>;
	//	用途: 通常状態
	//--------------------------------------------------------------------------------------
	class TorusDefaultState : public ObjState<RollingTorus>
	{
		TorusDefaultState(){}

	public:
		//ステートのインスタンス取得
		static shared_ptr<TorusDefaultState> Instance();

		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<RollingTorus>& Obj)override;

		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<RollingTorus>& Obj)override;

		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<RollingTorus>& Obj)override;
	};

	/**
	* @class　Firework_Obj
	* @brief　お祝いの時、AddGameObjectをしてエフェクトを出す
	* @author yuyu sike
	* @date 2016/05/12
	*/
	class Firework_Obj : public GameObject{
		Vector3 m_Pos;
		//インターバル
		size_t m_sztIntervalCount;
	public:
		Firework_Obj(const shared_ptr<Stage>& StagePtr, const Vector3 i_FirstPos);
		virtual ~Firework_Obj(){}
		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
	};
	/**
	* @class　Earth
	* @brief　下の土台
	* @author yuyu sike
	* @date 2016/05/20
	*/
	class Earth : public GameObject{
		Vector3 m_Pos;
		//インターバル
		size_t m_sztIntervalCount;
	public:
		Earth(const shared_ptr<Stage>& StagePtr, const Vector3 i_FirstPos);
		virtual ~Earth(){}
		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
	};
}
//endof  basedx11