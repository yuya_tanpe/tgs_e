#include "stdafx.h"
#include "Project.h"
namespace basedx11{
	//--------------------------------------------------------------------------------------
	//	class GameView : public GameObject;
	//	用途: ビュークラス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	GameView::GameView(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr)
	{
	}
	GameView::~GameView(){}

	//初期化
	void GameView::OnCreate(){
		SetUpdateActive(false);
	}

	//描画
	void GameView::OnDraw(){
		//雲を描画する
		DrawCloud();

		//目印の描画
		DrawMark();

		//プレイヤーを描画する
		DrawPlayer();

		//入力バーを描画する
		DrawInputBar();

		//敵の描画
		DrawEnemy();

		//アイテムの描画
		DrawItem();
	}
	//--------------------------------------------------------------------------------------
	//	struct GameView::CloudData;
	//	用途: 雲描画に行う際のインスタンス管理
	//--------------------------------------------------------------------------------------
	//雲の描画関連の構造体
	struct GameView::CloudData{
		shared_ptr<GameDocument> m_Document;
		shared_ptr<Cloud> m_WhiteCloud;
		shared_ptr<Cloud> m_NonCloud;
		shared_ptr<Cloud> m_ThunderCloud;

		CloudData(shared_ptr<GameDocument> doc, shared_ptr<Cloud> white, shared_ptr<Cloud> non, shared_ptr<Cloud> thunder)
		{
			this->m_Document = doc;
			this->m_WhiteCloud = white;
			this->m_NonCloud = non;
			this->m_ThunderCloud = thunder;
		}

		//白い雲のTransformの取得
		shared_ptr<Transform> GetTransform_WhiteCloud(){ return m_WhiteCloud->GetComponent<Transform>(); }

		//無雲のTransformの取得
		shared_ptr<Transform> GetTransform_NonCloud(){ return m_NonCloud->GetComponent<Transform>(); }

		//雷雲のTransformの取得
		shared_ptr<Transform> GetTransform_ThunderCloud(){ return m_ThunderCloud->GetComponent<Transform>(); }
	};

	//雲を描画する
	void GameView::DrawCloud(){
		auto DocumentPtr = GetStage()->GetSharedGameObject<GameDocument>(L"GameDocument");
		auto WhitePtr = GetStage()->GetSharedGameObject<Cloud>(L"WhiteCloud");
		auto NonePtr = GetStage()->GetSharedGameObject<Cloud>(L"NoneCloud");
		auto Thunder = GetStage()->GetSharedGameObject<Cloud>(L"ThunderCloud");
		//雲の描画に必要な構造体を作成
		GameView::CloudData PtrData(DocumentPtr, WhitePtr, NonePtr, Thunder);
		//左右入力があった場合、描画処理を変更する
		//特定の行だけ違うDrawが行う
		if (DocumentPtr->GetStickRLMove()){
			NextUnderCloud_Ex(PtrData, DocumentPtr->GetChangeRow());
			//Trueなら右移動
			if (DocumentPtr->GetRLStick()){
				MoveRightCloud(PtrData, DocumentPtr->GetChangeRow());
			}
			else{
				MoveLeftCloud(PtrData, DocumentPtr->GetChangeRow());
			}
		}
		else{
			//雲を下に移動させる描画を行う
			//左右入力を受けてないから全部の描画を担当する
			NextUnderCloud(PtrData);
		}
	}

	//雲を下に移動させる描画を行う
	//入力バーが一番下の時は、このDrawを呼ぶこと
	void GameView::NextUnderCloud(GameView::CloudData& data){
		//Xの値は固定値
		static float StartX = -2.5f;
		float StartZ = 1.0f - data.m_Document->GetTotalTime();
		Vector3 Pos;
		for (size_t y = 0; y < data.m_Document->GetCloudMap().size(); y++){
			for (size_t x = 0; x < data.m_Document->GetCloudMap()[y].size(); x++){
				Pos.x = StartX + x * 1.2f;
				Pos.y = 0;
				Pos.z = StartZ + y * 1.0f;
				switch (data.m_Document->GetCloudMap()[y][x]){
				case 1:
					//白色の雲
					data.GetTransform_WhiteCloud()->SetPosition(Pos);
					data.m_WhiteCloud->OnDraw();
					break;
				case 2:
					//雷雲
					data.GetTransform_ThunderCloud()->SetPosition(Pos);
					data.m_ThunderCloud->OnDraw();
					break;
				case 0:
				default:
					break;
				}
			}
		}
	}

	//雲を下に移動させる描画を行う※特定の行以外を描画
	void GameView::NextUnderCloud_Ex(GameView::CloudData& data, const size_t& row){
		//Xの値は固定値
		static float StartX = -2.5f;
		float StartZ = 1.0f - data.m_Document->GetTotalTime();
		Vector3 Pos;
		for (size_t y = 0; y < data.m_Document->GetCloudMap().size(); y++){
			for (size_t x = 0; x < data.m_Document->GetCloudMap()[y].size(); x++){
				//特定の行だけ返して描画しない
				if (y != row){
					//雲の描画
					Pos.x = StartX + x * 1.2f;
					Pos.y = 0;
					Pos.z = StartZ + y * 1.0f;
					switch (data.m_Document->GetCloudMap()[y][x]){
					case 1:
						data.GetTransform_WhiteCloud()->SetPosition(Pos);
						data.m_WhiteCloud->OnDraw();
						break;
					case 2:
						//雷雲
						data.GetTransform_ThunderCloud()->SetPosition(Pos);
						data.m_ThunderCloud->OnDraw();
						break;
					case 0:
					default:
						break;
					}
				}
			}
		}
	}

	//特定の行の雲を右に移動させる
	void GameView::MoveRightCloud(GameView::CloudData& data, size_t row){
		assert(row < 20 && row >= 0 && " 0 ~ 19の値で設定 ");

		float StartX = -2.5f;
		float StartZ = 1.0f - data.m_Document->GetTotalTime();
		Vector3 Pos;

		//３まで回す
		for (size_t x = 0; x < data.m_Document->GetLineVec(row).size(); x++){
			Pos.x = Lerp::CalculateLerp<float>(StartX - 1.2f + x * 1.2f, StartX + x * 1.2f, 0.0f,
				data.m_Document->GetRLMoveTime(), data.m_Document->GetRLMoveNowTime(), Lerp::rate::EaseOut);
			Pos.y = 0.0f;
			Pos.z = StartZ + row * 1.0f;

			switch (data.m_Document->GetCloudMap()[row][x]){
			case 1:
				data.GetTransform_WhiteCloud()->SetPosition(Pos);
				data.m_WhiteCloud->OnDraw();
				break;
			case 2:
				//雷雲
				data.GetTransform_ThunderCloud()->SetPosition(Pos);
				data.m_ThunderCloud->OnDraw();
				break;
			case 0:
			default:
				break;
			}
		}
	}
	//特定の行の雲を左に移動させる
	void GameView::MoveLeftCloud(GameView::CloudData& data, size_t row){
		assert(row < 20 && row >= 0 && " 0 ~ 19の値で設定 ");

		float StartX = -2.5f;
		float StartZ = 1.0f - data.m_Document->GetTotalTime();
		Vector3 Pos;

		//３まで回す
		for (size_t x = 0; x < data.m_Document->GetLineVec(row).size(); x++){
			Pos.x = Lerp::CalculateLerp<float>(StartX + 1.2f + x * 1.2f, StartX + x * 1.2f, 0.0f,
				data.m_Document->GetRLMoveTime(), data.m_Document->GetRLMoveNowTime(), Lerp::rate::EaseOut);
			Pos.y = 0;
			Pos.z = StartZ + row * 1.0f;

			switch (data.m_Document->GetCloudMap()[row][x]){
			case 1:
				data.GetTransform_WhiteCloud()->SetPosition(Pos);
				data.m_WhiteCloud->OnDraw();
				break;
			case 2:
				//雷雲
				data.GetTransform_ThunderCloud()->SetPosition(Pos);
				data.m_ThunderCloud->OnDraw();
				break;
			case 3:
			
				break;
			case 0:
			default:
				break;
			}
		}
	}

	//--------------------------------------------------------------------------------------
	//	struct GameView::BarData;
	//	用途: 雲描画に行う際のインスタンス管理
	//--------------------------------------------------------------------------------------
	struct GameView::BarData
	{
		shared_ptr<GameDocument> m_Document;
		shared_ptr<input_bar> m_inputbar;
	public:
		BarData(shared_ptr<GameDocument> doc, shared_ptr<input_bar> bar) {
			this->m_Document = doc;
			this->m_inputbar = bar;
		}

		//入力バーのTransformの取得
		shared_ptr<Transform> GetTransform_InputBar(){ return m_inputbar->GetComponent<Transform>(); }
	};

	//入力バーを描画する
	void GameView::DrawInputBar(){
		auto DocumentPtr = GetStage()->GetSharedGameObject<GameDocument>(L"GameDocument");
		auto BarPtr = GetStage()->GetSharedGameObject<input_bar>(L"input_bar");

		//構造体の作成
		GameView::BarData PtrBarData{ DocumentPtr, BarPtr };
		NextUnderBar(PtrBarData);
	}

	//入力バーを下に移動させる描画を行う
	void GameView::NextUnderBar(GameView::BarData& data){
		//Xの値は固定値
		static float StartX = -0.1f;
		//float KStartZ = 1.0f - data.m_Document->GetTotalTime();
		float StartZ = data.m_inputbar->GetNowRow() * 1.0f - data.m_Document->GetTotalTime();
		Vector3 Pos;
		//Vector3 KPos;
		for (size_t y = 0; y < data.m_Document->GetBarMap().size(); y++){
			for (size_t x = 0; x < data.m_Document->GetBarMap()[y].size(); x++){
				//入力バーのPosition
				Pos.x = StartX;
				Pos.y = 0.3f;
				Pos.z = StartZ + 1.0f;
				//Pos.z = Lerp::CalculateLerp<float>(StartZ, StartZ + 1.0f, 0,
				//data.m_Document->GetTotalTime(), data.m_Document->GetNowTime(), Lerp::rate::Linear);
				switch (data.m_Document->GetBarMap()[y][x]){
				case 1:
					data.GetTransform_InputBar()->SetPosition(Pos);
					data.m_inputbar->OnDraw();
					break;
				case 0:
				default:
					break;
				}
			}
		}
	}
	//--------------------------------------------------------------------------------------
	//	struct GameView::MarkData;
	//	用途: 雲描画に行う際のインスタンス管理
	//--------------------------------------------------------------------------------------
	struct GameView::MarkData
	{
		shared_ptr<GameDocument> m_Document;
		shared_ptr<Kanban> m_Kanban;
	public:
		MarkData(shared_ptr<GameDocument> doc, shared_ptr<Kanban> kanban) {
			this->m_Document = doc;
			this->m_Kanban = kanban;
		}

		//看板のTransformの取得
		shared_ptr<Transform> GetTransform_Kanabn(){ return m_Kanban->GetComponent<Transform>(); }
	};

	//入力バーを描画する
	void GameView::DrawMark(){
		auto DocumentPtr = GetStage()->GetSharedGameObject<GameDocument>(L"GameDocument");
		auto KanbanPtr = GetStage()->GetSharedGameObject<Kanban>(L"Kanban");

		//構造体の作成
		GameView::MarkData PtrMarkData{ DocumentPtr, KanbanPtr };
		NextUnderMark(PtrMarkData);
	}

	//入力バーを下に移動させる描画を行う
	void GameView::NextUnderMark(MarkData& data){
		//Xの値は固定値
		static float StartX = -0.1f;
		float KStartZ = 1.0f - data.m_Document->GetTotalTime();
		Vector3 KPos;
		for (size_t y = 0; y < data.m_Document->GetMarkMap().size(); y++){
			for (size_t x = 0; x < data.m_Document->GetMarkMap()[y].size(); x++){
				////看板のPosition
				KPos.x = StartX;
				KPos.y = 0.0f;
				KPos.z = KStartZ + y * 1.0f;
				switch (data.m_Document->GetMarkMap()[y][x]){
				case 1:
					data.GetTransform_Kanabn()->SetPosition(KPos);
					data.m_Kanban->OnDraw();
					break;
				case 0:
				default:
					break;
				}
			}
		}
	}

	//--------------------------------------------------------------------------------------
	//	struct GameView::PlayerData;
	//	用途: プレイヤーの描画に行う際のインスタンス管理
	//--------------------------------------------------------------------------------------
	struct GameView::PlayerData
	{
		shared_ptr<GameDocument> m_Document;
		shared_ptr<Sheep> m_Sheep;
	public:
		PlayerData(shared_ptr<GameDocument> doc, shared_ptr<Sheep> sheep) {
			this->m_Document = doc;
			this->m_Sheep = sheep;
		}

		//入力バーのTransformの取得
		shared_ptr<TransformMatrix> GetTransform_Player(){
			return m_Sheep->GetComponent<TransformMatrix>();
		}
	};

	//プレイヤーの描画
	void GameView::DrawPlayer(){
		auto DocumentPtr = GetStage()->GetSharedGameObject<GameDocument>(L"GameDocument");
		auto PlayerPtr = GetStage()->GetSharedGameObject<Sheep>(L"Sheep");

		//構造体の作成
		GameView::PlayerData PtrPlayerData{ DocumentPtr, PlayerPtr };

		//プレイヤーが下にいく描画
		NextUnderPlayer(PtrPlayerData);
	}

	//プレイヤーを下に移動させる
	void GameView::NextUnderPlayer(GameView::PlayerData& data){
		//Xの値は固定値
		static float StartX = -2.5f;
		float StartZ = 1.0f - data.m_Document->GetTotalTime();

		Vector3 Pos;
		for (size_t y = 0; y < data.m_Document->GetPlayerMap().size(); y++){
			for (size_t x = 0; x < data.m_Document->GetPlayerMap()[y].size(); x++){
				Pos.x = StartX + x * 1.2f;
				Pos.y = 0.45f;
				Pos.z = StartZ + y * 1.0f + 0.2f;
				//Pos.z = Lerp::CalculateLerp<float>(StartZ, StartZ + 1, 0,
				//	data.m_Document->GetTotalTime(), data.m_Document->GetNowTime(), Lerp::rate::Linear);
				if (data.m_Document->GetPlayerMap()[y][x]){
					//case 1:
					////	data.GetTransform_Player()->SetPosition(Pos);
					////	data.m_Sheep->OnDraw();

					Matrix4X4 WorldMat = data.GetTransform_Player()->GetWorldMatrix();
					Vector3 Scale, TempPos;
					Quaternion Qt;
					WorldMat.Decompose(Scale, Qt, TempPos);
					WorldMat.DefTransformation(Scale, Qt, Pos);

					WorldMat = WorldMat * data.m_Sheep->GetLocalAnimeMatrix();

					data.GetTransform_Player()->SetWorldMatrix(WorldMat);
					data.m_Sheep->OnDraw();
					/*	break;
					case 0:
					default:
					break;*/
				}
			}
		}
	}

	//--------------------------------------------------------------------------------------
	//	struct GameView::EnemyData;
	//	用途: 敵の描画に行う際のインスタンス管理
	//--------------------------------------------------------------------------------------
	struct GameView::EnemyData
	{
		shared_ptr<GameDocument> m_Document;
	public:
		EnemyData(shared_ptr<GameDocument> doc) {
			this->m_Document = doc;
		}
	};

	//敵の描画
	void GameView::DrawEnemy(){
		auto DocumentPtr = GetStage()->GetSharedGameObject<GameDocument>(L"GameDocument");

		GameView::EnemyData PtrEnemyData{ DocumentPtr };
		NextUnderEnemy(PtrEnemyData);
	}

	//敵が下に移動してくる描画を行う
	void GameView::NextUnderEnemy(GameView::EnemyData& data){
		//敵ポインターのマップを取得
		auto& EnemyPtrMap = data.m_Document->GetEnemyPtrMap();

		//Xの値は固定値
		static float StartX = -2.5f;
		float StartZ = 1.0f - data.m_Document->GetTotalTime();

		Vector3 Pos;
		for (size_t y = 0; y < EnemyPtrMap.size(); y++){
			for (size_t x = 0; x < EnemyPtrMap[y].size(); x++){
				Pos.x = StartX + x * 1.2f;
				Pos.y = 0.8f;
				Pos.z = StartZ + y * 1.0f;
				//Pos.z = Lerp::CalculateLerp<float>(StartZ, StartZ + 1, 0,
				//	data.m_Document->GetTotalTime(), data.m_Document->GetNowTime(), Lerp::rate::Linear);
				if (EnemyPtrMap[y][x] && !EnemyPtrMap[y][x]->IsRefreshAbled()){
					EnemyPtrMap[y][x]->GetComponent<Transform>()->SetPosition(Pos);
					EnemyPtrMap[y][x]->OnDraw();
				}

				/*switch (data.m_Document->GetEnemyMap()[y][x]){
				case 1:
					data.GetTransform_NonMoveEnemy()->SetPosition(Pos);
					data.m_NonMoveEnemy->OnDraw();
					break;
				case 0:
				default:
					break;
				}*/
			}
		}
	}

	//--------------------------------------------------------------------------------------
	//	struct GameView::ItemData;
	//	用途: 敵の描画に行う際のインスタンス管理
	//--------------------------------------------------------------------------------------
	struct GameView::ItemData
	{
		shared_ptr<GameDocument> m_Document;
		shared_ptr<Kanban> m_Kanban;
		shared_ptr<Cloud> m_WhiteCloud;

	public:
		ItemData(shared_ptr<GameDocument> doc, shared_ptr<Kanban> kanban, shared_ptr<Cloud> white) {
			this->m_Document = doc;
			this->m_Kanban = kanban;
			this->m_WhiteCloud = white;
		}
		//看板のTranformの取得
		shared_ptr<Transform> GetTranform_Kanban(){ return m_Kanban->GetComponent<Transform>(); }

		//看板下の雲のTranformの取得
		shared_ptr<Transform> GetTransform_WhiteCloud(){ return m_WhiteCloud->GetComponent<Transform>(); }
	};

	//アイテムの描画
	void GameView::DrawItem(){
		auto DocumentPtr = GetStage()->GetSharedGameObject<GameDocument>(L"GameDocument");
		auto KanbanPtr = GetStage()->GetSharedGameObject<Kanban>(L"Kanban");
		auto white = GetStage()->GetSharedGameObject<Cloud>(L"WhiteCloud");

		//アイテム描画用の構造体の作成
		GameView::ItemData PtrItemData{ DocumentPtr, KanbanPtr, white };

		//アイテムが下に移動していく描画
		NextUnderItem(PtrItemData);
	}

	//アイテムが下に移動してくる描画を行う
	void GameView::NextUnderItem(GameView::ItemData& data){
		auto& CoinPtrMap = data.m_Document->GetCoinPtrMap();

		//コイン関連の固定値
		//Xの値は固定値
		static float StartX = -2.5f;
		static float StartY = 0.6f;
		float StartZ =  1.0f - data.m_Document->GetTotalTime();
		Vector3 Pos;
	
		for (size_t y = 0; y < CoinPtrMap.size(); y++){
			for (size_t x = 0; x < CoinPtrMap[y].size(); x++){
				//コイン関連
				Pos.x = StartX + x * 1.2f;
				Pos.y = StartY;
				Pos.z = StartZ + y * 1.0f; 
				if (CoinPtrMap[y][x] && !CoinPtrMap[y][x]->IsRefreshAbled()){
					CoinPtrMap[y][x]->GetComponent<Transform>()->SetPosition(Pos);
					CoinPtrMap[y][x]->OnDraw();
				}
/*
				switch (data.m_Document->GetItemMap()[y][x]){
				case 1:
					data.GetTransform_CoinS()->SetPosition(Pos);
					data.m_CoinS->OnDraw();
					break;
				case 2:
				case 3:
				case 0:
				default:
					break;
				}*/
			}
		}
	}
}
//endof  basedx11