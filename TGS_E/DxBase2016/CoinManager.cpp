#include "stdafx.h"
#include "Project.h"

namespace basedx11{
	/**
	* @class CoinSprite
	* @brief コイン数表記 0 ~ 9までの表示
	* @author yuya
	* @date 2016/06/09
	*/
	/**
	* @class BonusItemSprite
	* @brief ボーナススコア表記 0 ~ 9までの表示
	* @author yuya
	* @date 2016/05/12
	*/

	//構築と破棄
	CoinSprite::CoinSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos, const bool DrawTF) :
		GameObject(StagePtr), m_StartPos(StartPos), m_DrawActive(DrawTF){
	}
	CoinSprite::~CoinSprite(){}

	//初期化
	void CoinSprite::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(1.0f, 1.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"NUMBER_TX");
		//透明処理
		SetAlphaActive(true);
		//描画するか
		SetDrawActive(m_DrawActive);
		//左上原点
		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftTopZeroPlusDownY);
		//スプライトの中のメッシュからバックアップの取得
		auto& SpVertexVec = PtrSprite->GetMeshResource()->GetBackupVerteces<VertexPositionColorTexture>();
		//各数字ごとにUV値を含む頂点データを配列化しておく
		for (size_t i = 0; i < 10; i++){
			float from = ((float)i) / 10.0f;
			float to = from + (1.0f / 10.0f);
			vector<VertexPositionColorTexture> NumVirtex =
			{
				//左上頂点
				VertexPositionColorTexture(
				SpVertexVec[0].position,
				Color4(1.0f, 1.0f, 1.0f, 1.0f),
				Vector2(from, 0)
				),
				//右上頂点
				VertexPositionColorTexture(
				SpVertexVec[1].position,
				Color4(1.0f, 1.0f, 1.0f, 1.0f),
				Vector2(to, 0)
				),
				//左下頂点
				VertexPositionColorTexture(
				SpVertexVec[2].position,
				Color4(1.0f, 1.0f, 1.0f, 1.0f),
				Vector2(from, 1.0f)
				),
				//右下頂点
				VertexPositionColorTexture(
				SpVertexVec[3].position,
				Color4(1.0f, 1.0f, 1.0f, 1.0f),
				Vector2(to, 1.0f)
				),
			};
			m_NumberVertexVec.push_back(NumVirtex);
		}

		auto MeshRes = PtrSprite->GetMeshResource();
		//動的にUV値が変わる頂点を設定する
		MeshRes->UpdateVirtexBuffer(m_NumberVertexVec[0]);
	}

	//描画を行う
	void CoinSprite::DrawActive(){
		//描画するか
		SetDrawActive(true);
	}
	//スコアの描画
	void CoinSprite::ScoreDraw(int score){
		if (score > 0){
			DrawActive();
		}
		//スプライトを取得
		auto PtrSprite = GetComponent<PCTSpriteDraw>();
		auto MeshRes = PtrSprite->GetMeshResource();
		//動的にUV値が変わる頂点を設定する
		MeshRes->UpdateVirtexBuffer(m_NumberVertexVec[score]);
	}

	/**
	* @class CoinManager
	* @brief コインスプライトの管理更新を行う
	* @author yuya
	* @date 2016/06/09
	*/
	CoinManager::CoinManager(shared_ptr<Stage>& StagePtr, shared_ptr<CoinSprite> KURAI_1, shared_ptr<CoinSprite> KURAI_10,
		shared_ptr<CoinSprite> KURAI_100, shared_ptr<CoinSprite> KURAI_1000) :

		GameObject(StagePtr), m_KURAI1(KURAI_1), m_KURAI10(KURAI_10), m_KURAI100(KURAI_100), m_KURAI1000(KURAI_1000),
		Coin(0), place(1)
	{
		//4桁対応
		for (int i = 0; i < 4; i++){
			place_array[i] = 0;   //任意の数字を代入
		}
	}
	
	//スコアの加点
	void CoinManager::Coin_1_Plus(){
		Coin++;
		for (int a = 0; a < 4; a++){
			place_array[a] = Coin / place % 10;
			place *= 10;
		}
		//桁の初期化
		place = 1;
		CoinUpdate(place_array[0], place_array[1], place_array[2], place_array[3]);
	}

	//コインを１0枚追加
	void CoinManager::Coin_10_Plus(){
		Coin += 10;
		for (int a = 0; a < 4; a++){
			place_array[a] = Coin / place % 10;
			place *= 10;
		}
		//桁の初期化
		place = 1;
		CoinUpdate(place_array[0], place_array[1], place_array[2], place_array[3]);
	}

	//コインスプライトの最終更新
	void CoinManager::CoinUpdate(const int score_1, const int score_10, const int score_100, const int score_1000){
		m_KURAI1->ScoreDraw(score_1);
		m_KURAI10->ScoreDraw(score_10);
		m_KURAI100->ScoreDraw(score_100);
		m_KURAI1000->ScoreDraw(score_1000);
	}
}
//endof  basedx11