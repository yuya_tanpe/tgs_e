#include "stdafx.h"
#include "Project.h"

namespace basedx11
{
	SideWall::SideWall(const shared_ptr<Stage>& StagePtr, const Vector3& i_start_pos) :
		GameObject(StagePtr),
		m_Pos(i_start_pos)
	{}

	//初期化
	void SideWall::OnCreate(){
		//Transformの設定
		auto PtrTrans = AddComponent<Transform>();
		PtrTrans->SetPosition(m_Pos);
		PtrTrans->SetRotation(0.0f, 0.0f, 0.0f);
		PtrTrans->SetScale(0.2f, 0.5f, 20.0f);
		SetDrawLayer(0);
		//描画コンポーネント
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetTextureResource(L"WHITE_TX");
	}

	RollingTorus::RollingTorus(const shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos), m_YRot(0),
		m_MaxRotationSpeed(20.0f),
		m_RotationSpeed(0.0f)
	{}

	RollingTorus::~RollingTorus(){}

	//初期化
	void RollingTorus::OnCreate(){
		auto Ptr = AddComponent<Transform>();
		Ptr->SetScale(0.5f, 0.5f, 0.5f);
		Ptr->SetRotation(XM_PIDIV2, m_YRot, 0.0f);
		Ptr->SetPosition(m_StartPos);

		//衝突判定をつける
		auto PtrCollision = AddComponent<CollisionSphere>();

		//トーラスのグループを得る
		auto Group = GetStage()->GetSharedObjectGroup(L"RollingTorusGroup");
		//トーラス同士は衝突しないようにしておく
		PtrCollision->SetExcludeCollisionGroup(Group);

		//重力をつける
		auto PtrGravity = AddComponent<Gravity>();
		//最下地点
		PtrGravity->SetBaseY(0.25f);
		//ジャンプスタート
		PtrGravity->StartJump(Vector3(0, 4.0f, 0));

		//影の作成
		auto ShadowPtr = AddComponent<Shadowmap>();
		//影の形状
		ShadowPtr->SetMeshResource(L"DEFAULT_TORUS");

		//描画コンポーネント
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		//メッシュの登録
		PtrDraw->SetMeshResource(L"DEFAULT_TORUS");

		PtrDraw->SetDiffuse(Color4(1.0f, 1.0f, 0, 1.0f));

		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<RollingTorus> >(GetThis<RollingTorus>());
		//最初のステートをDefaultStateに設定
		m_StateMachine->SetCurrentState(TorusDefaultState::Instance());
		//DefaultStateの初期化実行を行う
		m_StateMachine->GetCurrentState()->Enter(GetThis<RollingTorus>());
	}

	void RollingTorus::OnUpdate()
	{
		//ステートマシンのUpdateを行う
		//この中でステートの切り替えが行われる
		m_StateMachine->Update();
	}

	void RollingTorus::OnCollision(const shared_ptr<GameObject>& other)
	{
		SetDrawActive(false);
		SetUpdateActive(false);
	}

	//--------------------------------------------------------------------------------------
	//	class TorusDefaultState : public ObjState<RollingTorus>;
	//	用途: 通常状態
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<TorusDefaultState> TorusDefaultState::Instance()
	{
		static shared_ptr<TorusDefaultState> instance;
		if (!instance)
		{
			instance = shared_ptr<TorusDefaultState>(new TorusDefaultState);
		}
		return instance;
	}

	//ステートに入ったときに呼ばれる関数
	void TorusDefaultState::Enter(const shared_ptr<RollingTorus>& Obj){
		//何もしない
	}

	//ステート実行中に毎ターン呼ばれる関数
	void TorusDefaultState::Execute(const shared_ptr<RollingTorus>& Obj){
	}

	//ステートにから抜けるときに呼ばれる関数
	void TorusDefaultState::Exit(const shared_ptr<RollingTorus>& Obj){
		//何もしない
	}

	/**
	* @class　Firework_Obj
	* @brief　お祝いの時、AddGameObjectをしてエフェクトを出す
	* @author yuyu sike
	* @date 2016/05/12
	*/
	Firework_Obj::Firework_Obj(const shared_ptr<Stage>& StagePtr, const Vector3 i_FirstPos) :
		GameObject(StagePtr),
		m_Pos(i_FirstPos)
	{}
	//作成
	void Firework_Obj::OnCreate(){
		auto Trans = AddComponent<Transform>();
		Trans->SetPosition(m_Pos);
		Trans->SetRotation(Vector3(0, 0, 0));
		Trans->SetScale(Vector3(1.0f, 1.0f, 1.0f));
	}
	//変化
	void Firework_Obj::OnUpdate(){
		const size_t sztInterval = 2;			//インターバル処理に使うパーティクルが出るまでの待ちフレーム

		//////インターバル処理	3フレーム毎にエフェクトの呼び出し
		//if (m_sztIntervalCount++ < sztInterval){
		//	return;

		//}
		//else {
		//	m_sztIntervalCount = 0;

		//}

		//スパークの放出
		auto PtrSpark = GetStage()->GetSharedGameObject<Firework_Effect>(L"Firework_Effect", false);
		if (PtrSpark){
			PtrSpark->InsertFirework_Effect(GetComponent<Transform>()->GetPosition());
		}
		auto Trans = GetComponent<Transform>();
		Vector3 Position = Trans->GetPosition();
		Position += Vector3(0.0f, 0.2f, 0.0f);
		Trans->SetPosition(Position);
	}

	/**
	* @class　Earth
	* @brief　下の土台
	* @author yuyu sike
	* @date 2016/05/20
	*/
	Earth::Earth(const shared_ptr<Stage>& StagePtr, const Vector3 i_FirstPos) :
		GameObject(StagePtr),
		m_Pos(i_FirstPos)
	{}
	//作成
	void Earth::OnCreate(){
		auto Ptr = AddComponent<Transform>();
		Ptr->SetScale(50.0f, 50.0f, 50.0f);
		Ptr->SetRotation(90.0f, 90.0f, 0.0f);
		Ptr->SetPosition(m_Pos);

		//描画コンポーネント
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		//メッシュの登録
		PtrDraw->SetMeshResource(L"DEFAULT_SPHERE");
		PtrDraw->SetTextureResource(L"WATER_TX");
	}
	//変化
	void Earth::OnUpdate(){
	}
}
//endof  basedx11