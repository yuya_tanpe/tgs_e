#pragma once

#include "stdafx.h"

namespace basedx11
{
	class TitleUI : public GameObject
	{
		Vector3 m_Pos;

	public:
		//�\�z�Ɣj��
		TitleUI(shared_ptr<Stage>& StagePtr, const Vector3& Pos);
		virtual ~TitleUI();

		//������
		virtual void OnCreate() override;
	};

	// �r�̃C���X�g
	class Title_SheepSprite : public GameObject
	{
		Vector3 m_Pos;

	public:
		// �\�z�Ɣj��
		Title_SheepSprite(shared_ptr<Stage>& StagePtr, const Vector3& Pos);
		virtual ~Title_SheepSprite();

		// ������
		virtual void OnCreate() override;
	};
}
//endof  basedx1