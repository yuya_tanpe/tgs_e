#pragma once

#include "stdafx.h"

namespace basedx11
{



	class GameOverScene : public Stage
	{
		// リソースの作成
		void CreateResourses();

		//ビューの作成
		void CreateViews();

		// 背景の作成
		void CreateBackGround();

		// リザルトスプライトの作成
		void CreateResultSprite();

		// スコアスプライトの作成
		void CreateScoreSprite();

		// ボーナススコアスプライトの作成
		void CreateBonusScoreSprite();

		// トータルスコアスプライトの作成
		void CreateTotalScoreSprite();

		// Aボタンスプライトの作成
		void CreateAButtonSprite();

		// Bボタンスプライトの作成
		void CreateBButtonSprite();

		//進んだ距離の得点
		void CreateScoreNum();

		//ボーナススコアの表示
		void CrieateBonusNum();

		//合計スコア
		void CrieateTotalNum();


		//音のストップ
		void SoundStop(wstring soundname);

		//音の再生
		void SoundPlay(wstring soundname, float volume);

		//ボタンの再入力されないようにする
		bool IsButtonPush;

	public:
		//構築と破棄
		GameOverScene() :Stage(), IsButtonPush(false){}
		virtual ~GameOverScene(){}

		//初期化
		virtual void OnCreate()override;

		//更新
		virtual void OnUpdate()override;
	};
}
//endof  basedx11