#include "stdafx.h"
#include "Project.h"

namespace basedx11
{
	/**
	* @class GameSystem
	* @brief ゲームステージに常駐するobject　主に命令を送るだけ
	* @author　sike yuya
	* @date	2016/05/11
	*/
	GameSystem::GameSystem(shared_ptr<Stage>& StagePtr, const shared_ptr<ScoreManager> ptr_scoremanager) :
		GameObject(StagePtr), m_ScoreManagerPtr(ptr_scoremanager)
	{}

	void GameSystem::OnCreate(){
		// SEの再生
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"FIREWORKS");
	}

	//変化
	void GameSystem::OnUpdate(){}

	void GameSystem::OnEvent(const shared_ptr<Event>& event){
		if (event->m_MsgStr == L"Fifty_Comment"){
		}
		if (event->m_MsgStr == L"Boss_Alert"){
		}
	}
}
//endof  basedx11