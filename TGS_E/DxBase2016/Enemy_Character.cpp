#include "stdafx.h"
#include "Project.h"

namespace basedx11{

	NonMove_Enemy::NonMove_Enemy(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr)
	{}
	void  NonMove_Enemy::OnCreate(){
		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<NonMove_Enemy> >(GetThis<NonMove_Enemy>());
		//最初のステートをPlayer_NoHitStateに設定
		m_StateMachine->ChangeState(Player_NoHitState::Instance());
	}

	void NonMove_Enemy::OnUpdate(){
		//ステートマシンのUpdateを行う
		m_StateMachine->Update();
	}

	//ステートの初期化
	void NonMove_Enemy::ResetState(){
		//最初のステート設定
		m_StateMachine->ChangeState(Player_NoHitState::Instance());
	}

	//プレイヤーとの衝突したときに呼ぶ
	void NonMove_Enemy::GoPlayerKillMotion(){
		if (GetStateMachine()->GetCurrentState() == Player_NoHitState::Instance()){
			//各敵毎のアクション
			EnemyAction();
			//当たったステートに変更
			GetStateMachine()->ChangeState(PlayerKillState::Instance());
		}
	}

	//このオブジェクトがリフレッシュステートかの取得
	bool NonMove_Enemy::IsRefreshAbled(){
		if (GetStateMachine()->GetCurrentState() == Refresh_NonEnemyState::Instance()){
			return true;
		}
		return false;
	}

	//-----------------------------------------
	//-----Player_NoHitStateで使用Motion--------
	//-----------------------------------------
	//最初のステートでの動き
	void NonMove_Enemy::ExecFirstMotion(){}

	//-----------------------------------------
	//------PlayerKillStateで使用Motion--------
	//-----------------------------------------
	//当たった時のステート準備
	void NonMove_Enemy::StartHitMotion(){
		auto DocumentPtr = GetStage()->GetSharedGameObject<GameDocument>(L"GameDocument");
		auto& EnemyPtrMap = DocumentPtr->GetEnemyPtrMap();
		for (size_t y = 0; y < EnemyPtrMap.size(); y++){
			for (size_t x = 0; x < EnemyPtrMap[y].size(); x++){
				if (EnemyPtrMap[y][x] == GetThis<NonMove_Enemy>()){
					EnemyPtrMap[y][x] = nullptr;
				}
			}
		}
	}

	//敵がリフレッシュ待ちステートの準備
	void NonMove_Enemy::EndHitMotion(){
		auto DocumentPtr = GetStage()->GetSharedGameObject<GameDocument>(L"GameDocument");
		auto& EnemyPtrMap = DocumentPtr->GetEnemyPtrMap();
		for (size_t y = 0; y < EnemyPtrMap.size(); y++){
			for (size_t x = 0; x < EnemyPtrMap[y].size(); x++){
				if (EnemyPtrMap[y][x] == GetThis<NonMove_Enemy>()){
					EnemyPtrMap[y][x] = nullptr;
				}
			}
		}
	}

	//--------------------------------------------------------------------------------------
	//	class Player_NoHitState : public ObjState<Enemy>;
	//	用途: プレイヤーと当たってない時
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<Player_NoHitState> Player_NoHitState::Instance(){
		static shared_ptr<Player_NoHitState> instance;
		if (!instance){
			instance = shared_ptr<Player_NoHitState>(new Player_NoHitState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void Player_NoHitState::Enter(const shared_ptr<NonMove_Enemy>& Obj){}
	//ステート実行中に毎ターン呼ばれる関数
	void Player_NoHitState::Execute(const shared_ptr<NonMove_Enemy>& Obj){
		Obj->ExecFirstMotion();
	}
	//ステートにから抜けるときに呼ばれる関数
	void Player_NoHitState::Exit(const shared_ptr<NonMove_Enemy>& Obj){}

	//--------------------------------------------------------------------------------------
	//	class PlayerKillState : public ObjState<Enemy>;
	//	用途: プレイヤーと当たった時
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<PlayerKillState> PlayerKillState::Instance(){
		static shared_ptr<PlayerKillState> instance;
		if (!instance){
			instance = shared_ptr<PlayerKillState>(new PlayerKillState);
		}
		return instance;
	}
	void PlayerKillState::Enter(const shared_ptr<NonMove_Enemy>& Obj){
		Obj->StartHitMotion();
	}
	void PlayerKillState::Execute(const shared_ptr<NonMove_Enemy>& Obj){}
	void PlayerKillState::Exit(const shared_ptr<NonMove_Enemy>& Obj){
		Obj->EndHitMotion();
	}

	//--------------------------------------------------------------------------------------
	//	class Refresh_NonEnemyState : public ObjState<Enemy>;
	//	用途: リフレッシュ待ちステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<Refresh_NonEnemyState> Refresh_NonEnemyState::Instance(){
		static shared_ptr<Refresh_NonEnemyState> instance;
		if (!instance){
			instance = shared_ptr<Refresh_NonEnemyState>(new Refresh_NonEnemyState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void Refresh_NonEnemyState::Enter(const shared_ptr<NonMove_Enemy>& Obj){
	}
	//ステート実行中に毎ターン呼ばれる関数
	void Refresh_NonEnemyState::Execute(const shared_ptr<NonMove_Enemy>& Obj){}
	//ステートにから抜けるときに呼ばれる関数
	void Refresh_NonEnemyState::Exit(const shared_ptr<NonMove_Enemy>& Obj){}


	//--------------------------------------------------------------------------------------
	//	class KillEnemyState : public ObjState<Enemy>;
	//	用途: プレイヤーに倒されたとき
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<KillEnemyState> KillEnemyState::Instance(){
		static shared_ptr<KillEnemyState> instance;
		if (!instance){
			instance = shared_ptr<KillEnemyState>(new KillEnemyState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void KillEnemyState::Enter(const shared_ptr<NonMove_Enemy>& Obj){
	}
	//ステート実行中に毎ターン呼ばれる関数
	void KillEnemyState::Execute(const shared_ptr<NonMove_Enemy>& Obj){}
	//ステートにから抜けるときに呼ばれる関数
	void KillEnemyState::Exit(const shared_ptr<NonMove_Enemy>& Obj){}



	/**
	* @class Needle_Enemy
	* @brief とげとげの敵		Enemyクラスを継承
	* @author　sike yuya
	* @date	2016/04/18
	*/

	Needle_Enemy::Needle_Enemy(const shared_ptr<Stage>& Stageptr) :
		NonMove_Enemy(Stageptr)
	{}

	void Needle_Enemy::OnCreate(){

		NonMove_Enemy::OnCreate();
		//描画処理は行わない
		SetDrawActive(false);
		//自身を敵グループに追加
		auto Group = GetStage()->GetSharedObjectGroup(L"EnemyGroup");
		Group->IntoGroup(GetThis<Needle_Enemy>());

		//Transformの設定
		auto PtrTrans = AddComponent<Transform>();
		PtrTrans->SetPosition(0, 0, 0);
		PtrTrans->SetRotation(0.0f, 0.0f, 0.0f);
		PtrTrans->SetScale(0.4f, 0.4f, 0.4f);

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(1.0f, 1.0f, 1.0f),
			Vector3(0.0f, 0.0f, 0.0f),
			Vector3(0.0f, -0.5f, 0.0f)
			);

		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<PNTStaticModelNoLightDraw>();
		//描画するメッシュを設定
		PtrDraw->SetMeshResource(L"NON_ENEMY_MESH");
		PtrDraw->SetMeshToTransformMatrix(SpanMat);
		//透明処理
		SetAlphaActive(true);

		//サウンドを登録.
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Enemy");

		//アクションの追加
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddScaleInterval(1.0f);
		PtrAction->AddScaleTo(0.2f, Vector3(0.4f, 0.4f, 0.4f));
		PtrAction->AddScaleTo(0.2f, Vector3(0.35f, 0.35f, 0.35f));
		PtrAction->AddScaleTo(0.2f, Vector3(0.4f, 0.4f, 0.4f));
		PtrAction->AddScaleTo(0.2f, Vector3(0.35f, 0.35f, 0.35f));
		//ループする
		PtrAction->SetLooped(true);
		//アクション開始
		PtrAction->Run();
	}

	//各敵毎のアクション
	void Needle_Enemy::EnemyAction(){
		//SEの再生
		GetComponent<MultiSoundEffect>()->Start(L"Enemy", 0, 0.4f);

		//エフェクトの再生
		auto PtrEffect = GetStage()->GetSharedGameObject<NonMove_HitEffect>(L"NonMove_HitEffect", false);
		PtrEffect->InsertNonMoveEffect(GetComponent<Transform>()->GetPosition());
	
	}

	//リフレッシュ関数
	void Needle_Enemy::Refresh(){
		//Transformの設定
		auto PtrTrans =	AddComponent<Transform>();
		PtrTrans->SetPosition(0, 0, 0);
		PtrTrans->SetRotation(0.0f, 0.0f, 0.0f);
		PtrTrans->SetScale(0.4f, 0.4f, 0.4f);

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(1.0f, 1.0f, 1.0f),
			Vector3(0.0f, 0.0f, 0.0f),
			Vector3(0.0f, -0.5f, 0.0f)
			);

		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<PNTStaticModelNoLightDraw>();
		//描画するメッシュを設定
		PtrDraw->SetMeshResource(L"NON_ENEMY_MESH");
		PtrDraw->SetMeshToTransformMatrix(SpanMat);
		//透明処理
		SetAlphaActive(true);

		//基底クラスのステートの初期化
		NonMove_Enemy::ResetState();
	}
	
	/**
	* @class Lance_Enemy
	* @brief 槍を持ってる敵
	* @author　sike yuya
	* @date	2016/06/21
	*/
	//構築と破棄
	Lance_Enemy::Lance_Enemy(const shared_ptr<Stage>& StagePtr) :
		NonMove_Enemy(StagePtr)
	{}

	//初期化
	void Lance_Enemy::OnCreate(){

		//デフォルトステートの設定
		NonMove_Enemy::OnCreate();

		//描画処理は行わない
		SetDrawActive(false);
		//自身を敵グループに追加
		auto Group = GetStage()->GetSharedObjectGroup(L"EnemyGroup");
		Group->IntoGroup(GetThis<Lance_Enemy>());

		//Transformの設定
		auto PtrTrans = AddComponent<Transform>();
		PtrTrans->SetPosition(0, 0, 0);
		PtrTrans->SetRotation(0.0f, 0.0f, 0.0f);
		PtrTrans->SetScale(0.4f, 0.4f, 0.4f);

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(1.0f, 1.0f, 1.0f),
			Vector3(0.0f, XM_PI * 4, 0.0f),
			Vector3(0.0f, -0.5f, 0.0f)
			);


		//影をつける（シャドウマップを描画する）
		auto ShadowPtr = AddComponent<Shadowmap>();
		//影の形（メッシュ）を設定
		ShadowPtr->SetMeshResource(L"LANCE_MESH");
		ShadowPtr->SetMeshToTransformMatrix(SpanMat);

		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<PNTBoneModelDraw>();
		//描画するメッシュを設定
		PtrDraw->SetMeshResource(L"LANCE_MESH");
		PtrDraw->SetMeshToTransformMatrix(SpanMat);
		PtrDraw->AddAnimation(L"Default", 0, 72, true, 80.0f);
		PtrDraw->ChangeCurrentAnimation(L"Default");
		//透明処理
		SetAlphaActive(true);

		//サウンドを登録.
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Enemy");
	}

	void Lance_Enemy::OnUpdate(){
		//アニメーションを更新する
		auto PtrDraw = GetComponent<PNTBoneModelDraw>();
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		PtrDraw->UpdateAnimation(ElapsedTime);
	}

	void Lance_Enemy::EnemyAction(){
		//SEの再生
		GetComponent<MultiSoundEffect>()->Start(L"Enemy", 0, 0.4f);

		//エフェクトの再生
		auto PtrEffect = GetStage()->GetSharedGameObject<NonMove_HitEffect>(L"NonMove_HitEffect", false);
		PtrEffect->InsertNonMoveEffect(GetComponent<Transform>()->GetPosition());
	}

	void Lance_Enemy::Refresh(){
		//Transformの設定
		auto PtrTrans = AddComponent<Transform>();
		PtrTrans->SetPosition(0, 0, 0);
		PtrTrans->SetRotation(0.0f, 0.0f, 0.0f);
		PtrTrans->SetScale(0.4f, 0.4f, 0.4f);

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(1.0f, 1.0f, 1.0f),
			Vector3(0.0f, 0.0f, 0.0f),
			Vector3(0.0f, -0.5f, 0.0f)
			);


		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<PNTStaticModelNoLightDraw>();
		//描画するメッシュを設定
		PtrDraw->SetMeshResource(L"NON_ENEMY_MESH");
		PtrDraw->SetMeshToTransformMatrix(SpanMat);
		//透明処理
		SetAlphaActive(true);

		//基底クラスのステートの初期化
		NonMove_Enemy::ResetState();
	}
	
	/**
	* @class Bomb_Enemy
	* @brief 爆弾の敵
	* @author　sike yuya
	* @date	2016/06/21
	*/
	//構築と破棄
	Bomb_Enemy::Bomb_Enemy(const shared_ptr<Stage>& StagePtr) :
		NonMove_Enemy(StagePtr)
	{}

	//初期化
	void Bomb_Enemy::OnCreate(){
		//デフォルトステートの設定
		NonMove_Enemy::OnCreate();

		//描画処理は行わない
		SetDrawActive(false);
		//自身を敵グループに追加
		auto Group = GetStage()->GetSharedObjectGroup(L"EnemyGroup");
		Group->IntoGroup(GetThis<Bomb_Enemy>());

		//Transformの設定
		auto PtrTrans = AddComponent<Transform>();
		PtrTrans->SetPosition(0, 0, 0);
		PtrTrans->SetRotation(0.0f, 0.0f, 0.0f);
		PtrTrans->SetScale(0.4f, 0.4f, 0.4f);

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(1.0f, 1.0f, 1.0f),
			Vector3(0.0f, XM_PI * 4, 0.0f),
			Vector3(0.0f, -0.5f, 0.0f)
			);

		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<PNTBoneModelDraw>();
		//描画するメッシュを設定
		PtrDraw->SetMeshResource(L"FLY_MESH");
		PtrDraw->SetMeshToTransformMatrix(SpanMat);
		PtrDraw->AddAnimation(L"Default", 0, 40, true, 10.0f);
		PtrDraw->ChangeCurrentAnimation(L"Default");
		PtrDraw->SetOwnShadowActive(true);
		//透明処理
		SetAlphaActive(true);

		//サウンドを登録.
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Enemy");
	}

	void Bomb_Enemy::OnUpdate(){
		//アニメーションを更新する
		auto PtrDraw = GetComponent<PNTBoneModelDraw>();
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		PtrDraw->UpdateAnimation(ElapsedTime);
	}

	void Bomb_Enemy::EnemyAction(){
		//SEの再生
		GetComponent<MultiSoundEffect>()->Start(L"Enemy", 0, 0.4f);

		//エフェクトの再生
		auto PtrEffect = GetStage()->GetSharedGameObject<NonMove_HitEffect>(L"NonMove_HitEffect", false);
		PtrEffect->InsertNonMoveEffect(GetComponent<Transform>()->GetPosition());
	}

	void Bomb_Enemy::Refresh(){
		//Transformの設定
		auto PtrTrans = AddComponent<Transform>();
		PtrTrans->SetPosition(0, 0, 0);
		PtrTrans->SetRotation(0.0f, 0.0f, 0.0f);
		PtrTrans->SetScale(0.4f, 0.4f, 0.4f);

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(1.0f, 1.0f, 1.0f),
			Vector3(0.0f, 0.0f, 0.0f),
			Vector3(0.0f, -0.5f, 0.0f)
			);


		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<PNTStaticModelNoLightDraw>();
		//描画するメッシュを設定
		PtrDraw->SetMeshResource(L"NON_ENEMY_MESH");
		PtrDraw->SetMeshToTransformMatrix(SpanMat);
		//透明処理
		SetAlphaActive(true);

		//基底クラスのステートの初期化
		NonMove_Enemy::ResetState();
	}

}
//endof  basedx11