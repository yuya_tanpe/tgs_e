#include "stdafx.h"
#include "Project.h"

namespace basedx11
{
	//// モデル：カモメ
	//KamomeModel::KamomeModel(const shared_ptr<Stage>& StagePtr,
	//	const Vector3& Scale,
	//	const Vector3& Rotation,
	//	const Vector3& Position
	//	) :
	//	GameObject(StagePtr),
	//	m_Scale(Scale),
	//	m_Rotation(Rotation),
	//	m_Position(Position)
	//{
	//}

	//void KamomeModel::OnCreate()
	//{
	//	auto PtrTransform = AddComponent<Transform>();

	//	PtrTransform->SetScale(m_Scale);
	//	PtrTransform->SetRotation(m_Rotation);
	//	PtrTransform->SetPosition(m_Position);

	//	auto PtrRigit = AddComponent<Rigidbody>();
	//	auto PtrGravity = AddComponent<Gravity>();
	//	auto PtrColli = AddComponent<CollisionSphere>();

	//	Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
	//	SpanMat.DefTransformation(
	//		Vector3(1.0f, 1.0f, 1.0f),
	//		Vector3(0.0f, XM_PI, 0.0f),
	//		Vector3(0.0f, -1.0f, 0.0f)
	//		);

	//	//描画コンポーネントの設定
	//	auto PtrDraw = AddComponent<PNTStaticModelDraw>();

	//	auto ShadowPtr = AddComponent<Shadowmap>();
	//	ShadowPtr->SetMeshResource(L"KAMOME_MESH");
	//	ShadowPtr->SetMeshToTransformMatrix(SpanMat);

	//	//描画するメッシュを設定
	//	PtrDraw->SetMeshResource(L"KAMOME_MESH");
	//	PtrDraw->SetMeshToTransformMatrix(SpanMat);

	//	//透明処理
	//	SetAlphaActive(true);
	//}

	//// モデル：魚
	//FishModel::FishModel(const shared_ptr<Stage>& StagePtr,
	//	const Vector3& Scale,
	//	const Vector3& Rotation,
	//	const Vector3& Position
	//	) :
	//	GameObject(StagePtr),
	//	m_Scale(Scale),
	//	m_Rotation(Rotation),
	//	m_Position(Position)
	//{
	//}

	//void FishModel::OnCreate()
	//{
	//	auto PtrTransform = AddComponent<Transform>();

	//	PtrTransform->SetScale(m_Scale);
	//	PtrTransform->SetRotation(m_Rotation);
	//	PtrTransform->SetPosition(m_Position);

	//	auto PtrRigit = AddComponent<Rigidbody>();
	//	auto PtrGravity = AddComponent<Gravity>();
	//	auto PtrColli = AddComponent<CollisionSphere>();

	//	Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
	//	SpanMat.DefTransformation(
	//		Vector3(1.0f, 1.0f, 1.0f),
	//		Vector3(0.0f, XM_PI, 0.0f),
	//		Vector3(0.0f, 0.0f, 0.0f)
	//		);

	//	//描画コンポーネントの設定
	//	auto PtrDraw = AddComponent<PNTStaticModelDraw>();

	//	auto ShadowPtr = AddComponent<Shadowmap>();
	//	ShadowPtr->SetMeshResource(L"FISH_MESH");
	//	ShadowPtr->SetMeshToTransformMatrix(SpanMat);

	//	//描画するメッシュを設定
	//	PtrDraw->SetMeshResource(L"FISH_MESH");
	//	PtrDraw->SetMeshToTransformMatrix(SpanMat);

	//	//透明処理
	//	SetAlphaActive(true);
	//}

	//// モデル：アイテム
	//ItemModel::ItemModel(const shared_ptr<Stage>& StagePtr,
	//	const Vector3& Scale,
	//	const Vector3& Rotation,
	//	const Vector3& Position
	//	) :
	//	GameObject(StagePtr),
	//	m_Scale(Scale),
	//	m_Rotation(Rotation),
	//	m_Position(Position)
	//{
	//}

	//void ItemModel::OnCreate()
	//{
	//	auto PtrTransform = AddComponent<Transform>();

	//	PtrTransform->SetScale(m_Scale);
	//	PtrTransform->SetRotation(m_Rotation);
	//	PtrTransform->SetPosition(m_Position);

	//	auto PtrRigit = AddComponent<Rigidbody>();
	//	auto PtrGravity = AddComponent<Gravity>();
	//	auto PtrColli = AddComponent<CollisionSphere>();

	//	Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
	//	SpanMat.DefTransformation(
	//		Vector3(1.0f, 1.0f, 1.0f),
	//		Vector3(0.0f, XM_PI, 0.0f),
	//		Vector3(0.0f, -1.0f, 0.0f)
	//		);

	//	//描画コンポーネントの設定
	//	auto PtrDraw = AddComponent<PNTStaticModelDraw>();

	//	auto ShadowPtr = AddComponent<Shadowmap>();
	//	ShadowPtr->SetMeshResource(L"ITEM_MESH");
	//	ShadowPtr->SetMeshToTransformMatrix(SpanMat);

	//	//描画するメッシュを設定
	//	PtrDraw->SetMeshResource(L"ITEM_MESH");
	//	PtrDraw->SetMeshToTransformMatrix(SpanMat);

	//	//透明処理
	//	SetAlphaActive(true);
	//}

	//// モデル：クジラ�@
	//KujiraModel_1::KujiraModel_1(const shared_ptr<Stage>& StagePtr,
	//	const Vector3& Scale,
	//	const Vector3& Rotation,
	//	const Vector3& Position
	//	) :
	//	GameObject(StagePtr),
	//	m_Scale(Scale),
	//	m_Rotation(Rotation),
	//	m_Position(Position)
	//{
	//}

	//void KujiraModel_1::OnCreate()
	//{
	//	auto PtrTransform = AddComponent<Transform>();

	//	PtrTransform->SetScale(m_Scale);
	//	PtrTransform->SetRotation(m_Rotation);
	//	PtrTransform->SetPosition(m_Position);

	//	auto PtrRigit = AddComponent<Rigidbody>();
	//	auto PtrGravity = AddComponent<Gravity>();
	//	auto PtrColli = AddComponent<CollisionSphere>();

	//	Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
	//	SpanMat.DefTransformation(
	//		Vector3(1.0f, 1.0f, 1.0f),
	//		Vector3(0.0f, XM_PI, 0.0f),
	//		Vector3(0.0f, 0.0f, 0.0f)
	//		);

	//	//描画コンポーネントの設定
	//	auto PtrDraw = AddComponent<PNTStaticModelDraw>();

	//	auto ShadowPtr = AddComponent<Shadowmap>();
	//	ShadowPtr->SetMeshResource(L"KUJIRA_MESH_1");
	//	ShadowPtr->SetMeshToTransformMatrix(SpanMat);

	//	//描画するメッシュを設定
	//	PtrDraw->SetMeshResource(L"KUJIRA_MESH_1");
	//	PtrDraw->SetMeshToTransformMatrix(SpanMat);

	//	//透明処理
	//	SetAlphaActive(true);
	//}

	//// モデル：クジラ�A
	//KujiraModel_2::KujiraModel_2(const shared_ptr<Stage>& StagePtr,
	//	const Vector3& Scale,
	//	const Vector3& Rotation,
	//	const Vector3& Position
	//	) :
	//	GameObject(StagePtr),
	//	m_Scale(Scale),
	//	m_Rotation(Rotation),
	//	m_Position(Position)
	//{
	//}

	//void KujiraModel_2::OnCreate()
	//{
	//	auto PtrTransform = AddComponent<Transform>();

	//	PtrTransform->SetScale(m_Scale);
	//	PtrTransform->SetRotation(m_Rotation);
	//	PtrTransform->SetPosition(m_Position);

	//	auto PtrRigit = AddComponent<Rigidbody>();
	//	auto PtrGravity = AddComponent<Gravity>();
	//	auto PtrColli = AddComponent<CollisionSphere>();

	//	Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
	//	SpanMat.DefTransformation(
	//		Vector3(1.0f, 1.0f, 1.0f),
	//		Vector3(0.0f, XM_PI, 0.0f),
	//		Vector3(0.0f, 0.0f, 0.0f)
	//		);

	//	//描画コンポーネントの設定
	//	auto PtrDraw = AddComponent<PNTStaticModelDraw>();

	//	auto ShadowPtr = AddComponent<Shadowmap>();
	//	ShadowPtr->SetMeshResource(L"KUJIRA_MESH_2");
	//	ShadowPtr->SetMeshToTransformMatrix(SpanMat);

	//	//描画するメッシュを設定
	//	PtrDraw->SetMeshResource(L"KUJIRA_MESH_2");
	//	PtrDraw->SetMeshToTransformMatrix(SpanMat);

	//	//透明処理
	//	SetAlphaActive(true);
	//}

	//// モデル：クジラの子
	//KujiraChildModel::KujiraChildModel(const shared_ptr<Stage>& StagePtr,
	//	const Vector3& Scale,
	//	const Vector3& Rotation,
	//	const Vector3& Position
	//	) :
	//	GameObject(StagePtr),
	//	m_Scale(Scale),
	//	m_Rotation(Rotation),
	//	m_Position(Position)
	//{
	//}

	//void KujiraChildModel::OnCreate()
	//{
	//	auto PtrTransform = AddComponent<Transform>();

	//	PtrTransform->SetScale(m_Scale);
	//	PtrTransform->SetRotation(m_Rotation);
	//	PtrTransform->SetPosition(m_Position);

	//	auto PtrRigit = AddComponent<Rigidbody>();
	//	auto PtrGravity = AddComponent<Gravity>();
	//	auto PtrColli = AddComponent<CollisionSphere>();

	//	Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
	//	SpanMat.DefTransformation(
	//		Vector3(1.0f, 1.0f, 1.0f),
	//		Vector3(0.0f, XM_PI, 0.0f),
	//		Vector3(0.0f, -1.0f, 0.0f)
	//		);

	//	//描画コンポーネントの設定
	//	auto PtrDraw = AddComponent<PNTStaticModelDraw>();

	//	auto ShadowPtr = AddComponent<Shadowmap>();
	//	ShadowPtr->SetMeshResource(L"KUJIRA_CHILD_MESH");
	//	ShadowPtr->SetMeshToTransformMatrix(SpanMat);

	//	//描画するメッシュを設定
	//	PtrDraw->SetMeshResource(L"KUJIRA_CHILD_MESH");
	//	PtrDraw->SetMeshToTransformMatrix(SpanMat);

	//	//透明処理
	//	SetAlphaActive(true);
	//}

	//// モデル：ピラミッド
	//PiramidModel::PiramidModel(const shared_ptr<Stage>& StagePtr,
	//	const Vector3& Scale,
	//	const Vector3& Rotation,
	//	const Vector3& Position
	//	) :
	//	GameObject(StagePtr),
	//	m_Scale(Scale),
	//	m_Rotation(Rotation),
	//	m_Position(Position)
	//{
	//}

	//void PiramidModel::OnCreate()
	//{
	//	auto PtrTransform = AddComponent<Transform>();

	//	PtrTransform->SetScale(m_Scale);
	//	PtrTransform->SetRotation(m_Rotation);
	//	PtrTransform->SetPosition(m_Position);

	//	auto PtrRigit = AddComponent<Rigidbody>();
	//	auto PtrGravity = AddComponent<Gravity>();
	//	auto PtrColli = AddComponent<CollisionSphere>();

	//	Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
	//	SpanMat.DefTransformation(
	//		Vector3(1.0f, 1.0f, 1.0f),
	//		Vector3(0.0f, XM_PI, 0.0f),
	//		Vector3(0.0f, 0.0f, 0.0f)
	//		);

	//	//描画コンポーネントの設定
	//	auto PtrDraw = AddComponent<PNTStaticModelDraw>();

	//	auto ShadowPtr = AddComponent<Shadowmap>();
	//	ShadowPtr->SetMeshResource(L"PIRAMID_MESH");
	//	ShadowPtr->SetMeshToTransformMatrix(SpanMat);

	//	//描画するメッシュを設定
	//	PtrDraw->SetMeshResource(L"PIRAMID_MESH");
	//	PtrDraw->SetMeshToTransformMatrix(SpanMat);

	//	//透明処理
	//	SetAlphaActive(true);
	//}

	//// モデル：土
	//TutiModel::TutiModel(const shared_ptr<Stage>& StagePtr,
	//	const Vector3& Scale,
	//	const Vector3& Rotation,
	//	const Vector3& Position
	//	) :
	//	GameObject(StagePtr),
	//	m_Scale(Scale),
	//	m_Rotation(Rotation),
	//	m_Position(Position)
	//{
	//}

	//void TutiModel::OnCreate()
	//{
	//	auto PtrTransform = AddComponent<Transform>();

	//	PtrTransform->SetScale(m_Scale);
	//	PtrTransform->SetRotation(m_Rotation);
	//	PtrTransform->SetPosition(m_Position);

	//	auto PtrRigit = AddComponent<Rigidbody>();
	//	auto PtrGravity = AddComponent<Gravity>();
	//	auto PtrColli = AddComponent<CollisionSphere>();

	//	Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
	//	SpanMat.DefTransformation(
	//		Vector3(1.0f, 1.0f, 1.0f),
	//		Vector3(0.0f, XM_PI, 0.0f),
	//		Vector3(0.0f, 0.0f, 0.0f)
	//		);

	//	//描画コンポーネントの設定
	//	auto PtrDraw = AddComponent<PNTStaticModelDraw>();

	//	auto ShadowPtr = AddComponent<Shadowmap>();
	//	ShadowPtr->SetMeshResource(L"TUTI_MESH");
	//	ShadowPtr->SetMeshToTransformMatrix(SpanMat);

	//	//描画するメッシュを設定
	//	PtrDraw->SetMeshResource(L"TUTI_MESH");
	//	PtrDraw->SetMeshToTransformMatrix(SpanMat);

	//	//透明処理
	//	SetAlphaActive(true);
	//}

	//// モデル：木
	//WoodModel::WoodModel(const shared_ptr<Stage>& StagePtr,
	//	const Vector3& Scale,
	//	const Vector3& Rotation,
	//	const Vector3& Position
	//	) :
	//	GameObject(StagePtr),
	//	m_Scale(Scale),
	//	m_Rotation(Rotation),
	//	m_Position(Position)
	//{
	//}

	//void WoodModel::OnCreate()
	//{
	//	auto PtrTransform = AddComponent<Transform>();

	//	PtrTransform->SetScale(m_Scale);
	//	PtrTransform->SetRotation(m_Rotation);
	//	PtrTransform->SetPosition(m_Position);

	//	auto PtrRigit = AddComponent<Rigidbody>();
	//	auto PtrGravity = AddComponent<Gravity>();
	//	auto PtrColli = AddComponent<CollisionSphere>();

	//	Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
	//	SpanMat.DefTransformation(
	//		Vector3(1.0f, 1.0f, 1.0f),
	//		Vector3(0.0f, -3.0f, 0.0f),
	//		Vector3(0.0f, 0.0f, 0.0f)
	//		);

	//	//描画コンポーネントの設定
	//	auto PtrDraw = AddComponent<PNTStaticModelDraw>();

	//	auto ShadowPtr = AddComponent<Shadowmap>();
	//	ShadowPtr->SetMeshResource(L"WOOD_MESH");
	//	ShadowPtr->SetMeshToTransformMatrix(SpanMat);

	//	//描画するメッシュを設定
	//	PtrDraw->SetMeshResource(L"WOOD_MESH");
	//	PtrDraw->SetMeshToTransformMatrix(SpanMat);

	//	//透明処理
	//	SetAlphaActive(true);
	//}

	//// モデル：ヨット
	//YottoModel::YottoModel(const shared_ptr<Stage>& StagePtr,
	//	const Vector3& Scale,
	//	const Vector3& Rotation,
	//	const Vector3& Position
	//	) :
	//	GameObject(StagePtr),
	//	m_Scale(Scale),
	//	m_Rotation(Rotation),
	//	m_Position(Position)
	//{
	//}

	//void YottoModel::OnCreate()
	//{
	//	auto PtrTransform = AddComponent<Transform>();

	//	PtrTransform->SetScale(m_Scale);
	//	PtrTransform->SetRotation(m_Rotation);
	//	PtrTransform->SetPosition(m_Position);

	//	auto PtrRigit = AddComponent<Rigidbody>();
	//	auto PtrGravity = AddComponent<Gravity>();
	//	auto PtrColli = AddComponent<CollisionSphere>();

	//	Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
	//	SpanMat.DefTransformation(
	//		Vector3(1.0f, 1.0f, 1.0f),
	//		Vector3(0.0f, XM_PI, 0.0f),
	//		Vector3(0.0f, 0.0f, 0.0f)
	//		);

	//	//描画コンポーネントの設定
	//	auto PtrDraw = AddComponent<PNTStaticModelDraw>();

	//	auto ShadowPtr = AddComponent<Shadowmap>();
	//	ShadowPtr->SetMeshResource(L"YOTTO_MESH");
	//	ShadowPtr->SetMeshToTransformMatrix(SpanMat);

	//	//描画するメッシュを設定
	//	PtrDraw->SetMeshResource(L"YOTTO_MESH");
	//	PtrDraw->SetMeshToTransformMatrix(SpanMat);

	//	//透明処理
	//	SetAlphaActive(true);
	//}
}
//endof  basedx11