#include "stdafx.h"
#include "Project.h"

namespace basedx11
{
	/**
	* @class ResultSprite
	* @brief スコア表記 0 ~ 9までの表示
	* @author yuya
	* @date 2016/05/10
	*/
	//構築と破棄
	ResultSprite::ResultSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos, const wstring& TexName) :
		GameObject(StagePtr), m_StartPos(StartPos), m_Texname(TexName)
	{}
	ResultSprite::~ResultSprite(){}

	//初期化
	void ResultSprite::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(1.0f, 1.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//アクションの追加
		AddComponent<Action>();
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(m_Texname);
		SetDrawLayer(3);
		//透明処理
		SetAlphaActive(true);
		//左上原点
		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftTopZeroPlusDownY);
		//スプライトの中のメッシュからバックアップの取得
		auto& SpVertexVec = PtrSprite->GetMeshResource()->GetBackupVerteces<VertexPositionColorTexture>();
		//各数字ごとにUV値を含む頂点データを配列化しておく
		for (size_t i = 0; i < 10; i++){
			float from = ((float)i) / 10.0f;
			float to = from + (1.0f / 10.0f);
			vector<VertexPositionColorTexture> NumVirtex =
			{
				//左上頂点
				VertexPositionColorTexture(
				SpVertexVec[0].position,
				Color4(1.0f, 1.0f, 1.0f, 1.0f),
				Vector2(from, 0)
				),
				//右上頂点
				VertexPositionColorTexture(
				SpVertexVec[1].position,
				Color4(1.0f, 1.0f, 1.0f, 1.0f),
				Vector2(to, 0)
				),
				//左下頂点
				VertexPositionColorTexture(
				SpVertexVec[2].position,
				Color4(1.0f, 1.0f, 1.0f, 1.0f),
				Vector2(from, 1.0f)
				),
				//右下頂点
				VertexPositionColorTexture(
				SpVertexVec[3].position,
				Color4(1.0f, 1.0f, 1.0f, 1.0f),
				Vector2(to, 1.0f)
				),
			};
			m_NumberVertexVec.push_back(NumVirtex);
		}

		auto MeshRes = PtrSprite->GetMeshResource();
		//動的にUV値が変わる頂点を設定する
		MeshRes->UpdateVirtexBuffer(m_NumberVertexVec[0]);
	}

	//スコアの描画
	void ResultSprite::ScoreDraw(int score){
		//スプライトを取得
		auto PtrSprite = GetComponent<PCTSpriteDraw>();
		auto MeshRes = PtrSprite->GetMeshResource();
		//動的にUV値が変わる頂点を設定する
		MeshRes->UpdateVirtexBuffer(m_NumberVertexVec[score]);
	}

	// ScoreManager
	ResultScoreManager::ResultScoreManager(shared_ptr<Stage>& StagePtr,
		shared_ptr<ResultSprite> KURAI_1,
		shared_ptr<ResultSprite> KURAI_10,
		shared_ptr<ResultSprite> KURAI_100,
		shared_ptr<ResultSprite> KURAI_1000) :
		GameObject(StagePtr), m_KURAI1(KURAI_1),
		m_KURAI10(KURAI_10),
		m_KURAI100(KURAI_100),
		m_KURAI1000(KURAI_1000),
		place(1)
	{
		for (int i = 0; i < 4; i++)
		{
			place_array[i] = 0;   //任意の数字を代入
		}
	}

	void ResultScoreManager::OnCreate(){

	}

	//スコアの描画
	void ResultScoreManager::ScoreDraw(int num){
		for (int a = 0; a < 4; a++){
			place_array[a] = num / place % 10;
			place *= 10;
		}
		//桁の初期化
		place = 1;
		ScoreUpdate(place_array[0], place_array[1], place_array[2],place_array[3]);

	}
	//スコアスプライトの最終更新
	void ResultScoreManager::ScoreUpdate(const int score_1, const int score_10, const int score_100, const int score_1000){
		m_KURAI1->ScoreDraw(score_1);
		m_KURAI10->ScoreDraw(score_10);
		m_KURAI100->ScoreDraw(score_100);
		m_KURAI1000->ScoreDraw(score_1000);
	}

}
//endof  basedx11