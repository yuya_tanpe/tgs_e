#include "stdafx.h"
#include "Project.h"

namespace basedx11{
	/**
	* @class Coin_S
	* @brief 取った時に演出ありのオブジェクト
	* @author　sike yuya
	* @date	2016/06/08
	*/

	Coin_S::Coin_S(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr),m_HitTimer(0.0f)
	{}

	void Coin_S::OnCreate(){

		SetDrawActive(false);
		
		auto PtrTrans = AddComponent<Transform>();
		PtrTrans->SetPosition(0, 0, 0);
		PtrTrans->SetScale(Vector3(0.25f, 0.25f, 0.1f));
		PtrTrans->SetRotation(Vector3(0, 0, 0));

		// モデルとトランスフォームの間の差分行列
		Matrix4X4 SpanMat;
		SpanMat.DefTransformation(
			Vector3(0.8f, 0.8f, 2.0f),
			Vector3(0.0f, XM_PI, 0.0f),
			Vector3(0.0f, -0.5f, -0.0f)
			);

		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<PNTStaticModelCloudDraw>();
		//描画するメッシュを設定
		PtrDraw->SetMeshResource(L"COIN_MESH");
		PtrDraw->SetMeshToTransformMatrix(SpanMat);

		auto SE = AddComponent<MultiSoundEffect>();
		SE->AddAudioResource(L"ITEMS");

		//自身をグループに追加
		auto Group = GetStage()->GetSharedObjectGroup(L"CoinGroup");
		Group->IntoGroup(GetThis<Coin_S>());

		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<Coin_S> >(GetThis<Coin_S>());
		//最初のステートをDefaultStateに設定
		m_StateMachine->ChangeState(NoHitState::Instance());
	}

	void Coin_S::OnUpdate(){
		//ステートマシンのUpdateを行う
		m_StateMachine->Update();
	}

	void Coin_S::OnEvent(const shared_ptr<Event>& event){}
	//コインマネージャーに1枚追加する
	void Coin_S::Coin_1_Plus(){
		//コインマネージャーに1追加
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		assert(GameStagePtr && "ゲームステージの取得に失敗しました。");
		auto Coin = GameStagePtr->GetSharedGameObject<CoinManager>(L"CoinManager");
		Coin->Coin_1_Plus();
	}
	//プレイヤーとの衝突したときに呼ぶ
	void Coin_S::GoHitMotion(){
		if (GetStateMachine()->GetCurrentState() == NoHitState::Instance()){
			//マネージャーにコイン１枚追加
			Coin_1_Plus();
			//SEの再生
			GetComponent<MultiSoundEffect>()->Start(L"ITEMS", 0, 0.4f);

			//エフェクトの再生
			auto PtrCoinEffect = GetStage()->GetSharedGameObject<CoinEffect>(L"CoinEffect", false);
			PtrCoinEffect->InsertCoinEffect(GetComponent<Transform>()->GetPosition() + Vector3(0.0f, 1.0f, 0.0f));
		
			//当たったステートに変更
			GetStateMachine()->ChangeState(HitState::Instance());
		}
	}
	//リフレッシュ関数
	void Coin_S::Refresh(){
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetPosition(0, 0, 0);
		PtrTrans->SetScale(Vector3(0.25f, 0.25f, 0.1f));
		PtrTrans->SetRotation(Vector3(0, 0, 0));

		// モデルとトランスフォームの間の差分行列
		Matrix4X4 SpanMat;
		SpanMat.DefTransformation(
			Vector3(0.8f, 0.8f, 2.0f),
			Vector3(0.0f, XM_PI, 0.0f),
			Vector3(0.1f, 0.0f, -0.2f)
		);

		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<PNTStaticModelCloudDraw>();
		PtrDraw->SetMeshResource(L"COIN_MESH");
		PtrDraw->SetMeshToTransformMatrix(SpanMat);

		//自分は描画しない
		SetDrawActive(false);
		//最初のステート設定
		m_StateMachine->ChangeState(NoHitState::Instance());
	}
	//このオブジェクトがリフレッシュステートかの取得
	bool Coin_S::IsRefreshAbled()const{
		if (GetStateMachine()->GetCurrentState() ==	RefreshState::Instance()){
			return true;
		}
		return false;
	}

	//-----------------------------------------
	//---------NoHitStateで使用Motion----------
	//-----------------------------------------
	void Coin_S::ExecFirstMotion(){
		auto PtrTransform = AddComponent<Transform>();
		Quaternion Qt = PtrTransform->GetQuaternion();
		Quaternion QtSpan;
		QtSpan.RotationRollPitchYawFromVector(Vector3(0.0f, 0.05f, 0.0f));
		Qt *= QtSpan;
		PtrTransform->SetQuaternion(Qt);
	}
	//-----------------------------------------
	//---------HitStateで使用Motion----------
	//-----------------------------------------
	//当たった時のステート準備
	void Coin_S::StartHitMotion(){
		m_HitTimer = 0.0f;
		auto DocumentPtr = GetStage()->GetSharedGameObject<GameDocument>(L"GameDocument");
		auto& CoinPtrMap = DocumentPtr->GetCoinPtrMap();
		for (size_t y = 0; y < CoinPtrMap.size(); y++){
			for (size_t x = 0; x < CoinPtrMap[y].size(); x++){
				if (CoinPtrMap[y][x] == GetThis<Coin_S>()){
					CoinPtrMap[y][x] = nullptr;
				}
			}
		}
	}
	//プレイヤーと当たった時の動き
	bool Coin_S::ExecHitMotion(){
		auto PtrTransform = AddComponent<Transform>();
		Quaternion Qt = PtrTransform->GetQuaternion();
		Quaternion QtSpan;
		QtSpan.RotationRollPitchYawFromVector(Vector3(0.0f, 0.2f, 0.0f));
		Qt *= QtSpan;
		PtrTransform->SetQuaternion(Qt);
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		//当たった時間の計測
		m_HitTimer += ElapsedTime;
		if (m_HitTimer >= 1.0f){
			m_HitTimer = 0;
			return true;
		}
		return false;
	}
	//コインがリフレッシュ待ちステートの準備
	void Coin_S::EndHitMotion(){
		auto DocumentPtr = GetStage()->GetSharedGameObject<GameDocument>(L"GameDocument");
		auto& CoinPtrMap = DocumentPtr->GetCoinPtrMap();
		for (size_t y = 0; y < CoinPtrMap.size(); y++){
			for (size_t x = 0; x < CoinPtrMap[y].size(); x++){
				if (CoinPtrMap[y][x] == GetThis<Coin_S>()){
					CoinPtrMap[y][x] = nullptr;
				}
			}
		}
	}

	//--------------------------------------------------------------------------------------
	//	class NoHitState : public ObjState<Player>;
	//	用途: 通常移動
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<NoHitState> NoHitState::Instance(){
		static shared_ptr<NoHitState> instance;
		if (!instance){
			instance = shared_ptr<NoHitState>(new NoHitState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void NoHitState::Enter(const shared_ptr<Coin_S>& Obj){}
	//ステート実行中に毎ターン呼ばれる関数
	void NoHitState::Execute(const shared_ptr<Coin_S>& Obj){
		Obj->ExecFirstMotion();
	}
	//ステートにから抜けるときに呼ばれる関数
	void NoHitState::Exit(const shared_ptr<Coin_S>& Obj){}

	//--------------------------------------------------------------------------------------
	//	class HitState : public ObjState<Player>;
	//	用途: 通常移動
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<HitState> HitState::Instance(){
		static shared_ptr<HitState> instance;
		if (!instance){
			instance = shared_ptr<HitState>(new HitState);
		}
		return instance;
	}
	void HitState::Enter(const shared_ptr<Coin_S>& Obj){
		Obj->StartHitMotion();
	}
	void HitState::Execute(const shared_ptr<Coin_S>& Obj){
		if (Obj->ExecHitMotion()){
			Obj->GetStateMachine()->ChangeState(RefreshState::Instance());
		}
	}
	void HitState::Exit(const shared_ptr<Coin_S>& Obj){
		Obj->EndHitMotion();
	}

	//--------------------------------------------------------------------------------------
	//	class RefreshState : public ObjState<Coin_S>;
	//	用途: リフレッシュ待ちステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<RefreshState> RefreshState::Instance(){
		static shared_ptr<RefreshState> instance;
		if (!instance){
			instance = shared_ptr<RefreshState>(new RefreshState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void RefreshState::Enter(const shared_ptr<Coin_S>& Obj){
	}
	//ステート実行中に毎ターン呼ばれる関数
	void RefreshState::Execute(const shared_ptr<Coin_S>& Obj){}
	//ステートにから抜けるときに呼ばれる関数
	void RefreshState::Exit(const shared_ptr<Coin_S>& Obj){}

	/**
	* @class Fly_Item
	* @brief 取るとボーナススコアが上がるアイテム 浮いてるバージョン
	* @author　sike yuya
	* @date	2016/06/08
	*/
	//構築と破棄
	Fly_Item::Fly_Item(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr)
	{}

	//初期化
	void Fly_Item::OnCreate(){
		//描画処理は行わない
		SetDrawActive(false);
		GetStage()->SetSharedGameObject(L"Fly_Item", GetThis<Fly_Item>());

		//初期位置などの設定
		auto Ptr = AddComponent<Transform>();
		Ptr->SetScale(0.3f, 0.3f, 0.3f);
		Ptr->SetRotation(0.0f, 0.0f, 0.0f);
		Ptr->SetPosition(0, 0, 0);

		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		//描画するメッシュを設定
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		//描画するテクスチャを設定
		PtrDraw->SetTextureResource(L"RED_TX");
		// モデルとトランスフォームの間の差分行列
		//Matrix4X4 SpanMat;
		//SpanMat.DefTransformation(
		//	Vector3(1.0f, 1.0f, 1.0f),
		//	Vector3(0.0f, XM_PI, 0.0f),
		//	Vector3(0.0f, -1.0f, 0.0f)
		//	);

		////描画コンポーネントの設定
		//auto PtrDraw = AddComponent<PNTStaticModelDraw>();
		////描画するメッシュを設定
		//PtrDraw->SetMeshResource(L"ITEM_MESH");
		//PtrDraw->SetMeshToTransformMatrix(SpanMat);

		//サウンドを登録.
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"ITEML");

		//透明処理
		SetAlphaActive(true);
	}

	void Fly_Item::OnUpdate(){
	}

	////当たり判定
	//TODO : ドキュメントを取得して、同じセルだったら処理を行う

	//void Fly_Item::OnCollision(const shared_ptr<GameObject>& other){
	//	//プレイヤーと接触
	//	auto PlayerHit = dynamic_pointer_cast<Player_Character>(other);
	//	//直進してくる敵が接触
	//	auto StraightHit = dynamic_pointer_cast<Straight_Enemy>(other);
	//	//TODO : 動く敵との接触
	//	if (PlayerHit){
	//		//頭上を飛ぶ
	//		//PhonPhon();
	//		//エフェクト
	//		auto PtrPlayerMove_Effect = GetStage()->GetSharedGameObject<PlayerMove_Effect>(L"PlayerMove_Effect", false);
	//		if (PtrPlayerMove_Effect){
	//			PtrPlayerMove_Effect->InsertPlayerMove_Effect(PlayerHit->GetComponent<Transform>()->GetPosition());
	//		}
	//		//サウンドの再生
	//		auto SE = GetComponent<MultiSoundEffect>();
	//		SE->Start(L"ITEML", 0, 0.4f);
	//		//オブジェクトの消去
	//		EndObject();
	//	}
	//	else if (StraightHit){
	//		//assert(!"まだ実装してない");
	//	}
	//}

	void Fly_Item::OnEvent(const shared_ptr<Event>& event)
	{
		if (event->m_MsgStr == L"OBJ_ACTIVE")
		{
			SetUpdateActive(true);
		}

		if (event->m_MsgStr == L"OBJ_STOP")
		{
			SetUpdateActive(false);
		}
	}

	//オブジェクトを消してエフェクトを出し、音を出す
	void Fly_Item::EndObject(){
		SetDrawActive(false);
		SetUpdateActive(false);
	}
}
//endof  basedx11