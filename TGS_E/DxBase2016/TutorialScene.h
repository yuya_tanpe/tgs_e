#pragma once

#include "stdafx.h"

namespace basedx11
{
	//前宣言
	class Cloud;

	class TutorialScene : public Stage
	{
		//雲のパターンを入れる可変長配列
		vector< vector<CloudName>>  m_CloudPatternVec;
		//雲のオブジェクトを入れる可変長配列
		vector< vector< weak_ptr<GameObject>>> m_CloudObjectVec;
		//雲を入れる配列の大きさ	Row : 縦	 Col : 横
		size_t m_Rowsize;
		size_t m_Colsize;

		//ステートマシーン
		shared_ptr< StateMachine<TutorialScene> >  m_StateMachine;

		//待ち時間
		float m_waittime;

		//シーン移動時の二度とおさないようにする
		bool m_IsPushButton;

		//プレイヤーが操作する回数
		size_t m_PlayNum;

		// ---------- pattern ---------.
		//チュートリアルパターン
		vector< vector<CloudName>>  m_TutorialPatternVec;

		void CreateResourses();

		//ビューの作成
		void CreateViews();

		//背景の作成
		void CreateBackGround();

		//フレームの作成
		void CreateUI();

		//初期雲の作成
		void PatternToVec(vector< vector<CloudName>>& DifficultyVec);

		//1行分のセット
		void SetCloudVec(size_t Row, vector<vector<CloudName>>& List, int patternnum){
			m_CloudPatternVec[Row][0] = List[patternnum][0];
			m_CloudPatternVec[Row][1] = List[patternnum][1];
			m_CloudPatternVec[Row][2] = List[patternnum][2];
			m_CloudPatternVec[Row][3] = List[patternnum][3];
			m_CloudPatternVec[Row][4] = List[patternnum][4];
		}

		//雲の作成
		void CreateCloudVec();
		//雲作成メソッド
		void CreateWhiteCloud(size_t row, size_t col);
		void CreateNonCloud(size_t row, size_t col);

		//ユーザー入力で操作するバーの作成
		void CreateInput_bar();

		//プレイヤーの作成
		void CreatePlayer();
	public:
		//構築と破棄
		TutorialScene();
		virtual ~TutorialScene(){
			GetComponent<MultiSoundEffect>()->Stop(L"TUTORIALBGM");
		}

		//初期化
		virtual void OnCreate()override;

		//更新
		virtual void OnUpdate()override;

		//-----------------------------
		//FarstTutorialStateで使用するMotion
		//-----------------------------
		//Aボタンが押されたか
		bool Is_AButtonPush();
		//Bボタンが押されたか
		bool Is_BButtonPush();
		//Aボタンを押してゲームをスタートする
		bool Is_APushGameStart();
		//入力バーの移動アクションのスタート
		void StartAction_InputBar();
		//時間を置いてからの画像切り替え
		void ChangeController_Texture();
		//時間を図る
		bool WaitTime();
		//オブジェクトの位置等を初期化する
		void ResetObject();
		//SEの再生
		void SoundPlay(const wstring& SEname, const float& volume);
		//SEの再生
		void SoundStop(const wstring& SEname);

		//-----------------------------
		//SecondTutorialStateで使用するMotion
		//-----------------------------
		//シーンを移動する
		void SceneMove();
		//入力バーの非表示
		void Drawfalse_Inputbar(bool TF);
		//キャラクターが移動する
		void StartAction_Player();
		//画像切り替え
		void ChangeController_RButtonTexture();
		//再生している回数を図ってステートのリセットをする
		void PlayCount();
		//リセットしたときにキャラの位置を初期値に移動
		void PlayerReset();

		//現在のステートを返す
		shared_ptr< StateMachine<TutorialScene> > GetStateMachine() const{
			return m_StateMachine;
		}

		//row colを引数に、m_CloudVecからCloudNameを返す
		CloudName GetCloudNameVec(const size_t row, const size_t col){
			return m_CloudPatternVec[row][col];
		}

		//row colを引数にm_CloudObjectVecの中にweak_ptrを入れる
		void SetCloudObject(const size_t row, const size_t col, const weak_ptr<GameObject> wpCloud){
			m_CloudObjectVec[row][col] = wpCloud;
		}

		// ボタン類の作成
		void CreateButton();

		// ボタン類の表示・非表示
		void ButtonUI(bool a);
	};

	//--------------------------------------------------------------------------------------
	//	class FarstTutorialState : public ObjState<TutorialScene>;
	//	用途: 雲の移動を教えるステート
	//--------------------------------------------------------------------------------------
	class FarstTutorialState : public ObjState<TutorialScene>
	{
		FarstTutorialState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<FarstTutorialState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<TutorialScene>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<TutorialScene>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<TutorialScene>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class SecondTutorialState : public ObjState<TutorialScene>;
	//	用途: プレイヤーの移動を教えるステート
	//--------------------------------------------------------------------------------------
	class SecondTutorialState : public ObjState<TutorialScene>
	{
		SecondTutorialState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<SecondTutorialState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<TutorialScene>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<TutorialScene>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<TutorialScene>& Obj)override;
	};
}