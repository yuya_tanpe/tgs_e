#pragma once
#include "stdafx.h"

namespace basedx11{
	/**
	* @class Hide_Enemy
	* @brief 先を見えにくくする敵
	* @author　sike yuya
	* @date	2016/06/05
	*/
	//class Enemy;

	//class Hide_Enemy : public GameObject{
	//private:
	//	// ---------- member ----------.

	//	//初期位置
	//	Vector3 m_Pos;
	//	//セル座標
	//	Enemy::Cell m_Cell;
	//	//敵の名前
	//	CharacterName m_EnemyName;
	//	//ステートマシーン
	//	shared_ptr< StateMachine<Hide_Enemy> >  m_StateMachine;

	//	// ---------- Method -----------.

	//	//自分の名前の登録
	//	void SetEnemyName(const CharacterName& name);

	//	//自分の作成された場所を配列に登録
	//	void SetName_CharacterVec();

	//	//自身が消えたら配列から削除
	//	void Erase_CharacterVec();
	//public:
	//	Hide_Enemy(const shared_ptr<Stage>& StagePtr, const Vector3& i_start_pos, Enemy::Cell& i_cell);
	//	virtual ~Hide_Enemy(){}

	//	virtual void OnCreate() override;
	//	virtual void OnUpdate() override;

	//	// ---------- Accessor ----------.
	//	//現在のステートを返す
	//	shared_ptr< StateMachine<Hide_Enemy> > GetStateMachine() const{
	//		return m_StateMachine;
	//	}
	//};

	////--------------------------------------------------------------------------------------
	////	class HideState : public ObjState<Hide_Enemy>;
	////	用途: 隠れてるステート
	////--------------------------------------------------------------------------------------
	//class HideState : public ObjState<Hide_Enemy>
	//{
	//	HideState(){}
	//public:
	//	//ステートのインスタンス取得
	//	static shared_ptr<HideState> Instance();
	//	//ステートに入ったときに呼ばれる関数
	//	virtual void Enter(const shared_ptr<Hide_Enemy>& Obj)override;
	//	//ステート実行中に毎ターン呼ばれる関数
	//	virtual void Execute(const shared_ptr<Hide_Enemy>& Obj)override;
	//	//ステートにから抜けるときに呼ばれる関数
	//	virtual void Exit(const shared_ptr<Hide_Enemy>& Obj)override;
	//};

	////--------------------------------------------------------------------------------------
	////	class ActionState : public ObjState<Hide_Enemy>;
	////	用途: 画面に画像を出して邪魔をするステート
	////--------------------------------------------------------------------------------------
	//class ActionState : public ObjState<Hide_Enemy>
	//{
	//	ActionState(){}
	//public:
	//	//ステートのインスタンス取得
	//	static shared_ptr<ActionState> Instance();
	//	//ステートに入ったときに呼ばれる関数
	//	virtual void Enter(const shared_ptr<Hide_Enemy>& Obj)override;
	//	//ステート実行中に毎ターン呼ばれる関数
	//	virtual void Execute(const shared_ptr<Hide_Enemy>& Obj)override;
	//	//ステートにから抜けるときに呼ばれる関数
	//	virtual void Exit(const shared_ptr<Hide_Enemy>& Obj)override;
	//};
}
//endof  basedx11