#include "stdafx.h"
#include "Project.h"

namespace basedx11
{
	// リソースの作成
	void GameOverScene::CreateResourses()
	{
		wstring strTexture = App::GetApp()->m_wstrRelativeDataPath + L"ResultUI\\BackGround.png";
		App::GetApp()->RegisterTexture(L"BG_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"ResultUI\\TotalScore.png";
		App::GetApp()->RegisterTexture(L"TOTALSCORE_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"ResultUI\\Score.png";
		App::GetApp()->RegisterTexture(L"SCORE_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"ResultUI\\BonusScore.png";
		App::GetApp()->RegisterTexture(L"BONUSSCORE_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"ResultUI\\Result.png";
		App::GetApp()->RegisterTexture(L"RESULT_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"ResultUI\\GoTitle.png";
		App::GetApp()->RegisterTexture(L"BBUTTON_TITLE_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"ResultUI\\Retry.png";
		App::GetApp()->RegisterTexture(L"ABUTTON_RETRY_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStageUI\\number.png";
		App::GetApp()->RegisterTexture(L"NUMBER_TX", strTexture);

		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"START");
	}

	// ビューの作成
	void GameOverScene::CreateViews()
	{
		CreateDefaultRenderTargets(2048.0f);
		Shadowmap::SetViewSize(32.0f);
		auto PtrMultiView = GetComponent<MultiView>();
		auto PtrView = PtrMultiView->AddView();

		//ビューの矩形を設定（ゲームサイズ全体）
		Rect2D<float> rect(0, 0, (float)App::GetApp()->GetGameWidth(), (float)App::GetApp()->GetGameHeight());

		//背景色(RGB)
		Color4 ViewBkColor(0.0f, 0.125f, 0.3f, 1.0f);

		//最初のビューにパラメータの設定
		PtrView->ResetParamaters<Camera, MultiLight>(rect, ViewBkColor, 1, 0.0f, 1.0f);

		//最初のビューのビューのライトの設定
		auto PtrLight = PtrView->GetMultiLight()->GetLight(0);
		PtrLight->SetPositionToDirectional(-0.25f, 1.0f, -0.25f);

		//ビューのカメラの設定
		auto PtrCamera = PtrView->GetCamera();
		PtrCamera->SetEye(Vector3(0.0f, 0.0f, -10.0f));
		PtrCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
	}

	// リザルトスプライトの作成
	void GameOverScene::CreateResultSprite()
	{
		auto Ptr = AddGameObject<GameObject>();
		auto PtrTransMatrix = Ptr->GetComponent<TransformMatrix>();
		Quaternion Qt;
		Qt.RotationRollPitchYawFromVector(Vector3(0, 0, 0));
		Matrix4X4 WorldMat;
		WorldMat.DefTransformation(
			Vector3(6.3f, 3.5f, 1.0f),
			Qt,
			Vector3(0.0f, 2.5f, 0.0f)
			);
		PtrTransMatrix->SetWorldMatrix(WorldMat);

		//描画コンポーネントの追加
		auto DrawComp = Ptr->AddComponent<PNTStaticDraw>();

		//描画コンポーネントに形状（メッシュ）を設定
		DrawComp->SetMeshResource(L"DEFAULT_SQUARE");

		//描画コンポーネントテクスチャの設定
		DrawComp->SetTextureResource(L"RESULT_TX");

		//自分に影が映りこむようにする
		DrawComp->SetOwnShadowActive(false);

		Ptr->SetAlphaActive(true);

		Ptr->SetDrawLayer(2);
	}

	// スコアスプライトの作成
	void GameOverScene::CreateScoreSprite()
	{
		auto Ptr = AddGameObject<GameObject>();
		auto PtrTransMatrix = Ptr->GetComponent<TransformMatrix>();
		Quaternion Qt;
		Qt.RotationRollPitchYawFromVector(Vector3(0, 0, 0));
		Matrix4X4 WorldMat;
		WorldMat.DefTransformation(
			Vector3(6.0f, 2.0f, 1.0f),
			Qt,
			Vector3(-5.35f, 0.9f, 0.0f)
			);
		PtrTransMatrix->SetWorldMatrix(WorldMat);

		//描画コンポーネントの追加
		auto DrawComp = Ptr->AddComponent<PNTStaticDraw>();

		//描画コンポーネントに形状（メッシュ）を設定
		DrawComp->SetMeshResource(L"DEFAULT_SQUARE");

		//描画コンポーネントテクスチャの設定
		DrawComp->SetTextureResource(L"SCORE_TX");

		//自分に影が映りこむようにする
		DrawComp->SetOwnShadowActive(false);

		Ptr->SetAlphaActive(true);

		Ptr->SetDrawLayer(2);
	}

	// ボーナススコアスプライトの作成
	void GameOverScene::CreateBonusScoreSprite()
	{
		auto Ptr = AddGameObject<GameObject>();
		auto PtrTransMatrix = Ptr->GetComponent<TransformMatrix>();
		Quaternion Qt;
		Qt.RotationRollPitchYawFromVector(Vector3(0, 0, 0));
		Matrix4X4 WorldMat;
		WorldMat.DefTransformation(
			Vector3(6.0f, 2.0f, 1.0f),
			Qt,
			Vector3(-3.7f, -0.2f, 0.0f)
			);
		PtrTransMatrix->SetWorldMatrix(WorldMat);

		//描画コンポーネントの追加
		auto DrawComp = Ptr->AddComponent<PNTStaticDraw>();

		//描画コンポーネントに形状（メッシュ）を設定
		DrawComp->SetMeshResource(L"DEFAULT_SQUARE");

		//描画コンポーネントテクスチャの設定
		DrawComp->SetTextureResource(L"BONUSSCORE_TX");

		//自分に影が映りこむようにする
		DrawComp->SetOwnShadowActive(false);

		Ptr->SetAlphaActive(true);

		Ptr->SetDrawLayer(2);
	}

	// トータルスコアのテクスチャ作成
	void GameOverScene::CreateTotalScoreSprite()
	{
		auto Ptr = AddGameObject<GameObject>();
		auto PtrTransMatrix = Ptr->GetComponent<TransformMatrix>();
		Quaternion Qt;
		Qt.RotationRollPitchYawFromVector(Vector3(0, 0, 0));
		Matrix4X4 WorldMat;
		WorldMat.DefTransformation(
			Vector3(7.5f, 2.5f, 1.0f),
			Qt,
			Vector3(-3.0f, -1.3f, 0.0f)
			);
		PtrTransMatrix->SetWorldMatrix(WorldMat);

		//描画コンポーネントの追加
		auto DrawComp = Ptr->AddComponent<PNTStaticDraw>();

		//描画コンポーネントに形状（メッシュ）を設定
		DrawComp->SetMeshResource(L"DEFAULT_SQUARE");

		//描画コンポーネントテクスチャの設定
		DrawComp->SetTextureResource(L"TOTALSCORE_TX");

		//自分に影が映りこむようにする
		DrawComp->SetOwnShadowActive(false);

		Ptr->SetAlphaActive(true);

		Ptr->SetDrawLayer(2);
	}

	// Aボタンのスプライト作成
	void GameOverScene::CreateAButtonSprite()
	{
		auto Ptr = AddGameObject<GameObject>();
		auto PtrTransMatrix = Ptr->GetComponent<TransformMatrix>();
		Quaternion Qt;
		Qt.RotationRollPitchYawFromVector(Vector3(0, 0, 0));
		Matrix4X4 WorldMat;
		WorldMat.DefTransformation(
			Vector3(4.0f, 1.0f, 1.0f),
			Qt,
			Vector3(4.3f, -3.0f, 0.0f)
			);
		PtrTransMatrix->SetWorldMatrix(WorldMat);

		//描画コンポーネントの追加
		auto DrawComp = Ptr->AddComponent<PNTStaticDraw>();

		//描画コンポーネントに形状（メッシュ）を設定
		DrawComp->SetMeshResource(L"DEFAULT_SQUARE");

		//描画コンポーネントテクスチャの設定
		DrawComp->SetTextureResource(L"ABUTTON_RETRY_TX");

		//自分に影が映りこむようにする
		DrawComp->SetOwnShadowActive(false);

		Ptr->SetAlphaActive(true);

		Ptr->SetDrawLayer(2);
	}

	// Bボタンスプライトの作成
	void GameOverScene::CreateBButtonSprite()
	{
		auto Ptr = AddGameObject<GameObject>();
		auto PtrTransMatrix = Ptr->GetComponent<TransformMatrix>();
		Quaternion Qt;
		Qt.RotationRollPitchYawFromVector(Vector3(0, 0, 0));
		Matrix4X4 WorldMat;
		WorldMat.DefTransformation(
			Vector3(4.0f, 1.0f, 1.0f),
			Qt,
			Vector3(-3.5f, -3.0f, 0.0f)
			);
		PtrTransMatrix->SetWorldMatrix(WorldMat);

		//描画コンポーネントの追加
		auto DrawComp = Ptr->AddComponent<PNTStaticDraw>();

		//描画コンポーネントに形状（メッシュ）を設定
		DrawComp->SetMeshResource(L"DEFAULT_SQUARE");

		//描画コンポーネントテクスチャの設定
		DrawComp->SetTextureResource(L"BBUTTON_TITLE_TX");

		//自分に影が映りこむようにする
		DrawComp->SetOwnShadowActive(false);

		Ptr->SetAlphaActive(true);

		Ptr->SetDrawLayer(2);
	}

	//進んだ距離の得点
	void GameOverScene::CreateScoreNum(){
		auto Score_KURAI1000 = AddGameObject<ResultSprite>(Vector3(1300.0f, 430.0f, 0.0f),L"NUMBERRESULT_TX");
		auto Score_KURAI100 = AddGameObject<ResultSprite>(Vector3(1400.0f, 430.0f, 0.0f), L"NUMBERRESULT_TX");
		auto Score_KURAI10 = AddGameObject<ResultSprite>(Vector3(1500.0f, 430.0f, 0.0f), L"NUMBERRESULT_TX");
		auto Score_KURAI1 = AddGameObject<ResultSprite>(Vector3(1600.0f, 430.0f, 0.0f), L"NUMBERRESULT_TX");
		auto ScoreManager = AddGameObject<ResultScoreManager>(Score_KURAI1, Score_KURAI10, 
			Score_KURAI100, Score_KURAI1000);

		auto PtrScene = dynamic_pointer_cast<Scene>(App::GetApp()->GetSceneBase());
		ScoreManager->ScoreDraw(PtrScene->GetScore());
	}

	//ボーナススコアの表示
	void GameOverScene::CrieateBonusNum(){
		auto Bonus_KURAI1000 = AddGameObject<ResultSprite>(Vector3(1300.0f, 570.0f, 0.0f), L"NUMBERRESULT_TX");
		auto Bonus_KURAI100 = AddGameObject<ResultSprite>(Vector3(1400.0f, 570.0f, 0.0f), L"NUMBERRESULT_TX");
		auto Bonus_KURAI10 = AddGameObject<ResultSprite>(Vector3(1500.0f, 570.0f, 0.0f), L"NUMBERRESULT_TX");
		auto Bonus_KURAI1 = AddGameObject<ResultSprite>(Vector3(1600.0f, 570.0f, 0.0f), L"NUMBERRESULT_TX");
		auto ScoreManager = AddGameObject<ResultScoreManager>(Bonus_KURAI1, Bonus_KURAI10, 
			Bonus_KURAI100, Bonus_KURAI1000);

		auto PtrScene = dynamic_pointer_cast<Scene>(App::GetApp()->GetSceneBase());
		ScoreManager->ScoreDraw(PtrScene->GetCoin());
	}

	//合計スコア
	void GameOverScene::CrieateTotalNum(){
		auto Total_KURAI1000 = AddGameObject<ResultSprite>(Vector3(1300.0f, 730.0f, 0.0f), L"NUMBER01_TX");
		auto Total_KURAI100 = AddGameObject<ResultSprite>(Vector3(1400.0f, 730.0f, 0.0f), L"NUMBER01_TX");
		auto Total_KURAI10 = AddGameObject<ResultSprite>(Vector3(1500.0f, 730.0f, 0.0f), L"NUMBER01_TX");
		auto Total_KURAI1 = AddGameObject<ResultSprite>(Vector3(1600.0f, 730.0f, 0.0f), L"NUMBER01_TX");
		auto ScoreManager = AddGameObject<ResultScoreManager>(Total_KURAI1, Total_KURAI10,
			Total_KURAI100, Total_KURAI1000);
		//シーンの取得
		auto PtrScene = dynamic_pointer_cast<Scene>(App::GetApp()->GetSceneBase());
		auto Score = PtrScene->GetScore();
		auto Coin = PtrScene->GetCoin();
		ScoreManager->ScoreDraw(Score + Coin);
	}

	// 背景の作成
	void GameOverScene::CreateBackGround()
	{
		auto Ptr = AddGameObject<GameObject>();
		auto PtrTransMatrix = Ptr->GetComponent<TransformMatrix>();
		Quaternion Qt;
		Qt.RotationRollPitchYawFromVector(Vector3(0, 0, 0));
		Matrix4X4 WorldMat;
		WorldMat.DefTransformation(
			Vector3(14.75f, 8.3f, 1.0f),
			Qt,
			Vector3(0.0f, 0.0f, 0.0f)
			);
		PtrTransMatrix->SetWorldMatrix(WorldMat);

		//描画コンポーネントの追加
		auto DrawComp = Ptr->AddComponent<PNTStaticDraw>();

		//描画コンポーネントに形状（メッシュ）を設定
		DrawComp->SetMeshResource(L"DEFAULT_SQUARE");

		//描画コンポーネントテクスチャの設定
		DrawComp->SetTextureResource(L"BG_TX");

		//自分に影が映りこむようにする
		DrawComp->SetOwnShadowActive(false);
		Ptr->SetAlphaActive(true);
		Ptr->SetDrawLayer(1);
	}

	//サウンドの再生
	void GameOverScene::SoundPlay(wstring soundname, float volume)
	{
		auto pMultiSoundEffect = GetComponent<MultiSoundEffect>();
		pMultiSoundEffect->Start(soundname, 0, volume);
	}

	//初期化
	void GameOverScene::OnCreate()
	{
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"TUTORIAL_BUTTON1_SE");
		pMultiSoundEffect->AddAudioResource(L"GAMESTART");
		AddGameObject<Fade>(true, false);
		try{
			// リソースの作成
			CreateResourses();

			//ビュー類を作成する
			CreateViews();

			// リザルトスプライトの作成
			CreateResultSprite();

			// スコアスプライトの作成
			CreateScoreSprite();

			// ボーナススコアスプライトの作成
			CreateBonusScoreSprite();

			// トータルスコアスプライトの作成
			CreateTotalScoreSprite();

			// Aボタンスプライトの作成
			CreateAButtonSprite();

			// Bボタンスプライトの作成
			CreateBButtonSprite();

			// 背景の作成
			CreateBackGround();

			//進んだ距離の得点
			CreateScoreNum();

			//ボーナススコアの表示
			CrieateBonusNum();

			//合計スコア
			CrieateTotalNum();

		}
		catch (...){
			throw;
		}
	}

	//操作
	void GameOverScene::OnUpdate()
	{
		// コントローラーの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected)
		{
			// Aボタンが押されたらゲーム画面へ移動
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A && !IsButtonPush)
			{
				AddGameObject<Fade>(false, true);
				//name	:	youitirou
				//Aボタンを押されたときの音を鳴らす
				IsButtonPush = true;
				SoundPlay(L"GAMESTART", 0.6f);
				PostEvent(1.0f, GetThis<GameOverScene>(), App::GetApp()->GetSceneBase(), L"ToGame");
			}

			// Bボタンが押されたらタイトル画面へ移動
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_B && !IsButtonPush)
			{
				AddGameObject<Fade>(false, true);
				IsButtonPush = true;
				SoundPlay(L"TUTORIAL_BUTTON1_SE", 0.6f);
				PostEvent(1.0f, GetThis<GameOverScene>(), App::GetApp()->GetSceneBase(), L"ToTitle");
			}
		}
	}
}
//endof  basedx11