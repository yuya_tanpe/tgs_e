#pragma once

#include "stdafx.h"

namespace basedx11{
	class Player : public GameObject{
		bool m_LeftStick;
		size_t m_horizontal_line;

		//ゲーム内の５つの雲のライン 右移動
		void FirstLine_Right();
		void SecondLine_Right();
		void ThirdLine_Right();
		void FourthLine_Right();
		void FifthLine_Right();
		//ゲーム内の５つの雲のライン 左移動
		void FirstLine_Left();
		void SecondLine_Left();
		void ThirdLine_Left();
		void FourthLine_Left();
		void FifthLine_Left();

	public:
		//構築と破棄
		Player(const shared_ptr<Stage>& StagePtr);
		virtual ~Player(){}
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate() override;
		//デバッグ表示
		virtual void OnLastUpdate() override;

		//プレイヤーが今いるラインを取得
		size_t Gethorizontal_line(){
			return m_horizontal_line;
		}
	};
}
//endof  basedx11
