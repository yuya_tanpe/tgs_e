#include "stdafx.h"
#include "Project.h"

namespace basedx11
{
	TitleUI::TitleUI(shared_ptr<Stage>& StagePtr, const Vector3& Pos) :
		GameObject(StagePtr), m_Pos(Pos)
	{}

	TitleUI::~TitleUI(){}

	void TitleUI::OnCreate()
	{
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_Pos);
		PtrTransform->SetScale(7.0f, 2.3f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);

		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(100.0f, 100.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"TITLEBUTTON_TX");

		//センター原点
		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_CenterZeroPlusUpY);

		//両面描画
		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);

		auto Scale = GetComponent<Transform>()->GetScale();

		// 拡大・縮小
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddScaleTo(1.0f, Vector3(Scale.x - 0.5f, Scale.y - 0.2f, 1.0f));
		PtrAction->AddScaleTo(1.0f, Scale);
		PtrAction->SetLooped(true);
		PtrAction->Run();
	}


	// 羊のイラスト
	Title_SheepSprite::Title_SheepSprite(shared_ptr<Stage>& StagePtr, const Vector3& Pos) :
		GameObject(StagePtr), m_Pos(Pos)
	{}

	Title_SheepSprite::~Title_SheepSprite(){}

	void Title_SheepSprite::OnCreate()
	{
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_Pos);
		PtrTransform->SetScale(6.5f, 6.5f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);

		// スプライトを付ける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(100.0f, 100.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"SHEEP_TX");

		// センターを原点
		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_CenterZeroPlusUpY);

		// 透明描画
		SetAlphaActive(true);
	}

}
//endof  basedx11