#include "stdafx.h"
#include "Project.h"

namespace basedx11{
	//--------------------------------------------------------------------------------------
	//	class GameStage : public Stage;
	//	用途: ゲームステージクラス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	GameStage::GameStage() :Stage(),
		m_totaltime(0.0f), readycount(0), IsStart(false)	//Readyステートで使用	詳細は後日記述予定
	{}

	//リソースの作成と登録
	void GameStage::CreateResourses(){
		//仮背景のテクスチャ
		wstring strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStageUI\\watasi1.png";
		App::GetApp()->RegisterTexture(L"WATER_TX", strTexture);
		//スコアのテクスチャ
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStageUI\\Number.png";
		App::GetApp()->RegisterTexture(L"NUMBER_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"ResultUI\\Score.png";
		App::GetApp()->RegisterTexture(L"GAME_SCORE_TX", strTexture);

		//エフェクト
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Effect\\spark.png";
		App::GetApp()->RegisterTexture(L"SPARK_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Effect\\Circle01.png";
		App::GetApp()->RegisterTexture(L"CIRCLE_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Effect\\FireWork_Effect.png";
		App::GetApp()->RegisterTexture(L"FIREWORK_1", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Effect\\FireWork_Effect_2.png";
		App::GetApp()->RegisterTexture(L"FIREWORK_2", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Effect\\Hit.png";
		App::GetApp()->RegisterTexture(L"HIT_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Effect\\Particle.png";
		App::GetApp()->RegisterTexture(L"PARTICLE_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Effect\\ring.png";
		App::GetApp()->RegisterTexture(L"RING_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Effect\\zangeki.png";
		App::GetApp()->RegisterTexture(L"ZANGEKI_TX", strTexture);

		//SEの作成と登録
		wstring StartWav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\Start.wav";
		App::GetApp()->RegisterWav(L"Start", StartWav);
		wstring EnemyWav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\Enemy.wav";
		App::GetApp()->RegisterWav(L"Enemy", EnemyWav);
		wstring GoalWav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\Goal.wav";
		App::GetApp()->RegisterWav(L"Goal", GoalWav);
		wstring OverWav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\Over.wav";
		App::GetApp()->RegisterWav(L"Over", OverWav);
		wstring WalkWav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\Walk.wav";
		App::GetApp()->RegisterWav(L"Walk", WalkWav);
		wstring MoveWav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\MoveRL.wav";
		App::GetApp()->RegisterWav(L"MOVERL", MoveWav);
		wstring WaterWav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\WaterFall.wav";
		App::GetApp()->RegisterWav(L"WATER", WaterWav);
		wstring FireWorksWav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\firework.wav";
		App::GetApp()->RegisterWav(L"FIREWORKS", FireWorksWav);
		wstring ThenderWav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\Thender.wav";
		App::GetApp()->RegisterWav(L"THENDER", ThenderWav);
		//ゲームBGM
		wstring BGMWav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\BGM.wav";
		App::GetApp()->RegisterWav(L"GAMEBGM", BGMWav);
		//ジャンプする時の音
		wstring JumpWav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\jump.wav";
		App::GetApp()->RegisterWav(L"JUMP", JumpWav);
		//プレイヤーが落ちたときのBGM
		wstring JingleWav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\jingle.wav";
		App::GetApp()->RegisterWav(L"FALLBGM", JingleWav);
		//ゲームスタート時のカウント音
		wstring COUNTWav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\Count.wav";
		App::GetApp()->RegisterWav(L"COUNT", COUNTWav);

		//動けなかった時の音
		wstring NotMoveWav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\NotMove.wav";
		App::GetApp()->RegisterWav(L"NOTMOVE", NotMoveWav);
		wstring NotMove2Wav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\NotMove2.wav";
		App::GetApp()->RegisterWav(L"NOTMOVE2", NotMove2Wav);
		//ミサイルの警告音
		wstring misairuWav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\misairu.wav";
		App::GetApp()->RegisterWav(L"MISAIRU", misairuWav);
		//アイテムゲットしたときの音小さいほう
		wstring ItemSWav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\ItemGet.wav";
		App::GetApp()->RegisterWav(L"ITEMS", ItemSWav);
		//アイテムゲットしたときの音小さいほう
		wstring ItemLWav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\ItemGet2.wav";
		App::GetApp()->RegisterWav(L"ITEML", ItemLWav);
		//プレイヤーが25毎到達したとき
		wstring Score25Wav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\Score25.wav";
		App::GetApp()->RegisterWav(L"SCORE25", Score25Wav);
		//直進する敵が動かない敵と当たった時
		wstring NonEnemyHitWav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\NonEnemyHit.wav";
		App::GetApp()->RegisterWav(L"NONENEMYHIT", NonEnemyHitWav);
		//スタートピン
		wstring NoWav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\StartPin.wav";
		App::GetApp()->RegisterWav(L"STARTPIN", NoWav);

		//白色の雲
		App::GetApp()->RegisterStaticModelMesh(L"WHITECLOUD_MESH", App::GetApp()->m_wstrRelativeDataPath, L"kumo_ver2.bmf");
		//羊のモデル
		App::GetApp()->RegisterStaticModelMesh(L"KUMO_MESH", App::GetApp()->m_wstrRelativeDataPath, L"hituji.bmf");
		//ゴールテープのような物
		App::GetApp()->RegisterStaticModelMesh(L"RAN_MESH", App::GetApp()->m_wstrRelativeDataPath, L"ran.bmf");
		//とげとげの敵
		App::GetApp()->RegisterStaticModelMesh(L"NON_ENEMY_MESH", App::GetApp()->m_wstrRelativeDataPath, L"NonEnemy.bmf");
		//槍を持った敵
		App::GetApp()->RegisterBoneModelMesh(L"LANCE_MESH", App::GetApp()->m_wstrRelativeDataPath, L"Lance.bmf");
		//羽のついた敵
		App::GetApp()->RegisterBoneModelMesh(L"FLY_MESH", App::GetApp()->m_wstrRelativeDataPath, L"Fly.bmf");
		//雷雲
		App::GetApp()->RegisterStaticModelMesh(L"KAMINARI_2_MESH", App::GetApp()->m_wstrRelativeDataPath, L"kaminari_2.bmf");
		//コイン
		App::GetApp()->RegisterStaticModelMesh(L"COIN_MESH", App::GetApp()->m_wstrRelativeDataPath, L"Coin.bmf");
	}

	//ビュー類の作成
	void GameStage::CreateViews(){
		//最初にデフォルトのレンダリングターゲット類を作成する
		//パラメータの2048.0fはシャドウマップのサイズ。大きいほど影が細かくなる（4096.0fなど）
		//影を細かくするとメモリを大量に消費するので注意！
		CreateDefaultRenderTargets(2048.0f);
		//影のビューサイズの設定。この値を小さくすると影が表示される範囲が小さくなる。
		//値が小さいほうが影は細かくなる
		//スタティック変数なので一度設定したらその値はステージを超えて保持される。
		Shadowmap::SetViewSize(32.0f);
		//マルチビューコンポーネントの取得
		auto PtrMultiView = GetComponent<MultiView>();
		//マルチビューにビューの追加
		auto PtrView = PtrMultiView->AddView();
		//ビューの矩形を設定（ゲームサイズ全体）
		Rect2D<float> rect(0, 0, (float)App::GetApp()->GetGameWidth(), (float)App::GetApp()->GetGameHeight());
		//背景色(RGB)
		Color4 ViewBkColor(0.25f, 0.4f, 0.8f, 1.0f);
		//最初のビューにパラメータの設定
		PtrView->ResetParamaters<CustomCamera, MultiLight>(rect, ViewBkColor, 1, 0.0f, 1.0f);
		//最初のビューのビューのライトの設定
		auto PtrLight = PtrView->GetMultiLight()->GetLight(0);
		PtrLight->SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
		PtrLight->SetDiffuseColor(Color4(1.0f, 0.0f, 0.0f, 1.0f));
		//ビューのカメラの設定
		auto PtrCamera = PtrView->GetCamera();
		//カメラ位置	SetEye	カメラの位置		SetAt	カメラの注視位置
		PtrCamera->SetEye(Vector3(0.0f, 5.0f, -4.0f));
		PtrCamera->SetAt(Vector3(0.0f, 0.0f, 2.0f));
	}

	//-----------------確定関数------------------------
	//エフェクトの作成
	void GameStage::CreateEffect(){
		//お祝いのエフェクト
		auto Firework_EffectPtr = AddGameObject<Firework_Effect>();
		//シェア配列にスパークを追加
		SetSharedGameObject(L"Firework_Effect", Firework_EffectPtr);

		//プレイヤーが動いたときのエフェクト
		auto PlayerMove_EffectPtr = AddGameObject<PlayerMove_Effect>();
		//シェア配列にスパークを追加
		SetSharedGameObject(L"PlayerMove_Effect", PlayerMove_EffectPtr);

		//右の花火のエフェクト
		auto FireWorksRightPtr = AddGameObject<FireWorks_Right>();
		//シェア配列にスパークを追加
		SetSharedGameObject(L"FireWorks_Right", FireWorksRightPtr);

		//右の花火のエフェクト
		auto FireWorksLeftPtr = AddGameObject<FireWorks_Left>();
		//シェア配列にスパークを追加
		SetSharedGameObject(L"FireWorks_Left", FireWorksLeftPtr);

		//落ちたときのエフェクト
		auto CirlePtr = AddGameObject<Circle>();
		//シェア配列にスパークを追加
		SetSharedGameObject(L"Circle", CirlePtr);

		auto CoinEffectPtr = AddGameObject<CoinEffect>();
		//シェア配列にスパークを追加
		SetSharedGameObject(L"CoinEffect", CoinEffectPtr); 

		auto NonMove_HitPtr = AddGameObject<NonMove_HitEffect>();
		//シェア配列にスパークを追加
		SetSharedGameObject(L"NonMove_HitEffect", NonMove_HitPtr);

		//エフェクトはZバッファを使用する
		GetParticleManager()->SetZBufferUse(true);
	}
	//背景の作成
	void GameStage::CreateBackGround(){
		//ステージへのゲームオブジェクトの追加
		auto Ptr = AddGameObject<GameObject>();
		auto PtrTransMatrix = Ptr->GetComponent<TransformMatrix>();
		Quaternion Qt;
		Qt.RotationRollPitchYawFromVector(Vector3(XM_PIDIV2, 0, 0));
		Matrix4X4 WorldMat;
		WorldMat.DefTransformation(
			Vector3(40.0f, 40.0f, 1.0f),
			Qt,
			Vector3(0.0f, -1.0f, 4.0f)
			);
		PtrTransMatrix->SetWorldMatrix(WorldMat);

		//描画コンポーネントの追加
		auto DrawComp = Ptr->AddComponent<PNTStaticDraw>();
		DrawComp->SetDiffuse(Color4(0.0f, 0.0f, 0.0f, 0.3f));
		//描画コンポーネントに形状（メッシュ）を設定
		DrawComp->SetMeshResource(L"DEFAULT_SQUARE");
		//描画コンポーネントテクスチャの設定
		DrawComp->SetTextureResource(L"WATER_TX");
		//自分に影が映りこむようにする
		DrawComp->SetOwnShadowActive(true);
	}
	//スコアスプライト
	void GameStage::CreateScoreSprite(){
		//スコアオブジェクトの表示
		auto KURAI_100 = AddGameObject<ScoreSprite>(Vector3(279.0f, 100.0f, 0.00), false);
		auto KURAI_10 = AddGameObject<ScoreSprite>(Vector3(339.0f, 100.0f, 0.00), false);
		auto KURAI_1 = AddGameObject<ScoreSprite>(Vector3(409.0f, 100.0f, 0.00), true);
		//スコアマネージャー
		auto scoremanager = AddGameObject<ScoreManager>(KURAI_1, KURAI_10, KURAI_100);
		SetSharedGameObject(L"ScoreManager", scoremanager);
	}
	// スコアの文字作成
	void GameStage::CreateScoreTexture()
	{
		AddGameObject<ScoreUI>(Vector3(-830.0f, 450.0f, 0.0f));
	}
	//ボーナスアイテムスプライトの作成
	void GameStage::CreateCoinSprite(){
		//コインの数の1桁目
		auto KURAI_1 = AddGameObject<CoinSprite>(Vector3(1800.0f, 100.0f, 0.00), true);
		//コインの数の10桁目
		auto KURAI_10 = AddGameObject<CoinSprite>(Vector3(1730.0f, 100.0f, 0.00), false);
		//コインの数の100桁目
		auto KURAI_100 = AddGameObject<CoinSprite>(Vector3(1660.0f, 100.0f, 0.00), false);
		//コインの数の1000桁目
		auto KURAI_1000 = AddGameObject<CoinSprite>(Vector3(1590.0f, 100.0f, 0.00), false);
		//コインマネージャー
		auto CoinmanagerPtr = AddGameObject<CoinManager>(KURAI_1, KURAI_10, KURAI_100, KURAI_1000);
		SetSharedGameObject(L"CoinManager", CoinmanagerPtr);
	}
	// コインスプライトの作成
	void GameStage::CreateCoinUI()
	{
		AddGameObject<GameStage_CoinSprite>(Vector3(600.0f, 450.0f, 0.0f));
		AddGameObject<Fade>(true, false);
	}
	//ゲームオーバースプライトの作成
	void GameStage::CreateGameOverSprite(){
		AddGameObject<GameOverSprite>(Vector3(940.0f, -130.0f, 0.00));
	}
	//Readyスプライトの作成
	void GameStage::CreateReadySprite(){
		auto Ptr = AddGameObject<GameReadySprite>(Vector3(0.0f, 300.0f, 0.0f));
		SetSharedGameObject(L"READY", Ptr);
	}
	//文字列コンポーネントの追加
	void GameStage::AddCompoString(){
		//文字列をつける
		auto PtrString = AddComponent<StringSprite>();
		PtrString->SetText(L"");
		PtrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));
	}
	//デバッグ表示
	void GameStage::DebugOutput(){
		wstring Name(L"ゲームステージ ");
		Name += L"\n";
		auto fps = App::GetApp()->GetStepTimer().GetFramesPerSecond();
		wstring FPS(L"FPS: ");
		FPS += Util::UintToWStr(fps);
		FPS += L"\n";

		auto ObjNum = GetGameObjectVec().size();

		wstring	WstrObjNum(L"オブジェクトの数: ");
		WstrObjNum += Util::UintToWStr(ObjNum);
		WstrObjNum += L"\n";

		wstring str = Name + FPS + WstrObjNum;

		//文字列をつける
		auto PtrString = GetComponent<StringSprite>();
		PtrString->SetStartPosition(Point2D<float>(10.0f, 250.0f));
		PtrString->SetFontColor(Color4(1.0f, 0.0f, 0.0f, 1.0f));
		PtrString->SetText(str);
	}

	//---------------GameViewで使用予定-------------------------

	//雲の作成
	void GameStage::CreateCloud(){
		auto None = AddGameObject<Cloud>(CloudName::None);
		auto White = AddGameObject<Cloud>(CloudName::White);
		auto Thunder = AddGameObject<Cloud>(CloudName::Thunder);

		SetSharedGameObject(L"NoneCloud", None);
		SetSharedGameObject(L"WhiteCloud", White);
		SetSharedGameObject(L"ThunderCloud", Thunder);
	}
	//プレイヤーの作成
	void GameStage::CreatePlayer(){
		AddGameObject<Player>();
	}
	//入力バーの作成
	void GameStage::CreateInput_bar(){
		AddGameObject<input_bar>();
	}
	//アイテムの作成
	void GameStage::CreateItem(){
		AddGameObject<Fly_Item>();
	}
	//看板の作成
	void GameStage::CreateKanban(){
		AddGameObject<Kanban>();
	}
	//ドキュメントとビューの作成
	void GameStage::CreateGameDocumentView(){
		AddGameObject<GameDocument>();
		AddGameObject<GameView>();
	}

	//羊の作成
	void GameStage::CreateSheep(){
		AddGameObject<Sheep>();
	}
	//オブジェクトグループの作成
	void GameStage::CreateGroupVec(){
		//グループを作成するだけ
		CreateSharedObjectGroup(L"CoinGroup");
		CreateSharedObjectGroup(L"EnemyGroup");
	}

	//---------------微妙なやつ-------------------------

	//落ちたときの水エフェクト
	void GameStage::CreateEffectBox(Vector3 Pos){
		AddGameObject<EffectBox>(Vector3(0.5f, 3.0f, 0.0f), Pos);
		AddGameObject<EffectBox>(Vector3(-0.5f, 3.0f, 0.0f), Pos);
		AddGameObject<EffectBox>(Vector3(0.0f, 3.0f, 0.5f), Pos);
		AddGameObject<EffectBox>(Vector3(0.0f, 3.0f, -0.5f), Pos);
		AddGameObject<EffectBox>(Vector3(0.5f, 3.0f, 0.5f), Pos);
		AddGameObject<EffectBox>(Vector3(0.5f, 3.0f, -0.5f), Pos);
		AddGameObject<EffectBox>(Vector3(-0.5f, 3.0f, 0.5f), Pos);
		AddGameObject<EffectBox>(Vector3(-0.5f, 3.0f, -0.5f), Pos);
		AddGameObject<EffectBox>(Vector3(0.2f, 4.0f, 0.2f), Pos);
		AddGameObject<EffectBox>(Vector3(-0.2f, 4.0f, -0.2f), Pos);
	}

	//--------------作成予定---------------------------

	//初期化
	void GameStage::OnCreate(){
		try{
			//リソースの作成
			CreateResourses();
			//ビュー類を作成する
			CreateViews();
			//背景の作成
			CreateBackGround();
			//スコアの表示
			CreateScoreSprite();
			//スコアの文字表示
			CreateScoreTexture();
			//コインの表示
			CreateCoinSprite();
			//コインの文字
			CreateCoinUI();
			//エフェクトの作成
			CreateEffect();
			//ゲームスタート時のカウント画像
			CreateReadySprite();

			//------ビュークラスで使用するオブジェクトの作成
			//看板の作成
			CreateKanban();
			//雲の作成
			CreateCloud();
			//入力バーの作成
			CreateInput_bar();
			//アイテムの作成
			CreateItem();
			//羊の作成
			CreateSheep();
			//グループの作成
			CreateGroupVec();

			//--------ドキュメントの作成------------
			//ドキュメントビューの作成
			CreateGameDocumentView();

			//サウンドの追加
			AddComponent<MultiSoundEffect>()->AddAudioResource(L"COUNT");
			AddComponent<MultiSoundEffect>()->AddAudioResource(L"STARTPIN");

			//ステートマシンの構築
			m_StateMachine = make_shared< StateMachine<GameStage> >(GetThis<GameStage>());
			//最初のステートをGameReadyStateに設定
			m_StateMachine->ChangeState(GameReadyState::Instance());
		}
		catch (...){
			throw;
		}
	}

	//ステートの更新
	void GameStage::OnUpdate(){
		m_StateMachine->Update();
	}

	//---------------CreateReadySpriteで使用するMotion-----------------

	//時間測定
	bool GameStage::waittime(){
		auto elpsed = App::GetApp()->GetElapsedTime();
		m_totaltime += elpsed;

		if (m_totaltime >= 1.3f){
			m_totaltime = 0.0f;
			return true;
		}
		return false;
	}

	//時間が経過したら画像を変更する命令を送る
	void GameStage::TextureChange(){
		GetSharedGameObject<GameReadySprite>(L"READY")->NextTexture();
		GetComponent<MultiSoundEffect>()->Start(L"COUNT", 0, 0.5f);
		readycount++;
		if (readycount == 3){
			GetStateMachine()->ChangeState(PlayGameState::Instance());
			GetComponent<MultiSoundEffect>()->Start(L"STARTPIN", 0, 0.6f);
		}
	}

	//カウント時に使用する画像を消す
	void GameStage::fade(){
		IsStart = true;
		GetSharedGameObject<GameReadySprite>(L"READY")->Fadeout();
	}
	//ドキュメントへの命令
	void GameStage::Is_DocumentUpdate(bool tf){
		GetSharedGameObject<GameDocument>(L"GameDocument")->SetMapMove(tf);
	}

	//ランダムな値を返す	( 第1引数 最小値  第2引数 最大値 )
	int GameStage::RandToint(int min, int max){
		//--------------------------------------------------------------------------------------------------
		//TODO : 乱数生成　テンプレート使ってみたい		  ※メルセンヌが分からん
		random_device rnd;								  // 非決定的な乱数生成器を生成
		mt19937 mt(rnd());								  //  メルセンヌ・ツイスタの32ビット版、引数は初期シード値
		uniform_int_distribution<> rand3(min, max);        // [0, 99] 範囲の一様乱数
		//--------------------------------------------------------------------------------------------------
		return rand3(mt);
	}

	//左側に花火を出す
	void GameStage::OneLeftFireWorks(){
		
		auto PtrPlayerMove_Effect = GetSharedGameObject<FireWorks_Left>(L"FireWorks_Left", false);
		if (PtrPlayerMove_Effect){
			PtrPlayerMove_Effect->InsertFireWorks_Left(Vector3(-3.0f, -2.0f, 2.0f));
		}
	}
	//右側に花火を出す
	void GameStage::OneRightFireWorks(){

		auto PtrPlayerMove_Effect = GetSharedGameObject<FireWorks_Right>(L"FireWorks_Right", false);
		if (PtrPlayerMove_Effect){
			PtrPlayerMove_Effect->InsertFireWorks_Right(Vector3(3.2f, 0.0f, 2.0f));
		}
	}

	//--------------------------------------------------------------------------------------
	//	class GameReadyState : public ObjState<GameStage>;
	//	用途: ゲームスタート待ちステート
	//--------------------------------------------------------------------------------------
	shared_ptr<GameReadyState> GameReadyState::Instance(){
		static shared_ptr<GameReadyState> instance;
		if (!instance){
			instance = shared_ptr<GameReadyState>(new GameReadyState);
		}
		return instance;
	}
	void GameReadyState::Enter(const shared_ptr<GameStage>& Obj){
		//ドキュメントの停止
		Obj->Is_DocumentUpdate(false);
	}
	void GameReadyState::Execute(const shared_ptr<GameStage>& Obj){
		if (Obj->waittime()){
			//テクスチャ変え
			Obj->TextureChange();
		}
	}
	void GameReadyState::Exit(const shared_ptr<GameStage>& Obj){
	}

	//--------------------------------------------------------------------------------------
	//	class PlayGameState : public ObjState<GameStage>;
	//	用途: ゲーム中ステート
	//--------------------------------------------------------------------------------------
	shared_ptr<PlayGameState> PlayGameState::Instance(){
		static shared_ptr<PlayGameState> instance;
		if (!instance){
			instance = shared_ptr<PlayGameState>(new PlayGameState);
		}
		return instance;
	}
	void PlayGameState::Enter(const shared_ptr<GameStage>& Obj){
	}
	void PlayGameState::Execute(const shared_ptr<GameStage>& Obj){
		//スタート表示して時間経過後に消す
		if (Obj->waittime() && !Obj->IsStartTF()){
			Obj->fade();
			//ドキュメントの開始
			Obj->Is_DocumentUpdate(true);
		}
	}
	void PlayGameState::Exit(const shared_ptr<GameStage>& Obj){
	}
}
//endof  basedx11