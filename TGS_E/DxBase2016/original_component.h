#pragma once

#include "stdafx.h"

namespace basedx11{
	/**
	* @class Sky_Fall
	* @brief 常に下に流れるコンポーネント
	* @author　sike yuya
	* @date	 2016/-4/17
	*/
	class Sky_Fall : public Component {
	private:
		float m_speed;

	public:

		Sky_Fall(const shared_ptr<GameObject>& GameObjectPtr);
		virtual ~Sky_Fall();

		//操作
		virtual void OnUpdate()override;
		virtual void OnDraw()override{}

		void SetSpeed(float i_speed){
			m_speed = i_speed;
		}

		void Sky_FallSetActive(bool TF){
			SetUpdateActive(TF);
		}
	};
}
//endof  basedx11