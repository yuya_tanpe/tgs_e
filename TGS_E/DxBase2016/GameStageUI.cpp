#include "stdafx.h"
#include "Project.h"

namespace basedx11{
	//--------------------------------------------------------------------------------------
	//	class NumberSprite : public GameObject;
	//	用途: スコアオブジェクト
	//--------------------------------------------------------------------------------------
	//構築と破棄
	NumberSprite::NumberSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos, const bool DrawTF) :
		GameObject(StagePtr), m_StartPos(StartPos), m_TotalTime(0), DrawActive(DrawTF){
	}
	NumberSprite::~NumberSprite(){}

	//初期化
	void NumberSprite::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(1.0f, 1.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"NUMBER_TX");
		//透明処理
		SetAlphaActive(true);

		SetDrawActive(DrawActive);
		//左上原点
		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftTopZeroPlusDownY);
		//スプライトの中のメッシュからバックアップの取得
		auto& SpVertexVec = PtrSprite->GetMeshResource()->GetBackupVerteces<VertexPositionColorTexture>();
		//各数字ごとにUV値を含む頂点データを配列化しておく
		for (size_t i = 0; i < 10; i++){
			float from = ((float)i) / 10.0f;
			float to = from + (1.0f / 10.0f);
			vector<VertexPositionColorTexture> NumVirtex =
			{
				//左上頂点
				VertexPositionColorTexture(
				SpVertexVec[0].position,
				Color4(1.0f, 1.0f, 1.0f, 1.0f),
				Vector2(from, 0)
				),
				//右上頂点
				VertexPositionColorTexture(
				SpVertexVec[1].position,
				Color4(1.0f, 1.0f, 1.0f, 1.0f),
				Vector2(to, 0)
				),
				//左下頂点
				VertexPositionColorTexture(
				SpVertexVec[2].position,
				Color4(1.0f, 1.0f, 1.0f, 1.0f),
				Vector2(from, 1.0f)
				),
				//右下頂点
				VertexPositionColorTexture(
				SpVertexVec[3].position,
				Color4(1.0f, 1.0f, 1.0f, 1.0f),
				Vector2(to, 1.0f)
				),
			};
			m_NumberVertexVec.push_back(NumVirtex);
		}
		auto MeshRes = PtrSprite->GetMeshResource();
		//動的にUV値が変わる頂点を設定する
		MeshRes->UpdateVirtexBuffer(m_NumberVertexVec[0]);
	}

	void NumberSprite::OnUpdate(){
	}

	// スコアテクスチャ
	ScoreUI::ScoreUI(shared_ptr<Stage>& StagePtr, const Vector3& Pos) :
		GameObject(StagePtr), m_Pos(Pos)
	{}
	ScoreUI::~ScoreUI()
	{}

	//初期化
	void ScoreUI::OnCreate()
	{
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_Pos);
		PtrTransform->SetScale(4.5f, 1.5f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);

		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"GAME_SCORE_TX");

		//透明処理
		SetAlphaActive(true);

		//左上原点
		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_CenterZeroPlusUpY);
	}

	/**
	* @class ScoreframeSprite
	* @brief スコアの枠
	* @author yuya
	* @date 2016/05/12
	*/
	ScoreframeSprite::ScoreframeSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos){
	}
	ScoreframeSprite::~ScoreframeSprite(){}

	//初期化
	void ScoreframeSprite::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(4.0f, 2.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"SCORE_BAR");
		//透明処理
		SetAlphaActive(true);
		//左上原点
		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftTopZeroPlusDownY);
	}

	// コインスプライト
	GameStage_CoinSprite::GameStage_CoinSprite(shared_ptr<Stage>& StagePtr, const Vector3& Pos) :
		GameObject(StagePtr), m_Pos(Pos)
	{}
	GameStage_CoinSprite::~GameStage_CoinSprite()
	{}

	void GameStage_CoinSprite::OnCreate()
	{
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_Pos);
		PtrTransform->SetScale(1.0f, 1.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);

		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"COIN");

		//透明処理
		SetAlphaActive(true);

		//左上原点
		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_CenterZeroPlusUpY);
	}


	/**
	* @class GameOverSprite
	* @brief ゲームオーバーを知らせるスプライト
	* @author yuya
	* @date 2016/05/19
	*/
	GameOverSprite::GameOverSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos){
	}
	GameOverSprite::~GameOverSprite(){}

	//初期化
	void GameOverSprite::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(10.0f, 5.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"GAMEOVER_TX");
		//透明処理
		SetAlphaActive(true);
		SetAlphaExActive(true);
		//レイヤーの設定
		SetDrawLayer(2);
		//左上原点
		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftTopZeroPlusDownY);
		auto ActionPtr = AddComponent<Action>();
		ActionPtr->AddMoveTo(2.0f, Vector3(940.0f, 300.0f, 0.00));
		ActionPtr->AddRotateInterval(2.0f);
		ActionPtr->AddRotateTo(1.0f, Vector3(0.0f, 0.0f, 5.0f / 180.0f * XM_PI));
		ActionPtr->SetLooped(false);
		ActionPtr->Run();
	}

	void GameOverSprite::OnUpdate(){
		if (GetComponent<Action>()->IsArrived()){
			auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
			GameStagePtr->AddGameObject<Fade>(false, true);
		}
	}

	/**
	* @class GameReadySprite
	* @brief ゲーム始まる前の数字スプライト
	* @author yuya
	* @date 2016/06/10
	*/
	GameReadySprite::GameReadySprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos), m_TextureVec(3), m_texturenum(0)
	{
		m_TextureVec = { L"TWO_TX", L"ONE_TX", L"START_TX" };
	}
	GameReadySprite::~GameReadySprite(){}

	void GameReadySprite::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(3.0f, 1.5f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//透明処理
		SetAlphaActive(true);
		SetAlphaExActive(true);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(256.0f, 256.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"THREE_TX");
		//センター原点
		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_CenterZeroPlusUpY);

		AddComponent<Action>();
	}
	//次に表示するテクスチャを表示する
	void GameReadySprite::NextTexture(){
		//テクスチャの切り替え
		GetComponent<PCTSpriteDraw>()->SetTextureResource(m_TextureVec[m_texturenum++]);
		if (m_texturenum >= 3){
			m_texturenum = 0;
		}
	}

	//フェードアウト
	void GameReadySprite::Fadeout(){
		SetDrawActive(false);
		SetUpdateActive(false);
	}
}
//endof  basedx11