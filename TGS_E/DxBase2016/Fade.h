#pragma once

#include "stdafx.h"

namespace basedx11
{
	//--------------------------------------------------------------------------------------
	//	class Fade : public GameObject;
	//	用途: フェード用
	//--------------------------------------------------------------------------------------
	class Fade : public GameObject
	{
		//リソースの作成
		void CreateResourses();

		// FadeInするかどうか
		bool FadeInActive;

		// FadeOutするかどうか
		bool FadeOutActive;

		// 変更するアルファ値
		float FadeInAlpha = 1.0f;
		float FadeOutAlpha = 0.0f;

	public:
		//構築と破棄
		Fade(const shared_ptr<Stage>& StagePtr, bool FadeIn, bool FadeOut);
		virtual ~Fade(){}

		//初期化
		virtual void OnCreate() override;

		//更新
		virtual void OnUpdate() override;

		// アルファ値の取得
		float GetFadeInAlpha()
		{
			return FadeInAlpha;
		}
		float GetFadeOutAlpha()
		{
			return FadeOutAlpha;
		}
	};
}