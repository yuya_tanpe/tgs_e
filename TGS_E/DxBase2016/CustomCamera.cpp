#include "stdafx.h"
#include "Project.h"

namespace basedx11{
	CustomCamera::CustomCamera() : LookAtCamera()
	{}

	void CustomCamera::OnUpdate(){
		Vector3 NewAt(0.0f, 0.0f, 5.0f);
		auto TargetPtr = GetTargetObject();
		if (TargetPtr){
			//目指したい場所
			auto Mat = TargetPtr->GetComponent<TransformMatrix>()->GetWorldMatrix();
			Vector3 ToAt = Mat.PosInMatrix();
			Vector3 Work{ NewAt.x, ToAt.y, ToAt.z };
			NewAt = Lerp::CalculateLerp(GetAt(), Work, 0, 5.0f, 0.1f, Lerp::Linear);
		}
		//ステップ1、注視点と位置の変更
		Vector3 Span = GetAt() - GetEye();
		Vector3 NewEye = NewAt - Span;
		SetAt(NewAt);
		SetEye(NewEye);

		//カメラの更新
		Camera::OnUpdate();
	}
}