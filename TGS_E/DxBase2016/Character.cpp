#include "stdafx.h"
#include "Project.h"

namespace basedx11{
	/**
	* @class Player
	* @brief プレイヤー
	* @author　sike yuya
	* @date	2016/04/19 atNew 6/14
	*/

	Player::Player(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr),
		m_MoveDirection(Direction::Front),
		m_SideMove(false),
		m_StickReset(true)
	{}

	void Player::OnCreate(){
		//描画処理は行わない
		SetDrawActive(false);
		GetStage()->SetSharedGameObject(L"Player", GetThis<Player>());

		auto PtrTrans = AddComponent<Transform>();
		PtrTrans->SetPosition(0, 0, 0);
		PtrTrans->SetRotation(0, 0, 0);
		PtrTrans->SetScale(0.6f, 0.3f, 0.6f);

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(1.0f, 1.0f, 1.0f),
			Vector3(0.0f, XM_PI, 0.0f),
			Vector3(0.0f, -0.5f, 0.0f)
			);

		//影をつける（シャドウマップを描画する）
		auto ShadowPtr = AddComponent<Shadowmap>();
		//影の形（メッシュ）を設定
		ShadowPtr->SetMeshResource(L"KUMO_MESH");
		ShadowPtr->SetMeshToTransformMatrix(SpanMat);

		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<PNTStaticModelDraw>();
		//描画するメッシュを設定
		PtrDraw->SetMeshResource(L"KUMO_MESH");
		PtrDraw->SetMeshToTransformMatrix(SpanMat);

		//CustomCameraCameraを使用し、プレイヤーを注視するようにする
		auto PtrCamera = dynamic_pointer_cast<CustomCamera>(GetStage()->GetCamera(0));
		if (PtrCamera){
			//MyCameraに注目するオブジェクト（プレイヤー）の設定
			PtrCamera->SetTargetObject(GetThis<GameObject>());
		}

		//透過処理
		SetAlphaActive(true);

		//移動コンポーネントの追加
		auto PtrRotateTo = AddComponent<RotateTo>();
		//重力コンポーネントの追加
		auto PtrGravity = AddComponent<Gravity>();
		PtrGravity->SetBaseY(0.25f);

		//文字列をつける
		auto PtrString = AddComponent<StringSprite>();
		PtrString->SetText(L"");
		PtrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));

		//SEの登録
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Walk");
		pMultiSoundEffect->AddAudioResource(L"WATER");
		pMultiSoundEffect->AddAudioResource(L"THENDER");
		pMultiSoundEffect->AddAudioResource(L"GAMEBGM");
		pMultiSoundEffect->AddAudioResource(L"Jump");
		pMultiSoundEffect->AddAudioResource(L"NOTMOVE2");

		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<Player> >(GetThis<Player>());
		//最初のステートをDefaultStateに設定
		m_StateMachine->ChangeState(WaitTimeState::Instance());

		//BGMの再生
		SoundPlay(L"GAMEBGM", 0.5f);
	}

	//変化
	void Player::OnUpdate(){
		//ステートマシンのUpdateを行う
		//この中でステートの更新が行われる(Execute()関数が呼ばれる)
		m_StateMachine->Update();
	}
	//デバッグ関連
	void Player::OnLastUpdate(){
		//デバッグ表示
		//DebugOutput();
	}
	//イベント
	void Player::OnEvent(const shared_ptr<Event>& event)
	{
		if (event->m_MsgStr == L"OBJ_ACTIVE")
		{
			GetStateMachine()->ChangeState(WaitTimeState::Instance());
		}

		if (event->m_MsgStr == L"OBJ_STOP")
		{
			GetStateMachine()->ChangeState(StopState::Instance());
		}
	}

	//サウンドの再生
	void Player::SoundPlay(wstring soundname, float volume){
		auto pMultiSoundEffect = GetComponent<MultiSoundEffect>();
		pMultiSoundEffect->Start(soundname, 0, volume);
	}
	//サウンドのストップ
	void Player::SoundStop(wstring soundname){
		auto pMultiSoundEffect = GetComponent<MultiSoundEffect>();
		pMultiSoundEffect->Stop(soundname);
	}
	//デバッグ表示
	void Player::DebugOutput(){
		wstring Name(L"自分キャラクター関連 ");
		Name += L"\n";
		wstring Statename(L"name : ");
		if (m_StateMachine->GetCurrentState() == WaitTimeState::Instance()){
			Statename = L"WaitTimeState";
		}
		else if (m_StateMachine->GetCurrentState() == MoveState::Instance()){
			Statename = L"MoveUpState";
		}
		else if (m_StateMachine->GetCurrentState() == GameOverState::Instance()){
			Statename = L"GameOverState";
		}
		Statename += L"\n";

		wstring SideMoveWstr{ L"横に動いたか？ : " };
		if (m_SideMove){ SideMoveWstr += L"横に動きます"; }
		else{ SideMoveWstr += L"前に進みます"; }
		SideMoveWstr += L"\n";

		wstring WstrCharacter_Pos{ L"Player_Position : " };
		WstrCharacter_Pos += L"\n";
		auto PlayerPos = GetComponent<Transform>()->GetPosition();
		WstrCharacter_Pos += L"X = " + Util::FloatToWStr(PlayerPos.x) + L"\n";
		WstrCharacter_Pos += L"Y = " + Util::FloatToWStr(PlayerPos.y) + L"\n";
		WstrCharacter_Pos += L"Z = " + Util::FloatToWStr(PlayerPos.z) + L"\n";

		wstring str = Name + Statename + SideMoveWstr + WstrCharacter_Pos;

		//文字列をつける
		auto PtrString = GetComponent<StringSprite>();
		PtrString->SetStartPosition(Point2D<float>(10.0f, 10.0f));
		PtrString->SetFontColor(Color4(0.0f, 0.0f, 0.0f, 1.0f));
		PtrString->SetText(str);
	}

	//-----------------------------
	//WaitTimeStateで使用するMotion
	//-----------------------------
	//何秒間か待つ
	bool Player::WaitTime(const float& Intime){
		auto time = App::GetApp()->GetElapsedTime();
		m_waittime += time;
		if (m_waittime > Intime){
			m_waittime = 0.0f;
			return true;
		}
		return false;
	}
	//右スティックによる向き変更
	size_t Player::GetControllerStick(){
		//コントローラーの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		//スティックがリセットされているか
		if (m_StickReset){
			if (CntlVec[0].bConnected){
				if (abs(CntlVec[0].fThumbRY) >= 0.5f){
					//Y方向優先
					//上下は少し甘くする
					if (CntlVec[0].fThumbRY > 0.0f){
						m_StickReset = false;
						return 3;
					}
					else{
						m_StickReset = false;
						return 4;
					}
				}
				else if (abs(CntlVec[0].fThumbRX) >= 0.7f){
					//X方向
					if (CntlVec[0].fThumbRX < 0.0f){
						m_StickReset = false;
						return 1;
					}
					else{
						m_StickReset = false;
						return 2;
					}
				}
			}
		}
		else{
			//リセットされてない
			if (CntlVec[0].bConnected){
				if (CntlVec[0].fThumbRX == 0.0f && CntlVec[0].fThumbRY == 0.0f){
					m_StickReset = true;
				}
			}
		}
		return 0;
	}
	//スティック入力による向き変え
	void Player::Change_Direction(const size_t& num){
		switch (num)
		{
		case 1:
			//左入力
			NextLeftMove();
			break;
		case 2:
			//右入力
			NextRightMove();
			break;
		case 3:
			//上入力
			NextFrontMove();
			break;
		case 4:
			//下入力
			break;
		default:
			//入力なし
			break;
		}
	}
	//次に行く道を決定する（準備段階）
	void Player::NextFrontMove(){
		m_MoveDirection = Direction::Front;
		Change_Direction(m_MoveDirection);
	}
	void Player::NextRightMove(){
		m_MoveDirection = Direction::Right;
		//リミットの確認
		if (Limit_SideLine(m_MoveDirection)){
			Change_Direction(m_MoveDirection);
		}
		else{
			//TODO : 音の変更
			SoundPlay(L"NOTMOVE2", 0.3f);
			ResetDirection();
		}
	}
	void Player::NextLeftMove(){
		m_MoveDirection = Direction::Left;
		//リミットの確認
		if (Limit_SideLine(m_MoveDirection)){
			Change_Direction(m_MoveDirection);
		}
		else{
			//TODO : 音の変更
			SoundPlay(L"NOTMOVE2", 0.3f);
			ResetDirection();
		}
	}
	//DirectionをFrontに戻す
	void Player::ResetDirection(){
		m_MoveDirection = Direction::Front;
		Change_Direction(m_MoveDirection);
	}
	//プレイヤーの向きの変更
	void Player::Change_Direction(Direction dir){
		auto PtrRotate = GetComponent<RotateTo>();
		PtrRotate->SetParams(0.35f, Return_Direction(dir));
		PtrRotate->Run();
	}
	//プレイヤーの向きを変更する
	Vector3 Player::Return_Direction(Direction dir){
		switch (dir)
		{
		case Direction::Front:
			return Vector3(0.0f, 0.0f / 180.0f * XM_PI, 0.0f);
			break;
		case Direction::Right:
			return Vector3(0.0f, 90.0f / 180.0f * XM_PI, 0.0f);
			break;
		case Direction::Left:
			return Vector3(0.0f, 270.0f / 180.0f * XM_PI, 0.0f);
			break;
		default:
			assert(!"不正な値が入りました。引数のDirectionの値を確認してください。");
			return Vector3(0.0f, 0.0f, 0.0f);
			break;
		}
	}
	//左右の雲の行動制限　引数Directionで処理の内容が少し違うだけ
	bool Player::Limit_SideLine(const Direction& dir){
		//自分の位置（セル）を取得
		//TODO : ドキュメント側からPlayerのセル取得しておく

		/*switch (dir)
		{
		case Direction::Front:
		assert(!"本来通らない場所にアクセスされました。引数のDirectionを確認してください");
		break;
		case Direction::Right:
		if (Mycell.m_Col == 4){ return false; }
		return true;
		break;
		case Direction::Left:
		if (Mycell.m_Col == 0){ return false; }
		return true;
		break;
		default:
		assert(!"本来通らない場所にアクセスされました。引数のDirectionを確認してください");
		return false;
		break;
		}*/
		return false;
	}

	//-----------------------------
	//MoveStateで使用するMotion
	//-----------------------------
	//ジャンプ処理
	void Player::Jump(){
		auto PtrGravity = GetComponent<Gravity>();
		PtrGravity->StartJump(DirectionToVec3(m_MoveDirection));
		//name	:	youitirou
		//ジャンプしたときの音を鳴らす
		SoundPlay(L"Jump", 0.4f);
	}

	//ジャンプが終わったか
	bool Player::IsJump(){
		auto PtrGravity = GetComponent<Gravity>();
		if (PtrGravity->GetGravityVelocity().Length() <= 0){
			//name	:	youitirou
			//ジャンプしたときの音を止める
			SoundStop(L"Jump");
			return true;
		}
		return false;
	}
	//ジャンプをする向きを取得
	Vector3 Player::DirectionToVec3(Direction dir){
		switch (dir)
		{
		case Direction::Front:
			return Vector3{ 0.0f, 3.0f, 1.65f };
			break;
		case Direction::Back:
			return  Vector3{ 0.0f, 3.0f, -1.65f };
			break;
		case Direction::Right:
			return Vector3{ 1.65f, 3.0f, 0.0f };
			break;
		case Direction::Left:
			return Vector3{ -1.65f, 3.0f, 0.0f };
			break;
		default:
			assert(!"不正な値を検出　DirectionToVec3() ");
			return Vector3{ 0, 0, 0 };
			break;
		}
	}
	//ゲームステージ上にあるスコアを加算する
	void Player::ScoreUp_GameStage(){
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		assert(GameStagePtr && "ゲームステージの取得に失敗しました。");
		auto score = GameStagePtr->GetSharedGameObject<ScoreManager>(L"ScoreManager");
		score->ScorePlus();
	}
	//セルの移動
	void Player::CellMoveMethod(){
		switch (m_MoveDirection)
		{
		case Direction::Front:
			//前方に移動
			CellFront();
			ScoreUp_GameStage();
			break;
		case Direction::Right:
			//前方に移動
			CellRight();
			//雲の流れを止める、遅くする
			m_SideMove = true;
			break;
		case Direction::Left:
			//前方に移動
			CellLeft();
			//雲の流れを止める、遅くする
			m_SideMove = true;
			break;
		default:
			assert(!"不正な値を検出　CellMoveMethod() ");
			break;
		}
	}
	//セル更新
	//TODO : ドキュメントを使った処理に変更
	void Player::CellFront(){
		//ステージ取得
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		//TODO : ドキュメントを取得して、Playerを１つ進める

		/*Character::Cell Mycell;
		Mycell = GetCell();
		int nextrow = (Mycell.m_Row + 1) % 20;
		assert(Mycell.m_Row < 20);
		SetCell(nextrow, Mycell.m_Col);*/
	}
	void Player::CellRight(){
		/*Character::Cell Mycell;
		Mycell = GetCell();
		assert(Mycell.m_Col < 5);
		SetCell(Mycell.m_Row, Mycell.m_Col + 1);*/
	}
	void Player::CellLeft(){
		/*Character::Cell Mycell;
		Mycell = GetCell();
		assert(Mycell.m_Col > 0);
		SetCell(Mycell.m_Row, Mycell.m_Col - 1);*/
	}

	//-----------------------------
	//CloudSearchStateで使用するMotion
	//-----------------------------
	//下の雲を検出
	CloudName Player::Check_Under_Cloud(){
		//ステージ取得
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		//TODO : ドキュメントを取得して、現在いるプレイヤーのセルを取得
		//TODO : プレイヤーのセルと同じ位置をCloudMapから取得してくる
		return CloudName::White;
	}
	//下の雲の種類でステート切り替え
	void Player::Swich_State(const CloudName& name){
		switch (name)
		{
		case CloudName::None:
			GetStateMachine()->ChangeState(GameOverState::Instance());
			break;
		case CloudName::White:
			CreateMoveEffect();
			GetStateMachine()->ChangeState(WaitTimeState::Instance());
			break;
		case CloudName::Thunder:
			GetStateMachine()->ChangeState(ThunderState::Instance());
			break;
		default:
			assert(!"不正な値が入りました。CloudNameの値を確認してください");
			break;
		}
	}
	//白い雲だったらエフェクト出す
	void Player::CreateMoveEffect(){
		auto PtrPlayerMove_Effect = GetStage()->GetSharedGameObject<PlayerMove_Effect>(L"PlayerMove_Effect", false);
		if (PtrPlayerMove_Effect){
			PtrPlayerMove_Effect->InsertPlayerMove_Effect(GetComponent<Transform>()->GetPosition() + Vector3(0.0f, -0.1f, -0.1f));
		}
	}
	//メンバ変数の初期化
	void Player::Resetmethod(){
		m_SideMove = false;
	}

	//-----------------------------
	//GameOverStateで使用するMotion
	//-----------------------------
	//最低基準のラインを下げる
	void Player::DownBaseY(){
		auto GravityPtr = GetComponent<Gravity>();
		GravityPtr->SetBaseY(-2.0f);
	}
	//落ちたときのエフェクトとSE
	void Player::StartEffect_SE(){
		SoundPlay(L"WATER", 0.5f);
		//動いたときとエフェクト
		auto PtrCircle_Effect = GetStage()->GetSharedGameObject<Circle>(L"Circle", false);
		if (PtrCircle_Effect){
			PtrCircle_Effect->InsertCircle(GetComponent<Transform>()->GetPosition() + Vector3(0.0f, 0.0f, 0.0f));
		}
	}
	//落ちたときの水のエフェクト
	void Player::CreateWaterBlock_Sprite(){
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		GameStagePtr->CreateEffectBox(GetComponent<Transform>()->GetPosition());
		GameStagePtr->CreateGameOverSprite();
	}
	//ゲームオーバーに移行
	void Player::SceneMove_gameover(){
		PostEvent(3.5f, GetThis<Player>(), App::GetApp()->GetSceneBase(), L"ToGameOver");
	}

	//--------------------------------------------------------------------------------------
	//	class StopState : public ObjState<Player>;
	//	用途: 待つステート
	//--------------------------------------------------------------------------------------

	//ステートのインスタンス取得
	shared_ptr<StopState> StopState::Instance(){
		static shared_ptr<StopState> instance;
		if (!instance){
			instance = shared_ptr<StopState>(new StopState);
		}
		return instance;
	}
	void StopState::Enter(const shared_ptr<Player>& Obj){
	}
	void StopState::Execute(const shared_ptr<Player>& Obj){
	}
	//--------------------------------------------------------------------------------------
	//	class WaitTimeState : public ObjState<Player>;
	//	用途: 数秒待つステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<WaitTimeState> WaitTimeState::Instance(){
		static shared_ptr<WaitTimeState> instance;
		if (!instance){
			instance = shared_ptr<WaitTimeState>(new WaitTimeState);
		}
		return instance;
	}
	void WaitTimeState::Enter(const shared_ptr<Player>& Obj){
		//初期状態は前に進む
		Obj->ResetDirection();
	}
	void WaitTimeState::Execute(const shared_ptr<Player>& Obj){
		//スティック操作に修正
		Obj->Change_Direction(Obj->GetControllerStick());
		//待ち時間 1.0秒
		if (Obj->WaitTime(1.0f)){
			//	Obj->GetStateMachine()->ChangeState(MoveState::Instance());
		}
	}
	//--------------------------------------------------------------------------------------
	//	class MoveState : public ObjState<Player>;
	//	用途: 前に雲があり、前に進むState
	//--------------------------------------------------------------------------------------
	shared_ptr<MoveState> MoveState::Instance(){
		static shared_ptr<MoveState> instance;
		if (!instance){
			instance = shared_ptr<MoveState>(new MoveState);
		}
		return instance;
	}
	void MoveState::Enter(const shared_ptr<Player>& Obj){
		Obj->Jump();
		//セルの移動
		Obj->CellMoveMethod();
	}
	void MoveState::Execute(const shared_ptr<Player>& Obj){
		if (Obj->IsJump()){
			Obj->GetStateMachine()->ChangeState(CloudSearchState::Instance());
		}
	}

	//--------------------------------------------------------------------------------------
	//	class CloudSearchState : public ObjState<Player>;
	//	用途: 自身の下の雲を見るステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<CloudSearchState> CloudSearchState::Instance(){
		static shared_ptr<CloudSearchState> instance;
		if (!instance){
			instance = shared_ptr<CloudSearchState>(new CloudSearchState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void CloudSearchState::Enter(const shared_ptr<Player>& Obj){
		//自分の下に雲があるかの確認
		Obj->Swich_State(Obj->Check_Under_Cloud());
		//メンバ変数の初期化
		Obj->Resetmethod();
	}

	//--------------------------------------------------------------------------------------
	//	class ThunderState : public ObjState<Player>;
	//	用途: 痺れている時のステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<ThunderState> ThunderState::Instance(){
		static shared_ptr<ThunderState> instance;
		if (!instance){
			instance = shared_ptr<ThunderState>(new ThunderState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void ThunderState::Enter(const shared_ptr<Player>& Obj){
		//音の再生
		Obj->SoundPlay(L"THENDER", 0.5f);
	}

	void ThunderState::Execute(const shared_ptr<Player>& Obj){
		//痺れている時間
		if (Obj->WaitTime(1.5f)){
			//待ちステートに切り替え
			Obj->GetStateMachine()->ChangeState(WaitTimeState::Instance());
		}
	}
	void ThunderState::Exit(const shared_ptr<Player>& Obj){
		//びりびりの音を念のため消しておく
		Obj->SoundStop(L"THENDER");
	}

	//--------------------------------------------------------------------------------------
	//	class GameOverState : public ObjState<Player>;
	//	用途: 前に雲がないときに進んでしまったときのState
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<GameOverState> GameOverState::Instance(){
		static shared_ptr<GameOverState> instance;
		if (!instance){
			instance = shared_ptr<GameOverState>(new GameOverState);
		}
		return instance;
	}
	void GameOverState::Enter(const shared_ptr<Player>& Obj){
		//SetBaseYの値の変更　
		Obj->DownBaseY();
		//ダメだったときのSEとエフェクト
		Obj->CreateWaterBlock_Sprite();
		Obj->StartEffect_SE();
		Obj->SceneMove_gameover();
	}

	/**
	* @class Tutorial_Character
	* @brief チュートリアルで使うキャラクター
	* @author　sike yuya
	* @date	2016/05/26
	*/
	Tutorial_Character::Tutorial_Character(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr), m_JumpDirection(0.0f, 3.0f, 1.65f), m_Dir(Direction::Front), m_StickReset(true)
	{}

	//作成
	void Tutorial_Character::OnCreate(){
		auto PtrTrans = AddComponent<Transform>();
		PtrTrans->SetPosition(Vector3(2.0f, 0.5f, 2.0f));
		PtrTrans->SetRotation(0, 0, 0);
		PtrTrans->SetScale(0.6f, 0.3f, 0.6f);

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(1.0f, 1.0f, 1.0f),
			Vector3(0.0f, XM_PI, 0.0f),
			Vector3(0.0f, -0.5f, 0.0f)
			);

		//影をつける（シャドウマップを描画する）
		auto ShadowPtr = AddComponent<Shadowmap>();
		//影の形（メッシュ）を設定
		ShadowPtr->SetMeshResource(L"KUMO_MESH");
		ShadowPtr->SetMeshToTransformMatrix(SpanMat);

		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<PNTStaticModelDraw>();
		//描画するメッシュを設定
		PtrDraw->SetMeshResource(L"KUMO_MESH");
		PtrDraw->SetMeshToTransformMatrix(SpanMat);
		//透過処理
		SetAlphaActive(true);

		//アクションで操作
		AddComponent<Action>();
		//重力の設定
		auto PtrGravity = AddComponent<Gravity>();
		PtrGravity->SetBaseY(0.25f);

		//nsme : yoichiro
		// SEの再生
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Jump");

		auto PtrRotateTo = AddComponent<RotateTo>();
	}

	void Tutorial_Character::OnUpdate(){
		//向きを変える
		Change_Direction(GetControllerStick());
	}

	//name :: yoichiro
	void Tutorial_Character::SoundPlay(wstring soundname, float volume){
		auto pMultiSoundEffect = GetComponent<MultiSoundEffect>();
		pMultiSoundEffect->Start(soundname, 0, volume);
	}
	//name : yoichiro
	//サウンドのストップ
	void Tutorial_Character::SoundStop(wstring soundname){
		auto pMultiSoundEffect = GetComponent<MultiSoundEffect>();
		pMultiSoundEffect->Stop(soundname);
	}

	//ジャンプ処理
	void Tutorial_Character::Jump(){
		auto PtrGravity = GetComponent<Gravity>();
		PtrGravity->StartJump(m_JumpDirection);
		m_Dir = Direction::Front;
		m_JumpDirection = Vector3(0.0f, 3.0f, 1.65f);
		Change_Direction_D(m_Dir);

		//name	:	youitirou
		//ジャンプしたときの音を鳴らす
		SoundPlay(L"Jump", 0.6f);
	}
	//ジャンプが終わったか
	bool Tutorial_Character::IsJump(){
		auto PtrGravity = GetComponent<Gravity>();
		if (PtrGravity->GetGravityVelocity().Length() <= 0){
			//name : yoichiro
			//ジャンプしたときの音をならないようにする
			SoundStop(L"Jump");
			return true;
		}
		return false;
	}
	//プレイヤーの向きの変更
	void Tutorial_Character::Change_Direction_D(Direction dir){
		auto PtrRotate = GetComponent<RotateTo>();
		PtrRotate->SetParams(0.35f, Return_Direction(dir));
		PtrRotate->Run();
	}
	//向きを返す
	Vector3 Tutorial_Character::Return_Direction(Direction dir){
		switch (dir)
		{
		case Direction::Front:
			return Vector3(0.0f, 0.0f / 180.0f * XM_PI, 0.0f);
			break;
		case Direction::Right:
			return Vector3(0.0f, 90.0f / 180.0f * XM_PI, 0.0f);
			break;
		case Direction::Left:
			return Vector3(0.0f, 270.0f / 180.0f * XM_PI, 0.0f);
			break;
		default:
			assert(!"不正な値が入りました。引数のDirectionの値を確認してください。");
			return Vector3(0.0f, 0.0f, 0.0f);
			break;
		}
	}

	//右スティックによる向き変更
	size_t Tutorial_Character::GetControllerStick(){
		//コントローラーの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		//スティックがリセットされているか
		if (m_StickReset){
			if (CntlVec[0].bConnected){
				if (abs(CntlVec[0].fThumbRY) >= 0.5f){
					//Y方向優先
					//上下は少し甘くする
					if (CntlVec[0].fThumbRY > 0.0f){
						m_StickReset = false;
						return 3;
					}
					else{
						m_StickReset = false;
						return 4;
					}
				}
				else if (abs(CntlVec[0].fThumbRX) >= 0.7f){
					//X方向
					if (CntlVec[0].fThumbRX < 0.0f){
						m_StickReset = false;
						return 1;
					}
					else{
						m_StickReset = false;
						return 2;
					}
				}
			}
		}
		else{
			//リセットされてない
			if (CntlVec[0].bConnected){
				if (CntlVec[0].fThumbRX == 0.0f && CntlVec[0].fThumbRY == 0.0f){
					m_StickReset = true;
				}
			}
		}
		return 0;
	}
	//スティック入力による向き変え
	void Tutorial_Character::Change_Direction(const size_t& num){
		switch (num)
		{
		case 1:
			//左入力
			Change_Direction_D(Direction::Left);
			m_JumpDirection = Vector3(-1.65f, 3.0f, 0.0f);
			break;
		case 2:
			//右入力
			Change_Direction_D(Direction::Right);
			m_JumpDirection = Vector3(1.65f, 3.0f, 0.0f);
			break;
		case 3:
			//上入力
			Change_Direction_D(Direction::Front);
			m_JumpDirection = Vector3(0.0f, 3.0f, 1.65f);
			break;
		case 4:
			//下入力
			break;
		default:
			//入力なし
			break;
		}
	}

	//位置リセット
	void Tutorial_Character::ResetPos(){
		GetComponent<Transform>()->SetPosition(Vector3(2.0f, 0.5f, 2.0f));
	}
}
//endof  basedx11