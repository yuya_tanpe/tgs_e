#include "stdafx.h"
#include "Project.h"

namespace basedx11
{
	// -----------------------------
	// 音源提供の表示に使用
	// -----------------------------
	// リソースの作成
	void License::CreateResourse()
	{
		wstring strTexture = App::GetApp()->m_wstrRelativeDataPath + L"License.png";
		App::GetApp()->RegisterTexture(L"LICENSE_TX", strTexture);
	}

	// ビューの作成
	void License::CreateView()
	{
		//最初にデフォルトのレンダリングターゲット類を作成する
		//パラメータの2048.0fはシャドウマップのサイズ。大きいほど影が細かくなる（4096.0fなど）
		//影を細かくするとメモリを大量に消費するので注意！
		CreateDefaultRenderTargets(2048.0f);

		//影のビューサイズの設定。この値を小さくすると影が表示される範囲が小さくなる。
		//値が小さいほうが影は細かくなる
		//スタティック変数なので一度設定したらその値はステージを超えて保持される。
		Shadowmap::SetViewSize(32.0f);

		//マルチビューコンポーネントの取得
		auto PtrMultiView = GetComponent<MultiView>();

		//マルチビューにビューの追加
		auto PtrView = PtrMultiView->AddView();

		//ビューの矩形を設定（ゲームサイズ全体）
		Rect2D<float> rect(0, 0, (float)App::GetApp()->GetGameWidth(), (float)App::GetApp()->GetGameHeight());

		//背景色(RGB)
		Color4 ViewBkColor(0.0f, 0.125f, 0.3f, 1.0f);

		//最初のビューにパラメータの設定
		PtrView->ResetParamaters<Camera, MultiLight>(rect, ViewBkColor, 1, 0.0f, 1.0f);

		//最初のビューのビューのライトの設定
		auto PtrLight = PtrView->GetMultiLight()->GetLight(0);
		PtrLight->SetPositionToDirectional(-0.25f, 1.0f, -0.25f);

		//ビューのカメラの設定
		auto PtrCamera = PtrView->GetCamera();
		PtrCamera->SetEye(Vector3(0.0f, 0.0f, -10.0f));
		PtrCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
	}

	// 背景（画像）の作成
	void License::CreateBG()
	{
		auto Ptr = AddGameObject<GameObject>();
		auto PtrTransMatrix = Ptr->GetComponent<TransformMatrix>();
		Quaternion Qt;
		Qt.RotationRollPitchYawFromVector(Vector3(0, 0, 0));
		Matrix4X4 WorldMat;
		WorldMat.DefTransformation(
			Vector3(14.75f, 8.3f, 1.0f),
			Qt,
			Vector3(0.0f, 0.0f, 0.0f)
			);
		PtrTransMatrix->SetWorldMatrix(WorldMat);

		//描画コンポーネントの追加
		auto DrawComp = Ptr->AddComponent<PNTStaticDraw>();

		//描画コンポーネントに形状（メッシュ）を設定
		DrawComp->SetMeshResource(L"DEFAULT_SQUARE");

		//描画コンポーネントテクスチャの設定
		DrawComp->SetTextureResource(L"LICENSE_TX");

		//自分に影が映りこむようにする
		DrawComp->SetOwnShadowActive(false);
	}

	// Fadeの作成
	void License::CreateFade()
	{
		auto FadePtr = AddGameObject<Fade>(true, false);
	}

	//初期化
	void License::OnCreate()
	{
		try{
			// リソースの作成
			CreateResourse();

			//ビュー類を作成する
			CreateView();

			// 背景の作成
			CreateBG();

			// Fadeの作成
			CreateFade();
			//シーン移動
			PostEvent(1.0f, GetThis<License>(), App::GetApp()->GetSceneBase(), L"ToTitle");
		}
		catch (...){
			throw;
		}
	}

	// 更新
	void License::OnUpdate()
	{
		
	}


	// リソースの作成
	void TitleScene::CreateResourses()
	{
		// テクスチャ
		wstring strTexture = App::GetApp()->m_wstrRelativeDataPath + L"TitleUI\\Start.png";
		App::GetApp()->RegisterTexture(L"TITLE_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"TitleUI\\Sky.png";
		App::GetApp()->RegisterTexture(L"SKY_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"TitleUI\\TitleButton.png";
		App::GetApp()->RegisterTexture(L"TITLEBUTTON_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"TitleUI\\sheep.png";
		App::GetApp()->RegisterTexture(L"SHEEP_TX", strTexture);

		// サウンド
		wstring strSound = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\TitleSE.wav";
		App::GetApp()->RegisterWav(L"START", strSound);
		//BGMの作成
		//TODO : 容量が大きいので小さくするなどすること	元の音がでかいから小さくする
		wstring strBGM = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\loopTitle.wav";
		App::GetApp()->RegisterWav(L"BGM", strBGM);

		// SEの再生
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"START");
		pMultiSoundEffect->AddAudioResource(L"BGM");

	}

	// ビューの作成
	void TitleScene::CreateViews()
	{
		//最初にデフォルトのレンダリングターゲット類を作成する
		//パラメータの2048.0fはシャドウマップのサイズ。大きいほど影が細かくなる（4096.0fなど）
		//影を細かくするとメモリを大量に消費するので注意！
		CreateDefaultRenderTargets(2048.0f);

		//影のビューサイズの設定。この値を小さくすると影が表示される範囲が小さくなる。
		//値が小さいほうが影は細かくなる
		//スタティック変数なので一度設定したらその値はステージを超えて保持される。
		Shadowmap::SetViewSize(32.0f);

		//マルチビューコンポーネントの取得
		auto PtrMultiView = GetComponent<MultiView>();

		//マルチビューにビューの追加
		auto PtrView = PtrMultiView->AddView();

		//ビューの矩形を設定（ゲームサイズ全体）
		Rect2D<float> rect(0, 0, (float)App::GetApp()->GetGameWidth(), (float)App::GetApp()->GetGameHeight());

		//背景色(RGB)
		Color4 ViewBkColor(0.0f, 0.125f, 0.3f, 1.0f);

		//最初のビューにパラメータの設定
		PtrView->ResetParamaters<Camera, MultiLight>(rect, ViewBkColor, 1, 0.0f, 1.0f);

		//最初のビューのビューのライトの設定
		auto PtrLight = PtrView->GetMultiLight()->GetLight(0);
		PtrLight->SetPositionToDirectional(-0.25f, 1.0f, -0.25f);

		//ビューのカメラの設定
		auto PtrCamera = PtrView->GetCamera();
		PtrCamera->SetEye(Vector3(0.0f, 0.0f, -10.0f));
		PtrCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
	}

	// タイトルのテクスチャ作成
	void TitleScene::CreateTitle()
	{
		auto Ptr = AddGameObject<GameObject>();
		auto PtrTransMatrix = Ptr->GetComponent<TransformMatrix>();
		Quaternion Qt;
		Qt.RotationRollPitchYawFromVector(Vector3(0, 0, 0));
		Matrix4X4 WorldMat;
		WorldMat.DefTransformation(
			Vector3(8.0f, 4.0f, 1.0f),
			Qt,
			Vector3(0.0f, 1.0f, 0.0f)
			);
		PtrTransMatrix->SetWorldMatrix(WorldMat);

		//描画コンポーネントの追加
		auto DrawComp = Ptr->AddComponent<PNTStaticDraw>();
		//描画コンポーネントに形状（メッシュ）を設定
		DrawComp->SetMeshResource(L"DEFAULT_SQUARE");
		//描画コンポーネントテクスチャの設定
		DrawComp->SetTextureResource(L"TITLE_TX");
		//自分に影が映りこむようにする
		DrawComp->SetOwnShadowActive(false);
		Ptr->SetAlphaActive(true);
		SetDrawLayer(2);
	}

	void TitleScene::CreateBG()
	{
		auto Ptr = AddGameObject<GameObject>();
		auto PtrTransMatrix = Ptr->GetComponent<TransformMatrix>();
		Quaternion Qt;
		Qt.RotationRollPitchYawFromVector(Vector3(0, 0, 0));
		Matrix4X4 WorldMat;
		WorldMat.DefTransformation(
			Vector3(15.0f, 13.0f, 1.0f),
			Qt,
			Vector3(0.0f, 2.0f, 0.0f)
			);
		PtrTransMatrix->SetWorldMatrix(WorldMat);

		//描画コンポーネントの追加
		auto DrawComp = Ptr->AddComponent<PNTStaticDraw>();
		//描画コンポーネントに形状（メッシュ）を設定
		DrawComp->SetMeshResource(L"DEFAULT_SQUARE");
		//描画コンポーネントテクスチャの設定
		DrawComp->SetTextureResource(L"SKY_TX");
		//自分に影が映りこむようにする
		DrawComp->SetOwnShadowActive(false);
		Ptr->SetAlphaActive(true);
		SetDrawLayer(1);
	}

	// 羊のスプライト作成
	void TitleScene::CreateSheepSprite()
	{
		AddGameObject<Title_SheepSprite>(Vector3(-650.0f, 400.0f, 0.0f));
		SetDrawLayer(2);
	}

	// AボタンUIの作成
	void TitleScene::CreateAbuttonUI()
	{
		AddGameObject<TitleUI>(Vector3(0.0f, -370.0f, 0.0f));
	}

	//音のストップ
	void TitleScene::SoundStop(wstring soundname){
		auto pSound = GetComponent<MultiSoundEffect>();
		pSound->Stop(soundname);
	}

	//初期化
	void TitleScene::OnCreate()
	{
		try{
			// リソースの作成
			CreateResourses();

			//ビュー類を作成する
			CreateViews();

			// 背景の作成
			CreateBG();

			// タイトルの作成
			CreateTitle();

			// AボタンUIの作成
			CreateAbuttonUI();

			// 羊のスプライト作成
			CreateSheepSprite();

			//BGMの再生
			GetComponent<MultiSoundEffect>()->Start(L"BGM", 1, 0.3f);


		}
		catch (...){
			throw;
		}
	}

	//操作
	void TitleScene::OnUpdate()
	{
		// コントローラーの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		// Aボタンが押されたらステージセレクトシーンへ移動
		if (CntlVec[0].bConnected){
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A && !IsPushButton){
				//二度と押さないようにする
				IsPushButton = true;
				// Fade
				AddGameObject<Fade>(false, true);
				//シーン移動
				PostEvent(1.5f, GetThis<TitleScene>(), App::GetApp()->GetSceneBase(), L"ToStageSelect");
				//スタート時のサウンド再生
				auto pMultiSoundEffect = GetComponent<MultiSoundEffect>();
				pMultiSoundEffect->Start(L"START", 0, 0.5f);
			}
		}
	}
}
//end basedx11