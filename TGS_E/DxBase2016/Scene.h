#pragma once
#include "stdafx.h"

namespace basedx11
{
	//--------------------------------------------------------------------------------------
	//	class Scene : public SceneBase;
	//	用途: シーンクラス
	//--------------------------------------------------------------------------------------
	class Scene : public SceneBase
	{
	private:
		//画像リソースの作成
		void CreateResorce();
		//音リソースの作成
		void CreateSoundResorce();

		// ---------- member ----------.
	
		//コインの枚数
		int m_CoinNum;
		//進んだスコア
		int m_Score;

	public:
		//構築と破棄
		Scene() : m_Score(0), m_CoinNum(0)
		{}
		~Scene(){}
		//操作
		virtual void OnCreate()override;
		virtual void OnEvent(const shared_ptr<Event>& event)override;

		//進んだスコアの取得
		const int GetScore() const{
			return m_Score;
		}

		//取得したコインの取得
		const int GetCoin() const{
			return m_CoinNum;
		}

		void SetScore(int score){
			m_Score = score;
		}

		void SetCoin(int coin){
			m_CoinNum = coin;
		}
	};
}
//end basedx11
