#include "stdafx.h"
#include "Project.h"

namespace basedx11
{
	//--------------------------------------------------------------------------------------
	//	class Scene : public SceneBase;
	//	用途: シーンクラス
	//--------------------------------------------------------------------------------------
	//リソース作成
	void Scene::CreateResorce(){
		//ワーク用配列
		vector< vector<wstring> > wsTextureTable;

		struct NameTable
		{
			wstring texture;
			wstring resource;
			NameTable(const wstring& str1, const wstring& str2) :
				texture(str1),
				resource(str2){}
		};
		//ネームテーブルの作成
		//TODO : テクスチャの読み込み等の処理の完成
		vector<NameTable> nameTable = {
			//白い雲のテクスチャ
			{ L"ObjectTexture\\white.png", L"WHITE_TX" },
			//ユーザーが動かすUIのテクスチャ
			{ L"ObjectTexture\\purple.png", L"PURPLE_TX" },
			//テスト　プレイヤーのテクスチャ
			{ L"ObjectTexture\\red.png", L"RED_TX" },
			{ L"ObjectTexture\\Blue.png", L"BLUE_TX" },
			//コインの獲得数　画像
			{ L"GameStageUI\\Coin2.png", L"COIN" },
			//進んだ距離　画像
			{ L"ObjectTexture\\Circle.png", L"CIRCLE_TX" },
			{ L"ObjectTexture\\GameOver.png", L"GAMEOVER_TX" },
			//ゲームスタート時のカウント画像
			{ L"ObjectTexture\\One.png", L"ONE_TX" },
			{ L"ObjectTexture\\Two.png", L"TWO_TX" },
			{ L"ObjectTexture\\Three.png", L"THREE_TX" },
			{ L"ObjectTexture\\Start.png", L"START_TX" },
			//リザルト画面で使用するナンバー画像
			{ L"GameStageUI\\numberresult .png", L"NUMBERRESULT_TX" },
			{ L"GameStageUI\\number01.png", L"NUMBER01_TX" },
		};

		for (auto& nt : nameTable){
			wsTextureTable.push_back({ nt.texture, nt.resource });
		}

		//	: テクスチャを登録.
		for (auto& wsTexture : wsTextureTable) {
			const wstring wsTexturePath = App::GetApp()->m_wstrRelativeDataPath + wsTexture.at(0);
			const wstring wsKeyName = wsTexture.at(1);
			App::GetApp()->RegisterTexture(wsKeyName, wsTexturePath);
		}
	}
	//音リソースの作成
	void Scene::CreateSoundResorce(){
		// チュートリアル1で使用するAボタンを押したときに使用
		wstring StrTutorial = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\Tutorial_ButtonSE.wav";
		App::GetApp()->RegisterWav(L"TUTORIAL_BUTTON_SE", StrTutorial);
		//　チュートリアル2で使用するスタート押したときに使用
		wstring StrTutorial1 = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\Tutorial_ButtonSE1.wav";
		App::GetApp()->RegisterWav(L"TUTORIAL_BUTTON1_SE", StrTutorial1);
		wstring GameStartWav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\GameStart.wav";
		App::GetApp()->RegisterWav(L"GAMESTART", GameStartWav);
		wstring JumpWav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\jump.wav";
		App::GetApp()->RegisterWav(L"Jump", JumpWav);
		wstring BGM = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\TutorialBGM.wav";
		App::GetApp()->RegisterWav(L"TUTORIALBGM", BGM);
	}

	//リソース作成
	void Scene::OnCreate(){
		try{
			//マウスカーソルの非表示
			ShowCursor(false);
			//画像リソースの作成
			CreateResorce();
			//音素材のリソース作成
			CreateSoundResorce();
			//最初のアクティブステージの設定
			ResetActiveStage<License>();
		}
		catch (...){
			throw;
		}
	}

	void Scene::OnEvent(const shared_ptr<Event>& event)
	{
		if (event->m_MsgStr == L"ToStageSelect")
		{
			ResetActiveStage<TutorialScene>();
		}

		if (event->m_MsgStr == L"ToTitle")
		{
			ResetActiveStage<TitleScene>();
		}

		if (event->m_MsgStr == L"ToGame")
		{
			ResetActiveStage<GameStage>();
		}

		if (event->m_MsgStr == L"ToGameOver")
		{
			ResetActiveStage<GameOverScene>();
		}

		if (event->m_MsgStr == L"ToGameClear")
		{
			ResetActiveStage<GameClearScene>();
		}
	}

}
//end basedx11