#pragma once
#include "stdafx.h"

namespace basedx11{
	///**
	//* @class Blow_Enemy
	//* @brief オブジェクトを吹き飛ばす敵　（プレイヤーや敵など）
	//* @author　sike yuya
	//* @date	2016/06/04
	//*/

	//class Blow_Enemy : public GameObject{
	//private:
	//	// ---------- member ----------.

	//	//吹き飛ばす向き
	//	Direction m_Direction;
	//	//初期位置
	//	Vector3 m_Pos;
	//	//セル座標
	//	Enemy::Cell m_Cell;
	//	//前のターンいたセル座標
	//	Enemy::Cell m_BeforeCell;
	//	//敵の名前
	//	CharacterName m_EnemyName;
	//	//ステートマシーン
	//	shared_ptr< StateMachine<Blow_Enemy> >  m_StateMachine;

	//	// ---------- Method -----------.
	//
	//	//自分の名前の登録
	//	void SetEnemyName(const CharacterName& name);

	//	//自分の作成された場所を配列に登録
	//	void SetName_CharacterVec();
	//
	//	//自身が消えたら配列から削除
	//	void Erase_CharacterVec(const Enemy::Cell& cell);
	//
	//public:
	//	Blow_Enemy(const shared_ptr<Stage>& StagePtr, const Vector3& i_start_pos, Enemy::Cell& i_cell);
	//	virtual ~Blow_Enemy(){}

	//	virtual void OnCreate() override;
	//	virtual void OnUpdate() override;

	//	// ---------- Accessor ----------.

	//	//現在のステートを返す
	//	shared_ptr< StateMachine<Blow_Enemy> > GetStateMachine() const{
	//		return m_StateMachine;
	//	}

	//	//-----------SearchStateで使用するMotion-----------
	//	//上下左右のキャラクター配列を検索する
	//	void Search();
	//	//下の雲のセル座標を取得し、自身に設定する
	//	void GetCloudCell_SetMyCell();

	//	//-----------BlowStateで使用するMotion-----------
	//	//弾を作成する
	//	void CreateBlow_Ball();
	//	//Directionによって、弾の出現させる位置を決める
	//	Vector3 ReturnVec3_Direction();
	//	//自身をDirectionの反対側に飛ばす
	//	void StartBlowAction();
	//	//Directionを元に反対に自分を飛ばすVector3を返す
	//	Vector3 ReturnBlowVec();
	//};

	////--------------------------------------------------------------------------------------
	////	class SearchState : public ObjState<Blow_Enemy>;
	////	用途: 探索ステート
	////--------------------------------------------------------------------------------------
	//class SearchState : public ObjState<Blow_Enemy>
	//{
	//	SearchState(){}
	//public:
	//	//ステートのインスタンス取得
	//	static shared_ptr<SearchState> Instance();
	//	//ステートに入ったときに呼ばれる関数
	//	virtual void Enter(const shared_ptr<Blow_Enemy>& Obj)override;
	//	//ステート実行中に毎ターン呼ばれる関数
	//	virtual void Execute(const shared_ptr<Blow_Enemy>& Obj)override;
	//	//ステートにから抜けるときに呼ばれる関数
	//	virtual void Exit(const shared_ptr<Blow_Enemy>& Obj)override;
	//};

	////--------------------------------------------------------------------------------------
	////	class BlowState : public ObjState<Blow_Enemy>;
	////	用途: 吹き飛ばすステート
	////--------------------------------------------------------------------------------------
	//class BlowState : public ObjState<Blow_Enemy>
	//{
	//	BlowState(){}
	//public:
	//	//ステートのインスタンス取得
	//	static shared_ptr<BlowState> Instance();
	//	//ステートに入ったときに呼ばれる関数
	//	virtual void Enter(const shared_ptr<Blow_Enemy>& Obj)override;
	//	//ステート実行中に毎ターン呼ばれる関数
	//	virtual void Execute(const shared_ptr<Blow_Enemy>& Obj)override;
	//	//ステートにから抜けるときに呼ばれる関数
	//	virtual void Exit(const shared_ptr<Blow_Enemy>& Obj)override;
	//};

	///**
	//* @class Blow_Ball
	//* @brief オブジェクトを吹き飛ばす弾　（プレイヤーや敵など）
	//* @author　sike yuya
	//* @date	2016/06/06
	//*/

	//class Blow_Ball : public GameObject{
	//private:
	//	//吹き飛ばす向き
	//	Direction m_Direction;
	//	Vector3 m_Pos;
	//	float m_DestroyTime;

	//	//進む向きを決める
	//	void MoveDirection(const Direction& dir);

	//	//エフェクトと音の再生
	//	void StartEffect_Sound();

	//public:
	//	Blow_Ball(const shared_ptr<Stage>& Stageptr, const Vector3& pos, const Direction& blowdirection);
	//	virtual ~Blow_Ball(){}

	//	virtual void OnCreate() override;
	//	virtual void OnUpdate() override;
	//	virtual void OnCollision(const shared_ptr<GameObject>& other) override;

	//	//進行している向きを取得する
	//	Direction GetMoveDirection(){
	//		return m_Direction;
	//	}

	//};
}
//endof  basedx11