#pragma once

#include "stdafx.h"

namespace basedx11{
	/**
	* @class Player
	* @brief プレイヤー
	* @author　sike yuya
	* @date	2016/04/19 atnew 6/14
	*/
	class Player : public GameObject {
		// ---------- member ----------.

		//吹き飛ばされる時の向き
		Direction m_BlowDirection;
		//進行するときの向き
		Direction m_MoveDirection;
		//横に動く
		bool m_SideMove;
		//待ち時間
		float m_waittime;
		//スティック操作がリセットされたか
		bool m_StickReset;

		shared_ptr< StateMachine<Player> >  m_StateMachine;	//ステートマシーン

		// ---------- Method -----------.

		//デバッグ表示
		void DebugOutput();
		//ゲームステージ上にあるスコアを加算する
		void ScoreUp_GameStage();
		//向きを変更する値の取得
		Vector3 Return_Direction(Direction dir);
		//プレイヤーの向きの変更
		void Change_Direction(Direction dir);
		//DirectionからVector3を抽出
		Vector3 DirectionToVec3(Direction dir);
		//一回だけSEを鳴らす
		//TODO : これつかわないような設計にする
		void OneShotSE(wstring soundname, float volume);
	public:
		Player::Player(const shared_ptr<Stage>& StagePtr);
		virtual ~Player(){
			//BGMのストップ
			SoundStop(L"GAMEBGM");
		}

		virtual void OnCreate() override;
		virtual void OnUpdate() override;
		virtual void OnLastUpdate() override;
		virtual void OnEvent(const shared_ptr<Event>& event)override;

		// ---------- Accessor ----------.

		//現在のステートを返す
		shared_ptr< StateMachine<Player> > GetStateMachine() const{
			return m_StateMachine;
		}

		//プレイヤーが横に動くかを返す
		bool GetSideMove(){
			return m_SideMove;
		}

		// ---------- Motion ----------.
		//TODO : メソッドの整理
		//前の雲の位置を取得して、アクションを使って移動する
		void Move_Front();
		//自身の位置を更新
		void CellFront();
		void CellRight();
		void CellLeft();
		//音のストップ
		void SoundStop(wstring soundname);
		//音の再生
		void SoundPlay(wstring soundname, float volume);

		//-----------------------------
		//WaitTimeStateで使用するMotion
		//-----------------------------
		//待ち時間
		bool WaitTime(const float& time);

		//スティック入力による向き変え
		void Change_Direction(const size_t& num);
		//右スティックによる向き変更
		size_t GetControllerStick();
		//次に行く道を決定する（準備段階）
		void NextFrontMove();
		void NextRightMove();
		void NextLeftMove();
		//DirectionをFrontに戻す
		void ResetDirection();
		//左右の雲の行動制限
		bool Limit_SideLine(const Direction& dir);

		//-----------------------------
		//MoveStateで使用するMotion
		//-----------------------------
		//ジャンプ処理
		void Jump();
		//ジャンプが終わったか
		bool IsJump();
		//セルの移動
		void CellMoveMethod();

		//-----------------------------
		//CloudSearchStateで使用するMotion
		//-----------------------------
		//下の雲を取得
		CloudName Check_Under_Cloud();
		//下の雲の種類でステート切り替え
		void Swich_State(const CloudName& cloud);
		//白い雲だったらエフェクト出す
		void CreateMoveEffect();
		//メンバ変数の初期化
		void Resetmethod();
		//-----------------------------
		//BlowPlayerStateで使用するMotion
		//-----------------------------
		//吹き飛ばす方向を設定する
		void SetBlowDirection(const Direction& dir);
		//吹き飛ぶ処理
		void Blow();
		//吹き飛んだ時のセルの移動
		void CellBlowMethod();
		//-----------------------------
		//GameOverStateで使用するMotion
		//-----------------------------
		//最低基準のラインを下げる
		void DownBaseY();
		//落ちたときのエフェクトとSE
		void StartEffect_SE();
		//落ちたときの水のエフェクト
		void CreateWaterBlock_Sprite();
		//ゲームオーバーに移行
		void SceneMove_gameover();
	};

	//--------------------------------------------------------------------------------------
	//	class StopState : public ObjState<Player>;
	//	用途: 待つステート
	//--------------------------------------------------------------------------------------
	class StopState : public ObjState<Player>
	{
		StopState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<StopState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Player>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Player>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Player>& Obj){}
	};

	//--------------------------------------------------------------------------------------
	//	class WaitTimeState : public ObjState<Player>;
	//	用途: 数秒待つステート
	//--------------------------------------------------------------------------------------
	class WaitTimeState : public ObjState<Player>
	{
		WaitTimeState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<WaitTimeState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Player>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Player>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Player>& Obj){}
	};

	//--------------------------------------------------------------------------------------
	//	class CloudSearchState : public ObjState<Player>;
	//	用途: 自身の回りに雲があるか探すステート
	//--------------------------------------------------------------------------------------
	class CloudSearchState : public ObjState<Player>
	{
		CloudSearchState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<CloudSearchState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Player>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Player>& Obj){}
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Player>& Obj){}
	};

	//--------------------------------------------------------------------------------------
	//	class MoveState : public ObjState<Player>;
	//	用途: 前に雲があり、前に進むState
	//--------------------------------------------------------------------------------------
	class MoveState : public ObjState<Player>
	{
		MoveState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<MoveState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Player>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Player>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Player>& Obj){}
	};

	//--------------------------------------------------------------------------------------
	//	class ThunderState : public ObjState<Player>;
	//	用途: 痺れている時のステート
	//--------------------------------------------------------------------------------------
	class ThunderState : public ObjState<Player>
	{
		ThunderState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<ThunderState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Player>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Player>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Player>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class GameOverState : public ObjState<Player>;
	//	用途: 前に雲がないときに進んでしまったときのState
	//--------------------------------------------------------------------------------------
	class GameOverState : public ObjState<Player>
	{
		GameOverState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<GameOverState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Player>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Player>& Obj){}
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Player>& Obj){}
	};

	/**
	* @class Tutorial_Character
	* @brief チュートリアルで使うキャラクター
	* @author　sike yuya
	* @date	2016/05/26
	*/

	class Tutorial_Character : public GameObject
	{
		Vector3 m_JumpDirection;
		Direction m_Dir;
		bool m_StickReset;
	public:
		Tutorial_Character::Tutorial_Character(const shared_ptr<Stage>& StagePtr);
		virtual ~Tutorial_Character(){}

		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		//ジャンプ
		void Jump();
		//ジャンプし終わったか
		bool IsJump();

		//音のストップ
		void SoundStop(wstring soundname);
		//音の再生
		void SoundPlay(wstring soundname, float volume);

		//向き変更
		void Change_Direction_D(Direction dir);
		Vector3	Return_Direction(Direction dir);

		//右スティックによる向き変更
		size_t GetControllerStick();
		//スティック入力による向き変え
		void Change_Direction(const size_t& num);

		//位置リセット
		void ResetPos();
	};
}
//endof  basedx11
