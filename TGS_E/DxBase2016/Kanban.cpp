#include "stdafx.h"
#include "Project.h"

namespace basedx11{
	/**
	* @class Kanban
	* @brief 看板
	* @author　sike yuya
	* @date	 6/14
	*/

	Kanban::Kanban(const shared_ptr<Stage>& Stageptr) :
		GameObject(Stageptr)
	{}

	void Kanban::OnCreate(){
		//自分は描画しない
		SetDrawActive(false);
		GetStage()->SetSharedGameObject(L"Kanban", GetThis<Kanban>());

		//Transformの設定
		auto PtrTrans = AddComponent<Transform>();
		PtrTrans->SetPosition(0, 0, 0);
		PtrTrans->SetRotation(0.0f, 0.0f, 0.0f);
		PtrTrans->SetScale(1.0f, 0.5f, 0.5f);

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(1.0f, 1.0f, 1.0f),
			Vector3(0.0f, XM_PI * 2, 0.0f),
			Vector3(0.0f, 0.0f, 0.0f)
			);

		//描画コンポーネントの設定	雲専用のShader使用
		//看板用のシェーダー書いたほうがいいこぴぺでできるもの
		auto PtrDraw = AddComponent<PNTStaticModelCloudDraw>();
		//描画するメッシュを設定
		PtrDraw->SetMeshResource(L"RAN_MESH");
		PtrDraw->SetMeshToTransformMatrix(SpanMat);
		PtrDraw->SetOwnShadowActive(false);

		//透過処理
		SetAlphaActive(true);
	}
}
//endof  basedx11