#pragma once

#include "stdafx.h"

namespace basedx11{
	//--------------------------------------------------------------------------------------
	//class CoinEffect : public MultiParticle;
	//用途: コインを取った時のエフェクト
	//--------------------------------------------------------------------------------------
	class CoinEffect : public MultiParticle{
	public:
		//構築と破棄
		CoinEffect(shared_ptr<Stage>& StagePtr);
		virtual ~CoinEffect();
		//--------------------------------------------------------------------------------------
		//	void InsertCoinEffect(
		//	const Vector3& Pos	//放出位置（エミッター位置）
		//	);
		//用途: コインを取った時のエフェクト
		//戻り値: なし。
		//--------------------------------------------------------------------------------------
		void InsertCoinEffect(const Vector3& Pos);

		virtual void OnUpdate()override;
	};

	//--------------------------------------------------------------------------------------
	//class NonMove_HitEffect : public MultiParticle;
	//用途: 複数の煙クラス
	//--------------------------------------------------------------------------------------
	class NonMove_HitEffect : public MultiParticle{
	public:
		//構築と破棄
		NonMove_HitEffect(shared_ptr<Stage>& StagePtr);
		virtual ~NonMove_HitEffect();
		
		void InsertNonMoveEffect(const Vector3& Pos);

		virtual void OnUpdate()override;
	};

}
//endof  basedx11