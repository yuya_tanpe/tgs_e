#include "stdafx.h"
#include "Project.h"

namespace basedx11{
	////--------------------------------------------------------------------------------------
	////	class GameDocument : public GameObject;
	////	用途: ドキュメントクラス
	////--------------------------------------------------------------------------------------
	//初期化
	void GameDocument::OnCreate(){
		//文字列をつける
		auto PtrString = AddComponent<StringSprite>();
		PtrString->SetText(L"");
		PtrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));

		SetDrawActive(false);
		GetStage()->SetSharedGameObject(L"GameDocument", GetThis<GameDocument>());
		//簡単なパターンの作成
		Create_PatternList();
	}
	//更新
	void GameDocument::OnUpdate(){
		//流れを止めるときは、IsmapMoveをfalseにする
		if (m_IsMapMove){
			AllDocumentUpdate();
			
		
			//コイン
			IsHitCoin();
			//敵との接触
			IsNonEnemy();

		}
		//横に動くときにタイマースタート
		SideMove_StartTimer(GetStickRLMove());
		
	}
	//デバッグの表示
	void GameDocument::OnLastUpdate(){
		wstring WstrLineNum{ L"今" };
		WstrLineNum += Util::IntToWStr(LineNum) + L"行目の生成 ";
		WstrLineNum += L"\n";

		wstring WstrPatternNum{ L"今" };
		WstrPatternNum += Util::IntToWStr(PatternNum) + L"パターン目の生成 ";
		WstrPatternNum += L"\n";

		wstring WstrPattern{ L"生成パターンの番号" };
		WstrPattern += L"\n" + Util::IntToWStr(EasyList[0]) + L"\n";
		WstrPattern += Util::IntToWStr(EasyList[1]) + L"\n";
		WstrPattern += Util::IntToWStr(EasyList[2]) + L"\n";
		WstrPattern += Util::IntToWStr(EasyList[3]) + L"\n";

		wstring WstrIsMove{ L"現在ドキュメントが動いているか : " };
		if (m_IsMapMove){
			WstrIsMove += L"動いています";
		}
		else{
			WstrIsMove += L"止まっています";
		}

		wstring str = WstrLineNum + WstrPatternNum + WstrPattern + WstrIsMove;

		//文字列をつける
		auto PtrString = GetComponent<StringSprite>();
		PtrString->SetStartPosition(Point2D<float>(10.0f, 400.0f));
		PtrString->SetFontColor(Color4(1.0f, 0.0f, 0.0f, 1.0f));
		//PtrString->SetText(str);
	}

	//ドキュメントの更新
	void GameDocument::AllDocumentUpdate(){
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		auto IsPlayerUpdate = GetStage()->GetSharedGameObject<Sheep>(L"Sheep")->GetUpdateSheep();
		//合計時間の計測
		m_TotalTime += ElapsedTime;
		//TODO : ここにm_GameSpeedを入れる
		if (m_TotalTime >= 1.0f && IsPlayerUpdate){
			//入力バー
			BarMapNext();
			//雲
			CloudNext();
			//敵
			EnemyMapNext();
			//コイン
			CoinMapNext();
			//目印
			NextMarkMap();

			LineNum++;
			m_TotalTime = 0.0f;
			GetStage()->GetSharedGameObject<Sheep>(L"Sheep")->ResetBool();
		}
		wrap_around_LineNum();
	}
	//コインが当たったか
	void GameDocument::IsHitCoin(){
		//プレイヤーとの衝突判定など
		if (auto sh = m_CoinPtrMap[m_PlayerY][m_PlayerX]){
			if (sh->GetStateMachine()->GetCurrentState() == NoHitState::Instance()){
				sh->GoHitMotion();
			}
		}
	}

	//動かない敵と当たったか
	void GameDocument::IsNonEnemy(){
		//プレイヤーとの衝突判定など
		if (auto sh = m_EnemyPtrMap[m_PlayerY][m_PlayerX]){
			if (sh->GetStateMachine()->GetCurrentState() == Player_NoHitState::Instance()){
				sh->GoPlayerKillMotion();
				SetPlayerMapBack();
			}
		}
		//auto& map = GetEnemyMap();
		//auto EnemyNumber = m_EnemyMap[m_PlayerY][m_PlayerX];
		//switch (EnemyNumber)
		//{
		//case 1:
		//	//とげの敵と当たった
		//	m_EnemyMap[m_PlayerY][m_PlayerX] = 1;
		//	//SetPlayerMapBack();
		//	break;
		//case 2:
		//	break;
		//case 3:
		//	break;
		//default:
		//	break;
		//}

	}

	//--------マップ関連の生成-------------------

	//雲マップを一つ進める
	void GameDocument::CloudNext(){
		for (size_t line = 1; line < m_CloudMap.size(); line++){
			m_CloudMap[line - 1] = m_CloudMap[line];
		}
		//次生成するvector配列を取得する
		auto NextVec = GetNextRow(m_CloudMapList);

		//次生成する行を最後尾に代入する
		m_CloudMap[m_CloudMap.size() - 1] = NextVec[LineNum];
	}
	//入力バーのマップを一つ進める
	void GameDocument::BarMapNext(){
		//バーの取得
		auto BarRow = GetStage()->GetSharedGameObject<input_bar>(L"input_bar")->GetNowRow();
		if (BarRow == 0){
		}
		else{
			//バーを下げる
			GetStage()->GetSharedGameObject<input_bar>(L"input_bar")->BarDown();
			for (size_t line = 1; line < m_BarMap.size(); line++){
				m_BarMap[line - 1] = m_BarMap[line];
			}
			vector<UINT> NewLine = { 0, 0, 0, 0, 0 };
			m_BarMap[m_BarMap.size() - 1] = NewLine;
		}
	}
	//アイテムのマップをひとつ進める
	void GameDocument::NextMarkMap(){

		auto FirstMap = m_MarkMap[0];
		for (size_t line = 1; line < m_MarkMap.size(); line++){
			m_MarkMap[line - 1] = m_MarkMap[line];
		}

		//次生成する行を最後尾に代入する
		m_MarkMap[m_MarkMap.size() - 1] = FirstMap;
	}
	//敵のマップをひとつ進める
	void GameDocument::NextEnemyMap(){
		//for (size_t line = 1; line < m_EnemyMap.size(); line++){
		//	m_EnemyMap[line - 1] = m_EnemyMap[line];
		//}
		////次生成するvector配列を取得する
		//auto NextVec = GetNextRow(m_EnemyMapList);
		////次生成する行を最後尾に代入する
		//m_EnemyMap[m_EnemyMap.size() - 1] = NextVec[LineNum];
	}

	//---------ポインター型のマップを進める--------------

	//Itemマップを一つ進める
	void GameDocument::CoinMapNext(){
		//m_ItemPtrMapマップの変更
		for (size_t line = 1; line < m_CoinPtrMap.size(); line++){
			m_CoinPtrMap[line - 1] = m_CoinPtrMap[line];
		}

		//次生成するvector配列を取得する
		auto& NextVec = GetNextRow(m_ItemMapList);
		auto LineItemNum = NextVec[LineNum];
		vector< shared_ptr<Coin_S> > NewLine(5, nullptr);
		for (size_t i = 0; i < 5; i++){
			switch (LineItemNum[i])
			{
			case 0:
				break;
			case 1:
				NewLine[i] = GetCoin();
				break;
			default:
				break;
			}
		}
		m_CoinPtrMap[m_CoinPtrMap.size() - 1] = NewLine;
	}
	//敵マップを一つ進める
	//TODO : 下まで行ったらリフレッシュを呼ぶ
	void GameDocument::EnemyMapNext(){
		//m_ItemPtrMapマップの変更
		for (size_t line = 1; line < m_EnemyPtrMap.size(); line++){
			m_EnemyPtrMap[line - 1] = m_EnemyPtrMap[line];
		}

		//次生成するvector配列を取得する
		auto& NextVec = GetNextRow(m_EnemyMapList);
		auto LineItemNum = NextVec[LineNum];
		vector< shared_ptr<NonMove_Enemy> > NewLine(5, nullptr);
		for (size_t i = 0; i < 5; i++){
			switch (LineItemNum[i])
			{
			case 0:
				break;
			case 1:
				NewLine[i] = Get_NeedleEnemy();
				break;
			case 2:
				NewLine[i] = Get_LanceEnemy();
				break;
			case 3:
				NewLine[i] = Get_LanceEnemy();
				break;
			default:
				break;
			}
		}
		m_EnemyPtrMap[m_EnemyPtrMap.size() - 1] = NewLine;
	}

	//-----ポインターのオブジェクトを取得する----------

	//コインポインターの取得
	shared_ptr<Coin_S> GameDocument::GetCoin(){
		auto Group = GetStage()->GetSharedObjectGroup(L"CoinGroup");
		auto& vec = Group->GetGroupVector();
		for (auto& v : vec){
			if (!v.expired()){
				auto sh = dynamic_pointer_cast<Coin_S>(v.lock());
				if (sh->IsRefreshAbled()){
					//リフレッシュできる
					sh->Refresh();
					return sh;
				}
			}
		}
		//追加する
		auto newsh = GetStage()->AddGameObject<Coin_S>();
		return newsh;
	}

	//とげとげの敵ポインターの取得
	shared_ptr<NonMove_Enemy> GameDocument::Get_NeedleEnemy(){
		auto Group = GetStage()->GetSharedObjectGroup(L"EnemyGroup");
		auto& vec = Group->GetGroupVector();
		for (auto& v : vec){
			if (!v.expired()){
				auto sh = dynamic_pointer_cast<Needle_Enemy>(v.lock());
				if (sh == nullptr) break;
				if (sh->IsRefreshAbled()){
					//リフレッシュできる
					sh->Refresh();
					return sh;
				}
			}
		}
		//追加する
		//HACK : ここでエラー落ち　weak_ptrがなにか引っかかってるみたい
		auto newsh = GetStage()->AddGameObject<Needle_Enemy>();
		return newsh;
	}

	//槍持った敵ポインターの取得
	shared_ptr<NonMove_Enemy> GameDocument::Get_LanceEnemy(){
		auto Group = GetStage()->GetSharedObjectGroup(L"EnemyGroup");
		auto& vec = Group->GetGroupVector();
		for (auto& v : vec){
			if (!v.expired()){
				auto sh = dynamic_pointer_cast<Lance_Enemy>(v.lock());
				if (sh == nullptr) break;
				if (sh->IsRefreshAbled()){
					//リフレッシュできる
					sh->Refresh();
					return sh;
				}
			}
		}
		//追加する
		auto newsh = GetStage()->AddGameObject<Lance_Enemy>();
		return newsh;
	}

	//爆弾の敵ポインターの取得
	shared_ptr<NonMove_Enemy> GameDocument::Get_BombEnemy(){
		auto Group = GetStage()->GetSharedObjectGroup(L"EnemyGroup");
		auto& vec = Group->GetGroupVector();
		for (auto& v : vec){
			if (!v.expired()){
				auto sh = dynamic_pointer_cast<Bomb_Enemy>(v.lock());
				if (sh == nullptr) break;
				if (sh->IsRefreshAbled()){
					//リフレッシュできる
					sh->Refresh();
					return sh;
				}
			}
		}
		//追加する
		auto newsh = GetStage()->AddGameObject<Bomb_Enemy>();
		return newsh;
	}

	//-----------------雲関連------------------------

	//雲配列１行を右に移動させる
	void GameDocument::CloudMap_RightMove(const size_t& nowRow){
		assert(nowRow < 20 && nowRow >= 0 && " 0 ~ 19の値で設定 ");

		//参照コピー　これに入れていく
		auto& LineRowVec = GetLineVec(nowRow);
		auto LineRowVec_dummy = GetLineVec(nowRow);

		LineRowVec[0] = LineRowVec_dummy[4];
		LineRowVec[1] = LineRowVec_dummy[0];
		LineRowVec[2] = LineRowVec_dummy[1];
		LineRowVec[3] = LineRowVec_dummy[2];
		LineRowVec[4] = LineRowVec_dummy[3];
	}
	//雲配列１行を左に移動させる
	void GameDocument::CloudMap_LeftMove(const size_t& nowRow){
		assert(nowRow < 20 && nowRow >= 0 && " 0 ~ 19の値で設定 ");

		//参照コピー　これに入れていく
		auto& LineRowVec = GetLineVec(nowRow);
		auto LineRowVec_dummy = GetLineVec(nowRow);

		LineRowVec[0] = LineRowVec_dummy[1];
		LineRowVec[1] = LineRowVec_dummy[2];
		LineRowVec[2] = LineRowVec_dummy[3];
		LineRowVec[3] = LineRowVec_dummy[4];
		LineRowVec[4] = LineRowVec_dummy[0];
	}
	//左右に動かすときのタイマースタート
	void GameDocument::SideMove_StartTimer(bool tf){
		float ElapsedTime = App::GetApp()->GetElapsedTime();

		//左右移動をしたときタイマーが始まる
		if (tf){
			m_RLMoveTime += ElapsedTime;
			if (m_RLMoveTime > 0.7f){
				//値のリセット
				m_RLMoveTime = 0.0f;
				//左右の雲の移動が終わったのでリセットする
				Is_StickRLMove = false;
			}
		}
	}

	//---------------入力バー関連----------------------

	//入力バーマップを上に更新
	void GameDocument::BarMap_Up(const size_t& nowRow){
		size_t NextCell = nowRow + 1;
		//現在入力バーがいる行を取得
		auto NowBarMap_Row = GetBarMap()[nowRow];
		//次にバーが移動する１行をキープ
		auto NextBarMap_Row = GetBarMap()[NextCell];
		//現在いる入力バーからひとつ上の配列に移動する
		SetBarMap_Row(NextCell, NowBarMap_Row);
		//入力バーがいた１行に前にあった要素をペーストする
		SetBarMap_Row(nowRow, NextBarMap_Row);
	}
	//入力バーマップを下に更新
	void GameDocument::BarMap_Down(const size_t& nowRow){
		size_t NextCell = nowRow - 1;
		//現在入力バーがいる行を取得
		auto NowBarMap_Row = GetBarMap()[nowRow];
		//次にバーが移動する１行をキープ
		auto NextBarMap_Row = GetBarMap()[NextCell];
		//現在いる入力バーからひとつ下の配列に移動する
		SetBarMap_Row(NextCell, NowBarMap_Row);
		//入力バーがいた１行に下にあった要素をペーストする
		SetBarMap_Row(nowRow, NextBarMap_Row);
	}

	//-------------------------パターン関連の関数--------------------------

	//生成パターンの作成
	void GameDocument::Create_PatternList(){
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		for (int numList = 0; numList < 24; numList++){
			//ランダムな数の取得
			auto num = GameStagePtr->RandToint(0, 15);
			//配列にランダムな値の格納
			EasyList[numList] = num;
		}
	}
	//次生成するパターンの１行を取得する
	vector<vector<UINT>> GameDocument::GetNextRow(map<int, vector<vector<UINT>> >& CreateNextList){
		//0 ~ 4の番号取得
		auto randnum = EasyList[PatternNum];

		//配列の取得
		return CreateNextList[randnum];
	}
	//ラップアラウンドしてくれる
	void GameDocument::wrap_around_LineNum(){
		if (LineNum == 20){
			PatternNum++;
			LineNum = 0;
		}
	}

	//------------------プレイヤー関連------------------
	void GameDocument::PlayerMapRefresh(){
		for (size_t y = 0; y < m_PlayerMap.size(); y++){
			for (size_t x = 0; x < m_PlayerMap[y].size(); x++){
				if (m_PlayerY == y && m_PlayerX == x){
					m_PlayerMap[y][x] = 1;
				}
				else{
					m_PlayerMap[y][x] = 0;
				}
			}
		}
	}
	//プレイヤーが左端にいるか
	bool GameDocument::PlayerMapLeftCheck()const{
		if (m_PlayerX > 0){
			return true;
		}
		return false;
	}
	//プレイヤーが右端にいるか
	bool GameDocument::PlayerMapRightCheck()const{
		if (m_PlayerX < 4){
			return true;
		}
		return false;
	}
	//プレイヤーの位置を左にセル移動する
	void GameDocument::SetPlayerMapLeft(){
		if (m_PlayerX > 0){
			m_PlayerX--;
			PlayerMapRefresh();
		}
	}
	//プレイヤーの位置を右にセル移動する
	void GameDocument::SetPlayerMapRight(){
		if (m_PlayerX < 4){
			m_PlayerX++;
			PlayerMapRefresh();
		}
	}
	void GameDocument::SetPlayerMapFront(){
		m_PlayerY++;
		PlayerMapRefresh();
		for (size_t line = 1; line < m_PlayerMap.size(); line++){
			m_PlayerMap[line - 1] = m_PlayerMap[line];
		}
		vector<UINT> NewLine = { 0, 0, 0, 0, 0 };
		m_PlayerMap[m_PlayerMap.size() - 1] = NewLine;
	}

	void GameDocument::SetPlayerMapBack(){
		if (m_PlayerY > 0){
			m_PlayerY--;
			PlayerMapRefresh();
		}
	}

	//---------------ゲームオーバー関連--------------
	//雲マップの最後の１列だけ消去して落ちるようにする
	void GameDocument::LastLine_CloudDelete(){
		vector<UINT> NewLine = { 0, 0, 0, 0, 0 };
		m_CloudMap[0] = NewLine;
	}
}
//endof  basedx11