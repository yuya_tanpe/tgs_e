#pragma once

#include "stdafx.h"

namespace basedx11{
	/**
	* @class ScoreSprite
	* @brief スコア表記 0 ~ 9までの表示
	* @author yuya
	* @date 2016/05/10
	*/
	class ScoreSprite : public GameObject {
		Vector3 m_StartPos;
		//頂点の配列の配列（2次元配列）
		vector< vector<VertexPositionColorTexture> > m_NumberVertexVec;
		bool m_DrawActive;
	public:
		//構築と破棄
		ScoreSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos, const bool DrawTF);
		virtual ~ScoreSprite();

		//初期化
		virtual void OnCreate() override;

		//描画を行う
		void DrawActive();

		//スコアの描画
		void ScoreDraw(int score);
		//スケールを大きくする
		void Scaleup();
		//少し大きくして主張して、元の大きさに戻る
		void ScaleAction();
	};

	/**
	* @class ScoreManager
	* @brief スコアスプライトの管理更新を行う
	* @author yuya
	* @date 2016/05/10
	*/
	class ScoreManager : public GameObject
	{
	private:
		shared_ptr<ScoreSprite> m_KURAI1;
		shared_ptr<ScoreSprite> m_KURAI10;
		shared_ptr<ScoreSprite> m_KURAI100;

		//Newスコア
		int Score;
		//桁の管理
		int place;
		//桁の保存配列
		int place_array[3];

		//スコアスプライトの最終更新
		void ScoreUpdate(const int score_1, const int score_10, const int score_100);

	public:
		ScoreManager(shared_ptr<Stage>& StagePtr, shared_ptr<ScoreSprite> KURAI_1, shared_ptr<ScoreSprite> KURAI_10, shared_ptr<ScoreSprite> KURAI_100);
		~ScoreManager(){
			auto PtrScene = dynamic_pointer_cast<Scene>(App::GetApp()->GetSceneBase());
			PtrScene->SetScore(Score);
		}
		//作成
		virtual void OnCreate() override;

		//スコアの加点
		void ScorePlus();
		//スコアを主張する
		void ScoreScaleUpDown();

		//スコア合計の取得
		int GetScore(){
			return Score;
		}
	};
}
//endof  basedx11