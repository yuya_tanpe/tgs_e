#pragma once

#include "stdafx.h"

namespace basedx11
{
	/**
	* @class Tutorial_Frame
	* @brief チュートリアル画面のフレーム
	* @author yuya
	* @date 2016/05/26
	*/
	class Tutorial_Frame : public GameObject {
		Vector3 m_StartPos;
		//頂点の配列の配列（2次元配列）
		vector< vector<VertexPositionColorTexture> > m_NumberVertexVec;
		bool m_DrawActive;
	public:
		//構築と破棄
		Tutorial_Frame(shared_ptr<Stage>& StagePtr, const Vector3& StartPos, const bool DrawTF);
		virtual ~Tutorial_Frame();

		//初期化
		virtual void OnCreate() override;

		//描画を行う
		void DrawActive();

		//スコアの描画
		void ScoreDraw(int score);
	};

	/**
	* @class Tutorial_Word
	* @brief チュートリアル画面のフレーム
	* @author yuya
	* @date 2016/05/26
	*/
	class Tutorial_Word : public GameObject {
		Vector3 m_StartPos;
		//頂点の配列の配列（2次元配列）
		vector< vector<VertexPositionColorTexture> > m_NumberVertexVec;
	public:
		//構築と破棄
		Tutorial_Word(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~Tutorial_Word();

		//初期化
		virtual void OnCreate() override;
	};

	/**
	* @class Tutorial_Controller
	* @brief チュートリアル画面のフレーム
	* @author yuya
	* @date 2016/05/26
	*/

	class Tutorial_Controller : public GameObject {
		Vector3 m_StartPos;
		//頂点の配列の配列（2次元配列）
		vector< vector<VertexPositionColorTexture> > m_NumberVertexVec;
	public:
		//構築と破棄
		Tutorial_Controller(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~Tutorial_Controller();

		//初期化
		virtual void OnCreate() override;

		//画像の切り替えをする
		void StartAction_ChangeTexture();
		//画像の切り替えをする
		void StartAction_ButtonTexture();
		//画像の初期化
		void Reset();
	};

	// AボタンのUI
	class Tutorial_AButton : public GameObject
	{
		Vector3 m_Pos;

	public:
		// 構築と破棄
		Tutorial_AButton(shared_ptr<Stage>& StagePtr, const Vector3& Pos);
		virtual ~Tutorial_AButton();

		// 初期化
		virtual void OnCreate() override;

		void SetDrawUpdate(const bool& a);
	};

	// STARTボタンのUI
	class Tutorial_StartButton : public GameObject
	{
		Vector3 m_Pos;

	public:
		// 構築と破棄
		Tutorial_StartButton(shared_ptr<Stage>& StagePtr, const Vector3& Pos);
		virtual ~Tutorial_StartButton();

		// 初期化
		virtual void OnCreate() override;

		void SetDrawUpdate(const bool& a);
	};

	// BボタンのUI
	class Tutorial_BButton : public GameObject
	{
		Vector3 m_Pos;

	public:
		// 構築と破棄
		Tutorial_BButton(shared_ptr<Stage>& StagePtr, const Vector3& Pos);
		virtual ~Tutorial_BButton();

		// 初期化
		virtual void OnCreate() override;

		void SetDrawUpdate(const bool& a);
	};

	/**
	* @class Tutorial_Setumei
	* @brief チュートリアル画面のフレーム
	* @author yuya
	* @date 2016/05/26
	*/
	class Tutorial_Setumei : public GameObject {
		Vector3 m_StartPos;
		//頂点の配列の配列（2次元配列）
		vector< vector<VertexPositionColorTexture> > m_NumberVertexVec;
		wstring m_texture;
	public:
		//構築と破棄
		Tutorial_Setumei(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~Tutorial_Setumei();

		//初期化
		virtual void OnCreate() override;
		void ChangeTexture(wstring wstr);
	};
}
//endof  basedx11