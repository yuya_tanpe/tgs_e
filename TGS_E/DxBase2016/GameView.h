#pragma once
#include "stdafx.h"
namespace basedx11{
	//--------------------------------------------------------------------------------------
	//	class GameView : public GameObject;
	//	用途: ビュークラス
	//--------------------------------------------------------------------------------------
	class GameView : public GameObject{
		//-----------------------------雲関連の描画-------------------------------------
		//雲の描画関連の構造体
		struct CloudData;

		//雲を描画する
		void DrawCloud();

		//雲を下に移動させる描画を行う
		void NextUnderCloud(CloudData& data);
		//雲を下に移動させる描画を行う※特定の行以外を描画
		void NextUnderCloud_Ex(CloudData& data, const size_t& row);
		//特定の行の雲を右に移動させる
		void MoveRightCloud(CloudData& date, size_t Row);
		//特定の行の雲を左に移動させる
		void MoveLeftCloud(CloudData& date, size_t Row);

		//-----------------------------入力バーの描画-------------------------------------
		//入力バーの描画関連の構造体
		struct BarData;

		//入力バーを描画する
		void DrawInputBar();
		//入力バーを下に移動させる描画を行う
		void NextUnderBar(BarData& data);

		//-----------------------------目印の描画-------------------------------------
		//入力バーの描画関連の構造体
		struct MarkData;

		//入力バーを描画する
		void DrawMark();
		//入力バーを下に移動させる描画を行う
		void NextUnderMark(MarkData& data);

		//-----------------------------プレイヤーの描画-------------------------------------
		//プレイヤーの描画関連の構造体
		struct PlayerData;

		//プレイヤーの描画
		void DrawPlayer();
		//プレイヤーを下に移動させる
		void NextUnderPlayer(PlayerData& data);

		//-----------------------------敵の描画-------------------------------------
		//敵の描画関連の構造体
		struct EnemyData;

		//敵の描画
		void DrawEnemy();

		//敵が下に移動してくる描画を行う
		void NextUnderEnemy(EnemyData& data);

		//-----------------------------アイテムの描画-------------------------------------
		struct ItemData;

		//アイテムの描画
		void DrawItem();

		//アイテムが下に移動してくる描画を行う
		void NextUnderItem(ItemData& data);

	public:
		//構築と破棄
		GameView(const shared_ptr<Stage>& StagePtr);
		virtual ~GameView();
		//初期化
		virtual void OnCreate() override;
		//描画
		virtual void OnDraw() override;
	};
}
//endof  basedx11
