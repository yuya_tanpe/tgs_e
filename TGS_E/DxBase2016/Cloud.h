#pragma once
#include "stdafx.h"

namespace basedx11{
	/**
	* @class Cloud
	* @brief 雲のオブジェクトの基底クラス
	* @author　sike yuya
	* @date	New 4/30 atNew 6/14
	*/
	class Cloud : public GameObject{
		// ---------- member ----------.
		//雲の名前
		CloudName m_Tagname;

	public:
		Cloud(const shared_ptr<Stage>& StagePtr, const CloudName& Name);
		virtual ~Cloud(){}
		virtual void OnCreate() override;
		virtual void OnEvent(const shared_ptr<Event>& event)override;

		// ---------- Accessor ----------.

		//自身の雲の名前取得
		CloudName GetCloudName(){
			return m_Tagname;
		}

		// ---------- Method -----------.

		//Tagnameによってメッシュを変える
		wstring Select_Meshresorce(const CloudName& name);
	};

	/**
	* @class Tutorial_cloud
	* @brief Cloud継承のオブジェクト　白い雲
	* @author　sike yuya
	* @date	New 5/26
	*/
	class Tutorial_cloud : public GameObject
	{
		Vector3 m_Pos;
		bool m_draw;
	public:
		Tutorial_cloud(const shared_ptr<Stage>& StagePtr, const Vector3& i_pos, bool TF);
		virtual ~Tutorial_cloud(){}
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
	};
}
//endof  basedx11
